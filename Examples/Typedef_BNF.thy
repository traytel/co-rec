theory Typedef_BNF
imports
  "~~/src/HOL/Library/BNF_Axiomatization"
  Lift_BNF
begin

typedef 'a nonempty_list = "{xs :: 'a list. xs \<noteq> []}"
  by blast

lift_bnf (no_warn_wits) (neset: 'a) nonempty_list
  for map: nemap rel: nerel
  by simp_all

typedef ('a :: finite, 'b) fin_nonempty_list = "{(xs :: 'a set, ys :: 'b list). ys \<noteq> []}"
  by blast

lift_bnf (dead 'a :: finite, 'b) fin_nonempty_list
  by auto

datatype 'a tree = Leaf | Node 'a "'a tree nonempty_list"

record 'a point =
  xCoord :: 'a
  yCoord :: 'a

copy_bnf ('a, 's) point_ext

typedef 'a it = "UNIV :: 'a set"
  by blast

copy_bnf (plugins del: size) 'a it

typedef ('a, 'b) T_prod = "UNIV :: ('a \<times> 'b) set"
  by blast

copy_bnf ('a, 'b) T_prod

typedef ('a, 'b, 'c) T_func = "UNIV :: ('a \<Rightarrow> 'b * 'c) set"
  by blast

copy_bnf ('a, 'b, 'c) T_func

typedef ('a, 'b) sum_copy = "UNIV :: ('a + 'b) set"
  by blast

copy_bnf ('a, 'b) sum_copy

typedef ('a, 'b) T_sum = "{Inl x | x. True} :: ('a + 'b) set"
  by blast

lift_bnf (no_warn_wits) ('a, 'b) T_sum [wits: "Inl :: 'a \<Rightarrow> 'a + 'b"]
  by (auto simp: map_sum_def sum_set_defs split: sum.splits)

typedef ('key, 'value) alist = "{xs :: ('key \<times> 'value) list. (distinct \<circ> map fst) xs}"
  morphisms impl_of Alist
proof
  show "[] \<in> {xs. (distinct o map fst) xs}"
    by simp
qed

lift_bnf (dead 'k, 'v) alist [wits: "Nil :: ('k \<times> 'v) list"]
  by simp_all

typedef 'a myopt = "{X :: 'a set. finite X \<and> card X \<le> 1}" by (rule exI[of _ "{}"]) auto
lemma myopt_type_def: "type_definition
  (\<lambda>X. if card (Rep_myopt X) = 1 then Some (the_elem (Rep_myopt X)) else None)
  (\<lambda>x. Abs_myopt (case x of Some x \<Rightarrow> {x} | _ \<Rightarrow> {}))
  (UNIV :: 'a option set)"
  apply unfold_locales
    apply (auto simp: Abs_myopt_inverse dest!: card_eq_SucD split: option.splits)
   apply (metis Rep_myopt_inverse)
  apply (metis One_nat_def Rep_myopt Rep_myopt_inverse Suc_le_mono card_0_eq le0 le_antisym mem_Collect_eq nat.exhaust)
  done

copy_bnf 'a myopt via myopt_type_def


declare [[bnf_internals,typedef_overloaded]]
bnf_axiomatization 'a F [wits: "'a \<Rightarrow> 'a F"]
consts F :: "'a F set"

axiomatization where
  inhab_F: "\<exists>x. x \<in> F" and
  map_closed_F: "x \<in> F \<Longrightarrow> map_F f x \<in> F" and
  zip_closed_F: "\<lbrakk>map_F fst z \<in> F; map_F snd z \<in> F\<rbrakk> \<Longrightarrow> \<exists>z' \<in> F. set_F z' \<subseteq> set_F z \<and> map_F fst z' = map_F fst z  \<and> map_F snd z' = map_F snd z" and
  wit_closed_F: "wit_F a \<in> F"


typedef 'a G = "F :: 'a F set"
  by (rule inhab_F)

definition "map_G f = Abs_G o map_F f o Rep_G"
definition "set_G = set_F o Rep_G"
definition "rel_G R = BNF_Def.vimage2p Rep_G Rep_G (rel_F R)"
definition "wit_G a = Abs_G (wit_F a)"

locale A begin
lemmas Rep_G = type_definition.Rep[OF type_definition_G]
lemmas Rep_G_inverse = type_definition.Rep_inverse[OF type_definition_G]
lemmas Rep_G_cases = type_definition.Rep_cases[OF type_definition_G]
lemmas Abs_G_inverse = type_definition.Abs_inverse[OF type_definition_G]
lemmas Abs_G_inject = type_definition.Abs_inject[OF type_definition_G]


bnf "'a G"
  map: map_G
  sets: set_G
  bd: bd_F
  wits: wit_G
  rel: rel_G
unfolding map_G_def set_G_def rel_G_def wit_G_def
apply -
apply (rule ext) apply (unfold F.map_id id_apply o_apply Rep_G_inverse) apply (rule refl)
apply (rule ext) apply (unfold F.map_comp o_apply Abs_G_inverse[OF map_closed_F[OF Rep_G]]) apply (rule refl)
apply (rule iffD2[OF Abs_G_inject[OF map_closed_F[OF Rep_G] map_closed_F[OF Rep_G]]]) apply (rule F.map_cong0) apply assumption
apply (rule ext) apply (unfold F.set_map o_apply Abs_G_inverse[OF map_closed_F[OF Rep_G]]) apply (rule refl)
apply (rule F.bd_card_order)
apply (rule F.bd_cinfinite)
apply (rule F.set_bd)
apply (rule vimage2p_relcompp_mono) apply (rule ord_eq_le_trans[OF F.rel_compp[symmetric]]) apply (rule order_refl)
apply (rule ext)+ apply (unfold OO_Grp_alt mem_Collect_eq o_apply vimage2p_def)
  apply (unfold F.in_rel Bex_def mem_Collect_eq)
  apply (rule iffI)
  apply (erule exE conjE)+
  apply (frule zip_closed_F[OF ssubst_mem[OF _ Rep_G] ssubst_mem[OF _ Rep_G]], assumption)
  apply (erule bexE conjE)+
  apply (erule Rep_G_cases)
  apply hypsubst
  apply (rule exI conjI)+
  apply (erule order_trans, assumption)
  apply (rule conjI)+
   apply (rule trans[OF iffD2[OF Abs_G_inject[OF map_closed_F[OF Rep_G] Rep_G]] Rep_G_inverse])
  apply (erule trans, assumption)
   apply (rule trans[OF iffD2[OF Abs_G_inject[OF map_closed_F[OF Rep_G] Rep_G]] Rep_G_inverse])
  apply (erule trans, assumption)

  apply (elim exE conjE)
  apply (intro exI conjI)
  apply assumption
  apply (rule iffD1[OF Abs_G_inject[OF map_closed_F[OF Rep_G] Rep_G]])
  apply (erule trans[OF _ Rep_G_inverse[symmetric]])
  apply (rule iffD1[OF Abs_G_inject[OF map_closed_F[OF Rep_G] Rep_G]])
  apply (erule trans[OF _ Rep_G_inverse[symmetric]])
apply (unfold Abs_G_inverse[OF wit_closed_F]) apply (drule F.wit) apply assumption
done

end
