theory Compat_Rec_Fun
imports Main
begin

section {* Library *}

lemma all_mem_range1:
  "(\<And>y. y \<in> range f \<Longrightarrow> P y) \<equiv> (\<And>x. P (f x)) "
  by (rule equal_intr_rule) fast+

lemma all_mem_range2:
  "(\<And>fa y. fa \<in> range f \<Longrightarrow> y \<in> range fa \<Longrightarrow> P y) \<equiv> (\<And>x xa. P (f x xa))"
  by (rule equal_intr_rule) fast+

lemma all_mem_range3:
  "(\<And>fa fb y. fa \<in> range f \<Longrightarrow> fb \<in> range fa \<Longrightarrow> y \<in> range fb \<Longrightarrow> P y) \<equiv> (\<And>x xa xb. P (f x xa xb))"
  by (rule equal_intr_rule) fast+

lemma all_mem_range4:
  "(\<And>fa fb fc y. fa \<in> range f \<Longrightarrow> fb \<in> range fa \<Longrightarrow> fc \<in> range fb \<Longrightarrow> y \<in> range fc \<Longrightarrow> P y) \<equiv>
   (\<And>x xa xb xc. P (f x xa xb xc))"
  by (rule equal_intr_rule) fast+

lemma all_mem_range5:
  "(\<And>fa fb fc fd y. fa \<in> range f \<Longrightarrow> fb \<in> range fa \<Longrightarrow> fc \<in> range fb \<Longrightarrow> fd \<in> range fc \<Longrightarrow>
     y \<in> range fd \<Longrightarrow> P y) \<equiv>
   (\<And>x xa xb xc xd. P (f x xa xb xc xd))"
  by (rule equal_intr_rule) fast+

lemma all_mem_range6:
  "(\<And>fa fb fc fd fe ff y. fa \<in> range f \<Longrightarrow> fb \<in> range fa \<Longrightarrow> fc \<in> range fb \<Longrightarrow> fd \<in> range fc \<Longrightarrow>
     fe \<in> range fd \<Longrightarrow> ff \<in> range fe \<Longrightarrow> y \<in> range ff \<Longrightarrow> P y) \<equiv>
   (\<And>x xa xb xc xd xe xf. P (f x xa xb xc xd xe xf))"
  by (rule equal_intr_rule) (fastforce, fast)

lemma all_mem_range7:
  "(\<And>fa fb fc fd fe ff fg y. fa \<in> range f \<Longrightarrow> fb \<in> range fa \<Longrightarrow> fc \<in> range fb \<Longrightarrow> fd \<in> range fc \<Longrightarrow>
     fe \<in> range fd \<Longrightarrow> ff \<in> range fe \<Longrightarrow> fg \<in> range ff \<Longrightarrow> y \<in> range fg \<Longrightarrow> P y) \<equiv>
   (\<And>x xa xb xc xd xe xf xg. P (f x xa xb xc xd xe xf xg))"
  by (rule equal_intr_rule) (fastforce, fast)

lemma all_mem_range8:
  "(\<And>fa fb fc fd fe ff fg fh y. fa \<in> range f \<Longrightarrow> fb \<in> range fa \<Longrightarrow> fc \<in> range fb \<Longrightarrow> fd \<in> range fc \<Longrightarrow>
     fe \<in> range fd \<Longrightarrow> ff \<in> range fe \<Longrightarrow> fg \<in> range ff \<Longrightarrow> fh \<in> range fg \<Longrightarrow> y \<in> range fh \<Longrightarrow> P y) \<equiv>
   (\<And>x xa xb xc xd xe xf xg xh. P (f x xa xb xc xd xe xf xg xh))"
  by (rule equal_intr_rule) (fastforce, fast)

lemmas all_mem_range = all_mem_range1 all_mem_range2 all_mem_range3 all_mem_range4 all_mem_range5
  all_mem_range6 all_mem_range7 all_mem_range8


section {* ML code *}

ML {*
open Ctr_Sugar
open BNF_Util
open BNF_Tactics
open BNF_FP_Util
open BNF_LFP_Rec_Sugar
open BNF_FP_Def_Sugar
*}

ML {*
fun mk_compat_rec_rhs ctxt fpTs Cs (recs as rec1 :: _) =
  let
    fun repair_rec_arg_args [] [] = []
      | repair_rec_arg_args ((g_T as Type (@{type_name fun}, _)) :: g_Ts) (g :: gs) =
        let
          val (x_Ts, body_T) = strip_type g_T;
        in
          (case try HOLogic.dest_prodT body_T of
            NONE => [g]
          | SOME (fst_T, _) =>
            if member (op =) fpTs fst_T then
              let val (xs, _) = mk_Frees "x" x_Ts ctxt in
                map (fn mk_proj => fold_rev Term.lambda xs (mk_proj (Term.list_comb (g, xs))))
                  [HOLogic.mk_fst, HOLogic.mk_snd]
              end
            else
              [g])
          :: repair_rec_arg_args g_Ts gs
        end
      | repair_rec_arg_args (g_T :: g_Ts) (g :: gs) =
        if member (op =) fpTs g_T then
          let
            val j = find_index (member (op =) Cs) g_Ts;
            val h = nth gs j;
            val g_Ts' = nth_drop j g_Ts;
            val gs' = nth_drop j gs;
          in
            [g, h] :: repair_rec_arg_args g_Ts' gs'
          end
        else
          [g] :: repair_rec_arg_args g_Ts gs;

    fun repair_back_rec_arg f_T f' =
      let
        val g_Ts = Term.binder_types f_T;
        val (gs, _) = mk_Frees "g" g_Ts ctxt;
      in
        fold_rev Term.lambda gs (Term.list_comb (f',
          flat_rec_arg_args (repair_rec_arg_args g_Ts gs))) 
      end;

    val f_Ts = binder_fun_types (fastype_of rec1);
    val (fs', _) = mk_Frees "f" (replicate (length f_Ts) Term.dummyT) ctxt; 

    fun mk_rec' recx =
      fold_rev Term.lambda fs' (Term.list_comb (recx, map2 repair_back_rec_arg f_Ts fs'))
      |> Syntax.check_term ctxt;
  in
    map mk_rec' recs
  end;
*}

ML {*
val compatN = "compat"
*}

ML {*
fun define_compat_recs fpTs Cs recs lthy =
  let
    val b_names = Name.variant_list [] (map base_name_of_typ fpTs);

    fun mk_binding b_name =
      Binding.qualify false (compatN ^ "_" ^ b_name)
        (Binding.prefix_name (compatN ^ "_" ^ recN ^ "_") (Binding.name b_name));

    val bs = map mk_binding b_names;
    val rhss = mk_compat_rec_rhs lthy fpTs Cs recs;
  in
    fold_map3 (fn fpT => fn b => define_co_rec_as Least_FP fpT Cs b) fpTs bs rhss lthy
  end;
*}

ML {*
fun mk_compat_rec_thmss ctxt rec0_thms rec_defs (recs as rec1 :: _) =
  let
    val f_Ts = binder_fun_types (fastype_of rec1);
    val (fs, _) = mk_Frees "f" f_Ts ctxt;
    val frecs = map (fn recx => Term.list_comb (recx, fs)) recs;

    fun mk_ctrs_of (Type (T_name, As)) =
      map (mk_ctr As) (#ctrs (the (ctr_sugar_of ctxt T_name)));

    val fpTs = map (domain_type o body_fun_type o fastype_of) recs;
    val fpTs_frecs = fpTs ~~ frecs;
    val ctrss = map mk_ctrs_of fpTs;
    val fss = unflat ctrss fs;

    fun mk_rec_call g n (Type (@{type_name fun}, [dom_T, ran_T])) =
        Abs (Name.uu, dom_T, mk_rec_call g (n + 1) ran_T)
      | mk_rec_call g n fpT =
        let
          val frec = the (AList.lookup (op =) fpTs_frecs fpT);
          val xg = Term.list_comb (g, map Bound (n - 1 downto 0));
        in frec $ xg end;

    fun mk_rec_arg_arg g_T g = g :: (if exists_subtype_in fpTs g_T then [mk_rec_call g 0 g_T] else []);

    fun mk_goal frec ctr f =
      let
        val g_Ts = binder_types (fastype_of ctr);
        val (gs, _) = mk_Frees "g" g_Ts ctxt;
        val gctr = Term.list_comb (ctr, gs);
        val fgs = flat_rec_arg_args (map2 mk_rec_arg_arg g_Ts gs);
      in
        fold_rev (fold_rev Logic.all) [fs, gs] (mk_Trueprop_eq (frec $ gctr, Term.list_comb (f, fgs)))
      end;

    fun mk_goals ctrs fs frec = map2 (mk_goal frec) ctrs fs;

    val goalss = map3 mk_goals ctrss fss frecs;

    fun tac ctxt =
      unfold_thms_tac ctxt (@{thms o_apply fst_conv snd_conv} @ rec_defs @ rec0_thms) THEN
      HEADGOAL (rtac refl);

    fun prove goal =
      Goal.prove_sorry ctxt [] [] goal (tac o #context)
      |> Thm.close_derivation;
  in
    map (map prove) goalss
  end;
*}

ML {*
fun define_compat_rec_derive_thms recs rec_thmss fpTs lthy =
  let
    val Cs = map (body_type o fastype_of) recs;
    val ((recs', rec'_defs), lthy') = define_compat_recs fpTs Cs recs lthy |>> split_list;
    val rec'_thmss = mk_compat_rec_thmss lthy' (flat rec_thmss) rec'_defs recs';
  in
    ((recs', rec'_thmss), lthy')
  end;
*}


section {* Examples *}

subsection {* Depth 1 *}

datatype 'a xa = Xa1 | Xa2 "'a \<Rightarrow> 'a xa"

thm xa.induct xa.rec
term rec_xa

datatype_new 'a xb = Xb1 | Xb2 "'a \<Rightarrow> 'a xb"

thm xb.induct xb.rec
term rec_xb

thm xa.induct xb.induct[unfolded all_mem_range]


subsection {* Depth 2 *}

datatype ('a, 'b) ya = Ya1 | Ya2 "'a \<Rightarrow> 'b \<Rightarrow> ('a, 'b) ya"

thm ya.induct ya.rec
term rec_ya

datatype_new ('a, 'b) yb = Yb1 | Yb2 "'a \<Rightarrow> 'b \<Rightarrow> ('a, 'b) yb"

thm yb.induct yb.rec
term rec_yb

thm ya.induct yb.induct[unfolded all_mem_range]


subsection {* Mixed Depth, Complicated *}

datatype
  ('a, 'b) za = Za1 | Za2 "'a \<Rightarrow> 'b \<Rightarrow> 'a \<Rightarrow> ('a, 'b) za" "'a \<Rightarrow> ('a, 'b) za'" and
  ('a, 'b) za' = Za' "('a, 'b) za"

thm za_za'.induct za_za'.inducts za_za'.rec
term rec_za_za'_1
term rec_za_za'_2

datatype_new
  ('a, 'b) zb = Zb1 | Zb2 "'a \<Rightarrow> 'b \<Rightarrow> 'a \<Rightarrow> ('a, 'b) zb" "'a \<Rightarrow> ('a, 'b) zb'" and
  ('a, 'b) zb' = Zb' "('a, 'b) zb"

thm zb_zb'.induct zb.induct zb'.induct zb.rec zb'.rec
term rec_zb
term rec_zb'

thm za_za'.induct zb_zb'.induct[unfolded all_mem_range]
thm za_za'.inducts(1) zb.induct[unfolded all_mem_range]
thm za_za'.inducts(2) zb'.induct[unfolded all_mem_range]


section {* ML examples *}

local_setup {*
snd o define_compat_rec_derive_thms [@{term rec_xb}] [@{thms xb.rec}] [@{typ "'b xb"}]
*}

term compat_rec_xb
thm compat_rec_xb_def

local_setup {*
snd o define_compat_rec_derive_thms [@{term rec_yb}] [@{thms yb.rec}] [@{typ "('b, 'c) yb"}]
*}

term compat_rec_yb
thm compat_rec_yb_def

local_setup {*
snd o define_compat_rec_derive_thms [@{term rec_zb}, @{term rec_zb'}] [@{thms zb.rec}, @{thms zb'.rec}]
  [@{typ "('b, 'c) zb"}, @{typ "('b, 'c) zb'"}]
*}

term compat_rec_zb
term compat_rec_zb'
thm compat_rec_zb_def compat_rec_zb'_def

end
