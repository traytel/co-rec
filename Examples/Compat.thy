theory Compat
imports "~~/src/HOL/BNF/BNF_LFP"
begin


datatype_new 'a lst = Nl | Cns 'a "'a lst"

datatype_compat lst


datatype_new 'b w = W | W' "'b w * 'b list"

(* no support for sums of products
datatype_compat w
*)


datatype_new ('c, 'b) s = L 'c | R 'b
datatype_new 'd x = X | X' "('d x lst, 'd list) s"

datatype_compat s
datatype_compat x

datatype_new 'a tttre = TTTre 'a "'a tttre lst lst lst"

primrec g_tre and g_tres and g_tress and g_tresss where
  "g_tre (TTTre a tsss) = {a} \<union> g_tresss tsss" 
| "g_tres Nl = {}" 
| "g_tres (Cns t ts) = g_tre t \<union> g_tres ts" 
| "g_tress Nl = {}" 
| "g_tress (Cns ts tss) = g_tres ts \<union> g_tress tss" 
| "g_tresss Nl = {}" 
| "g_tresss (Cns tss tsss) = g_tress tss \<union> g_tresss tsss"

datatype_compat tttre

primrec h_tre and h_tres and h_tress and h_tresss where
  "h_tre (TTTre a tsss) = {a} \<union> h_tresss tsss" 
| "h_tres Nl = {}" 
| "h_tres (Cns t ts) = h_tre t \<union> h_tres ts" 
| "h_tress Nl = {}" 
| "h_tress (Cns ts tss) = h_tres ts \<union> h_tress tss" 
| "h_tresss Nl = {}" 
| "h_tresss (Cns tss tsss) = h_tress tss \<union> h_tresss tsss"

function j_tre and j_tres and j_tress and j_tresss where
  "j_tre (TTTre a tsss) = {a} \<union> j_tresss tsss" 
| "j_tres Nl = {}" 
| "j_tres (Cns t ts) = j_tre t \<union> j_tres ts" 
| "j_tress Nl = {}" 
| "j_tress (Cns ts tss) = j_tres ts \<union> j_tress tss" 
| "j_tresss Nl = {}" 
| "j_tresss (Cns tss tsss) = j_tress tss \<union> j_tresss tsss"
by auto (metis (no_types) Inr_not_Inl lst.exhaust obj_sumE tttre.exhaust)


datatype_new 'a ftre = FEmp | FTre "'a \<Rightarrow> 'a ftre lst"

datatype_compat ftre

function f_ftre :: "'a ftre \<Rightarrow> 'a ftre" where
  "f_ftre FEmp = FEmp" 
| "f_ftre (FTre f) = FTre (lst_map f_ftre o f)"
by auto (metis ftre.exhaust)


datatype_new 'a foo = Foo | Foo' 'a "'a bar" and 'a bar = Bar | Bar' 'a "'a foo"

datatype_compat foo bar


datatype_new 'a tre = Tre 'a "'a tre lst"

datatype_compat tre

fun f_tre and f_tres where
  "f_tre (Tre a ts) = {a} \<union> f_tres ts" 
| "f_tres Nl = {}" 
| "f_tres (Cns t ts) = f_tres ts"


datatype_new 'a f = F 'a and 'a g = G 'a
datatype_new h = H "h f" | H'

datatype_compat f g
datatype_compat h


datatype_new myunit = MyUnity

datatype_compat myunit

fun f_myunit where
  "f_myunit MyUnity = Suc 0"


datatype_new mylist = MyNil | MyCons nat mylist

datatype_compat mylist

fun f_mylist where
  "f_mylist MyNil = 0" 
| "f_mylist (MyCons _ xs) = Suc (f_mylist xs)"


datatype_new foo' = FooNil | FooCons bar' foo' and bar' = Bar

datatype_compat bar' foo'

fun f_foo and f_bar where
  "f_foo FooNil = 0" 
| "f_foo (FooCons bar foo) = Suc (f_foo foo) + f_bar bar" 
| "f_bar Bar = Suc 0"


locale opt begin

datatype_new 'a opt = Non | Som 'a

datatype_compat opt

fun f_opt where
  "f_opt Non = 0" 
| "f_opt (Som n) = Suc n"

end

datatype funky = Funky "funky tre" | Funky'


primrec set_tre :: "'a tre \<Rightarrow> 'a set" and set_tres :: "'a tre lst \<Rightarrow> 'a set" where
  "set_tre (Tre a ts) = insert a (set_tres ts)" 
| "set_tres Nl = {}" 
| "set_tres (Cns t ts) = set_tre t \<union> set_tres ts"

datatype fnky = Fnky "nat tre"


datatype_new tree = Tree "tree foo"

datatype_compat tree


ML {* Datatype_Data.get_info @{theory} @{type_name tree} *}

end
