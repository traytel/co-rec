theory Corec_Massage_Call
imports Complex_Main "~~/src/HOL/BNF/BNF"
begin

no_translations
  "[x, xs]" == "x # [xs]"
  "[x]" == "x # []"

no_notation
  Nil ("[]") and
  Cons (infixr "#" 65)

hide_const Nil Cons hd tl map

datatype_new (set: 'a) list (map: map rel: list_all2) =
  null: Nil ("[]")
| Cons (hd: 'a) (tl: "'a list") (infixr "#" 65)

syntax "_list" :: "args \<Rightarrow> 'a list" ("[(_)]")

translations
  "[x, xs]" == "x # [xs]"
  "[x]" == "x # []"

codatatype 'a tree = Node "'a tree list"

consts size :: "'a tree \<Rightarrow> nat"
consts massage :: 'a

ML {* open BNF_Util BNF_Def BNF_FP_Def_Sugar BNF_FP_Util BNF_FP_Rec_Sugar_Util *}

ML {*
val fnames = ["f", "fa", "fb"];
fun get_kks t =
  map_index (fn (i, s) =>
    if exists_subterm (fn Free (s', _) => s' = s | _ => false) t then SOME i else NONE) fnames
  |> map_filter I;
val has_call = not o null o get_kks;
fun massage check res_U t =
  massage_direct_corec_call
    @{context}
    (not o null o get_kks)
    (fn U1 => fn T1 => fn t =>
       if U1 = @{typ bool} then
         if has_call t then @{term True} else @{term False}
       else if U1 = T1 then
         if has_call t then Const (@{const_name undefined}, U1) else t
       else
         if has_call t then Const (@{const_name massage}, T1 --> U1) $ t
         else Const (@{const_name undefined}, U1))
    [] res_U t
  |> tap (Syntax.check_term @{context})
  |> tap (Output.urgent_message o Syntax.string_of_term @{context})
  handle ERROR msg => if check then error msg else (warning ("Error: " ^ msg); Term.dummy)
       | Fail msg => if check then raise Fail msg else (warning ("Fail: " ^ msg); Term.dummy)
*}

type_synonym 'a TT0 = "'a tree"
type_synonym 'a UU0 = bool
type_synonym 'a VV0 = "'a tree"
type_synonym 'a WW0 = nat

type_synonym 'a TT = "'a tree"
type_synonym 'a UU = bool
type_synonym 'a VV = "'a tree"
type_synonym 'a WW = "nat \<times> int \<times> real"

lemma
  fixes f :: "nat \<Rightarrow> 'a tree"
  fixes fa :: "nat \<Rightarrow> int \<Rightarrow> real \<Rightarrow> 'a tree"
  fixes fb :: "nat \<times> int \<Rightarrow> real \<Rightarrow> 'a tree"
  fixes g0 :: "nat \<Rightarrow> 'a tree"
  fixes g :: "nat \<Rightarrow> real \<Rightarrow> 'a tree"
  fixes p :: "nat \<Rightarrow> bool"
  shows True
proof -

ML_val {* massage true @{typ "'a UU0"} @{term "fa n i r :: 'a TT0"} *}
ML_val {* massage true @{typ "'a VV0"} @{term "fa n i r :: 'a TT0"} *}
ML_val {* massage true @{typ "'a WW0"} @{term "fa n i r :: 'a TT0"} *}

ML_val {* massage true @{typ "'a UU0"} @{term "if n = 0 then fa n i r else Node [] :: 'a TT0"} *}
ML_val {* massage true @{typ "'a VV0"} @{term "if n = 0 then fa n i r else Node [] :: 'a TT0"} *}
ML_val {* massage true @{typ "'a WW0"} @{term "if n = 0 then fa n i r else Node []  :: 'a TT0"} *}

ML_val {* massage true @{typ "'a UU0"}
  @{term "if n = 0 then fa n i r else if n = 1 then Node []
          else if n = 2 then fa (Suc n) 0 r' else Node [Node []] :: 'a TT0"} *}
ML_val {* massage true @{typ "'a VV0"}
  @{term "if n = 0 then fa n i r else if n = 1 then Node []
          else if n = 2 then fa (Suc n) 0 r' else Node [Node []] :: 'a TT0"} *}
ML_val {* massage true @{typ "'a WW0"}
  @{term "if n = 0 then fa n i r else if n = 1 then Node []
          else if n = 2 then fa (Suc n) 0 r' else Node [Node []] :: 'a TT0"} *}

ML_val {* massage true @{typ "'a UU0"}
  @{term "if n = 0 then if n = 1 then Node [] else fa n i r
          else let m = Suc n in if m = 2 then fa (Suc n) 0 r' else Node [Node []] :: 'a TT0"} *}
ML_val {* massage true @{typ "'a VV0"}
  @{term "if n = 0 then if n = 1 then Node [] else fa n i r
          else let m = Suc n in if m = 2 then fa (Suc n) 0 r' else Node [Node []] :: 'a TT0"} *}
ML_val {* massage true @{typ "'a WW0"}
  @{term "if n = 0 then if n = 1 then Node [] else fa n i r
          else let m = Suc n in if m = 2 then fa (Suc n) 0 r' else Node [Node []] :: 'a TT0"} *}

oops

end
