theory Rec_Suite
imports "~~/src/HOL/BNF/BNF"
begin


datatype_new simple = X1 | X2
term simple_rec

primrec_new f_simple :: "simple \<Rightarrow> nat" where
  "f_simple X1 = 1" |
  "f_simple X2 = 2"
print_theorems

primrec_new f2_simple :: "nat \<Rightarrow> simple \<Rightarrow> nat" where
  "f2_simple n X1 = 1 + n" |
  "f2_simple n X2 = n * 2"
print_theorems

primrec_new f4_simple :: "nat \<Rightarrow> 'a list \<Rightarrow> simple \<Rightarrow> bool \<Rightarrow> nat" where
  "f4_simple n xs X1 b = (if b then 1 + n else length xs)" |
  "f4_simple n xs X2 b = n * 2"
print_theorems


datatype_new simple' = X1' unit | X2' unit
term simple'_rec

primrec_new f_simple' :: "simple' \<Rightarrow> nat" where
  "f_simple' (X1' u) = 1" |
  "f_simple' (X2' u) = 2"
print_theorems


datatype_new 'a mylist = MyNil | MyCons 'a "'a mylist"
term mylist_rec

primrec_new f_mylist :: "'a mylist \<Rightarrow> nat" where
  "f_mylist MyNil = 0" |
  "f_mylist (MyCons x xs) = 1 + f_mylist xs"
print_theorems

primrec_new f2_mylist :: "'a list \<Rightarrow> 'a mylist \<Rightarrow> nat" where
  "f2_mylist ys MyNil = length ys" |
  "f2_mylist ys (MyCons x xs) = 1 + f2_mylist (tl ys) xs"
print_theorems

primrec_new g_mylist :: "'a mylist \<Rightarrow> 'a option" where
  "g_mylist MyNil = None" |
  "g_mylist (MyCons x xs) = Some x"
print_theorems

primrec_new h_mylist :: "'a mylist \<Rightarrow> 'a mylist list" where
  "h_mylist MyNil = [MyNil]" |
  "h_mylist (MyCons x xs) = MyCons x xs # h_mylist xs"
print_theorems

locale A =
  fixes a :: "'a mylist"
begin

primrec_new h_mylist :: "'a mylist \<Rightarrow> 'a mylist list" where
  "h_mylist MyNil = [a]" |
  "h_mylist (MyCons x xs) = MyCons x xs # h_mylist xs"
print_theorems

end

datatype_new ('b, 'c) some_passive = SP1 "('b, 'c) some_passive" | SP2 'b | SP3 'c
term some_passive_rec

primrec_new f_some_passive :: "('b, nat) some_passive \<Rightarrow> nat" where
  "f_some_passive (SP1 x) = 1 + f_some_passive x" |
  "f_some_passive (SP2 b) = 2" |
  "f_some_passive (SP3 c) = 3 + c"
print_theorems


datatype_new ('b, 'c) some_dead = SD1 'c | SD2 "'b \<Rightarrow> ('b, 'c) some_dead" | SD3 "('b, 'c) some_dead"
term some_dead_rec

primrec_new f_some_dead :: "('b, nat) some_dead \<Rightarrow> nat" where
  "f_some_dead (SD1 _) = 1" |
  "f_some_dead (SD2 _) = 2" |
  "f_some_dead (SD3 sd) = 1 + f_some_dead sd"
print_theorems


datatype_new 'a bin_tree = TNl | TNd "'a bin_tree" "'a bin_tree"

primrec_new flatten :: "'a bin_tree \<Rightarrow> 'a list" where
  "flatten TNl = []" |
  "flatten (TNd l r) = flatten l @ flatten r"
print_theorems

primrec_new mirror :: "'a bin_tree \<Rightarrow> 'a bin_tree" where
  "mirror TNl = TNl" |
  "mirror (TNd l r) = TNd (mirror r) (mirror l)"
print_theorems


datatype_new 'a tree = TEmpty | TNode 'a "'a forest"
 and 'a forest = FNil | FCons (fhd: "'a tree") (ftl: "'a forest")
term tree_rec
term forest_rec

primrec_new f_tree :: "'a tree \<Rightarrow> nat" and f_forest :: "'a forest \<Rightarrow> nat list" where
  "f_tree TEmpty = 0" |
  "f_tree (TNode a f) = 1 + listsum (f_forest f)" |
  "f_forest FNil = []" |
  "f_forest (FCons t f) = f_tree t # f_forest f"
print_theorems

primrec_new f2_tree :: "nat \<Rightarrow> 'a tree \<Rightarrow> ('a \<Rightarrow> nat) \<Rightarrow> nat"
and         f2_forest :: "('a \<Rightarrow> nat) \<Rightarrow> 'a forest \<Rightarrow> nat list \<Rightarrow> nat" where
  "f2_tree n TEmpty w = n" |
  "f2_tree n (TNode a f) w = w a + f2_forest (w(a := 0)) f []" |
  "f2_forest w FNil ns = listsum ns" |
  "f2_forest w (FCons t f) ns = f2_forest w f (f2_tree 0 t w # ns)"
print_theorems

primrec_new f3_forest :: "'a forest \<Rightarrow> nat list" and f3_tree :: "'a tree \<Rightarrow> nat" where
  "f3_forest FNil = []" |
  "f3_forest (FCons t f) = f3_tree t # f3_forest f" |
  "f3_tree TEmpty = 0" |
  "f3_tree (TNode a f) = 1 + listsum (f3_forest f)"
print_theorems

primrec_new g_tree :: "'a tree \<Rightarrow> 'a tree" where
  "g_tree TEmpty = TEmpty" |
  "g_tree (TNode a f) = fhd f"
print_theorems

primrec_new g_forest :: "'a forest \<Rightarrow> nat list" where
  "g_forest FNil = []" |
  "g_forest (FCons t f) = 0 # g_forest f"
print_theorems


datatype_new hfset = HFset "hfset mylist"
term hfset_rec

primrec_new f_hfset :: "hfset \<Rightarrow> nat" where
  "f_hfset (HFset hs) = f_mylist (mylist_map f_hfset hs)"
print_theorems

primrec_new f2_hfset :: "nat \<Rightarrow> hfset \<Rightarrow> nat \<Rightarrow> nat" where
  "f2_hfset n (HFset hs) k = f_mylist (mylist_map (\<lambda>h. f2_hfset (n * 2) h (k + 1)) hs)"
print_theorems


datatype_new tree2 = TEmpty2 | TNode2 forest2
  and forest2 = Forest2 "tree2 list"

primrec_new f_tree2 :: "tree2 \<Rightarrow> nat" and f_forest2 :: "forest2 \<Rightarrow> nat" where
  "f_tree2 TEmpty2 = 0" |
  "f_tree2 (TNode2 f) = f_forest2 f" |
  "f_forest2 (Forest2 ts) = listsum (map f_tree2 ts)"
print_theorems

primrec_new f_tree3 :: "tree2 \<Rightarrow> nat" and f_forest3 :: "forest2 \<Rightarrow> nat" where
  R[simp del]: "f_forest3 (Forest2 ts) = listsum (map f_tree2 ts)" |
  z[symmetric]: "f_tree3 (TNode2 f) = f_forest2 f" |
  y: "f_tree3 TEmpty2 = 0"
print_theorems


(*
primrec_new evn :: "nt \<Rightarrow> bool" and odd :: "nt \<Rightarrow> bool" where
  "evn Zo = True" |
  "odd Zo = False" |
  "evn (Sc n) = odd n" |
  "odd (Sc n) = evn n"
print_theorems
*)

end
