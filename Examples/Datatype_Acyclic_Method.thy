theory Datatype_Acyclic_Method
imports "~~/src/HOL/Sledgehammer2d"
begin


datatype nt = Zr | Sc nt

lemma "P Zr \<Longrightarrow> x \<noteq> Sc x"
sledgehammer [remote_spass_pirate, isar_proofs, dont_try0_isar, overlord] ()


lemma size_ne_size_imp_ne: "size x \<noteq> size y \<Longrightarrow> x \<noteq> y"
by (erule contrapos_nn) (rule arg_cong)

thm size_ne_size_imp_ne


datatype nt = Zr | Sc nt

lemma "Sc (Sc (Sc x)) = Sc x \<Longrightarrow> False"
by (simp add: size_ne)

lemma "size n < size (Sc n)" by (simp add: size_ne)
lemma "size n < size (Sc (Sc n))" by (simp add: size_ne)


datatype 'a lst = Nl | Cns 'a "'a lst"

lemma [THEN size_le_ne]: "size xs < size (Cns x xs)" by simp
lemma [THEN size_le_ne]: "size xs < size (Cns x (Cns x' xs))" by simp
lemma [THEN size_le_ne]: "size xs < size (Cns x (Cns x' (Cns x'' xs)))" by simp


datatype 'a lst' = Nl' | Cns1 'a "'a lst'" | Cns2 'a "'a lst'"

lemma [THEN size_le_ne]: "size xs < size (Cns1 x1 (Cns2 x2 xs))" by simp


datatype 'a tr = Tr 'a "'a tr lst"

lemma [THEN size_le_ne]: "size t < size (Tr x (Cns t Nl))" by simp
lemma [THEN size_le_ne]: "size t < size (Tr x (Cns (Tr y (Cns t NL)) Nl))" by simp

end
