theory set_cases
imports "~~/src/HOL/BNF_LFP"

begin

declare [[bnf_note_all]]

datatype_new 'a llist
  = LNil
  | LCons (lhead: 'a) (ltail: "'a llist")
  where
    "ltail LNil = LNil"

datatype_new 'a maybe
  = Nothing
  | Just (just: 'a)

datatype_new 'a ltree
  = LNode (lval: 'a) (lchildren: "'a ltree llist")

datatype_new ('a, 'b) example
  = N1 'a
  | N2 'b
  | N3 'a 'b
  | N4
  | ConsA 'a "('a, 'b) example"
  | ConsB 'b "('a, 'b) example"

datatype_new 'a tree = Node 'a "'a forest"
       and 'a forest = FNil | FCons "'a tree" "'a forest"

datatype_new 'a A = A1 'a | A2 'a
datatype_new ('a, 'b) B = B1 'a | B2 'b
datatype_new ('a, 'b) C = C1 "'a A" | C2 "('a, 'b) B"
datatype_new 'a D1 = D1 "('a, unit) B A"
datatype_new 'a D2 = D2 "('a * unit) A"
datatype_new 'a D3 = D3 "('a, (unit, nat) B) B A"
datatype_new 'a D4 = D4 "('a, ('a, nat) B) B A"
datatype_new 'a D5 = D5 "('a, ('a D5, nat) B) B A"
datatype_new 'a D6 = D6 "('a, ('a, ('a, ('a, ('a, nat) B) B) B) B) B"

datatype_new ('a, 'b, 'c, 'd) E = E "'a * ('b, ('c, ('d * nat)) C A) B"

datatype_new ('a, 'b) F = F "'a \<Rightarrow> 'b"

(*datatype_new & datatypes*)
lemma llist_set_cases0 [elim?, cases set: set_llist]:
  assumes "x \<in> set_llist xs"
  obtains xs' where "xs = LCons x xs'"
     | x' xs' where "xs = LCons x' xs'" "x \<in> set_llist xs'"
  using assms by (cases xs) auto

(*test*)
inductive_cases ex1: "x \<in> set_llist LNil"
inductive_cases ex2: "x \<in> set_llist (LCons y xs)"
thm ex1 ex2

ML {*
open Ctr_Sugar_Tactics
open Ctr_Sugar_Util

fun mk_set_cases_tac ctxt ct assms exhaust sets =
  HEADGOAL (rtac (cterm_instantiate_pos [SOME ct] exhaust) THEN_ALL_NEW hyp_subst_tac ctxt) THEN
  unfold_thms_tac ctxt sets THEN
  REPEAT_DETERM (HEADGOAL
    (eresolve_tac @{thms FalseE emptyE singletonE UnE UN_E} ORELSE'
     hyp_subst_tac ctxt ORELSE'
     (SELECT_GOAL (SOLVE (HEADGOAL (eresolve_tac assms THEN' REPEAT_DETERM o atac))))))
*}

thm llist.set_cases
lemma llist_set_cases[rotated -1]:
  assumes "(\<And>arg. a = LCons elem arg \<Longrightarrow> thesis)"
      and "(\<And>arg1 arg2. a = LCons arg1 arg2 \<Longrightarrow> elem \<in> set_llist arg2 \<Longrightarrow> thesis)"
  shows "elem \<in> set_llist a \<Longrightarrow> thesis"
  apply (tactic {* mk_set_cases_tac
    @{context}
    @{cterm a}
    @{thms assms}
    @{thm llist.exhaust}
    @{thms llist.set}
  *})
(*
  apply (rule llist.exhaust[of a])
  apply hypsubst
  apply (unfold llist.set empty_iff)
  apply (erule FalseE)

  apply hypsubst
  apply (unfold llist.set)
  apply (erule UnE)
  apply (erule singletonE)
  apply hypsubst
  apply (tactic {* ALLGOALS (SELECT_GOAL (SOLVE (HEADGOAL (eresolve_tac @{thms assms} THEN' REPEAT_DETERM o atac)))) *}) *)
done

lemma maybe_set_cases[rotated -1]:
  assumes "a = Just elem \<Longrightarrow> thesis"
  shows "elem \<in> set_maybe a \<Longrightarrow> thesis"
  apply (tactic {* mk_set_cases_tac
    @{context}
    @{cterm a}
    @{thms assms}
    @{thm maybe.exhaust}
    @{thms maybe.set}
  *})
(*   apply (rule maybe.exhaust[of a])
  apply hypsubst
  apply (unfold maybe.set empty_iff)
  apply (erule FalseE)

  apply hypsubst
  apply (unfold maybe.set)
  apply (erule singletonE)
  apply hypsubst
  apply (tactic {* ALLGOALS (SELECT_GOAL (SOLVE (HEADGOAL (eresolve_tac @{thms assms} THEN' REPEAT_DETERM o atac)))) *}) *)
done

lemma ltree_set_cases[rotated -1]:
  assumes "\<And>arg. a = LNode elem arg \<Longrightarrow> thesis"
      and "\<And>arg1 arg2 x. a = LNode arg1 arg2 \<Longrightarrow> x \<in> set_llist arg2 \<Longrightarrow> elem \<in> set_ltree x \<Longrightarrow> thesis"
  shows "elem \<in> set_ltree a \<Longrightarrow> thesis"
  apply (tactic {* mk_set_cases_tac
    @{context}
    @{cterm a}
    @{thms assms}
    @{thm ltree.exhaust}
    @{thms ltree.set}
  *})
(*   apply (rule ltree.exhaust[of a])
  apply hypsubst
  apply (unfold ltree.set)
  apply (erule UnE)
  apply (erule singletonE)
  apply hypsubst
  apply (tactic {* HEADGOAL (SELECT_GOAL (SOLVE (HEADGOAL (eresolve_tac @{thms assms} THEN' REPEAT_DETERM o atac)))) *})
  apply (erule UN_E)
  apply (tactic {* HEADGOAL (SELECT_GOAL (SOLVE (HEADGOAL (eresolve_tac @{thms assms} THEN' REPEAT_DETERM o atac)))) *}) *)
done

thm example.set_cases
lemma example_set_cases_1[rotated -1]:
  assumes "a = N1 elem \<Longrightarrow> thesis"
      and "\<And>arg. a = N3 elem arg \<Longrightarrow> thesis"
      and "\<And>arg. a = ConsA elem arg \<Longrightarrow> thesis"
      and "\<And>arg1 arg2. a = ConsA arg1 arg2 \<Longrightarrow> elem \<in> set1_example arg2 \<Longrightarrow> thesis"
      and "\<And>arg1 arg2. a = ConsB arg1 arg2 \<Longrightarrow> elem \<in> set1_example arg2 \<Longrightarrow> thesis"
  shows "elem \<in> set1_example a \<Longrightarrow> thesis"
  apply (tactic {* mk_set_cases_tac
    @{context}
    @{cterm a}
    @{thms assms}
    @{thm example.exhaust}
    @{thms example.set}
  *})
(*   apply (rule example.exhaust[of a])
  apply hypsubst
  apply (unfold example.set)
  apply (erule singletonE)
  apply hypsubst
  apply (tactic {* HEADGOAL (SELECT_GOAL (SOLVE (HEADGOAL (eresolve_tac @{thms assms} THEN' REPEAT_DETERM o atac)))) *})

  apply hypsubst
  apply (unfold example.set empty_iff)
  apply (erule FalseE)

  apply hypsubst
  apply (unfold example.set singleton_iff)
  apply hypsubst
  apply (tactic {* HEADGOAL (SELECT_GOAL (SOLVE (HEADGOAL (eresolve_tac @{thms assms} THEN' REPEAT_DETERM o atac)))) *})

  apply hypsubst
  apply (unfold example.set empty_iff)
  apply (erule FalseE)

  apply hypsubst
  apply (unfold example.set)
  apply (erule UnE)
  apply (unfold singleton_iff)
  apply hypsubst
  apply (tactic {* HEADGOAL (SELECT_GOAL (SOLVE (HEADGOAL (eresolve_tac @{thms assms} THEN' REPEAT_DETERM o atac)))) *})
  apply (tactic {* HEADGOAL (SELECT_GOAL (SOLVE (HEADGOAL (eresolve_tac @{thms assms} THEN' REPEAT_DETERM o atac)))) *})

  apply hypsubst
  apply (unfold example.set)
  apply (tactic {* HEADGOAL (SELECT_GOAL (SOLVE (HEADGOAL (eresolve_tac @{thms assms} THEN' REPEAT_DETERM o atac)))) *}) *)
done

lemma example_set_cases_2[rotated -1]:
  assumes "a = N2 elem \<Longrightarrow> thesis"
      and "\<And>arg. a = N3 arg elem \<Longrightarrow> thesis"
      and "\<And>arg1 arg2. a = ConsA arg1 arg2 \<Longrightarrow> elem \<in> set2_example arg2 \<Longrightarrow> thesis"
      and "\<And>arg. a = ConsB elem arg \<Longrightarrow> thesis"
      and "\<And>arg1 arg2. a = ConsB arg1 arg2 \<Longrightarrow> elem \<in> set2_example arg2 \<Longrightarrow> thesis"
  shows "elem \<in> set2_example a \<Longrightarrow> thesis"
  apply (tactic {* mk_set_cases_tac
    @{context}
    @{cterm a}
    @{thms assms}
    @{thm example.exhaust}
    @{thms example.set}
  *})
done

lemma tree_set_cases[rotated -1]:
  assumes "\<And>arg. a = Node elem arg \<Longrightarrow> thesis"
      and "\<And>arg1 arg2. a = Node arg1 arg2 \<Longrightarrow> elem \<in> set_forest arg2 \<Longrightarrow> thesis"
  shows "elem \<in> set_tree a \<Longrightarrow> thesis"
  apply (tactic {* mk_set_cases_tac
    @{context}
    @{cterm a}
    @{thms assms}
    @{thm tree.exhaust}
    @{thms tree.set}
  *})
done

lemma forest_set_cases[rotated -1]:
  assumes "\<And>arg1 arg2. a = FCons arg1 arg2 \<Longrightarrow> elem \<in> set_tree arg1 \<Longrightarrow> thesis"
      and "\<And>arg1 arg2. a = FCons arg1 arg2 \<Longrightarrow> elem \<in> set_forest arg2 \<Longrightarrow> thesis"
  shows "elem \<in> set_forest a \<Longrightarrow> thesis"
  apply (tactic {* mk_set_cases_tac
    @{context}
    @{cterm a}
    @{thms assms}
    @{thm forest.exhaust}
    @{thms forest.set}
  *})
done

lemma A_set_cases[rotated -1]:
  assumes "a = A1 elem \<Longrightarrow> thesis"
      and "a = A2 elem \<Longrightarrow> thesis"
  shows "elem \<in> set_A a \<Longrightarrow> thesis"
  apply (tactic {* mk_set_cases_tac
    @{context}
    @{cterm a}
    @{thms assms}
    @{thm A.exhaust}
    @{thms A.set}
  *})
done

lemma B_set_cases_1[rotated -1]:
  assumes "a = B1 elem \<Longrightarrow> thesis"
  shows "elem \<in> set1_B a \<Longrightarrow> thesis"
  apply (tactic {* mk_set_cases_tac
    @{context}
    @{cterm a}
    @{thms assms}
    @{thm B.exhaust}
    @{thms B.set}
  *})
done

lemma B_set_cases_2[rotated -1]:
  assumes "a = B2 elem \<Longrightarrow> thesis"
  shows "elem \<in> set2_B a \<Longrightarrow> thesis"
  apply (tactic {* mk_set_cases_tac
    @{context}
    @{cterm a}
    @{thms assms}
    @{thm B.exhaust}
    @{thms B.set}
  *})
done

lemma C_set_cases_1[rotated -1]:
  assumes "\<And>arg. a = C1 arg \<Longrightarrow> elem \<in> set_A arg \<Longrightarrow> thesis"
      and "\<And>arg. a = C2 arg \<Longrightarrow> elem \<in> set1_B arg \<Longrightarrow> thesis"
  shows "elem \<in> set1_C a \<Longrightarrow> thesis"
  apply (tactic {* mk_set_cases_tac
    @{context}
    @{cterm a}
    @{thms assms}
    @{thm C.exhaust}
    @{thms C.set}
  *})
done

lemma C_set_cases_2[rotated -1]:
  assumes "\<And>arg. a = C2 arg \<Longrightarrow> elem \<in> set2_B arg \<Longrightarrow> thesis"
  shows "elem \<in> set2_C a \<Longrightarrow> thesis"
  apply (tactic {* mk_set_cases_tac
    @{context}
    @{cterm a}
    @{thms assms}
    @{thm C.exhaust}
    @{thms C.set}
  *})
done

lemma D1_set_cases[rotated -1]:
  assumes "\<And>arg x. a = D1 arg \<Longrightarrow> x \<in> set_A arg \<Longrightarrow> elem \<in> set1_B x \<Longrightarrow> thesis"
  shows "elem \<in> set_D1 a \<Longrightarrow> thesis"
  apply (tactic {* mk_set_cases_tac
    @{context}
    @{cterm a}
    @{thms assms}
    @{thm D1.exhaust}
    @{thms D1.set}
  *})
done

lemma D2_set_cases[rotated -1]:
  assumes "\<And>arg x. a = D2 arg \<Longrightarrow> x \<in> set_A arg \<Longrightarrow> elem \<in> Basic_BNFs.fsts x \<Longrightarrow> thesis"
  shows "elem \<in> set_D2 a \<Longrightarrow> thesis"
  apply (tactic {* mk_set_cases_tac
    @{context}
    @{cterm a}
    @{thms assms}
    @{thm D2.exhaust}
    @{thms D2.set}
  *})
done

lemma D3_set_cases[rotated -1]:
  assumes "\<And>arg x. a = D3 arg \<Longrightarrow> x \<in> set_A arg \<Longrightarrow> elem \<in> set1_B x \<Longrightarrow> thesis"
  shows "elem \<in> set_D3 a \<Longrightarrow> thesis"
  apply (tactic {* mk_set_cases_tac
    @{context}
    @{cterm a}
    @{thms assms}
    @{thm D3.exhaust}
    @{thms D3.set}
  *})
done

lemma D4_set_cases[rotated -1]:
  assumes "\<And>arg x. a = D4 arg \<Longrightarrow> x \<in> set_A arg \<Longrightarrow> elem \<in> set1_B x \<Longrightarrow> thesis"
      and "\<And>arg x x'. a = D4 arg \<Longrightarrow> x \<in> set_A arg \<Longrightarrow> x' \<in> set2_B x \<Longrightarrow> elem \<in> set1_B x' \<Longrightarrow> thesis"
  shows "elem \<in> set_D4 a \<Longrightarrow> thesis"
  apply (tactic {* mk_set_cases_tac
    @{context}
    @{cterm a}
    @{thms assms}
    @{thm D4.exhaust}
    @{thms D4.set}
  *})
done

lemma D5_set_cases[rotated -1]:
  assumes "\<And>arg x. a = D5 arg \<Longrightarrow> x \<in> set_A arg \<Longrightarrow> elem \<in> set1_B x \<Longrightarrow> thesis"
      and "\<And>arg x x' x''. a = D5 arg \<Longrightarrow> x \<in> set_A arg \<Longrightarrow> x' \<in> set2_B x \<Longrightarrow> x'' \<in> set1_B x' \<Longrightarrow> elem \<in> set_D5 x'' \<Longrightarrow> thesis"
  shows "elem \<in> set_D5 a \<Longrightarrow> thesis"
  apply (tactic {* mk_set_cases_tac
    @{context}
    @{cterm a}
    @{thms assms}
    @{thm D5.exhaust}
    @{thms D5.set}
  *})
done

lemma D6_set_cases[rotated -1]:
  assumes "\<And>arg. a = D6 arg \<Longrightarrow> elem \<in> set1_B arg \<Longrightarrow> thesis"
      and "\<And>arg x. a = D6 arg \<Longrightarrow> x \<in> set2_B arg \<Longrightarrow> elem \<in> set1_B x \<Longrightarrow> thesis"
      and "\<And>arg x x'. a = D6 arg \<Longrightarrow> x \<in> set2_B arg \<Longrightarrow> x' \<in> set2_B x \<Longrightarrow> elem \<in> set1_B x' \<Longrightarrow> thesis"
      and "\<And>arg x x' x''. a = D6 arg \<Longrightarrow> x \<in> set2_B arg \<Longrightarrow> x' \<in> set2_B x \<Longrightarrow> x'' \<in> set2_B x' \<Longrightarrow> elem \<in> set1_B x'' \<Longrightarrow> thesis"
      and "\<And>arg x x' x'' x'''. a = D6 arg \<Longrightarrow> x \<in> set2_B arg \<Longrightarrow> x' \<in> set2_B x \<Longrightarrow> x'' \<in> set2_B x' \<Longrightarrow> x''' \<in> set2_B x'' \<Longrightarrow> elem \<in> set1_B x''' \<Longrightarrow> thesis"
  shows "elem \<in> set_D6 a \<Longrightarrow> thesis"
  apply (tactic {* mk_set_cases_tac
    @{context}
    @{cterm a}
    @{thms assms}
    @{thm D6.exhaust}
    @{thms D6.set}
  *})
done

lemma E_set_cases_1[rotated -1]:
  assumes "\<And>arg. a = E arg \<Longrightarrow> elem \<in> Basic_BNFs.fsts arg \<Longrightarrow> thesis"
  shows "elem \<in> set1_E a \<Longrightarrow> thesis"
  apply (tactic {* mk_set_cases_tac
    @{context}
    @{cterm a}
    @{thms assms}
    @{thm E.exhaust}
    @{thms E.set}
  *})
done

lemma E_set_cases_2[rotated -1]:
  assumes "\<And>arg x. a = E arg \<Longrightarrow> x \<in> Basic_BNFs.snds arg \<Longrightarrow> elem \<in> set1_B x \<Longrightarrow> thesis"
  shows "elem \<in> set2_E a \<Longrightarrow> thesis"
  apply (tactic {* mk_set_cases_tac
    @{context}
    @{cterm a}
    @{thms assms}
    @{thm E.exhaust}
    @{thms E.set}
  *})
done

lemma E_set_cases_3[rotated -1]:
  assumes "\<And>arg x x' x'' x'''. a = E arg \<Longrightarrow> x \<in> Basic_BNFs.snds arg \<Longrightarrow> x'' \<in> set2_B x \<Longrightarrow> x''' \<in> set_A x'' \<Longrightarrow> elem \<in> set1_C x''' \<Longrightarrow> thesis"
  shows "elem \<in> set3_E a \<Longrightarrow> thesis"
  apply (tactic {* mk_set_cases_tac
    @{context}
    @{cterm a}
    @{thms assms}
    @{thm E.exhaust}
    @{thms E.set}
  *})
done

lemma E_set_cases_4[rotated -1]:
  assumes "\<And>arg x x' x'' x''' x''''. a = E arg \<Longrightarrow> x \<in> Basic_BNFs.snds arg \<Longrightarrow> x'' \<in> set2_B x \<Longrightarrow> x''' \<in> set_A x'' \<Longrightarrow> x'''' \<in> set2_C x''' \<Longrightarrow> elem \<in> Basic_BNFs.fsts x'''' \<Longrightarrow> thesis"
  shows "elem \<in> set4_E a \<Longrightarrow> thesis"
  apply (tactic {* mk_set_cases_tac
    @{context}
    @{cterm a}
    @{thms assms}
    @{thm E.exhaust}
    @{thms E.set}
  *})
done

lemma F_set_cases[rotated -1]:
  assumes "\<And>arg. a = F arg \<Longrightarrow> elem \<in> range arg \<Longrightarrow> thesis"
  shows "elem \<in> set_F a \<Longrightarrow> thesis"
  apply (tactic {* mk_set_cases_tac
    @{context}
    @{cterm a}
    @{thms assms}
    @{thm F.exhaust}
    @{thms F.set}
  *})
done

end
