theory Hall_of_Shame
imports Main
begin

locale A = fixes a :: int
begin

codatatype x = X (y: int) | Y where "y Y = a"
end

interpretation A 10 .

primcorec f :: "int \<Rightarrow> x" where
  "f x = X 5"

codatatype 'a llist = LNil | LCons 'a "'a llist"

datatype 'a rose_tree = Rose_Tree 'a "'a rose_tree list"

codatatype ('call, 'ret) call
  = Complete_Call "('call \<times> 'ret) rose_tree" "('call, 'ret) call"
  | Unfinished_call 'call "('call, 'ret) call"
  | Nil_call

consts matching_return :: "nat \<Rightarrow> ('call + 'ret) llist \<Rightarrow> (('call + 'ret) list \<times> 'ret \<times> ('call + 'ret) llist) option"

consts complete_call_trees :: "('call + 'ret) list \<Rightarrow> ('call \<times> 'ret) rose_tree list \<times> ('call + 'ret) list"

primcorec call_tree :: "('call + 'ret) llist \<Rightarrow> ('call, 'ret) call"
where
  "call_tree xs =
  (case xs of LNil \<Rightarrow> Nil_call
   | LCons (Inl c) xs' \<Rightarrow>
     (case matching_return 0 xs' of None \<Rightarrow> Unfinished_call c (call_tree xs')
      | Some (ys, ret, xs'') \<Rightarrow> Complete_Call (Rose_Tree (c, ret) (fst (complete_call_trees (ys)))) (call_tree xs'')))"

end
