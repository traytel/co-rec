theory disc_transfer
imports "~~/src/HOL/Transfer"
begin

context lifting_syntax (* enables the ===> syntax for rel_fun *)
begin

declare [[bnf_note_all]]

datatype_new 'a list
  = nil: Nil
  | Cons (head: 'a) (tail: "'a list")
  where
    "tail Nil = Nil"

datatype_new 'a option
  = is_none: None
  | is_some: Some (the: 'a)

datatype_new 'a tree
  = is_node: Node (val: 'a) (children: "'a tree list")

datatype_new ('a, 'b) example
  = is_N1: N1 'a
  | is_N2: N2 'b
  | is_N3: N3 'a 'b
  | is_N4: N4
  | is_ConsA: ConsA 'a "('a, 'b) example"
  | is_ConsB: ConsB 'b "('a, 'b) example"

datatype_new 'a tree_mutual = is_TNode: TNode 'a "'a forest_mutual"
         and 'a forest_mutual = is_FNil: FNil | is_FCons: FCons "'a tree_mutual" "'a forest_mutual"

datatype_new ('a, 'b) F1 = is_F1: F1 "'a \<Rightarrow> 'b"

datatype_new ('a, 'b, 'c) F2 = is_F2: F2 "'a \<Rightarrow> 'b \<Rightarrow> 'c"

datatype_new 'a A1 = is_A1: A1 nat
         and 'a A2 = is_A2: A2 'a

ML {*
fun mk_disc_transfer_tac rel_sel exhaust_disc distinct_disc=
  let
    fun handle_last_disc iffD =
      HEADGOAL (rtac (rotate_prems (~1) exhaust_disc) THEN'
      atac THEN'
      REPEAT_DETERM o (rotate_tac (~1) THEN'
        dtac (rotate_prems 1 iffD) THEN'
        atac THEN'
        rotate_tac (~1) THEN'
        etac (rotate_prems 1 notE) THEN'
        eresolve_tac distinct_disc))
  in
    REPEAT_DETERM (HEADGOAL (rtac @{thm rel_funI} THEN'
      dtac (rel_sel RS iffD1) THEN'
      REPEAT_DETERM o (etac conjE) THEN'
      (atac ORELSE' rtac iffI))) THEN
    TRY (handle_last_disc iffD2) THEN
    TRY (handle_last_disc iffD1)
  end;
*}

thm list.disc_transfer
lemma list_disc_transfer: "(rel_list R ===> op =) nil nil"
  apply (tactic {* mk_disc_transfer_tac @{thm list.rel_sel} @{thm list.exhaust_disc} @{thms (* list.distinct_disc *)} *})
  (* apply (rule rel_funI)
  apply (drule iffD1[OF list.rel_sel])
  apply (erule conjE)
  apply assumption *)
done

thm option.disc_transfer
lemma option_disc_transfer:
  "(rel_option R ===> op =) is_none is_none"
  "(rel_option R ===> op =) is_some is_some"
  apply (tactic {* mk_disc_transfer_tac @{thm option.rel_sel} @{thm option.exhaust_disc} @{thms option.distinct_disc} *})
  (* apply (rule rel_funI, drule iffD1[OF option.rel_sel], (erule conjE)+, (assumption | rule iffI))+ *)

  (* apply (rule option.exhaust_disc[rotated -1]) *)
  (* apply assumption *)
  (* apply (rotate_tac -1, drule iffD2[rotated], assumption, rotate_tac -1, erule notE[rotated], erule option.distinct_disc)+ *)

  (* apply (rule option.exhaust_disc[rotated -1]) *)
  (* apply assumption *)
  (* apply (rotate_tac -1, drule iffD1[rotated], assumption, rotate_tac -1, erule notE[rotated], erule option.distinct_disc)+ *)
  done

thm tree.disc_transfer
lemma tree_disc_transfer:
  "(rel_tree R ===> op =) is_node is_node"
  apply (tactic {* mk_disc_transfer_tac @{thm tree.rel_sel} @{thm tree.exhaust_disc} @{thms (* tree.distinct_disc *)} *})

  (* apply (rule tree.exhaust_disc[rotated -1]) *)
  (* apply assumption *)
  (* apply (rotate_tac -1) *)
  (* apply (drule iffD2[rotated -1]) *)
  (* apply (rule refl) *)
  (* apply (rule tree.exhaust_disc[rotated -1]) *)
  (* apply assumption *)
  done

thm example.disc_transfer
lemma example_disc_transfer:
  "(rel_example R1 R2 ===> op =) is_N1 is_N1"
  "(rel_example R1 R2 ===> op =) is_N2 is_N2"
  "(rel_example R1 R2 ===> op =) is_N3 is_N3"
  "(rel_example R1 R2 ===> op =) is_N4 is_N4"
  "(rel_example R1 R2 ===> op =) is_ConsA is_ConsA"
  "(rel_example R1 R2 ===> op =) is_ConsB is_ConsB"
  apply (tactic {* mk_disc_transfer_tac @{thm example.rel_sel} @{thm example.exhaust_disc} @{thms example.distinct_disc} *})
  (* apply (rule rel_funI, drule iffD1[OF example.rel_sel], (erule conjE)+, (assumption | rule iffI))+ *)

  (* apply (rule example.exhaust_disc[rotated -1]) *)
  (* apply assumption *)
  (* apply (rotate_tac -1, drule iffD2[rotated], assumption, rotate_tac -1, erule notE[rotated], erule example.distinct_disc)+ *)

  (* apply (rule example.exhaust_disc[rotated -1]) *)
  (* apply assumption *)
  (* apply (rotate_tac -1, drule iffD1[rotated], assumption, rotate_tac -1, erule notE[rotated], erule example.distinct_disc)+ *)
  done

thm tree_mutual.disc_transfer
lemma tree_mutual_disc_transfer:
  "(rel_tree_mutual R ===> op =) is_TNode is_TNode"
  apply (rule rel_funI)
  apply (drule iffD1[OF tree_mutual.rel_sel])
  apply (erule conjE)
  by (metis tree_mutual.exhaust_disc)

thm forest_mutual.disc_transfer
lemma forest_mutual_disc_transfer:
  "(rel_forest_mutual R ===> op =) is_FNil is_FNil"
  "(rel_forest_mutual R ===> op =) is_FCons is_FCons"
  apply (tactic {* mk_disc_transfer_tac @{thm forest_mutual.rel_sel} @{thm forest_mutual.exhaust_disc} @{thms forest_mutual.distinct_disc} *})
  (* apply (rule rel_funI, drule iffD1[OF forest_mutual.rel_sel], (erule conjE)+, (assumption | rule iffI))+ *)

  (* apply (rule forest_mutual.exhaust_disc[rotated -1]) *)
  (* apply assumption *)
  (* apply (rotate_tac -1, drule iffD2[rotated], assumption, rotate_tac -1, erule notE[rotated], erule forest_mutual.distinct_disc)+ *)

  (* apply (rule forest_mutual.exhaust_disc[rotated -1]) *)
  (* apply assumption *)
  (* apply (rotate_tac -1, drule iffD1[rotated], assumption, rotate_tac -1, erule notE[rotated], erule forest_mutual.distinct_disc)+ *)
done

thm F1.disc_transfer
lemma F1_disc_transfer:
  "(rel_F1 R ===> op =) is_F1 is_F1"
  apply (tactic {* mk_disc_transfer_tac @{thm F1.rel_sel} @{thm F1.exhaust_disc} @{thms (* F1.distinct_disc *)} *})
  (* apply (rule rel_funI, drule iffD1[OF F1.rel_sel], ((erule conjE)+, (assumption | rule iffI) | erule rel_funE, rule refl))+ *)
  (* apply (rule iffI) *)

  (* apply (rule F1.exhaust_disc[rotated -1]) *)
  (* apply assumption *)
  (* apply (rotate_tac -1) *)
  (* apply (drule iffD2[rotated]) *)
  (* apply (rule refl) *)
  (* apply (rule F1.exhaust_disc[rotated -1]) *)
  (* apply assumption *)
done

thm F2.disc_transfer
lemma F2_disc_transfer:
  "(rel_F2 R ===> op =) is_F2 is_F2"
  apply (tactic {* mk_disc_transfer_tac @{thm F2.rel_sel} @{thm F2.exhaust_disc} @{thms (* F2.distinct_disc *)} *})
  (* apply (rule rel_funI, drule iffD1[OF F2.rel_sel], ((erule conjE)+, (assumption | rule iffI) | erule rel_funE, rule refl))+ *)
  (* apply (rule iffI) *)

  (* apply (rule F2.exhaust_disc[rotated -1]) *)
  (* apply assumption *)
  (* apply (rotate_tac -1) *)
  (* apply (drule iffD2[rotated]) *)
  (* apply (rule refl) *)
  (* apply (rule F2.exhaust_disc[rotated -1]) *)
  (* apply assumption *)
done

thm A1.disc_transfer
lemma A1_disc_transfer:
  "(rel_A1 R ===> op =) is_A1 is_A1"
  apply (tactic {* mk_disc_transfer_tac @{thm A1.rel_sel} @{thm A1.exhaust_disc} @{thms (* A1.distinct_disc *)} *})
  (* apply (rule rel_funI, drule iffD1[OF A1.rel_sel], (((erule conjE)+)?, (assumption | rule iffI) | erule rel_funE, rule refl))+ *)

  (* apply (rule A1.exhaust_disc[rotated -1]) *)
  (* apply assumption *)
  (* apply (rotate_tac -1) *)
  (* apply (drule iffD2[rotated -1]) *)
  (* apply (rule refl) *)
  (* apply (rule A1.exhaust_disc[rotated -1]) *)
  (* apply assumption *)
done

thm A2.disc_transfer
lemma A2_disc_transfer:
  "(rel_A2 R ===> op =) is_A2 is_A2"
  apply (tactic {* mk_disc_transfer_tac @{thm A2.rel_sel} @{thm A2.exhaust_disc} @{thms (* A2.distinct_disc *)} *})
  (* apply (rule rel_funI, drule iffD1[OF A2.rel_sel], (((erule conjE)+)?, (assumption | rule iffI) | erule rel_funE, rule refl))+ *)

  (* apply (rule A2.exhaust_disc[rotated -1]) *)
  (* apply assumption *)
  (* apply (rotate_tac -1) *)
  (* apply (drule iffD2[rotated -1]) *)
  (* apply (rule refl) *)
  (* apply (rule A2.exhaust_disc[rotated -1]) *)
  (* apply assumption *)
done

end

end
