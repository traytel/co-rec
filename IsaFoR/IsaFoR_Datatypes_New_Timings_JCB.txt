generic_assm_proof

Construction of BNFs: 55.55 s 
Normalization & sealing of BNFs: 3.48 s 
Extracted terms & thms: 160 ms 
Checked nonemptiness: 0 ms 
Derived simple theorems: 110 ms 
Algebra definition & thms: 169 ms 
Proved nonemptiness: 6 ms 
Morphism definition & thms: 309 ms 
Isomorphism definition & thms: 23 ms 
Copy thms: 38 ms 
Bounds: 32 ms 
min_algs definition & thms: 726 ms 
Minimal algebra definition & thms: 268 ms 
Initiality definition & thms: 876 ms 
THE TYPEDEFs & Rep/Abs thms: 686 ms 
ctor definitions & thms: 263 ms 
fold definitions & thms: 137 ms 
dtor definitions & thms: 114 ms 
rec definitions & thms: 175 ms 
induction: 22 ms 
bnf constants for the new datatypes: 1.52 s 
map functions for the new datatypes: 34 ms 
set functions for the new datatypes: 194 ms 
helpers for BNF properties: 149 ms 
registered new datatypes as BNFs: 221 ms 
relator induction: 128 ms 
FP construction in total: 6.59 s 
Constructors, discriminators, selectors, etc., for the new datatype: 18.49 s
----------------------------------------------------------------------------------------------------

assm

Construction of BNFs: 36.68 s 
Normalization & sealing of BNFs: 2.62 s 
Extracted terms & thms: 143 ms 
Checked nonemptiness: 0 ms 
Derived simple theorems: 83 ms 
Algebra definition & thms: 186 ms 
Proved nonemptiness: 4 ms 
Morphism definition & thms: 395 ms 
Isomorphism definition & thms: 48 ms 
Copy thms: 53 ms 
Bounds: 54 ms 
min_algs definition & thms: 762 ms 
Minimal algebra definition & thms: 492 ms 
Initiality definition & thms: 8.61 s 
THE TYPEDEFs & Rep/Abs thms: 690 ms 
ctor definitions & thms: 130 ms 
fold definitions & thms: 104 ms 
dtor definitions & thms: 79 ms 
rec definitions & thms: 97 ms 
induction: 12 ms 
bnf constants for the new datatypes: 435 ms 
map functions for the new datatypes: 22 ms 
set functions for the new datatypes: 45 ms 
helpers for BNF properties: 55 ms 
registered new datatypes as BNFs: 103 ms 
relator induction: 103 ms 
FP construction in total: 12.86 s 
Constructors, discriminators, selectors, etc., for the new datatype: 7.0 s
----------------------------------------------------------------------------------------------------

dp_nontermination_proofs and friends

Construction of BNFs: 222.03 s 
Normalization & sealing of BNFs: 18.4 s 
Extracted terms & thms: 342 ms 
Checked nonemptiness: 0 ms 
Derived simple theorems: 343 ms 
Algebra definition & thms: 407 ms 
Proved nonemptiness: 85 ms 
Morphism definition & thms: 1.07 s 
Isomorphism definition & thms: 41 ms 
Copy thms: 363 ms 
Bounds: 549 ms 
min_algs definition & thms: 80.65 s 
Minimal algebra definition & thms: 135.9 s 
Initiality definition & thms: 39.54 s 
THE TYPEDEFs & Rep/Abs thms: 14.47 s 
ctor definitions & thms: 3.52 s 
fold definitions & thms: 1.29 s 
dtor definitions & thms: 1.68 s 
rec definitions & thms: 2.22 s 
induction: 519 ms 
bnf constants for the new datatypes: 5.89 s 
map functions for the new datatypes: 98 ms 
set functions for the new datatypes: 605 ms 
helpers for BNF properties: 617 ms 
registered new datatypes as BNFs: 658 ms 
relator induction: 1.38 s 
FP construction in total: 293.68 s 
Constructors, discriminators, selectors, etc., for the new datatype: 55.25 s\
----------------------------------------------------------------------------------------------------

dp_termination_proofs and friends

Construction of BNFs: 617.71 s 
Normalization & sealing of BNFs: 27.05 s 
Extracted terms & thms: 715 ms 
Checked nonemptiness: 0 ms 
Derived simple theorems: 432 ms 
Algebra definition & thms: 791 ms 
Proved nonemptiness: 116 ms 
Morphism definition & thms: 2.34 s 
Isomorphism definition & thms: 100 ms 
Copy thms: 226 ms 
Bounds: 766 ms 
min_algs definition & thms: 84.0 s 
Minimal algebra definition & thms: 21.03 s 
Initiality definition & thms: 221.79 s 
THE TYPEDEFs & Rep/Abs thms: 27.64 s 
ctor definitions & thms: 6.68 s 
fold definitions & thms: 2.16 s 
dtor definitions & thms: 4.08 s 
rec definitions & thms: 2.99 s 
induction: 845 ms 
bnf constants for the new datatypes: 7.99 s 
map functions for the new datatypes: 148 ms 
set functions for the new datatypes: 789 ms 
helpers for BNF properties: 890 ms 
registered new datatypes as BNFs: 979 ms 
relator induction: 1.84 s 
FP construction in total: 391.62 s 
Constructors, discriminators, selectors, etc., for the new datatype: 218.0 s
