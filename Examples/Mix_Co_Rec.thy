theory Mix_Co_Rec
imports "~~/src/HOL/BNF/BNF"
begin

codatatype natstream = SCons (shd: nat) (stl: natstream)

function ff :: "nat \<Rightarrow> nat \<Rightarrow> natstream" where
"ff 2 j = (if j = 0 then undefined else SCons 0 (ff 3 (j - 1)))" |
"ff 3 j = (if j = 0 then undefined else stl (ff 1 (j + 1)))" |
"ff 1 j = (if j = 0 then undefined else SCons 4 (ff 2 (j - 1)))" |
"n \<notin> {1, 2, 3} \<Longrightarrow> ff n j = undefined"
by force+
termination sorry

fun snth where
"snth xs 0 = shd xs" |
"snth xs (Suc i) = snth (stl xs) i"

definition shdi_f :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
[simp]: "shdi_f n i = snth (ff n (i + 1)) i"

function fff :: "nat \<Rightarrow> nat \<Rightarrow> natstream" where
"fff n i = SCons (shdi_f n i) (fff n (i + 1))"
by auto
termination sorry

declare fff.simps [simp del]

definition f :: "nat \<Rightarrow> natstream" where
[simp]: "f n = fff n 0"

lemma "f 2 = SCons 0 (f 3)"
apply simp
apply (subst fff.simps)
sledgehammer [e]


oops
"f 3 = stl (f 1)" |
"f 1 = (if j = 0 then undefined else SCons 4 (ff 2 (j - 1)))" |
"n \<notin> {1, 2, 3} \<Longrightarrow> ff n j = undefined"






codatatype natstream = Stream nat natstream

ML {*
BNF_FP_Rec_Sugar_Util.corec_specs_of [@{binding f}]
  [@{typ nat}] [@{typ natstream}]
  (fn t => if exists_subterm (fn Free ("f", _) => true | Free ("g", _) => true | _ => false) t then [0] else [])
  [[]]
  @{context}
*}


end
