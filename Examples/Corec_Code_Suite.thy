theory Corec_Code_Suite
imports Corec_Suite_Base
begin

primcorec f1_s2' :: "nat \<Rightarrow> s2'" where
  "f1_s2' n = (if n = 0 then Y1' () else Y2' ())"
print_theorems

primcorec f2_s2' :: "nat \<Rightarrow> s2'" where
  "f2_s2' n = (if n = 0 then Y1' ()
     else let m = n in if m = 1 then Y2' ()
     else if m = 2 \<and> n < 3 then Y1' ()
     else if m = 3 then Y2' ()
     else Y1' ())"
print_theorems

primcorec f3_s2' :: "nat \<Rightarrow> s2'" where
  "f3_s2' n = (if n = 0 then Y1' ()
     else if n = 1 then some_value
     else Y2' ())"
print_theorems

primcorec f4_s2' :: "s5 \<Rightarrow> s2'" where
  "f4_s2' n = (case n of Z1 \<Rightarrow> Y1' () | Z3 \<Rightarrow> Y2' () | _ \<Rightarrow> Y1' some_value)"
print_theorems

primcorec f1_s5 :: "nat \<Rightarrow> s5" where
  "f1_s5 n = (if n = 1 then Z1
     else if n = 3 then Z3
     else if n = 4 then Z4 ()
     else if n = 5 then Z5
     else if n > 500 then Z2 n (Suc n)
     else some_value)"
print_theorems

primcorec f5_llist :: "'a llist \<Rightarrow> 'a llist \<Rightarrow> 'a llist" where
  "f5_llist xs ys =
     (if lnull xs then ys
      else LCons (lhd xs) (f5_llist (ltl xs) ys))"
print_theorems

end
