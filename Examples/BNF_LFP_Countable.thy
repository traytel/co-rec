theory BNF_LFP_Countable
imports
  "~~/src/HOL/Library/Countable"
  "~~/src/HOL/Library/FSet"
begin

section {* Tactics *}

ML {*
open BNF_FP_Rec_Sugar_Util
open BNF_Util
open BNF_Tactics
*}

ML {*
fun nchotomy_tac nchotomy =
  HEADGOAL (rtac (nchotomy RS @{thm all_reg[rotated]}) THEN'
    REPEAT_ALL_NEW (resolve_tac [allI, impI] ORELSE' eresolve_tac [exE, disjE]));
*}

ML {*
fun meta_spec_mp_tac 0 = K all_tac
  | meta_spec_mp_tac n =
    dtac meta_spec THEN' meta_spec_mp_tac (n - 1) THEN' dtac meta_mp THEN' atac;
*}

ML {*
val use_induction_hypothesis_tac =
  DEEPEN (1, 1000000 (* large number *))
    (fn depth => meta_spec_mp_tac depth THEN' etac allE THEN' etac impE THEN' atac THEN' atac) 0;
*}

ML {*
fun same_ctr_tac ctxt injects recs map_congs' inj_map_strongs' =
  HEADGOAL (asm_full_simp_tac (ss_only (injects @ recs @ map_congs' @
        @{thms sum_encode_eq prod_encode_eq sum.inject prod.inject to_nat_split id_apply snd_conv simp_thms})
      ctxt) THEN_MAYBE'
    TRY o REPEAT_ALL_NEW (rtac conjI) THEN_ALL_NEW
    REPEAT_ALL_NEW (eresolve_tac (conjE :: inj_map_strongs')) THEN_ALL_NEW
    (atac ORELSE' use_induction_hypothesis_tac));
*}

ML {*
fun distinct_ctrs_tac ctxt recs =
  HEADGOAL (asm_full_simp_tac (ss_only (recs @
    @{thms sum_encode_eq sum.inject sum.distinct simp_thms}) ctxt));
*}

ML {*
fun endgame_tac ctxt n encode_injective =
  TRY (Class.intro_classes_tac []) THEN
  unfold_thms_tac ctxt @{thms inj_on_def ball_UNIV} THEN
  ALLGOALS (rtac exI THEN' rtac allI THEN'
    resolve_tac (map (fn i => encode_injective RS mk_conjunctN n i) (1 upto n)));
*}


section {* A unit type *}

datatype_new d = D

abbreviation "d_encode \<equiv> rec_d (0 :: nat)"

lemma d_encode_injective: "\<forall>y. d_encode x = d_encode y \<longrightarrow> x = y"
  apply (rule d.induct)
  apply (tactic {* nchotomy_tac @{thm d.nchotomy} *})
  apply (tactic {* same_ctr_tac @{context} [] @{thms d.rec} [] [] *})
  done

lemma d_ex_injective: "\<exists>to_nat :: d \<Rightarrow> nat. inj to_nat"
  apply (tactic {* endgame_tac @{context} 1 @{thm d_encode_injective} *})
  done

instance d :: countable
  apply (tactic {* endgame_tac @{context} 1 @{thm d_encode_injective} *})
  done


section {* Lists *}

datatype_new 'a l = N | C 'a "'a l"

abbreviation "l_encode \<equiv> rec_l (sum_encode (Inl 0)) (\<lambda>x xs xsa. sum_encode (Inr (prod_encode (to_nat x, xsa))))"

lemma l_encode_injective: "\<forall>y. l_encode x = l_encode y \<longrightarrow> x = y"
  apply (rule l.induct)
   apply (tactic {* nchotomy_tac @{thm l.nchotomy} *})
    apply (tactic {* same_ctr_tac @{context} @{thms l.inject} @{thms l.rec} [] [] *})
   apply (tactic {* distinct_ctrs_tac @{context} @{thms l.rec} *})
  apply (tactic {* nchotomy_tac @{thm l.nchotomy} *})
  apply (tactic {* distinct_ctrs_tac @{context} @{thms l.rec} *})
  apply (tactic {* same_ctr_tac @{context} @{thms l.inject} @{thms l.rec} [] [] *})
  done

lemma l_ex_injective: "\<exists>to_nat :: ('a :: countable) l \<Rightarrow> nat. inj to_nat"
  apply (tactic {* endgame_tac @{context} 1 @{thm l_encode_injective} *})
  done

instance l :: (countable) countable
  apply (tactic {* endgame_tac @{context} 1 @{thm l_encode_injective} *})
  done


section {* Terminated Lists *}

datatype_new ('a, 'b) x = XN 'b | XC 'a "('a, 'b) x"

abbreviation "x_encode \<equiv> rec_x (\<lambda>y. sum_encode (Inl (to_nat y))) (\<lambda>x xs xsa. sum_encode (Inr (prod_encode (to_nat x, xsa))))"

lemma x_encode_injective: "\<forall>y. x_encode x = x_encode y \<longrightarrow> x = y"
  apply (rule x.induct)
   apply (tactic {* nchotomy_tac @{thm x.nchotomy} *})
    apply (tactic {* same_ctr_tac @{context} [] @{thms x.rec} [] [] *})
   apply (tactic {* distinct_ctrs_tac @{context} @{thms x.rec} *})
  apply (tactic {* nchotomy_tac @{thm x.nchotomy} *})
   apply (tactic {* distinct_ctrs_tac @{context} @{thms x.rec} *})
  apply (tactic {* same_ctr_tac @{context} [] @{thms x.rec} [] [] *})
  done

lemma x_ex_injective: "\<exists>to_nat :: ('a :: countable, 'b :: countable) x \<Rightarrow> nat. inj to_nat"
  apply (tactic {* endgame_tac @{context} 1 @{thm x_encode_injective} *})
  done

instance x :: (countable, countable) countable
  apply (tactic {* endgame_tac @{context} 1 @{thm x_encode_injective} *})
  done


section {* Lists with Additional Constructors *}

datatype_new 'a ml = N | C 'a "'a ml" | C' "'a ml" | C'' "'a ml"

abbreviation "ml_encode \<equiv> rec_ml (sum_encode (Inl (sum_encode (Inl 0))))
  (\<lambda>x xs xsa. sum_encode (Inl (sum_encode (Inr (prod_encode (to_nat x, xsa))))))
  (\<lambda>xs xsa. sum_encode (Inr (sum_encode (Inl xsa))))
  (\<lambda>xs xsa. sum_encode (Inr (sum_encode (Inr xsa))))"

lemma ml_encode_injective: "\<forall>y. ml_encode x = ml_encode y \<longrightarrow> x = y"
  apply (rule ml.induct)
     apply (tactic {* nchotomy_tac @{thm ml.nchotomy} *})
        apply (tactic {* same_ctr_tac @{context} @{thms ml.inject} @{thms ml.rec} [] [] *})
       apply (tactic {* distinct_ctrs_tac @{context} @{thms ml.rec} *})
      apply (tactic {* distinct_ctrs_tac @{context} @{thms ml.rec} *})
     apply (tactic {* distinct_ctrs_tac @{context} @{thms ml.rec} *})
    apply (tactic {* nchotomy_tac @{thm ml.nchotomy} *})
       apply (tactic {* distinct_ctrs_tac @{context} @{thms ml.rec} *})
      apply (tactic {* same_ctr_tac @{context} @{thms l.inject} @{thms ml.rec} [] [] *})
     apply (tactic {* distinct_ctrs_tac @{context} @{thms ml.rec} *})
    apply (tactic {* distinct_ctrs_tac @{context} @{thms ml.rec} *})
   apply (tactic {* nchotomy_tac @{thm ml.nchotomy} *})
      apply (tactic {* distinct_ctrs_tac @{context} @{thms ml.rec} *})
     apply (tactic {* distinct_ctrs_tac @{context} @{thms ml.rec} *})
    apply (tactic {* same_ctr_tac @{context} @{thms l.inject} @{thms ml.rec} [] [] *})
   apply (tactic {* distinct_ctrs_tac @{context} @{thms ml.rec} *})
  apply (tactic {* nchotomy_tac @{thm ml.nchotomy} *})
     apply (tactic {* distinct_ctrs_tac @{context} @{thms ml.rec} *})
    apply (tactic {* distinct_ctrs_tac @{context} @{thms ml.rec} *})
   apply (tactic {* distinct_ctrs_tac @{context} @{thms ml.rec} *})
  apply (tactic {* same_ctr_tac @{context} @{thms l.inject} @{thms ml.rec} [] [] *})
  done

lemma ml_ex_injective: "\<exists>to_nat :: ('a :: countable) ml \<Rightarrow> nat. inj to_nat"
  apply (tactic {* endgame_tac @{context} 1 @{thm ml_encode_injective} *})
  done

instance ml :: (countable) countable
  apply (tactic {* endgame_tac @{context} 1 @{thm ml_encode_injective} *})
  done


section {* Mutual Lists and Rose Trees *}

datatype_new
  'a tl = TN | TC "'a mt" "'a tl" and
  'a mt = MT 'a "'a tl"

abbreviation "tl_encode \<equiv> rec_tl (sum_encode (Inl 0)) (\<lambda>t ts ta tsa. sum_encode (Inr (prod_encode (ta, tsa)))) (\<lambda>x ts tsa. prod_encode (to_nat x, tsa))"
abbreviation "mt_encode \<equiv> rec_mt (sum_encode (Inl 0)) (\<lambda>t ts ta tsa. sum_encode (Inr (prod_encode (ta, tsa)))) (\<lambda>x ts tsa. prod_encode (to_nat x, tsa))"

lemma tl_mt_encode_injective:
  "(\<forall>us::('a::countable) tl. tl_encode ts = tl_encode us \<longrightarrow> ts = us) &
   (\<forall>u::'a mt. mt_encode t = mt_encode u \<longrightarrow> t = u)"
  apply (rule tl_mt.induct)
    apply (tactic {* nchotomy_tac @{thm tl.nchotomy} *})
     apply (tactic {* same_ctr_tac @{context} @{thms tl.inject} @{thms tl.rec} [] [] *})
    apply (tactic {* distinct_ctrs_tac @{context} @{thms tl.rec} *})
   apply (tactic {* nchotomy_tac @{thm tl.nchotomy} *})
    apply (tactic {* distinct_ctrs_tac @{context} @{thms tl.rec} *})
   apply (tactic {* same_ctr_tac @{context} @{thms tl.inject} @{thms tl.rec} [] [] *})
  apply (tactic {* nchotomy_tac @{thm mt.nchotomy} *})
  apply (tactic {* same_ctr_tac @{context} @{thms mt.inject} @{thms mt.rec} [] [] *})
  done

lemma tl_mt_ex_injective:
  "\<exists>to_nat :: ('a :: countable) tl \<Rightarrow> nat. inj to_nat"
  "\<exists>to_nat :: ('a :: countable) mt \<Rightarrow> nat. inj to_nat"
  apply (tactic {* endgame_tac @{context} 2 @{thm tl_mt_encode_injective} *})
  done

instance tl :: (countable) countable
  apply (tactic {* endgame_tac @{context} 2 @{thm tl_mt_encode_injective} *})
  done

instance mt :: (countable) countable
  apply (tactic {* endgame_tac @{context} 2 @{thm tl_mt_encode_injective} *})
  done


section {* Rose Trees with List of Subtrees *}

datatype_new 'a t = T nat 'a "'a t l"

abbreviation "t_encode \<equiv> rec_t (\<lambda>n x ts. prod_encode (n, prod_encode (to_nat x, to_nat (map_l snd ts))))"

lemma t_encode_injective: "\<forall>u. t_encode t = t_encode u \<longrightarrow> t = u"
  apply (rule t.induct)
  apply (tactic {* nchotomy_tac @{thm t.nchotomy} *})
  apply (tactic {* same_ctr_tac @{context} @{thms t.inject} @{thms t.rec}
    @{thms l.map_comp[unfolded o_def]} @{thms l.inj_map_strong[rotated -1]} *})
  done

lemma t_ex_injective: "\<exists>to_nat :: ('a :: countable) t \<Rightarrow> nat. inj to_nat"
  apply (tactic {* endgame_tac @{context} 1 @{thm t_encode_injective} *})
  done

instance t :: (countable) countable
  apply (tactic {* endgame_tac @{context} 1 @{thm t_encode_injective} *})
  done


section {* Weird Nested Type *}

datatype_new n = N "n + nat"

abbreviation "n_encode \<equiv> rec_n (\<lambda>s. to_nat (map_sum snd id s))"

lemma n_encode_injective: "\<forall>u. n_encode t = n_encode u \<longrightarrow> t = u"
  apply (rule n.induct)
  apply (tactic {* nchotomy_tac @{thm n.nchotomy} *})
  apply (tactic {* same_ctr_tac @{context} @{thms n.inject} @{thms n.rec}
    @{thms sum.map_comp[unfolded o_def]} @{thms sum.inj_map_strong[rotated -1]} *})
  done

lemma n_ex_injective: "\<exists>to_nat :: n \<Rightarrow> nat. inj to_nat"
  apply (tactic {* endgame_tac @{context} 1 @{thm n_encode_injective} *})
  done

instance n :: countable
  apply (tactic {* endgame_tac @{context} 1 @{thm n_encode_injective} *})
  done


section {* Binary Rose Trees *}

datatype_new 'a b = B nat 'a "'a b l" "'a b l"

abbreviation
  "b_encode \<equiv> rec_b (\<lambda>n x ts us. prod_encode (n, prod_encode (to_nat x,
     prod_encode (to_nat (map_l snd ts), to_nat (map_l snd us)))))"

lemma b_encode_injective: "\<forall>u. b_encode t = b_encode u \<longrightarrow> t = u"
  apply (rule b.induct)
  apply (tactic {* nchotomy_tac @{thm b.nchotomy} *})
  apply (tactic {* same_ctr_tac @{context} @{thms b.inject} @{thms b.rec}
    @{thms l.map_comp[unfolded o_def]} @{thms l.inj_map_strong[rotated -1]} *})
  done

lemma b_ex_injective: "\<exists>to_nat :: ('a :: countable) b \<Rightarrow> nat. inj to_nat"
  apply (tactic {* endgame_tac @{context} 1 @{thm b_encode_injective} *})
  done

instance b :: (countable) countable
  apply (tactic {* endgame_tac @{context} 1 @{thm b_encode_injective} *})
  done


section {* Rose Trees with a Finite Set of Subtrees *}

datatype_new 'a ft = T nat 'a "'a ft fset"

instance fset :: (countable) countable
  sorry

abbreviation "ft_encode \<equiv> rec_ft (\<lambda>n x ts. prod_encode (n, prod_encode (to_nat x, to_nat (snd |`| ts))))"

lemma ft_encode_injective: "\<forall>u. ft_encode t = ft_encode u \<longrightarrow> t = u"
  apply (rule ft.induct)
  apply (tactic {* nchotomy_tac @{thm ft.nchotomy} *})
  apply (tactic {* same_ctr_tac @{context} @{thms ft.inject} @{thms ft.rec}
    @{thms fset.map_comp[unfolded o_def]} @{thms fset.inj_map_strong[rotated -1]} *})
  done

lemma ft_ex_injective: "\<exists>to_nat :: ('a :: countable) ft \<Rightarrow> nat. inj to_nat"
  apply (tactic {* endgame_tac @{context} 1 @{thm ft_encode_injective} *})
  done

instance ft :: (countable) countable
  apply (tactic {* endgame_tac @{context} 1 @{thm ft_encode_injective} *})
  done


section {* Mixed Rose Trees *}

datatype_new 'a u =
  UL nat 'a "'a u l"
| UF 'a "'a u fset"

abbreviation "u_encode \<equiv>
  rec_u (\<lambda>n x us. sum_encode (Inl (prod_encode (n, prod_encode (to_nat x, to_nat (map_l snd us))))))
    (\<lambda>x us. sum_encode (Inr (prod_encode (to_nat x, to_nat (snd |`| us)))))"

lemma u_encode_injective: "\<forall>u. u_encode t = u_encode u \<longrightarrow> t = u"
  apply (rule u.induct)
   apply (tactic {* nchotomy_tac @{thm u.nchotomy} *})
    apply (tactic {* same_ctr_tac @{context} @{thms u.inject} @{thms u.rec}
      @{thms l.map_comp[unfolded o_def]} @{thms l.inj_map_strong[rotated -1]} *})
   apply (tactic {* distinct_ctrs_tac @{context} @{thms u.rec} *})
  apply (tactic {* nchotomy_tac @{thm u.nchotomy} *})
   apply (tactic {* distinct_ctrs_tac @{context} @{thms u.rec} *})
  apply (tactic {* same_ctr_tac @{context} @{thms u.inject} @{thms u.rec}
    @{thms fset.map_comp[unfolded o_def]} @{thms fset.inj_map_strong[rotated -1]} *})
  done

lemma u_ex_injective: "\<exists>to_nat :: ('a :: countable) u \<Rightarrow> nat. inj to_nat"
  apply (tactic {* endgame_tac @{context} 1 @{thm u_encode_injective} *})
  done

instance u :: (countable) countable
  apply (tactic {* endgame_tac @{context} 1 @{thm u_encode_injective} *})
  done


section {* Two-Level Rose Trees *}

datatype_new 'a tt = TT nat 'a "'a tt l fset"

abbreviation "tt_encode \<equiv> rec_tt (\<lambda>n x ts. prod_encode (n, prod_encode (to_nat x, to_nat (fimage (map_l snd) ts))))"

lemma tt_encode_injective: "\<forall>u. tt_encode t = tt_encode u \<longrightarrow> t = u"
  apply (rule tt.induct)
  apply (tactic {* nchotomy_tac @{thm tt.nchotomy} *})
  apply (tactic {* same_ctr_tac @{context} @{thms tt.inject} @{thms tt.rec}
    @{thms fset.map_comp[unfolded o_def] l.map_comp[unfolded o_def]}
    @{thms fset.inj_map_strong[rotated -1] l.inj_map_strong[rotated -1]} *})
  done

lemma tt_ex_injective: "\<exists>to_nat :: ('a :: countable) tt \<Rightarrow> nat. inj to_nat"
  apply (tactic {* endgame_tac @{context} 1 @{thm tt_encode_injective} *})
  done

instance tt :: (countable) countable
  apply (tactic {* endgame_tac @{context} 1 @{thm tt_encode_injective} *})
  done


section {* Weird Three-Level Rose Trees *}

datatype_new 'a ttt = TT nat 'a "('a ttt + 'a ttt l) fset"

abbreviation "ttt_encode \<equiv> rec_ttt (\<lambda>n x ts.
  prod_encode (n, prod_encode (to_nat x, to_nat (fimage (map_sum snd (map_l snd)) ts))))"

lemma ttt_encode_injective: "\<forall>u. ttt_encode t = ttt_encode u \<longrightarrow> t = u"
  apply (rule ttt.induct)
  apply (tactic {* nchotomy_tac @{thm ttt.nchotomy} *})
  apply (tactic {* same_ctr_tac @{context} @{thms ttt.inject} @{thms ttt.rec}
    @{thms fset.map_comp[unfolded o_def] l.map_comp[unfolded o_def] sum.map_comp[unfolded o_def]}
    @{thms fset.inj_map_strong[rotated -1] sum.inj_map_strong[rotated -1] l.inj_map_strong[rotated -1]} *})
  done

lemma ttt_ex_injective: "\<exists>to_nat :: ('a :: countable) ttt \<Rightarrow> nat. inj to_nat"
  apply (tactic {* endgame_tac @{context} 1 @{thm ttt_encode_injective} *})
  done

instance ttt :: (countable) countable
  apply (tactic {* endgame_tac @{context} 1 @{thm ttt_encode_injective} *})
  done

end
