theory Wrong_Induction_Principle
imports "~~/src/HOL/BNF/BNF"
begin

ML {* open Datatype_Aux *}


text {* Nested Recursion *}

declare [[bnf_note_all]]

datatype_new 'a list0 = Nil0 | Cons0 'a "'a list0"

datatype_new 'a tre0 = Nod0 'a "'a tre0 list"

datatype 'a tre = Nod 'a "'a tre list"

thm tre0.induct

lemma tre0_induct_mult1:
  assumes "\<And>a list. P2 list \<Longrightarrow> P1 (Nod0 a list)" "P2 []" "\<And>tre list. \<lbrakk>P1 tre; P2 list\<rbrakk> \<Longrightarrow> P2 (tre # list)"
  shows "P1 tre"
proof (induct rule: tre0.induct)
  case (Nod0 a chs)
  thus "P1 (Nod0 a chs)"
  proof (intro assms(1), induct chs)
    case Nil show "P2 []" by (rule assms(2))
  next
    case (Cons t ts)
    hence "P1 t" by simp
    with Cons show "P2 (t # ts)" by (intro assms(3)) simp_all
  qed
qed

lemma tre0_induct_mult2:
"\<lbrakk>\<And>a list. P2 list \<Longrightarrow> P1 (Nod0 a list); P2 []; \<And>tre list. \<lbrakk>P1 tre; P2 list\<rbrakk> \<Longrightarrow> P2 (tre # list)\<rbrakk> \<Longrightarrow>
 P2 list"
proof (induct list)
  case Nil thus ?case by -
next
  case (Cons t ts)
  show ?case
  apply (rule Cons(4))
   apply (rule tre0_induct_mult1)
     apply (erule Cons(2))
    apply (rule Cons(3))
   apply (erule Cons(4), assumption)
  apply (rule Cons(1))
    apply (erule Cons(2))
   apply (rule Cons(3))
  apply (erule Cons(4), assumption)
  done
qed

lemma tre0_induct_mult:
 "\<lbrakk>\<And>a list. P2 list \<Longrightarrow> P1 (Nod0 a list); P2 []; \<And>tre list. \<lbrakk>P1 tre; P2 list\<rbrakk> \<Longrightarrow> P2 (tre # list)\<rbrakk> \<Longrightarrow>
  P1 tre \<and> P2 list"
  by (metis tre0_induct_mult1 tre0_induct_mult2)


axiomatization set_tre :: "'a tre0 \<Rightarrow> 'a set"
and set_tres :: "'a tre0 list \<Rightarrow> 'a set" where
ax1[simp]: "set_tre (Nod0 a ts) = insert a (set_tres ts)" and
ax2[simp]: "set_tres [] = {}" and
ax3[simp]: "set_tres (t # ts) = set_tre t \<union> set_tres ts"

lemma "set_tre (tre0_map f t) = image f (set_tre t) \<and>
       set_tres (map (tre0_map f) ts) = image f (set_tres ts)"
proof (induct rule: tre0_induct_mult)
qed auto

lemma "set_tre (tre0_map f t) = image f (set_tre t)"
proof (induct t)
  case (Nod0 a us)
  hence "set_tres (map (tre0_map f) us) = image f (set_tres us)" by (induct us) auto
  thus ?case by simp
qed

end

