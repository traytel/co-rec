theory Map_o_Corec
imports Main
begin

(* TODO: rename to "map_o_dtor_corec" (and same with "dtor_unfold_o_map") *)

lemma id_bnf_id_bnf_apply: "BNF_Composition.id_bnf (BNF_Composition.id_bnf x) = x"
  sorry

declare [[bnf_note_all]]

datatype 'a l = N | C 'a "'a l"

thm
  l.ctor_rec_o_map
  l.rec_o_map

codatatype 'a ll = LN | LC 'a "'a ll"

thm
  ll.dtor_corec_o_map

term corec_ll
thm ll.corec[no_vars]

lemma "map_ll f \<circ> corec_ll p g21 q22 g221 g222 = corec_ll p (f \<circ> g21) q22 (map_ll f \<circ> g221) g222"
  unfolding corec_ll_def ll.dtor_corec_o_map map_pre_ll_def
  unfolding id_bnf_id_bnf_apply if_distrib[of BNF_Composition.id_bnf]
    if_distrib[of "map_sum f g" for f g] o_def id_def map_sum.simps map_prod_simp
  apply (rule refl)
  done

codatatype 'a llb = LNb | LCb 'a "'a llb" nat

thm llb.dtor_corec_o_map

term corec_llb
thm llb.corec[no_vars]

lemma Rep_Abs_llb_pre_llb: "Rep_llb_pre_llb (Abs_llb_pre_llb x) = x"
  sorry

lemma "map_llb (f::'b\<Rightarrow>'c) \<circ> corec_llb p g21 q22 g221 g222 g23 =
    corec_llb p (f \<circ> g21) q22 (map_llb f \<circ> g221) g222 g23"
  unfolding corec_llb_def llb.dtor_corec_o_map map_pre_llb_def
  unfolding Rep_Abs_llb_pre_llb if_distrib[of Rep_llb_pre_llb] if_distrib[of Abs_llb_pre_llb]
    if_distrib[of "map_sum f g" for f g] o_def id_def map_sum.simps map_prod_simp
  apply (rule refl)
  done

codatatype
  'a x = X "'a y" 'a nat 'a and
  'a y = Y "'a x"

thm
  x.dtor_corec_o_map
  y.dtor_corec_o_map

term corec_x
term corec_y
thm
  x.corec[no_vars]
  y.corec[no_vars]

lemma Rep_Abs_x_pre_x: "Rep_x_pre_x (Abs_x_pre_x x) = x"
  sorry

lemma "map_x f \<circ> corec_x q11 g111 g112 g12 g13 g14 q2 g21 g22 =
    corec_x q11 (map_y f \<circ> g111) g112 (f \<circ> g12) g13 (f \<circ> g14) q2 (map_x f \<circ> g21) g22"
  unfolding corec_x_def x.dtor_corec_o_map map_pre_x_def map_pre_y_def
  unfolding Rep_Abs_x_pre_x if_distrib[of Rep_x_pre_x] if_distrib[of Abs_x_pre_x]
    id_bnf_id_bnf_apply if_distrib[of BNF_Composition.id_bnf]
    if_distrib[of "map_sum f g" for f g] o_def id_def map_sum.simps map_prod_simp
  apply (rule refl)
  done

lemma "map_y f \<circ> corec_y q11 g111 g112 g12 g13 g14 q2 g21 g22 =
    corec_y q11 (map_y f \<circ> g111) g112 (f \<circ> g12) g13 (f \<circ> g14) q2 (map_x f \<circ> g21) g22"
  unfolding corec_y_def y.dtor_corec_o_map map_pre_x_def map_pre_y_def
  unfolding Rep_Abs_x_pre_x if_distrib[of Rep_x_pre_x] if_distrib[of Abs_x_pre_x]
    id_bnf_id_bnf_apply if_distrib[of BNF_Composition.id_bnf]
    if_distrib[of "map_sum f g" for f g] o_def id_def map_sum.simps map_prod_simp
  apply (rule refl)
  done

(*
thm list.rec_o_map
thm list.size_o_map
thm size_list_def
*)

end
