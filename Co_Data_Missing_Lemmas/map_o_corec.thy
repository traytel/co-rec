theory map_o_corec
imports "~~/src/HOL/BNF_Greatest_Fixpoint"
begin

context lifting_syntax (* enables the ===> syntax for rel_fun *)
begin

declare [[bnf_note_all]]

datatype ('a, 'b) pair = Pair 'a 'b

(* live direct *)
codatatype 'a ld1 = LD1 'a
codatatype 'a ld2 = LD2 'a 'a
codatatype 'a ld3 = LD3 'a 'a 'a

(* multi live direct *)
codatatype ('a, 'b) mld1 = MLD1 'a 'b

(* live indirect *)
codatatype 'a li1 = LI1 "('a, nat) pair"
codatatype 'a li2 = LI2 "('a, 'a) pair"
codatatype 'a li3 = LI3 "('a, ('a, 'a) pair) pair"

(* multi live indirect *)
codatatype ('a, 'b) mli1 = LI1 "('a, 'b) pair"

(* nested corecursion *)
codatatype 'a nc1 = NC1 "(nat, 'a nc1) pair"
codatatype 'a nc2 = NC2 "('a nc2, 'a nc2) pair"

(* multi nested corecursion *)
codatatype ('a, 'b) mnc1 = MNC1 "(('a, 'b) mnc1, ('a, 'b) mnc1) pair"

(* mutually corecursion direct *)
codatatype 'a mcd1 = MCD1 "'a mcd1"
codatatype 'a mcd2a = MCD1 "'a mcd2b"
       and 'a mcd2b = MCD2 "'a mcd2a"

(* mutually corecursion indirect *)
codatatype 'a mci1a = MCI1 "(nat, 'a mci1b) pair"
       and 'a mci1b = MCI2 "(nat, 'a mci1a) pair"

(* multi constructors *)
codatatype 'a mc1 = MC1a 'a
codatatype 'a mc2 = MC2a 'a | MC2b
codatatype 'a mc3 = MC3a 'a | MC23 | MC3c
codatatype 'a mc4 = MC3a 'a | MC23 | MC3c | MC3d
codatatype 'a mc5 = MC3a 'a | MC23 | MC3c | MC3d | MC3e

(* multi empty-constructors*)
codatatype 'a mec1 = MC1a
codatatype 'a mec2 = MC2a | MC2b
codatatype 'a mec3 = MC3a | MC23 | MC3c
codatatype 'a mec4 = MC3a | MC23 | MC3c | MC3d
codatatype 'a mec5 = MC3a | MC23 | MC3c | MC3d | MC3e

lemma map_sum_if_distrib_then:
  "\<And>f g e x y. map_sum f g (if e then Inl x else y) = (if e then Inl (f x) else map_sum f g y)"
  "\<And>f g e x y. map_sum f g (if e then Inr x else y) = (if e then Inr (g x) else map_sum f g y)"
  by simp_all

lemma map_sum_if_distrib_else:
  "\<And>f g e x y. map_sum f g (if e then x else Inl y) = (if e then map_sum f g x else Inl (f y))"
  "\<And>f g e x y. map_sum f g (if e then x else Inr y) = (if e then map_sum f g x else Inr (g y))"
  by simp_all

lemma map_sum_if_distrib_both:
  "\<And>f g e x y. map_sum f g (if e then Inl x else Inr y) = (if e then Inl (f x) else Inr (g y))"
  "\<And>f g e x y. map_sum f g (if e then Inr x else Inl y) = (if e then Inr (g x) else Inl (f y))"
by simp_all

ML \<open>
open Ctr_Sugar_Util
open BNF_Tactics

fun mk_co_rec_o_map_tac ctxt co_rec_def pre_map_defs map_ident0s abs_inverses rep_inverses
    xtor_co_rec_o_map =
  let
    val rec_o_map_simps = @{thms o_def[abs_def] id_def case_prod_app case_sum_map_sum
      case_prod_map_prod id_bnf_def map_prod_simp if_distrib[THEN sym]
      map_sum_if_distrib_then map_sum_if_distrib_else map_sum.simps};
  in
    HEADGOAL (subst_tac @{context} (SOME [1, 2]) [co_rec_def]) THEN
    HEADGOAL (rtac (xtor_co_rec_o_map RS trans)) THEN
    HEADGOAL (CONVERSION Thm.eta_long_conversion) THEN
    HEADGOAL (asm_simp_tac (ss_only (pre_map_defs @ distinct Thm.eq_thm_prop
      (map_ident0s @ abs_inverses @ rep_inverses) @ rec_o_map_simps) ctxt))
  end;
\<close>

thm ld1.map_o_corec
lemma "map_ld1 f o corec_ld1 g = corec_ld1 (f o g)"
  (* unfolding corec_ld1_def *)
  (* apply (rule trans[OF ld1.dtor_corec_o_map])
  (* apply (tactic {* CONVERSION Thm.eta_long_conversion 1 *}) *)
  apply (simp only: map_pre_ld1_def ld1.map_id0[unfolded id_def] (* ld1.abs_inverse *)
    o_def[abs_def] id_def case_prod_app case_sum_map_sum case_prod_map_prod id_bnf_def) *)
  apply (tactic \<open>mk_co_rec_o_map_tac @{context}
    @{thm corec_ld1_def}
    @{thms map_pre_ld1_def}
    @{thms ld1.map_id0[unfolded id_def]}
    @{thms }
    @{thms }
    @{thm ld1.dtor_corec_o_map}\<close>)
done

thm ld2.map_o_corec
lemma "map_ld2 f o corec_ld2 g1 g2 = corec_ld2 (f o g1) (f o g2)"
  apply (tactic \<open>mk_co_rec_o_map_tac @{context}
    @{thm corec_ld2_def}
    @{thms map_pre_ld2_def}
    @{thms ld2.map_id0[unfolded id_def]}
    @{thms }
    @{thms }
    @{thm ld2.dtor_corec_o_map}\<close>)
done

thm ld3.map_o_corec
lemma "map_ld3 f o corec_ld3 g1 g2 g3 = corec_ld3 (f o g1) (f o g2) (f o g3)"
  apply (tactic \<open>mk_co_rec_o_map_tac @{context}
    @{thm corec_ld3_def}
    @{thms map_pre_ld3_def}
    @{thms ld3.map_id0[unfolded id_def]}
    @{thms }
    @{thms }
    @{thm ld3.dtor_corec_o_map}\<close>)
done

thm mld1.map_o_corec
lemma "map_mld1 f1 f2 o corec_mld1 g1 g2 = corec_mld1 (\<lambda>x. f1 (g1 x)) (\<lambda>x. f2 (g2 x))"
  apply (tactic \<open>mk_co_rec_o_map_tac @{context}
    @{thm corec_mld1_def}
    @{thms map_pre_mld1_def}
    @{thms mld1.map_id0[unfolded id_def]}
    @{thms }
    @{thms }
    @{thm mld1.dtor_corec_o_map}\<close>)
done

thm li1.map_o_corec
lemma "map_li1 f o corec_li1 g = corec_li1 (map_pair f id o g)"
  apply (tactic \<open>mk_co_rec_o_map_tac @{context}
    @{thm corec_li1_def}
    @{thms map_pre_li1_def}
    @{thms li1.map_id0[unfolded id_def]}
    @{thms }
    @{thms }
    @{thm li1.dtor_corec_o_map}\<close>)
done

thm li2.map_o_corec
lemma "map_li2 f o corec_li2 g = corec_li2 (map_pair f f o g)"
  apply (tactic \<open>mk_co_rec_o_map_tac @{context}
    @{thm corec_li2_def}
    @{thms map_pre_li2_def}
    @{thms li2.map_id0[unfolded id_def]}
    @{thms }
    @{thms }
    @{thm li2.dtor_corec_o_map}\<close>)
done

thm li3.map_o_corec
lemma "map_li3 f o corec_li3 g = corec_li3 (map_pair f (map_pair f f) o g)"
  apply (tactic \<open>mk_co_rec_o_map_tac @{context}
    @{thm corec_li3_def}
    @{thms map_pre_li3_def}
    @{thms li3.map_id0[unfolded id_def]}
    @{thms }
    @{thms }
    @{thm li3.dtor_corec_o_map}\<close>)
done

thm mli1.map_o_corec
lemma "map_mli1 f1 f2 o corec_mli1 g = corec_mli1 (map_pair f1 f2 o g)"
  apply (tactic \<open>mk_co_rec_o_map_tac @{context}
    @{thm corec_mli1_def}
    @{thms map_pre_mli1_def}
    @{thms mli1.map_id0[unfolded id_def]}
    @{thms }
    @{thms }
    @{thm mli1.dtor_corec_o_map}\<close>)
done

thm nc1.map_o_corec
lemma "map_nc1 f o corec_nc1 g = corec_nc1 (map_pair id (map_sum (map_nc1 f) id) o g)"
  apply (tactic \<open>mk_co_rec_o_map_tac @{context}
    @{thm corec_nc1_def}
    @{thms map_pre_nc1_def}
    @{thms nc1.map_id0[unfolded id_def]}
    @{thms Abs_nc1_pre_nc1_inverse[OF UNIV_I]}
    @{thms Rep_nc1_pre_nc1_inverse}
    @{thm nc1.dtor_corec_o_map}\<close>)
done

thm Rep_nc1_pre_nc1_inverse Abs_nc1_pre_nc1_inverse[OF UNIV_I]

thm nc2.map_o_corec
lemma "map_nc2 f o corec_nc2 g = corec_nc2 (map_pair (map_sum (map_nc2 f) id) (map_sum (map_nc2 f) id) o g)"
  apply (tactic \<open>mk_co_rec_o_map_tac @{context}
    @{thm corec_nc2_def}
    @{thms map_pre_nc2_def}
    @{thms nc2.map_id0[unfolded id_def]}
    @{thms Abs_nc2_pre_nc2_inverse[OF UNIV_I]}
    @{thms Rep_nc2_pre_nc2_inverse}
    @{thm nc2.dtor_corec_o_map}\<close>)
done

thm mnc1.map_o_corec
lemma "map_mnc1 f1 f2 o corec_mnc1 g = corec_mnc1 (map_pair (map_sum (map_mnc1 f1 f2) id) (map_sum (map_mnc1 f1 f2) id) o g)"
  apply (tactic \<open>mk_co_rec_o_map_tac @{context}
    @{thm corec_mnc1_def}
    @{thms map_pre_mnc1_def}
    @{thms mnc1.map_id0[unfolded id_def]}
    @{thms Abs_mnc1_pre_mnc1_inverse[OF UNIV_I]}
    @{thms Rep_mnc1_pre_mnc1_inverse}
    @{thm mnc1.dtor_corec_o_map}\<close>)
done

thm mcd1.map_o_corec
lemma "map_mcd1 f o corec_mcd1 q g h = corec_mcd1 q (map_mcd1 f o g) h"
  apply (tactic \<open>mk_co_rec_o_map_tac @{context}
    @{thm corec_mcd1_def}
    @{thms map_pre_mcd1_def}
    @{thms mcd1.map_id0[unfolded id_def]}
    @{thms Abs_mcd1_pre_mcd1_inverse[OF UNIV_I]}
    @{thms Rep_mcd1_pre_mcd1_inverse}
    @{thm mcd1.dtor_corec_o_map}\<close>)
done

thm mcd2a.map_o_corec
lemma "map_mcd2a f o corec_mcd2a q1 g1 h1 q2 g2 h2 = corec_mcd2a q1 (map_mcd2b f o g1) h1 q2 (map_mcd2a f o g2) h2"
  apply (tactic \<open>mk_co_rec_o_map_tac @{context}
    @{thm corec_mcd2a_def}
    @{thms map_pre_mcd2a_def map_pre_mcd2b_def}
    @{thms mcd2a.map_id0[unfolded id_def]}
    @{thms Abs_mcd2a_pre_mcd2a_inverse[OF UNIV_I]}
    @{thms Rep_mcd2a_pre_mcd2a_inverse}
    @{thm mcd2a.dtor_corec_o_map}\<close>)
done

thm mci1a.map_o_corec
lemma "map_mci1a f o corec_mci1a g1 g2 =
  corec_mci1a (map_pair id (map_sum (map_mci1b f) id) o g1) (map_pair id (map_sum (map_mci1a f) id) o g2)"
  apply (tactic \<open>mk_co_rec_o_map_tac @{context}
    @{thm corec_mci1a_def}
    @{thms map_pre_mci1a_def map_pre_mci1b_def}
    @{thms mci1a.map_id0[unfolded id_def]}
    @{thms Abs_mci1a_pre_mci1a_inverse[OF UNIV_I]}
    @{thms Rep_mci1a_pre_mci1a_inverse}
    @{thm mci1a.dtor_corec_o_map}\<close>)
done

thm mci1b.map_o_corec
lemma "map_mci1b f o corec_mci1b g1 g2 =
  corec_mci1b (map_pair id (map_sum (map_mci1b f) id) o g1) (map_pair id (map_sum (map_mci1a f) id) o g2)"
  apply (tactic \<open>mk_co_rec_o_map_tac @{context}
    @{thm corec_mci1b_def}
    @{thms map_pre_mci1a_def map_pre_mci1b_def}
    @{thms mci1b.map_id0[unfolded id_def]}
    @{thms Abs_mci1a_pre_mci1a_inverse[OF UNIV_I]}
    @{thms Rep_mci1a_pre_mci1a_inverse}
    @{thm mci1b.dtor_corec_o_map}\<close>)
done

thm mc1.map_o_corec
lemma "map_mc1 f o corec_mc1 g = corec_mc1 (f o g)"
  apply (tactic \<open>mk_co_rec_o_map_tac @{context}
    @{thm corec_mc1_def}
    @{thms map_pre_mc1_def}
    @{thms mc1.map_id0[unfolded id_def]}
    @{thms }
    @{thms }
    @{thm mc1.dtor_corec_o_map}\<close>)
done

thm mc2.map_o_corec
term corec_mc2

(* lemma Abs_mc2_pre_mc2_o_Rep_mc2_pre_mc2: "Abs_mc2_pre_mc2 \<circ> Rep_mc2_pre_mc2 = id"
  by (simp add: o_def Rep_mc2_pre_mc2_inverse id_def) *)

lemma "map_mc2 f o corec_mc2 p g = corec_mc2 p (f o g)"
term corec_mc2
  apply (tactic \<open>mk_co_rec_o_map_tac @{context}
    @{thm corec_mc2_def}
    @{thms map_pre_mc2_def}
    @{thms mc2.map_id0[unfolded id_def]}
    @{thms }
    @{thms }
    @{thm mc2.dtor_corec_o_map}\<close>)
  (* apply (tactic \<open>HEADGOAL (subst_tac @{context} (SOME [1, 2]) [@{thm corec_mc2_def}])\<close>)
  apply (tactic \<open>HEADGOAL (rtac (@{thm mc2.dtor_corec_o_map} RS trans))\<close>)
  apply (tactic \<open>HEADGOAL (CONVERSION Thm.eta_long_conversion)\<close>)
  apply (tactic \<open>HEADGOAL (asm_simp_tac (ss_only (@{thms map_pre_mc2_def} @
    distinct Thm.eq_thm_prop (@{thms mc2.map_id0[unfolded id_def]} @
      @{thms Abs_mc2_pre_mc2_inverse[OF UNIV_I]}) @
    @{thms o_def[abs_def] id_def case_prod_app case_sum_map_sum
      case_prod_map_prod id_bnf_def map_prod_simp map_sum_if}) @{context}))\<close>) *)

done

thm mc3.map_o_corec
lemma "map_mc3 f o corec_mc3 p1 g p2 = corec_mc3 p1 (f o g) p2"
  apply (tactic \<open>mk_co_rec_o_map_tac @{context}
    @{thm corec_mc3_def}
    @{thms map_pre_mc3_def}
    @{thms mc3.map_id0[unfolded id_def]}
    @{thms }
    @{thms }
    @{thm mc3.dtor_corec_o_map}\<close>)
done

thm mc3.map_o_corec
term corec_mc4
lemma "map_mc4 f o corec_mc4 p1 g p2 p3 = corec_mc4 p1 (f o g) p2 p3"
  apply (tactic \<open>mk_co_rec_o_map_tac @{context}
    @{thm corec_mc4_def}
    @{thms map_pre_mc4_def}
    @{thms mc4.map_id0[unfolded id_def]}
    @{thms Abs_mc4_pre_mc4_inverse[OF UNIV_I]}
    @{thms }
    @{thm mc4.dtor_corec_o_map}\<close>)
done

end

end
