theory Rec_Massage_Map
imports Complex_Main "~~/src/HOL/BNF/BNF"
begin

datatype_new 'a tree = Node "'a tree list"

consts size :: "'a tree \<Rightarrow> nat"

ML {* open BNF_Util BNF_Def BNF_FP_Def_Sugar BNF_FP_Util BNF_FP_Rec_Sugar_Util *}

ML {*
val fnames = ["f", "fa", "fb"];
fun get_kks t =
  map_index (fn (i, s) =>
    if exists_subterm (fn Free (s', _) => s' = s | _ => false) t then SOME i else NONE) fnames
  |> map_filter I;
fun massage check y y' t =
  massage_indirect_rec_call
    @{context}
    (not o null o get_kks)
    (fn T1 => fn U1 => fn t =>
       let val T2 = range_type (fastype_of t) in
         Const (@{const_name undefined}, (T1 --> T2) --> HOLogic.mk_prodT (T1, U1) --> T2) $ t
       end)
    [] y y' t
  |> tap (Syntax.check_term @{context})
  |> tap (Output.urgent_message o Syntax.string_of_term @{context})
  handle ERROR msg => if check then error msg else (warning ("Error: " ^ msg); Term.dummy)
       | Fail msg => if check then raise Fail msg else (warning ("Fail: " ^ msg); Term.dummy)
*}

lemma
  fixes fa :: "'a tree \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> real"
  fixes fb :: "nat \<Rightarrow> 'a tree \<Rightarrow> int \<Rightarrow> real"
  fixes ts :: "'a tree list"
  fixes ps :: "('a tree \<times> (nat \<Rightarrow> int \<Rightarrow> real)) list"
  fixes t :: "'a tree"
  fixes p :: "'a tree \<times> (nat \<Rightarrow> int \<Rightarrow> real)"
  shows True
proof -

txt {* 1Aa *}

ML_val {* massage true @{term ts} @{term ps} @{term "map fa ts"} *}
ML_val {* massage true @{term ts} @{term ps} @{term "map (\<lambda>t. fa t) ts"} *}
ML_val {* massage true @{term ts} @{term ps} @{term "map (\<lambda>t n. fa t n) ts"} *}
ML_val {* massage true @{term ts} @{term ps} @{term "map (\<lambda>t n i. fa t n i) ts"} *}
ML_val {* massage false @{term ts} @{term ps} @{term "mymap fa ts"} *}
ML_val {* massage false @{term ts} @{term ps} @{term "fa (hd ts)"} *}

txt {* 1Ab *}

ML_val {* massage true @{term ts} @{term ps} @{term "map (\<lambda>n t. fb t n) ts"} *}
ML_val {* massage true @{term ts} @{term ps} @{term "map (\<lambda>n t i. fb t n i) ts"} *}

txt {* 1Ba *}

ML_val {* massage true @{term ts} @{term ps} @{term "map (\<lambda>t. (t, fa t)) ts"} *}
ML_val {* massage true @{term ts} @{term ps} @{term "map (\<lambda>t. (t, \<lambda>n. fa t n)) ts"} *}
ML_val {* massage true @{term ts} @{term ps} @{term "map (\<lambda>t. (t, \<lambda>n i. fa t n i)) ts"} *}

txt {* 1Bb *}

ML_val {* massage true @{term ts} @{term ps} @{term "map (\<lambda>t. (t, \<lambda>n. fb n t)) ts"} *}

txt {* 1C *}

ML_val {* massage true @{term ts} @{term ps} @{term ts} *}
ML_val {* massage true @{term ts} @{term ps} @{term "map (\<lambda>t. t) ts"} *}

txt {* 2Aa *}

ML_val {* massage true @{term ts} @{term ps} @{term "map ((\<lambda>g. g (Suc 0) (-1)) o fa) ts"} *}
ML_val {* massage true @{term ts} @{term ps} @{term "map (h o (\<lambda>t. fa t)) ts"} *}
ML_val {* massage true @{term ts} @{term ps} @{term "map (k o h o (\<lambda>t. fa t)) ts"} *}

txt {* 2Ab *}

ML_val {* massage true @{term ts} @{term ps}
  @{term "map ((\<lambda>g. g (Suc 0) (-1)) o (\<lambda>t n. fb n t)) ts"} *}
ML_val {* massage true @{term ts} @{term ps} @{term "map (h o (\<lambda>t n. fb n t)) ts"} *}
ML_val {* massage true @{term ts} @{term ps} @{term "map (k o h o (\<lambda>t n. fb n t)) ts"} *}

txt {* 2Ba *}

ML_val {* massage true @{term ts} @{term ps}
  @{term "map ((\<lambda>(t, g). g (size t) (-1)) o (\<lambda>t. (t, fa t))) ts"} *}
ML_val {* massage true @{term ts} @{term ps} @{term "map (h o (\<lambda>t. (t, fa t))) ts"} *}
ML_val {* massage true @{term ts} @{term ps} @{term "map (k o h o (\<lambda>t. (t, fa t))) ts"} *}

txt {* 2Bb *}

ML_val {* massage true @{term ts} @{term ps}
  @{term "map ((\<lambda>(t, g). g (size t) (-1)) o (\<lambda>t. (t, \<lambda>n. fb n t))) ts"} *}

txt {* 2C *}

ML_val {* massage true @{term ts} @{term ps} @{term "map (size o (\<lambda>t. t)) ts"} *}

txt {* 3Aa *}

ML_val {* massage true @{term ts} @{term ps} @{term "map (\<lambda>t. fa t (Suc 0) (-1)) ts"} *}
ML_val {* massage true @{term ts} @{term ps} @{term "map (\<lambda>t. fa t (Suc 0)) ts"} *}
ML_val {* massage true @{term ts} @{term ps}
  @{term "map (\<lambda>t. h (fa t (Suc 0) (-1) + fa t 0 (-2))) ts"} *}

txt {* 3Ab *}

ML_val {* massage true @{term ts} @{term ps} @{term "map (\<lambda>t. fb (Suc 0) t (-1)) ts"} *}
ML_val {* massage true @{term ts} @{term ps} @{term "map (\<lambda>t. fb (Suc 0) t) ts"} *}
ML_val {* massage true @{term ts} @{term ps}
  @{term "map (\<lambda>t. h (fb (Suc 0) t (-1) + fb 0 t (-2))) ts"} *}

txt {* 3Ba *}

ML_val {* massage true @{term ts} @{term ps} @{term "map (\<lambda>t. fa t (size t) (-1)) ts"} *}
ML_val {* massage true @{term ts} @{term ps} @{term "map (\<lambda>t. fa t (size t)) ts"} *}
ML_val {* massage true @{term ts} @{term ps}
  @{term "map (\<lambda>t. h t (fa t (Suc 0) (-1) + fa t 0 (-2))) ts"} *}

txt {* 3Bb *}

ML_val {* massage true @{term ts} @{term ps} @{term "map (\<lambda>t. fb (size t) t (-1)) ts"} *}
ML_val {* massage true @{term ts} @{term ps} @{term "map (\<lambda>t. fb (size t) t) ts"} *}

txt {* 3C *}

ML_val {* massage true @{term ts} @{term ps} @{term "map size ts"} *}
ML_val {* massage true @{term ts} @{term ps} @{term "map id ts"} *}

txt {* Misc *}

ML_val {* massage false @{term ts} @{term ps} @{term "map fa (id ts)"} *}
ML_val {* massage false @{term ts} @{term ps} @{term "map fa (map id ts)"} *}
ML_val {* massage false @{term ts} @{term ps}
  @{term "map (\<lambda>(t, g) \<Rightarrow> (fa t, g)) (map (\<lambda>t. (t, fa t)) ts)"} *}

oops

end
