theory Old_Work_In_Progress
imports Rec_Suite
begin

axiomatization g_hfset :: "hfset \<Rightarrow> nat" and
               g_hfset_mylist :: "hfset mylist \<Rightarrow> nat" where
    g_hfset_simps1: "g_hfset (HFset hs) = g_hfset_mylist hs"
and g_hfset_mylist_simps1: "g_hfset_mylist MyNil = 0"
and g_hfset_mylist_simps2: "g_hfset_mylist (MyCons h hs) = 1 + g_hfset_mylist hs"

datatype_new hfset' = HFset' hfset_mylist'
 and hfset_mylist' = MyNil' | MyCons' hfset' hfset_mylist'

definition hfset2_ctor_rec ::
    "(hfset mylist \<times> 'b \<Rightarrow> 'a) \<Rightarrow> (unit + (hfset \<times> 'a) \<times> hfset mylist \<times> 'b \<Rightarrow> 'b) \<Rightarrow> hfset \<Rightarrow> 'a"
  where
    "hfset2_ctor_rec t1 t2 =
      hfset_ctor_rec (t1 o id
        <mylist_map fst, mylist_ctor_rec (t2 o pre_mylist_map id (map_pair (mylist_map fst) id))>)"

definition mylist2_ctor_rec ::
    "(hfset mylist \<times> 'b \<Rightarrow> 'a) \<Rightarrow> (unit + (hfset \<times> 'a) \<times> hfset mylist \<times> 'b \<Rightarrow> 'b) \<Rightarrow> hfset mylist \<Rightarrow> 'b"
  where
  "mylist2_ctor_rec t1 t2 =
     mylist_ctor_rec (t2 o pre_mylist_map (<id, hfset2_ctor_rec t1 t2>) id)"

lemma convol_apply_o: "<f, g> (h x) = <f o h, g o h> x"
  unfolding convol_def by auto

lemma hfset2_ctor_rec: "hfset2_ctor_rec f g (hfset_ctor x) = f (id (<id , mylist2_ctor_rec f g>) x)"
  unfolding hfset2_ctor_rec_def mylist2_ctor_rec_def hfset.ctor_rec
    o_apply convol_o id_o fst_convol convol_apply_o id_apply
    mylist.map_comp0[symmetric] mylist.map_id
  apply (rule arg_cong[of _ _ f])
(*
  apply (rule G0.map_cong) (*arg_cong*)
  apply (rule refl)
*)
  apply (rule fun_cong[OF arg_cong2[of _ _ _ _ BNF_Def.convol]])
  apply (rule refl)
  apply (rule mylist.ctor_rec_unique)
  apply (rule ext)
  unfolding pre_mylist.map_comp
    id_apply fst_conv convol_def map_pair_def o_def prod.cases
    mylist.ctor_map mylist.ctor_rec mylist.map_comp mylist.map_id[unfolded id_def]
  ..

lemma mylist2_ctor_rec:
  "mylist2_ctor_rec f g (mylist_ctor x) = g (pre_mylist_map (<id , hfset2_ctor_rec f g>) (<id , mylist2_ctor_rec f g>) x)"
  unfolding hfset2_ctor_rec_def mylist2_ctor_rec_def mylist.ctor_rec pre_mylist.map_comp o_def id_apply
  ..

end
