theory set_intros
imports "~~/src/HOL/BNF_GFP"

begin

declare [[bnf_note_all]]

codatatype 'a llist
  = LNil
  | LCons (lhead: 'a) (ltail: "'a llist")
  where
    "ltail LNil = LNil"

codatatype 'a maybe
  = Nothing
  | Just (just: 'a)

codatatype 'a ltree
  = LNode (lval: 'a) (lchildren: "'a ltree llist")

codatatype ('a, 'b) example
  = N1 'a
  | N2 'b
  | N3 'a 'b
  | N4
  | ConsA 'a "('a, 'b) example"
  | ConsB 'b "('a, 'b) example"

codatatype 'a tree = Node 'a "'a forest"
       and 'a forest = FNil | FCons "'a tree" "'a forest"

codatatype 'a A = A1 'a | A2 'a
codatatype ('a, 'b) B = B1 'a | B2 'b
codatatype ('a, 'b) C = C1 "'a A" | C2 "('a, 'b) B"
codatatype 'a D = D "('a * 'a D * nat) A" "('a * nat) A"
codatatype ('a, 'b, 'c, 'd) E = E "'a * ('b, ('c, ('d * nat)) C A) B"

codatatype ('a, 'b) F = F "'a \<Rightarrow> 'b"

(*codatatype only*)
(* lemma ll_set_intros:
  "x \<in> set_llist (LCons x xs)"
  "x \<in> set_llist xs \<Longrightarrow> x \<in> set_llist (LCons x' xs)"
by simp_all *)

ML {*
open Ctr_Sugar_Tactics
open Ctr_Sugar_Util

fun mk_set_intros_tac ctxt sets =
  TRYALL Goal.conjunction_tac THEN
  unfold_thms_tac ctxt sets THEN
  TRYALL (REPEAT o
    (resolve_tac @{thms UnI1 UnI2} ORELSE' eresolve_tac @{thms UN_I UN_I[rotated]}) THEN'
    (rtac @{thm singletonI} ORELSE' atac));
*}

lemma llist_set_intros:
  "x \<in> set_llist (LCons x xs)"
  "x \<in> set_llist xs \<Longrightarrow> x \<in> set_llist (LCons x' xs)"
apply (tactic {* mk_set_intros_tac @{context} @{thms llist.simps(18,19)} *})
thm llist.set llist.simps(18,19)
find_theorems "set_llist (LCons _ _) = _"
(*
apply (unfold llist.set)
apply ((rule UnI1, rule singletonI | rule UnI2, assumption))+
*)
done

thm llist.sel_set llist_set_intros

lemma maybe_set_intros:
  "x \<in> set_maybe (Just x)"
apply (tactic {* mk_set_intros_tac @{context} @{thms maybe.set} *})
(*
apply (unfold maybe.set)
apply (((rule UnI1)? , rule singletonI | rule UnI2, assumption))+
*)
done

lemma ltree_set_intros:
  "x \<in> set_ltree (LNode x ys)"
  "x \<in> set_llist xs \<Longrightarrow> x' \<in> set_ltree x \<Longrightarrow> x' \<in> set_ltree (LNode y xs)"
apply (tactic {* mk_set_intros_tac @{context} @{thms ltree.set} *})
(*
apply (unfold ltree.set)
apply ((rule UnI1)? , rule singletonI | (rule UnI2, (rule bexI[THEN UN_iff[THEN iffD2]])?)+ , assumption+)+
*)

done

lemma example_set_intros:
  "\<And> arg. arg \<in> set1_example (N1 arg)"
  "\<And> arg. arg \<in> set2_example (N2 arg)"
  "\<And> arg1 arg2. arg1 \<in> set1_example (N3 arg1 arg2)"
  "\<And> arg1 arg2. arg2 \<in> set2_example (N3 arg1 arg2)"
  "\<And> arg1 arg2 x. arg1 \<in> set1_example (ConsA arg1 arg2)"
  "\<And> arg1 arg2 x. x \<in> set1_example arg2 \<Longrightarrow> x \<in> set1_example (ConsA arg1 arg2)"
  "\<And> arg1 arg2 x. x \<in> set2_example arg2 \<Longrightarrow> x \<in> set2_example (ConsA arg1 arg2)"
  "\<And> arg1 arg2 x. arg1 \<in> set2_example (ConsB arg1 arg2)"
  "\<And> arg1 arg2 x. x \<in> set1_example arg2 \<Longrightarrow> x \<in> set1_example (ConsB arg1 arg2)"
  "\<And> arg1 arg2 x. x \<in> set2_example arg2 \<Longrightarrow> x \<in> set2_example (ConsB arg1 arg2)"
apply (tactic {* mk_set_intros_tac @{context} @{thms example.set} *})
done

thm example.sel_set example_set_intros

lemma tree_set_intros:
  "\<And> arg1 arg2. arg1 \<in> set_tree (Node arg1 arg2)"
  "\<And> arg1 arg2 x. x \<in> set_forest arg2 \<Longrightarrow> x \<in> set_tree (Node arg1 arg2)"
apply (tactic {* mk_set_intros_tac @{context} @{thms tree.set} *})
done

lemma forest_set_intros:
  "\<And> arg1 arg2 x. x \<in> set_tree arg1 \<Longrightarrow> x \<in> set_forest (FCons arg1 arg2)"
  "\<And> arg1 arg2 x. x \<in> set_forest arg2 \<Longrightarrow> x \<in> set_forest (FCons arg1 arg2)"
apply (tactic {* mk_set_intros_tac @{context} @{thms forest.set} *})
done

lemma A_set_intros:
  "\<And> arg. arg \<in> set_A (A1 arg)"
  "\<And> arg. arg \<in> set_A (A2 arg)"
apply (tactic {* mk_set_intros_tac @{context} @{thms A.set} *})
done

lemma B_set_intros:
  "\<And> arg. arg \<in> set1_B (B1 arg)"
  "\<And> arg. arg \<in> set2_B (B2 arg)"
apply (tactic {* mk_set_intros_tac @{context} @{thms B.set} *})
done

lemma C_set_intros:
  "\<And> arg x. x \<in> set_A arg \<Longrightarrow> x \<in> set1_C (C1 arg)"
  "\<And> arg x. x \<in> set1_B arg \<Longrightarrow> x \<in> set1_C (C2 arg)"
  "\<And> arg x. x \<in> set2_B arg \<Longrightarrow> x \<in> set2_C (C2 arg)"
apply (tactic {* mk_set_intros_tac @{context} @{thms C.set} *})
done

thm C.sel_set C_set_intros

lemma D_set_intros:
  "\<And> arg1 arg2 x x'. x \<in> set_A arg1 \<Longrightarrow> x' \<in> Basic_BNFs.fsts x \<Longrightarrow> x' \<in> set_D (D arg1 arg2)"
  "\<And> arg1 arg2 x x' x'' x'''. x \<in> set_A arg1 \<Longrightarrow> x' \<in> Basic_BNFs.snds x \<Longrightarrow> x'' \<in> Basic_BNFs.fsts x' \<Longrightarrow> x''' \<in> set_D x'' \<Longrightarrow> x''' \<in> set_D (D arg1 arg2)"
  "\<And> arg1 arg2 x x'. x \<in> set_A arg2 \<Longrightarrow> x' \<in> Basic_BNFs.fsts x \<Longrightarrow> x' \<in> set_D (D arg1 arg2)"
apply (tactic {* mk_set_intros_tac @{context} @{thms D.set} *})
(*
apply (unfold D.set)
apply (tactic {* ((REPEAT o resolve_tac [UnI1,UnI2]) THEN' (rtac @{thm singletonI} ORELSE' REPEAT1 o ((REPEAT o rtac @{thm UN_I}) THEN' atac))) 1 *})
apply (tactic {* ((REPEAT o rtac UnI2)               THEN' (rtac @{thm singletonI} ORELSE' REPEAT1 o ((REPEAT o rtac @{thm UN_I}) THEN' atac))) 1 *})
apply (tactic {* ((REPEAT o resolve_tac [UnI1,UnI2]) THEN' (rtac @{thm singletonI} ORELSE' REPEAT1 o ((REPEAT o rtac @{thm UN_I}) THEN' atac))) 1 *})
*)
done

lemma E_set_intros:
  "\<And> arg x. x \<in> Basic_BNFs.fsts arg \<Longrightarrow> x \<in> set1_E (E arg)"
  "\<And> arg x x'. x \<in> Basic_BNFs.snds arg \<Longrightarrow> x' \<in> set1_B x \<Longrightarrow> x' \<in> set2_E (E arg)"
  "\<And> arg x x' x'' x'''. x \<in> Basic_BNFs.snds arg \<Longrightarrow> x' \<in> set2_B x \<Longrightarrow> x'' \<in> set_A x' \<Longrightarrow> x''' \<in> set1_C x'' \<Longrightarrow> x''' \<in> set3_E (E arg)"
  "\<And> arg x x' x'' x''' x''''. x \<in> Basic_BNFs.snds arg \<Longrightarrow> x' \<in> set2_B x \<Longrightarrow> x'' \<in> set_A x' \<Longrightarrow> x''' \<in> set2_C x'' \<Longrightarrow> x'''' \<in> Basic_BNFs.fsts x''' \<Longrightarrow> x'''' \<in> set4_E (E arg)"
apply (tactic {* mk_set_intros_tac @{context} @{thms E.set} *})
done

thm E.sel_set E_set_intros

lemma F_set_intros:
  "\<And> arg x. x \<in> range arg \<Longrightarrow> x \<in> set_F (F arg)"
apply (tactic {* mk_set_intros_tac @{context} @{thms F.set} *})
done

end
