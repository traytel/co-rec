theory inj_map_strong
imports "~~/src/HOL/BNF_LFP"
begin

declare [[bnf_note_all]]

datatype_new 'a list
  = Nil
  | Cons (head: 'a) (tail: "'a list")
  where
    "tail Nil = Nil"
thm list.rel_mono_strong list.rel_map

datatype_new 'a maybe
  = Nothing
  | Just (just: 'a)

datatype_new 'a tree
  = Node (val: 'a) (children: "'a tree list")

datatype_new ('a, 'b) example
  = N1 'a
  | N2 'b
  | N3 'a 'b
  | N4
  | ConsA 'a "('a, 'b) example"
  | ConsB 'b "('a, 'b) example"

datatype_new 'a tree_mutual = Node 'a "'a forest_mutual"
       and 'a forest_mutual = FNil | FCons "'a tree_mutual" "'a forest_mutual"

ML {*
open BNF_Util
open BNF_Tactics

(* fun mk_inj_map_strong_tac ctxt rel_eq rel_maps rel_mono_strong map_id =
  unfold_thms_tac ctxt (no_refl ((rel_eq RS sym) :: map_id :: rel_maps)) THEN
  HEADGOAL (etac rel_mono_strong) THEN
  TRYALL (Goal.assume_rule_tac ctxt) *)

fun mk_inj_map_strong_tac ctxt rel_eq rel_maps rel_mono_strong =
  let
    val rel_eq' = rel_eq RS @{thm predicate2_eqD};
    val rel_maps' = map (fn thm => thm RS iffD1) rel_maps
  in
    HEADGOAL (dtac (rel_eq' RS iffD2) THEN' rtac (rel_eq' RS iffD1)) THEN
    EVERY (map (HEADGOAL o dtac) rel_maps') THEN
    HEADGOAL (etac rel_mono_strong) THEN
    TRYALL (Goal.assume_rule_tac ctxt)
  end
*}

lemma "\<And>x xa f1 f2 f1a f2a.
  (\<And>z1 z1a. z1 \<in> maybe.set1_pre_maybe x \<Longrightarrow> z1a \<in> maybe.set1_pre_maybe xa \<Longrightarrow> f1 z1 = f1a z1a \<Longrightarrow> z1 = z1a) \<Longrightarrow>
  (\<And>z2 z2a. z2 \<in> maybe.set2_pre_maybe x \<Longrightarrow> z2a \<in> maybe.set2_pre_maybe xa \<Longrightarrow> f2 z2 = f2a z2a \<Longrightarrow> z2 = z2a) \<Longrightarrow>
  maybe.map_pre_maybe f1 f2 x = maybe.map_pre_maybe f1a f2a xa \<Longrightarrow> x = xa "
  apply (tactic {* mk_inj_map_strong_tac @{context} @{thm pre_maybe.rel_eq} @{thms pre_maybe.rel_map} @{thm pre_maybe.rel_mono_strong} *})
(*   apply (drule iffD2[OF predicate2_eqD[OF pre_maybe.rel_eq]])
  apply (rule iffD1[OF predicate2_eqD[OF pre_maybe.rel_eq]])
  apply (drule iffD1[OF pre_maybe.rel_map(1)])
  apply (drule iffD1[OF pre_maybe.rel_map(2)])
  apply (erule pre_maybe.rel_mono_strong)
  apply assumption+ *)
  done

thm list.inj_map_strong
thm list.rel_mono_strong list.rel_map
lemma list_inj_map_strong:
  "(\<And>x y. x \<in> set_list a \<Longrightarrow> y \<in> set_list b \<Longrightarrow> f x = f y \<Longrightarrow> x = y) \<Longrightarrow> map_list f a = map_list f b \<Longrightarrow> a = b"
  apply (tactic {* mk_inj_map_strong_tac @{context} @{thm list.rel_eq} @{thms list.rel_map} @{thm list.rel_mono_strong} *})
(*   unfolding list.rel_eq[symmetric]
  unfolding list.rel_map
  apply (erule list.rel_mono_strong)
  apply assumption *)
done

lemma maybe_inj_map_strong:
  "(\<And>x y. x \<in> set_maybe a \<Longrightarrow> y \<in> set_maybe b \<Longrightarrow> f x = f y \<Longrightarrow> x = y) \<Longrightarrow> map_maybe f a = map_maybe f b \<Longrightarrow> a = b"
  apply (tactic {* mk_inj_map_strong_tac @{context} @{thm maybe.rel_eq} @{thms maybe.rel_map} @{thm maybe.rel_mono_strong} *})
(*   unfolding maybe.rel_eq[symmetric] maybe.rel_map
  apply (erule maybe.rel_mono_strong)
  apply assumption *)
done

lemma tree_inj_map_strong:
  "(\<And>x y. x \<in> set_tree a \<Longrightarrow> y \<in> set_tree b \<Longrightarrow> f x = f y \<Longrightarrow> x = y) \<Longrightarrow> map_tree f a = map_tree f b \<Longrightarrow> a = b"
  apply (tactic {* mk_inj_map_strong_tac @{context} @{thm tree.rel_eq} @{thms tree.rel_map} @{thm tree.rel_mono_strong} *})
(*   unfolding tree.rel_eq[symmetric] tree.rel_map
  apply (erule tree.rel_mono_strong)
  apply assumption *)
done

lemma example_inj_map_strong:
  "(\<And>x y. x \<in> set1_example a \<Longrightarrow> y \<in> set1_example b \<Longrightarrow> f1 x = f1 y \<Longrightarrow> x = y) \<Longrightarrow>
   (\<And>x y. x \<in> set2_example a \<Longrightarrow> y \<in> set2_example b \<Longrightarrow> f2 x = f2 y \<Longrightarrow> x = y) \<Longrightarrow>
   map_example f1 f2 a = map_example f1 f2 b \<Longrightarrow> a = b"
  apply (tactic {* mk_inj_map_strong_tac @{context} @{thm example.rel_eq} @{thms example.rel_map} @{thm example.rel_mono_strong} *})
(*   unfolding example.rel_eq[symmetric] example.rel_map
  apply (erule example.rel_mono_strong)
  apply assumption + *)
done

lemma tree_mutual_inj_map_strong:
  "(\<And>x y. x \<in> set_tree_mutual a \<Longrightarrow> y \<in> set_tree_mutual b \<Longrightarrow> f x = f y \<Longrightarrow> x = y) \<Longrightarrow> map_tree_mutual f a = map_tree_mutual f b \<Longrightarrow> a = b"
  apply (tactic {* mk_inj_map_strong_tac @{context} @{thm tree_mutual.rel_eq} @{thms tree_mutual.rel_map} @{thm tree_mutual.rel_mono_strong} *})
done

lemma forest_mutual_inj_map_strong:
  "(\<And>x y. x \<in> set_forest_mutual a \<Longrightarrow> y \<in> set_forest_mutual b \<Longrightarrow> f x = f y \<Longrightarrow> x = y) \<Longrightarrow> map_forest_mutual f a = map_forest_mutual f b \<Longrightarrow> a = b"
  apply (tactic {* mk_inj_map_strong_tac @{context} @{thm forest_mutual.rel_eq} @{thms forest_mutual.rel_map} @{thm forest_mutual.rel_mono_strong} *})
done

end
