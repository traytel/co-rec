theory Corec_Suite_Base
imports "~~/src/HOL/BNF_GFP"
begin


declare [[bnf_note_all]]

consts some_cond :: bool
       some_value :: 'a

datatype_new xnat = XZero | XSuc xnat

datatype_new ('a, 'b) xprod (map: map_xpair) = XPair (xfst: 'a) (xsnd: 'b)

codatatype s1 = is_X1: X1

codatatype s2 = Y1 | Y2

codatatype s2' = Y1' unit | Y2' unit

codatatype s3 = A1 | A2 | A3

codatatype s5 = Z1 | Z2 nat nat | Z3 | Z4 unit | Z5

codatatype 'a stream (map: smap) = SCons (shd: 'a) (stl: "'a stream")

codatatype 'a llist (map: lmap) = lnull: LNil | LCons (lhd: 'a) (ltl: "'a llist")

codatatype ('b, 'c) some_passive = SP1 "('b, 'c) some_passive" | SP2 'b | SP3 'c

codatatype ('b, 'c) some_dead = SD1 'c | SD2 "'b \<Rightarrow> ('b, 'c) some_dead" | SD3 "('b, 'c) some_dead"

codatatype
  'a tree = TEmpty | TNode 'a "'a forest" and
  'a forest = FNil | FCons (fhd: "'a tree") (ftl: "'a forest")

codatatype x = is_X: X and y = is_Y: Y and z = is_Z: Z nat

codatatype hfset = HFset "hfset llist"

codatatype 'a btree = LNull | LNode (lb: 'a) (bsub: "('a btree, 'a btree) xprod")

codatatype 'a copt = CNone | CSome 'a

codatatype 'a ctree = CNode (clb: 'a) (cleft: "'a ctree copt") (cright: "'a ctree copt")

codatatype
  ('a, 'b) k = K1 | K2 'a 'b | K3 "('a, 'b) k" | K4 "('a, 'b) l list" and
  ('a, 'b) l = L1 "('a, 'b) k" | L2 "'a \<Rightarrow> 'a"


ML {* open Ctr_Sugar_Tactics BNF_Util BNF_Tactics BNF_FP_Util
  BNF_FP_Rec_Sugar_Util BNF_GFP_Rec_Sugar_Tactics *}

ML {*
fun compute_ms code_ctrs =
  map (Logic.count_prems o prop_of) code_ctrs
*}

end
