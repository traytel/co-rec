 theory Quotient_BNF
imports
  "~~/src/HOL/Library/BNF_Axiomatization"
  "~~/src/HOL/Cardinals/Cardinals"
begin

declare [[bnf_note_all]]

lemma part_equivp_reflp: "part_equivp R \<Longrightarrow> R x y \<Longrightarrow> R x x"
  by (metis part_equivp_def)

lemma part_equivp_reflp_Domainp: "part_equivp R \<Longrightarrow> Domainp R x \<Longrightarrow> R x x"
  by (metis part_equivp_def Domainp.simps)

lemma part_equivp_reflpI: "part_equivp R \<Longrightarrow> R y z \<Longrightarrow> x = y \<Longrightarrow> R x y"
  by (metis part_equivp_def)

definition "list_eq xs ys = (set xs = set ys)"

lemma "part_equivp list_eq"
  unfolding part_equivp_def list_eq_def fun_eq_iff by auto
lemma "rel_fun list_eq list_eq (map f) (map f)"
  by (auto simp: list_eq_def)
lemma "rel_fun list_eq op = set set"
  by (auto simp: list_eq_def)
lemma "\<lbrakk>Domainp list_eq (map fst x); DomainP list_eq (map snd x)\<rbrakk> \<Longrightarrow> DomainP list_eq x"
  by (auto simp: list_eq_def)

lemma listset_alt: "xs \<in> listset Xs \<longleftrightarrow> list_all2 (op \<in>) xs Xs"
  by (induct Xs arbitrary: xs) (auto simp: set_Cons_def elim: list.rel_cases)

lemma listset_map_vimage:
  "listset (map (\<lambda>x. f -` {x}) xs) = vimage (map f) {xs}"
  by (induct xs) (auto simp: set_Cons_def)

find_theorems "lists _ = Collect _"

lemma "f ` set xs = set ys \<Longrightarrow> ys \<in> map f ` {ys. set xs = set ys}"
  apply (rule set_mp[OF image_mono[of "lists (set xs)"]])
  apply (auto simp add: lists_def2) []
  apply (induct ys arbitrary: xs)
  apply simp
  apply (auto simp: image_iff) []
  sledgehammer
 
term vimage

abbreviation "list_eq_F \<equiv> {c. \<exists>x. list_eq x x \<and> c = Collect (list_eq x)}"
lemma "X \<in> list_eq_F \<Longrightarrow> map f ` X \<in> list_eq_F"
  apply (auto simp: list_eq_def[abs_def] intro: exI[of _ "map f x" for x] imageI)
  apply (rule_tac x="map f x" in exI)
  apply (auto simp: )
  apply blast
  apply auto
  apply (intro exI conjI)

lemma "map fst ` Collect (list_eq z) = Collect (list_eq (map fst z))"
  apply (auto simp: list_eq_def[abs_def])
  apply force
  apply (rule image_eqI[rotated] CollectI)+
  oops

declare [[bnf_note_all]]

bnf_axiomatization 'a F

functor map_F by (simp_all only: F.map_id0 F.map_comp0)

consts Q :: "'a F \<Rightarrow> 'a F \<Rightarrow> bool"

axiomatization where
  equivp_Q: "part_equivp Q" (* and

  map_F_Q_transfer: "\<And>f. rel_fun Q Q (map_F f) (map_F f)" and
  set_F_Q_transfer: "rel_fun Q (op =) set_F set_F" and
  zip_closedQ: "\<And>x. \<lbrakk>Domainp Q (map_F fst x); DomainP Q (map_F snd x)\<rbrakk> \<Longrightarrow> DomainP Q x"
*)
quotient_type 'a G = "'a F" / partial: Q
  by (rule equivp_Q)

abbreviation "F \<equiv> {c. \<exists>x. Q x x \<and> c = Collect (Q x)}"

axiomatization where
  map_closedQ: "\<And>X. X \<in> F \<Longrightarrow> map_F f ` X \<in> F" and
  zip_closedQ: "\<And>X. map_F fst ` X \<in> F \<Longrightarrow> map_F snd ` X \<in> F \<Longrightarrow> X \<in> F"

thm type_definition_G

(*
lift_definition map_G :: "('a \<Rightarrow> 'b) \<Rightarrow> 'a G \<Rightarrow> 'b G" is map_F by (rule rel_funD[OF map_F_Q_transfer])
*)
definition "map_G f x = Abs_G (\<Union>y \<in> map_F f ` Rep_G x. Collect (Q y))"
definition "set_G x = (\<Union>y \<in> Rep_G x. set_F y)"
definition "rel_G R x y = rel_set (rel_F R) (Rep_G x) (Rep_G y)"

lemma Abs_G_Union_Rep_G[simp]: "Abs_G (\<Union>y\<in>Rep_G x. Collect (Q y)) = x"
  by (cases x) (auto simp: Abs_G_inverse intro!: iffD2[OF Abs_G_inject]
    elim: part_equivp_transp[OF equivp_Q])
(*
lemma [simplified]: "map_F f ` Rep_G x \<in> {c. \<exists>x. Q x x \<and> c = Collect (Q x)}"
  apply (cases x)
  apply hypsubst
  apply (erule CollectE conjE exE)+
  apply (rule CollectI exI conjI)+
  apply (erule rel_funD[OF map_F_Q_transfer])
  apply (subst Abs_G_inverse)
  apply (rule CollectI exI conjI)+
  apply assumption
  apply assumption
  apply hypsubst
  apply safe
  apply (erule rel_funD[OF map_F_Q_transfer])
  apply (auto simp: image_iff)
  apply (rule exI conjI)+
  apply assumption
  sorry*)

lemma Abs_G_inverse_Q: "\<lbrakk>x \<in> Rep_G (Abs_G (Collect (Q y))); Domainp Q y\<rbrakk> \<Longrightarrow> Q y x"
  by (subst (asm) Abs_G_inverse)
    (auto intro: exI[of _ y] part_equivp_reflp_Domainp[OF equivp_Q] part_equivp_symp[OF equivp_Q])

bnf "'a G"
  map: map_G
  sets: set_G
  bd: bd_F
  rel: rel_G
apply -
prefer 9
apply (unfold OO_Grp_alt mem_Collect_eq fun_eq_iff)
apply safe
apply (auto simp: rel_G_def set_G_def rel_set_def map_G_def F.in_rel) []
apply (case_tac x)
apply (case_tac xa)
apply (auto simp: Abs_G_inverse Abs_G_inject) []
apply (drule spec)
apply (drule mp)
apply assumption
apply (drule spec)
apply (drule mp)
apply assumption
apply auto
apply (rule exI[of _ "Abs_G (Collect (Q x))" for x])
apply (subst (1 2 3) Abs_G_inverse)
apply (rule CollectI exI conjI[rotated])+
apply (rule refl)
apply (rule part_equivp_reflp_Domainp[OF equivp_Q zip_closedQ])
apply (auto elim: part_equivp_symp[OF equivp_Q]) [2]
apply (rule CollectI exI conjI[rotated])+
apply (rule refl)
apply (rule part_equivp_reflp_Domainp[OF equivp_Q zip_closedQ])
apply (auto elim: part_equivp_symp[OF equivp_Q]) [2]
apply (rule CollectI exI conjI[rotated])+
apply (rule refl)
apply (rule part_equivp_reflp_Domainp[OF equivp_Q zip_closedQ])
apply (auto elim: part_equivp_symp[OF equivp_Q]) [2]
apply (auto elim: rel_funD[OF map_F_Q_transfer] part_equivp_transp[OF equivp_Q]
  dest: rel_funD[OF set_F_Q_transfer])
apply (subst Abs_G_inject)
apply (rule CollectI exI conjI[rotated])+
apply safe
apply (rule part_equivp_transp[OF equivp_Q rel_funD[OF map_F_Q_transfer], rotated])
apply assumption
apply assumption
apply (rule UN_I[rotated] CollectI)+
apply assumption
apply (rule CollectI)
apply (rule part_equivp_reflp_Domainp[OF equivp_Q zip_closedQ])
apply (auto elim: part_equivp_symp[OF equivp_Q]) [2]
apply assumption
apply (rule CollectI exI conjI[rotated])+
apply safe
apply (rule part_equivp_transp[OF equivp_Q rel_funD[OF map_F_Q_transfer], rotated])
apply assumption
apply (rule part_equivp_reflp_Domainp[OF equivp_Q zip_closedQ])
apply (auto elim: part_equivp_symp[OF equivp_Q]) [2]
apply assumption
apply assumption
apply (rule part_equivp_transp[OF equivp_Q rel_funD[OF map_F_Q_transfer], rotated])
apply assumption
apply assumption
apply (rule UN_I[rotated] CollectI)+
apply assumption
apply (rule CollectI)
apply (rule part_equivp_reflp_Domainp[OF equivp_Q zip_closedQ])
apply (auto elim: part_equivp_symp[OF equivp_Q]) [2]

apply (subst Abs_G_inject)
apply (rule CollectI exI conjI[rotated])+
apply safe
apply (rule part_equivp_transp[OF equivp_Q rel_funD[OF map_F_Q_transfer], rotated])
apply assumption
apply assumption
apply (rule UN_I[rotated] CollectI)+
apply assumption
apply (rule CollectI)
apply (rule part_equivp_reflp_Domainp[OF equivp_Q zip_closedQ])
apply (auto elim: part_equivp_symp[OF equivp_Q]) [2]
apply (rule part_equivp_reflp_Domainp[OF equivp_Q])
apply (erule DomainPI[of Q, OF part_equivp_symp[OF equivp_Q]])
apply (rule CollectI exI conjI[rotated])+
apply (rule refl)
apply assumption
apply (auto elim: part_equivp_symp[OF equivp_Q]) [2]
apply (rule part_equivp_transp[OF equivp_Q, rotated])
apply assumption
apply (erule part_equivp_transp[OF equivp_Q _ rel_funD[OF map_F_Q_transfer], rotated])
apply assumption
apply (rule exI conjI)+
apply (rule part_equivp_reflp_Domainp[OF equivp_Q zip_closedQ])
apply (auto elim: part_equivp_symp[OF equivp_Q]) [2]
apply (rule part_equivp_transp[OF equivp_Q, rotated 1])
apply assumption
apply (erule part_equivp_symp[OF equivp_Q])

apply (auto simp: rel_G_def set_G_def rel_set_def map_G_def F.in_rel Abs_G_inverse) []
apply (subst (asm) Abs_G_inverse)
apply safe
apply (rule exI conjI[rotated])+
apply safe
apply (auto elim: part_equivp_symp[OF equivp_Q]) [2]
apply ()

apply (rule ext) apply (simp only: map_G_def F.map_id0 image_id id_apply Abs_G_Union_Rep_G)
apply (rule ext) apply (simp only: map_G_def[abs_def] F.map_comp0)
  apply (auto simp only: o_def) []
  apply (subst Abs_G_inverse)
  apply (auto simp: image_image ) [2]
apply (auto simp: map_G_def Abs_G_inject set_G_def
  intro!: image_eqI[of _ "map_F f" for f] F.map_cong0 intro: sym) [1]
apply (rule ext) apply (auto simp: map_G_def[abs_def] set_G_def o_def Abs_G_inverse F.set_map ) []
apply (rule F.bd_card_order)
apply (rule F.bd_cinfinite)
defer
apply (rule predicate2I) apply (auto simp only: rel_G_def F.rel_compp rel_set_OO[symmetric]) []
apply (unfold OO_Grp_alt mem_Collect_eq fun_eq_iff)
apply safe
apply (auto simp: rel_G_def set_G_def rel_set_def map_G_def F.in_rel) []
apply (case_tac x)
apply (case_tac xa)
apply (auto simp: Abs_G_inverse Abs_G_inject) []
apply (drule spec)
apply (drule mp)
apply assumption
apply (drule spec)
apply (drule mp)
apply assumption
apply auto
apply (rule exI[of _ "Abs_G (Collect (Q x))" for x])
apply (subst (1 2 3) Abs_G_inverse)
apply (rule CollectI exI conjI[rotated])+
apply (rule refl)
apply (rule part_equivp_reflp_Domainp[OF equivp_Q zip_closedQ])
apply (auto elim: part_equivp_symp[OF equivp_Q]) [2]
apply (rule CollectI exI conjI[rotated])+
apply (rule refl)
apply (rule part_equivp_reflp_Domainp[OF equivp_Q zip_closedQ])
apply (auto elim: part_equivp_symp[OF equivp_Q]) [2]
apply (rule CollectI exI conjI[rotated])+
apply (rule refl)
apply (rule part_equivp_reflp_Domainp[OF equivp_Q zip_closedQ])
apply (auto elim: part_equivp_symp[OF equivp_Q]) [2]
apply (auto elim: rel_funD[OF map_F_Q_transfer] part_equivp_transp[OF equivp_Q]
  dest: rel_funD[OF set_F_Q_transfer])
apply (auto simp: image_Collect) []
defer
defer
apply (auto simp: rel_G_def set_G_def rel_set_def map_G_def F.in_rel Abs_G_inverse) []
sorry

lift_definition set_G :: "'a G \<Rightarrow> 'a set" is set_F by (rule rel_funD[OF set_F_Q_transfer])
(*"\<lambda>x. \<Union>y \<in> {y. Q x y}. set_F y"
  apply auto
  apply (metis equivp_Q part_equivp_def)+
  done
*)
lift_definition rel_G :: "('a \<Rightarrow> 'b \<Rightarrow> bool) \<Rightarrow> 'a G \<Rightarrow> 'b G \<Rightarrow> bool" is
  "\<lambda>R x y. rel_set (rel_F R) {y. Q x y} {x. Q y x}"
  apply (auto simp: rel_set_def)
  apply (metis equivp_Q part_equivp_def)+
  done

bnf "'a G"
  map: map_G
  sets: set_G
  bd: bd_F
  rel: rel_G
apply -
apply (rule ext) apply transfer apply (simp only: F.map_id id_apply)
apply (rule ext) apply transfer apply (auto simp only: F.map_comp o_apply rel_funD[OF map_F_Q_transfer]) []
apply transfer apply (subst F.map_cong0)
  prefer 2 apply (rule rel_funD[OF map_F_Q_transfer]) apply assumption apply blast
apply (rule ext) apply transfer apply (simp only: o_apply F.set_map)
apply (rule F.bd_card_order)
apply (rule F.bd_cinfinite)
apply transfer apply (rule F.set_bd)
apply (rule predicate2I) apply transfer apply (auto simp only: F.rel_compp rel_set_OO[symmetric]) []
apply (unfold OO_Grp_alt mem_Collect_eq fun_eq_iff)
apply safe
apply transfer
apply (auto simp: rel_set_def F.in_rel) []
apply (drule spec)
apply (drule mp)
apply assumption
apply (drule spec)
apply (drule mp)
apply assumption
apply auto
apply (rule exI conjI[rotated])+
apply assumption
apply (erule part_equivp_symp[OF equivp_Q])
apply assumption
apply (rule part_equivp_reflp_Domainp[OF equivp_Q zip_closedQ])
apply (auto simp: Domainp.simps elim: part_equivp_symp[OF equivp_Q]) [2]
apply transfer
apply (rule rel_setI)
apply (rule bexI[rotated] CollectI)+
apply (erule rel_funD[OF map_F_Q_transfer])
apply (auto simp: F.rel_map F.in_rel) []
apply (rule exI conjI[rotated])+
apply (rule refl)
prefer 2
apply assumption
prefer 2
apply (erule part_equivp_symp[OF equivp_Q])
nitpick
apply (rule zip_closedQ)

apply (unfold OO_Grp_alt mem_Collect_eq relcompp_apply) [] apply transfer
  apply (auto simp only: F.rel_compp Bex_def mem_Collect_eq) []
  apply (rotate_tac -4)
  apply (intro exI conjI)
  apply (rule rel_funD[OF map_F_Q_transfer])
  defer
  apply (auto simp: F.set_map F.map_comp) [3]
find_theorems "(_ OO _) _ _"
  apply (erule imageE)
  apply (r)
  apply assumption
  apply (erule part_equivp_reflpI[OF equivp_Q])
  apply (rule trans[OF F.map_comp F.map_cong0[OF snd_sndOp[symmetric]]])
  apply (rule part_equivp_transp[OF equivp_Q, rotated])
  apply assumption
  apply (erule part_equivp_reflpI[OF equivp_Q])
  apply (rule trans[OF F.map_comp])
find_theorems "fst" "BNF_Def.sndOp _ _"
thm bspec[OF csquare_fstOp_sndOp[unfolded csquare_def]]
  apply (rule snd_sndOp[symmetric])
  apply (subst snd_o_sndOp)
  apply (subst F.map_comp)
done

end
