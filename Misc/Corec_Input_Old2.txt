Terminology

    codatatype x = C1 ... | ... | Cn ...

gives rise to constructors C1, ..., Cn, discriminators is_C1, ..., is_Cn,
and selectors un_C11, un_C12, etc. Discriminators and selectors are
collectively called destructors.


"Select" Expression

Isabelle's

    if P1 then t1
    else if P2 then t2
    ...
    else if P(n-1) then t(n-1)
    else tn

is asymmetric; implicitly, the condition triggering t2 is ~ P1 & P2. I
propose to introduce a new construct (or abbreviation)

    select
      P1 => t1
    | P2 => t2
    | ...
    | P(n-1) => t(n-1)
    | Pn => tn

defined as

    SOME x.
      (P1 & x = t1) |
      (P2 & x = t2) |
      ... |
      (P(n-1) & x = t(n-1)) |
      (Pn & x = tn)

inspired by Dijkstra's guarded command language. If several branches are
enabled, one of the enabled branches is selected (but it is not specified
which one). If none is enabled, the result is completely unspecified.

In the context of "primcorec" below, we will require that conditions are
exclusive, i.e. Pi ==> ~ Pj for i ~= j.


Outer Syntax of "primcorec"

    primcorec f1 [:: T1] and ... and fn [:: Tn] where
    <formula> [<of-syntax>] |
    ... |
    <formula> [<of-syntax>]
    <proof>

This defines mutually corecursive functions f1 to fn. In sequential mode
(explained below), <proof> is simply "qed"; otherwise, some conditions are
generated.


1. Constructor-Style Specification

1a. Canonical Format

There must be exactly n formulas, each of the form

    fj x1 ... xm =
      (select
         P1 => C1 ...
       | P2 => C2 ...
         ...
       | P(n-1) => C(n-1) ...
       | Pn => Cn ...)

where f :: T1 -> ... -> Tm -> T, Xi :: Ti for all i, and C1, ..., Cn are
the n constructors associated with the codatatype T returned by the
function (in any order).

Each corecursive argument to a constructor is of the form

    (if Q then t else fk t1 ... tp)

and each noncorecursive argument is of the form

    t

where fk is any of the f1, ..., fn functions (not necessarily fj). All the
terms above (P1, ..., Pn, Q, t, t1, ..., tp) may not refer to the fj's.

For nested recursion, "fk" is replaced by an appropriate "map" call, e.g.
"map fk" or "map_pair fk fl". These calls can be detected in somewhat the
same way as in the non-co case. More on this later.

At the end of the command, n(n - 1)/2 exclusiveness proof obligations are
generated, of the form

    [| Pi; Pj |] ==> False

for 1 <= i, j <= n.


1b. Normalization of Inner "If"s

Instead of

    (if Q then t else fk t1 ... tp)

it is also acceptable to have any of

    u
    (if R1 then u1 else if R2 then u2 ... else if R(n-1) then u(n-1) else un)
    (select R1 => u1 | R2 => u2 | ... | R(n-1) => u(n-1) | Rn => un)

where u is either a corecursive call ("fk t1 ... tp") or a term t that
contains no corecursive call.

These forms can be reduced to the general form as follows:

    fk t1 ... tp == (if False then undefined else fk t1 ... tp)
    t == (if True then t else undefined)
    (if R1 then u1 else if R2 then u2 ... else if R(n-1) then u(n-1) else un) ==
      (if Ri1 | ... | Rik then


If the term doesn't have the expected form, it may be because another "if"
construct is used, e.g. "case". The user will have the opportunity of
registering equations for such constructs, for unfolding them
(e.g. "(case x of X ⇒ f1 | Y ⇒ f2) = (if is_X x then f1 else f2)").
### no corecursion in x
In this case, try to expand the outermost constant and see if this does
the trick.





2. Destructor-Style Specification
