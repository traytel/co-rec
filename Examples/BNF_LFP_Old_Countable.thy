theory BNF_LFP_Old_Countable
imports "~~/src/HOL/Library/Countable"
begin


section {* Prelude *}

lemma Least_inverseI: "ALL y. \<exists>x::nat. f x = y ==> f (LEAST x. f x = y) = y"
  apply (elim allE exE)
  apply (erule LeastI)
  done


section {* Lists *}

datatype_new 'a l = N | C 'a "'a l"

function
  nth_l :: "nat \<Rightarrow> ('a::countable) l"
where
  "nth_l 0 = undefined"
| "nth_l (Suc n) =
  (case sum_decode n of
    Inl i \<Rightarrow> N
  | Inr i \<Rightarrow>
    (case prod_decode i of
      (a, b) \<Rightarrow> C (from_nat a) (nth_l b)))"
  by pat_completeness auto
  termination
  by (relation "measure id")
    (auto simp add: sum_encode_eq [symmetric] prod_encode_eq [symmetric]
      le_imp_less_Suc le_sum_encode_Inl le_sum_encode_Inr
      le_prod_encode_1 le_prod_encode_2)

lemma nth_l_covers: "\<exists>n. nth_l n = x"
  apply (induct x)
   apply (rule_tac x = "Suc (sum_encode (Inl 0))" in exI)
   apply simp
  apply (erule exE)
  apply (rule_tac x = "Suc (sum_encode (Inr (prod_encode (to_nat x1, n))))" in exI)
  apply simp
  done

instance l :: (countable) countable
  apply default
  apply (rule_tac x = "\<lambda>y. LEAST n. nth_l n = y" in exI)
  apply (rule_tac g = nth_l in inj_on_inverseI)
  apply (rule Least_inverseI)
  apply (rule allI)
  apply (rule nth_l_covers)
  done


section {* Terminatd Lists *}

datatype_new ('a, 'b) x = XN 'b | XC 'a "('a, 'b) x"

function
  nth_x :: "nat \<Rightarrow> ('a::countable, 'b::countable) x"
where
  "nth_x 0 = undefined"
| "nth_x (Suc n) =
  (case sum_decode n of
    Inl i \<Rightarrow> XN (from_nat i)
  | Inr i \<Rightarrow>
    (case prod_decode i of
      (a, b) \<Rightarrow> XC (from_nat a) (nth_x b)))"
  by pat_completeness auto
  termination
  by (relation "measure id")
    (auto simp add: sum_encode_eq [symmetric] prod_encode_eq [symmetric]
      le_imp_less_Suc le_sum_encode_Inl le_sum_encode_Inr
      le_prod_encode_1 le_prod_encode_2)

lemma nth_x_covers: "\<exists>n. nth_x n = x"
  apply (induct x)
   apply (rule_tac x = "Suc (sum_encode (Inl (to_nat xa)))" in exI)
   apply simp
  apply (erule exE)
  apply (rule_tac x = "Suc (sum_encode (Inr (prod_encode (to_nat x1a, n))))" in exI)
  apply simp
  done

instance x :: (countable, countable) countable
  apply default
  apply (rule_tac x = "\<lambda>y. LEAST n. nth_x n = y" in exI)
  apply (rule_tac g = nth_x in inj_on_inverseI)
  apply (rule Least_inverseI)
  apply (rule allI)
  apply (rule nth_x_covers)
  done


section {* Mutual Lists and Rose Trees *}

datatype_new
  'a tl = TN | TC "'a mt" "'a tl" and
  'a mt = MT 'a "'a tl"

function
  nth_tl :: "nat \<Rightarrow> ('a::countable) tl" and
  nth_mt :: "nat \<Rightarrow> ('a::countable) mt"
where
  "nth_tl 0 = undefined"
| "nth_tl (Suc n) =
  (case sum_decode n of
    Inl i \<Rightarrow> TN
  | Inr i \<Rightarrow>
    (case prod_decode i of
      (a, b) \<Rightarrow> TC (nth_mt a) (nth_tl b)))"
| "nth_mt 0 = undefined"
| "nth_mt (Suc n) =
  (case prod_decode n of
    (a, b) \<Rightarrow> MT (from_nat a) (nth_tl b))"
  by pat_completeness auto
  termination
  by (relation "measure (case_sum id id)")
    (auto simp add: sum_encode_eq [symmetric] prod_encode_eq [symmetric]
      le_imp_less_Suc le_sum_encode_Inl le_sum_encode_Inr
      le_prod_encode_1 le_prod_encode_2)

lemma nth_tl_mt_covers:
  "(\<exists>n. nth_tl n = (x::('a::countable) tl)) & (\<exists>n. nth_mt n = (x'::'a mt))"
  apply (rule tl_mt.induct[of _ _ x x'])
    apply (rule_tac x = "Suc (sum_encode (Inl 0))" in exI)
    apply simp
   apply (elim exE)
   apply (rule_tac x = "Suc (sum_encode (Inr (prod_encode (n, na))))" in exI)
   apply simp
  apply (elim exE)
  apply (rule_tac x = "Suc (prod_encode (to_nat x1, n))" in exI)
  apply simp
  done

instance tl :: (countable) countable
  apply default
  apply (rule_tac x = "\<lambda>y. LEAST n. nth_tl n = y" in exI)
  apply (rule_tac g = nth_tl in inj_on_inverseI)
  apply (rule Least_inverseI)
  apply (rule allI)
  apply (rule nth_tl_mt_covers[THEN conjunct1])
  done

instance mt :: (countable) countable
  apply default
  apply (rule_tac x = "\<lambda>y. LEAST n. nth_mt n = y" in exI)
  apply (rule_tac g = nth_mt in inj_on_inverseI)
  apply (rule Least_inverseI)
  apply (rule allI)
  apply (rule nth_tl_mt_covers[THEN conjunct2])
  done


section {* Rose Trees with List of Subtrees *}

datatype_new 'a t = T nat 'a "'a t l"

lemma le_prod_encode_3: "a \<le> c \<Longrightarrow> a \<le> prod_encode (c, b)"
  by (metis le_prod_encode_1 le_trans)

lemma le_prod_encode_4: "b \<le> c \<Longrightarrow> b \<le> prod_encode (a, c)"
  by (metis le_prod_encode_2 le_trans)
  
thm le_prod_encode_1 le_prod_encode_2[no_vars]

function
  nth_t_l :: "nat \<Rightarrow> ('a::countable) t l" and
  nth_t :: "nat \<Rightarrow> ('a::countable) t"
where
  "nth_t_l 0 = undefined"
| "nth_t_l (Suc n) =
  (case sum_decode n of
    Inl i \<Rightarrow> N
  | Inr i \<Rightarrow>
    (case prod_decode i of
      (a, b) \<Rightarrow> C (nth_t a) (nth_t_l b)))"
| "nth_t 0 = undefined"
| "nth_t (Suc n) =
  (case prod_decode n of
    (a, b) \<Rightarrow>
    (case prod_decode b of
      (ba, bb) \<Rightarrow> T (from_nat a) (from_nat ba) (nth_t_l bb)))"
  by pat_completeness auto
  termination
  by (relation "measure (case_sum id id)")
    (auto simp add: sum_encode_eq [symmetric] prod_encode_eq [symmetric]
      le_imp_less_Suc le_sum_encode_Inl le_sum_encode_Inr
      le_prod_encode_1 le_prod_encode_2 le_prod_encode_3 le_prod_encode_4)

primrec id_t_l and id_t where
  "id_t_l N = N" |
  "id_t_l (C t ts) = C (id_t t) (id_t_l ts)" |
  "id_t (T n a ts) = T n a (id_t_l ts)"
thm id_t_l.induct id_t.induct id_t_l_id_t.induct

lemma nth_t_l_t_covers:
  "(\<exists>n. nth_t_l n = (x::('a::countable) t l)) & (\<exists>n. nth_t n = (x'::'a t))"
  apply (rule id_t_l_id_t.induct[of _ _ x x'])
    apply (rule_tac x = "Suc (sum_encode (Inl 0))" in exI)
    apply simp
   apply (elim exE)
   apply (rule_tac x = "Suc (sum_encode (Inr (prod_encode (n, na))))" in exI)
   apply simp
  apply (elim exE)
  apply (rule_tac x = "Suc (prod_encode (to_nat x1, n))" in exI)
  apply simp
  done

instance tl :: (countable) countable
  apply default
  apply (rule_tac x = "\<lambda>y. LEAST n. nth_tl n = y" in exI)
  apply (rule_tac g = nth_tl in inj_on_inverseI)
  apply (rule Least_inverseI)
  apply (rule allI)
  apply (rule nth_tl_mt_covers[THEN conjunct1])
  done

instance mt :: (countable) countable
  apply default
  apply (rule_tac x = "\<lambda>y. LEAST n. nth_mt n = y" in exI)
  apply (rule_tac g = nth_mt in inj_on_inverseI)
  apply (rule Least_inverseI)
  apply (rule allI)
  apply (rule nth_tl_mt_covers[THEN conjunct2])
  done

end
