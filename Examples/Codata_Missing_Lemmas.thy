theory Codata_Missing_Lemmas
imports "~~/src/HOL/BNF_Greatest_Fixpoint"
begin

declare [[bnf_note_all]]

codatatype (ls: 'a) ll =
  ln: LN | LC (lhd: 'a) (ltl: "'a ll")
  for map: lm rel: lr
  where "ltl LN = LN"

end
