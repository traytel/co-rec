theory Rec_Map_Arg
imports "~~/src/HOL/BNF/BNF"
begin


typedecl real

datatype_new 'a tree = Node "'a tree list"
term tree_rec

consts size :: "'a tree \<Rightarrow> nat"


primrec_new f_1Aa :: "'a tree \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> real" where
  "f_1Aa (Node ts) n i = undefined i n (map f_1Aa ts)"
print_theorems

primrec_new f_1Ab :: "nat \<Rightarrow> 'a tree \<Rightarrow> int \<Rightarrow> real" where
  "f_1Ab n (Node ts) i = undefined i n (map (\<lambda>t n. f_1Ab n t) ts)"
print_theorems

primrec_new f_1Ba :: "'a tree \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> real" where
  "f_1Ba (Node ts) n i = undefined i n (map (\<lambda>t. (t, f_1Ba t)) ts)"
print_theorems

primrec_new f_1Bb :: "nat \<Rightarrow> 'a tree \<Rightarrow> int \<Rightarrow> real" where
  "f_1Bb n (Node ts) i = undefined i n (map (\<lambda>t. (t, (\<lambda>n. f_1Bb n t))) ts)"
print_theorems

primrec_new f_1C :: "'a tree \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> real" where
  "f_1C (Node ts) n i = undefined i n ts"
print_theorems

primrec_new f_2Aa :: "'a tree \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> real" where
  "f_2Aa (Node ts) n i = undefined i n (map ((\<lambda>g. g (Suc 0) (-1)) o f_2Aa) ts)"
print_theorems

primrec_new f_2Ab :: "nat \<Rightarrow> 'a tree \<Rightarrow> int \<Rightarrow> real" where
  "f_2Ab n (Node ts) i = undefined i n (map ((\<lambda>g. g (Suc 0) (-1)) o (\<lambda>t n. f_2Ab n t)) ts)"
print_theorems

primrec_new f_2Ba :: "'a tree \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> real" where
  "f_2Ba (Node ts) n i = undefined i n (map ((\<lambda>(t, g). g (size t) (-1)) o (\<lambda>t. (t, f_2Ba t))) ts)"
print_theorems

primrec_new f_2Bb :: "nat \<Rightarrow> 'a tree \<Rightarrow> int \<Rightarrow> real" where
  "f_2Bb n (Node ts) i = undefined i n (map ((\<lambda>(t, g). g (size t) (-1)) o (\<lambda>t. (t, (\<lambda>n. f_2Bb n t)))) ts)"
print_theorems

primrec_new f_2C :: "'a tree \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> real" where
  "f_2C (Node ts) n i = undefined i n (map (size o (\<lambda>x. x)) ts)"
print_theorems

primrec_new f_3Aa :: "'a tree \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> real" where
  "f_3Aa (Node ts) n i = undefined i n (map (\<lambda>t. f_3Aa t (Suc 0) (-1)) ts)"
print_theorems

primrec_new f_3Ab :: "nat \<Rightarrow> 'a tree \<Rightarrow> int \<Rightarrow> real" where
  "f_3Ab n (Node ts) i = undefined i n (map (\<lambda>t. f_3Ab (Suc 0) t (-1)) ts)"
print_theorems

primrec_new f_3Ba :: "'a tree \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> real" where
  "f_3Ba (Node ts) n i = undefined i n (map (\<lambda>t. f_3Ba t (size t) (-1)) ts)"
print_theorems

primrec_new f_3Bb :: "nat \<Rightarrow> 'a tree \<Rightarrow> int \<Rightarrow> real" where
  "f_3Bb n (Node ts) i = undefined i n (map (\<lambda>t. f_3Bb (size t) t (-1)) ts)"
print_theorems

primrec_new f_3Ca :: "'a tree \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> real" where
  "f_3Ca (Node ts) n i = undefined i n (map size ts)"
print_theorems

primrec_new f_3Cb :: "'a tree \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> real" where
  "f_3Cb (Node ts) n i = undefined i n (map id ts)"
print_theorems

primrec_new f_4a :: "nat \<Rightarrow> 'a tree \<Rightarrow> int \<Rightarrow> real" where
  "f_4a n (Node ts) i = undefined i n (map (\<lambda>t. f_4a (size t) t (-1)) ts) (map (f_4a 0) ts)"
print_theorems

end

