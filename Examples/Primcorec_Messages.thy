theory Primcorec_Messages
imports Main
begin

codatatype nt = _: Z | S nt
codatatype bt = X | NN bt bt
codatatype rt = T nt "rt list"
codatatype rtt = TT nt "rtt list list"
codatatype xx = XX and yy = YY and zz = ZZ | ZZZ | ZZZZ
codatatype q = One q | Two q q | Three q q q | Four q q q q
codatatype 'a s = N | SS 'a 'a "'a s"
codatatype 'a t = TTT 'a "'a t s"
codatatype 'a tre = Tre 'a "'a tre" "'a tre list"
datatype Two = E1 | E2
codatatype T1 = C1 Two T1 | M T1 T1
codatatype T2 = C2 Two T2 | N T2 T2
datatype ('a, 'b) mysum = _: MyInl 'a | _: MyInr 'b

consts mymap :: "('a \<Rightarrow> 'b) \<Rightarrow> 'a list \<Rightarrow> 'b list"


text {* Somewhat Suboptimal *}

(* could be clearer, e.g. 'id' is not a constructor, 'let', 'if', or known 'case' *)
primcorec f2' :: "nt \<Rightarrow> nt" where
  "f2' n = id (f2' n)"

(* could be clearer, e.g. 'undefined' is not a constructor etc.,
   or partially applied corecursive call *)
primcorec f2'' :: "nt \<Rightarrow> nt" where
  "f2'' n = undefined f2''"

primcorec f18 :: "nt \<Rightarrow> rtt" where
  "f18 n = TT n [[TT n [[f18 n]]]]"

(* not clear whether this should be detected by the massaging function or by its caller *)
primcorec f18' :: "nt \<Rightarrow> nt t" where
  "f18' n = TTT n (SS (TTT n (SS (f18' n) undefined s.N)) undefined s.N)"

(* this one works and should keep working, no matter what happens with "f18'"
   ("f18'" is bad because it has two layers of constructors;
   "primitive" corecursion should always have exactly one.) *)
primcorec f18'' :: "nt \<Rightarrow> nt t" where
  "f18'' n = TTT n (SS (f18'' n) undefined s.N)"

(* fail gracefully -- non-primitive pattern in right-hand side,
   corecursive call must appear directly under a single constructor *)
primcorec f21 :: "nt \<Rightarrow> nt" where
  "f21 n = S (S (f21 n))"

(* fail gracefully -- ditto *)
primcorec f22 :: "nt \<Rightarrow> nt" where
  "f22 n = S (id (f22 n))"

(* fail gracefully -- ditto *)
primcorec f23 :: "nt \<Rightarrow> nt" where
  "f23 n = S ((id o f23) n)"

(* fail gracefully -- ditto *)
primcorec f24 :: "nt \<Rightarrow> nt" where
  "f24 n = S ((%x. id (f24 x)) n)"

(* clearer message:
   malformed discriminator formula (argument does not start with function name) *)
primcorec f28 :: "nt \<Rightarrow> q" where
  "m = Z \<Longrightarrow> is_One (some_name m)"

(* clearer message: e.g. overspecified constructor in
     "m = Z \<Longrightarrow> is_One (f29 m)"
     "m = Z \<Longrightarrow> f29 m = One (f29 m)"
*) 
primcorec f29 :: "nt \<Rightarrow> q" where
  "m = Z \<Longrightarrow> is_One (f29 m)" |
  "m = S Z \<Longrightarrow> is_Two (f29 m)" |
  "m = Z \<Longrightarrow> f29 m = One (f29 m)"

primcorec f46 :: "nt \<Rightarrow> nt \<Rightarrow> nt" where
  "m = Z \<Longrightarrow> f46 m n = Z" |
  "f46 m n = (if n = Z then Z else n)"

(* fail gracefully: the corecursive call shouldn't be under "undefined" *)
primcorec f47 :: "nat \<Rightarrow> nat t" where
  "f47 n = TTT n (SS (f47 (n + 1)) (undefined (f47 n)) s.N)"

(* all of these should fail more gracefully, with an indication that the corecursive call may
   not occur under arbitrary function calls (here, "id") *)
primcorec f50 where "f50 = Tre 0 (id f50) (Cons f50 Nil)"
primcorec f51 where "f51 = Tre 0 (id f51) (Cons f51 Nil)"
primcorec f52 where "f52 = Tre 0 f52 (Cons (id f52) Nil)"

(* the "C2 E2" makes the definition non primitive; the error should make it
   clear that corecursive calls must appear under exactly one constructor
   not more *)
primcorec f57 :: "Two option => T1 => T2" where
 "f57 two_opt t1 =
    (case two_opt of
      None =>
      (case t1 of
         C1 y x => N (f57 (Some y) x)(f57 (Some y) x)
       | M x y => N (f57 None x) (f57 None y))
    | Some two =>
      (if two = E1 then N (f57 None t1) (C2 E2 (f57 None t1))
       else C2 E1 (f57 None t1)))"


text {* Good *}

primcorec f2 :: "nt \<Rightarrow> nt" where
  "f2 n = f2 (S n)"

primcorec f7' :: "rt \<Rightarrow> rt" where
  "f7' t = T (un_T1 t) (map (f7' o f7') (un_T2 t))"

primcorec f25 :: "nt \<Rightarrow> q" where
  "n = Z \<Longrightarrow> is_One (f25 n)" |
  "n = Z \<Longrightarrow> is_Two (f25 n)"

primcorec f1 :: nat where
  "f1 = Suc 0"

primcorec f3 :: "nt \<Rightarrow> nt" where
  "f3 Z = Z"

primcorec f4 :: "nt \<Rightarrow> nt \<Rightarrow> nt" where
  "f4 x x = S x"

primcorec f4' :: "q \<Rightarrow> q \<Rightarrow> q" where
  "is_One (f4' m m)"

primcorec f4'' :: "nt \<Rightarrow> nt \<Rightarrow> nt" where
  "x = x \<Longrightarrow> f4'' x x = S x"

primcorec f4''' :: "nt \<Rightarrow> nt \<Rightarrow> nt" where
  "f4''' x x = (if x = x then S x else Z)"

primcorec f5 :: "nt \<Rightarrow> nt" where
  "f5 x = S (f5 x)" |
  "f5 x = S (f5 x)"

primcorec f5' :: "nt \<Rightarrow> nt" where
  "\<not> is_Z (f5' x)"
| "\<not> is_Z (f5' x)"

primcorec f6 :: "nt \<Rightarrow> nt" where
  "f6 x = y"

primcorec f6' :: "nt \<Rightarrow> nt" where
  "is_Z b \<Longrightarrow> is_Z (f6' a)"

primcorec f7 :: "rt \<Rightarrow> rt" where
  "f7 t = T (un_T1 t) (mymap f7 (un_T2 t))"

primcorec f13 :: "nt \<Rightarrow> nt" where
  "f13 x \<equiv> x"

primcorec f14 :: "nt \<Rightarrow> nt" where
  "f14 x = x \<Longrightarrow> x = x"

primcorec f15 :: xx and f16 :: yy and f16b :: zz where
  "f15 = XX"

primcorec f15' :: xx and f16' :: zz where
  "f15' = XX"

primcorec f17 :: "nt \<Rightarrow> rtt" where
  "f17 n = TT n (map (mymap f17) [[S n]])"

primcorec f19 :: "nt \<Rightarrow> nt" and f19 :: "bt \<Rightarrow> bt" where
  "f19 n = n"

primcorec f20 :: "bt \<Rightarrow> bt \<Rightarrow> bt" where
  "f20 b = NN b"

primcorec f26 :: "nt \<Rightarrow> q" where
  "m = Z \<Longrightarrow> is_One (f26 n)"

primcorec f27 :: "nt \<Rightarrow> q" where
  "m = Z \<Longrightarrow> f28 m = One (f28 (S m))"

primcorec f31 :: "nt \<Rightarrow> nt \<Rightarrow> nt" where
  "f31 n = undefined"

primcorec f33 :: "(nt \<Rightarrow> nt) \<Rightarrow> nt \<Rightarrow> nt" where
  "f33 f = S o f33 f"

primcorec f34 :: "(nt \<Rightarrow> nt) \<Rightarrow> nt \<Rightarrow> nt" where
  "f34 f = S o f"

(* should fail gracefully -- the "Three" case is overspecified
   (since it is both specified by un_Three1 and the last equation) *)
primcorec f35 :: "nt \<Rightarrow> q" where
  "n = Z \<Longrightarrow> is_One (f35 n)" |
  "n = S Z \<Longrightarrow> is_Two (f35 n)" |
  "un_One (f35 n) = f35 n" |
  "un_Two1 (f35 n) = f35 n" |
  "un_Three1 (f35 n) = f35 n" |
  "un_Four1 (f35 n) = f35 n" |
  "n = S (S Z) \<Longrightarrow> f35 n = Three (f35 (S n)) (f35 n) (f35 n)"

(* same as above (except that the specification is merely redundant instead of
   inconsistent, which means the error appears a bit later -- but we don't need
   to distinguish between redundancy and inconsistency in error messages
   (esp. that this is an undecidable problem) *)
primcorec f36 :: "nt \<Rightarrow> q" where
  "n = Z \<Longrightarrow> is_One (f36 n)" |
  "n = S Z \<Longrightarrow> is_Two (f36 n)" |
  "un_One (f36 n) = f36 n" |
  "un_Two1 (f36 n) = f36 n" |
  "un_Three1 (f36 n) = f36 n" |
  "un_Four1 (f36 n) = f36 n" |
  "n = S (S Z) \<Longrightarrow> f36 n = Three (f36 n) (f36 n) (f36 n)"

primcorec f37 :: q where
  "f37 = f37 \<Longrightarrow> f37 = One f37"

primcorec f38 :: "nt \<Rightarrow> nt \<Rightarrow> nt" where
  "is_Z (f38 m n) \<longleftrightarrow> (is_Z m \<and> is_Z n)"

primcorec f39 :: "nt \<Rightarrow> nt \<Rightarrow> nt" where
  "un_S (f39 m n) = n"

primcorec f40 :: "nt \<Rightarrow> nt \<Rightarrow> nt" where
  "un_S (un_S (f40 m n)) = n"

primcorec f41 :: "nt \<Rightarrow> nt \<Rightarrow> nt" where
  "un_S (f41 m m) = Z"

primcorec f42 :: "nt \<Rightarrow> nt \<Rightarrow> nt" where
  "un_S (f42 m m) = m"

primcorec f43 :: "nt \<Rightarrow> nt \<Rightarrow> nt" where
  "m \<equiv> Z \<Longrightarrow> f43 m n = n"

primcorec f44 :: "nt \<Rightarrow> nt \<Rightarrow> nt" where
  "f44 m n \<equiv> n"

primcorec f45 :: "nt \<Rightarrow> nt \<Rightarrow> nt" where
  "m = Z \<Longrightarrow> f45 m n = (if n = Z then Z else n)"

primcorec f48 :: "nat \<Rightarrow> nat s" where
  "n = 0 \<Longrightarrow> f48 n = s.N" |
  "_ \<Longrightarrow> n > 0 \<Longrightarrow> f49 n = SS s.N"

locale l =
  fixes x :: nat
begin
  primcorec f53 where
    "f53 x = SS x x (f53 x)"
  thm f53.ctr

  primcorec f54 where
    "f54 y = SS x y (f54 y)"
  thm f54.ctr
end

end
