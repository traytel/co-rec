theory Bug_Dmi2
imports "~~/src/HOL/BNF/BNF_GFP"
begin

codatatype l0 = L0 k0
and k0 = K0 l0

thm l0_k0.dtor_coinduct

codatatype 'a k = K 'a
codatatype l = L "l k"

;primcorecursive f :: "'a \<Rightarrow> l" and fs :: "'a \<Rightarrow> l k" where
"un_L (f b) = fs b" |
"un_K (fs b) = f b"
done



text {* NEXT *}

codatatype ('a, 'b) llist (map: lmap) =
  lnull: LNil | LCons (lhd: "'a * 'b") (ltl: "('a, 'b) llist")
codatatype 'a hfset = HFset "('a hfset, 'a) llist"

text {* *}

;primcorecursive g :: "'a \<Rightarrow> 'a hfset" and gs :: "'a \<Rightarrow> ('a hfset, 'a) llist" where
"un_HFset (g b) = gs b" |
"\<not> lnull (gs b)" |
"lhd (gs b) = (g b, b)" |
"ltl (gs b) = gs b"
done

locale A
begin
(* BUG for LORENZ? *)
codatatype lk = K l
and l = L lk

;primcorecursive f :: "'a \<Rightarrow> l" and fs :: "'a \<Rightarrow> lk" where
"un_L (f b) = fs b" |
"un_K (fs b) = f b"
done

end
