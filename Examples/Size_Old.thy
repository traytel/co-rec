theory Size_Old
imports "~~/src/HOL/Library/FSet"
begin

datatype 'a l = N | C 'a "'a l"

fun id_l where
  "id_l N = N"
| "id_l (C x xs) = C x (id_l xs)"

datatype 'a t = T 'a "'a t list"
thm l.size t.size

fun id_t where
  "id_t (T x xs) = T x (map id_t xs)"

end
