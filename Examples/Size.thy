theory Size
imports "~~/src/HOL/Library/FSet"
begin


class syze =
  fixes syze :: "'a \<Rightarrow> nat"

section {* Products *}

local_setup {* fn lthy =>
BNF_Def.bnf_of lthy @{type_name prod}
|> the
|> BNF_Def.inj_map_of_bnf
|> (fn thm => Local_Theory.note ((@{binding prod_inj_map}, []), [thm]) lthy)
|> snd
*}


section {* Prelude *}

declare [[bnf_note_all]]

lemma case_prod_app: "case_prod f x y = case_prod (\<lambda>l r. f l r y) x"
  by (case_tac x) simp

lemma case_sum_o_map_sum: "case_sum l r (map_sum f g x) = case_sum (l \<circ> f) (r \<circ> g) x"
  by (case_tac x) simp+

lemma case_prod_o_map_prod: "case_prod h (map_prod f g x) = case_prod (\<lambda>l r. h (f l) (g r)) x"
  by (case_tac x) simp+

lemma snd_o_convol: "(snd \<circ> (\<lambda>x. (f x, g x))) = g"
  by (rule ext) simp

lemma inj_on_convol_id: "inj_on (\<lambda>x. (x, f x)) X"
  unfolding inj_on_def by simp

ML {* open Ctr_Sugar_Util BNF_Util BNF_Tactics *}

ML {*
val rec_o_map_thms =
  @{thms o_def id_apply case_prod_app case_sum_o_map_sum case_prod_o_map_prod
      BNF_Comp.id_bnf_comp_def};

fun mk_rec_o_map_tac ctxt def map_pre_defs abs_inverses ctor_rec_o_map =
  unfold_thms_tac ctxt [def] THEN
  HEADGOAL (rtac (ctor_rec_o_map RS trans) THEN'
    K (PRIMITIVE (Conv.fconv_rule Thm.eta_long_conversion)) THEN'
    asm_simp_tac (ss_only (map_pre_defs @ abs_inverses @ rec_o_map_thms) ctxt));
*}

ML {*
val syze_o_map_thms = @{thms o_apply prod_inj_map inj_on_id snd_comp_apfst[unfolded apfst_def]};

fun mk_syze_o_map_tac ctxt syze_def rec_o_map inj_maps syze_o_maps =
  let
    val syze_o_maps' = map (fn th => unfold_thms ctxt [o_apply] (th RS fun_cong)) syze_o_maps;
  in
    unfold_thms_tac ctxt [syze_def] THEN
    HEADGOAL (rtac (rec_o_map RS trans) THEN'
      asm_simp_tac (ss_only (inj_maps @ syze_o_maps @ syze_o_maps' @ syze_o_map_thms) ctxt))
  end;
*}


section {* Lists *}

datatype_new 'a l = N nat nat | C 'a "'a l"

local_setup {* fn lthy =>
BNF_FP_Def_Sugar.fp_sugar_of lthy @{type_name l}
|> the
|> #absT_info
|> #abs_inverse
|> (fn thm => Local_Theory.note ((@{binding l_abs_inverse}, []), [thm]) lthy)
|> snd
*}

local_setup {* fn lthy =>
BNF_FP_Def_Sugar.fp_sugar_of lthy @{type_name l}
|> the
|> #fp_res
|> #xtor_co_rec_o_map_thms
|> (fn thms => Local_Theory.note ((@{binding l_ctor_rec_o_map}, []), thms) lthy)
|> snd
*}

definition l_syze :: "('a \<Rightarrow> nat) \<Rightarrow> 'a l \<Rightarrow> nat" where
  "l_syze \<equiv> \<lambda>f. rec_l (%_ _. 0) (\<lambda>x _ y. f x + y + Suc 0)"

instantiation l :: (type) syze begin
definition syze_l where "syze_l = l_syze (\<lambda>_. 0)"
instance ..
end

lemmas l_syze_def' = l_syze_def[THEN meta_eq_to_obj_eq, THEN fun_cong, THEN fun_cong]

lemmas l_syze_simps[simp] =
  trans[OF l_syze_def' l.rec(1), folded l_syze_def']
  trans[OF l_syze_def' l.rec(2), folded l_syze_def']

lemmas syze_l_simps[simp] =
  l_syze_simps[of "\<lambda>_. 0", unfolded add_0_left add_0_right, folded syze_l_def]

thm l_syze_simps syze_l_simps

lemma l_rec_o_map:
  "rec_l f1 f2 \<circ> map_l g = rec_l f1 (\<lambda>x xs y. f2 (g x) (map_l g xs) y)"
  by (tactic {* mk_rec_o_map_tac @{context} @{thm rec_l_def} @{thms map_pre_l_def}
    @{thms l_abs_inverse} @{thm l_ctor_rec_o_map} *})

lemma l_syze_o_map: "inj f \<Longrightarrow> l_syze g \<circ> map_l f = l_syze (g \<circ> f)"
  by (tactic {* mk_syze_o_map_tac @{context} @{thm l_syze_def} @{thm l_rec_o_map} [] [] *})


section {* Terminated Lists *}

datatype_new ('a, 'b) x = XN 'b | XC 'a "('a, 'b) x"

local_setup {* fn lthy =>
BNF_FP_Def_Sugar.fp_sugar_of lthy @{type_name x}
|> the
|> #fp_res
|> #xtor_co_rec_o_map_thms
|> (fn thms => Local_Theory.note ((@{binding x_ctor_rec_o_map}, []), thms) lthy)
|> snd
*}

definition x_syze :: "('a \<Rightarrow> nat) \<Rightarrow> ('b \<Rightarrow> nat) \<Rightarrow> ('a, 'b) x \<Rightarrow> nat" where
  "x_syze \<equiv> \<lambda>f g. rec_x (\<lambda>y. g y + Suc 0) (\<lambda>x _ z. f x + z + Suc 0)"

instantiation x :: (type, type) syze begin
definition syze_x where "syze_x = x_syze (\<lambda>_. 0) (\<lambda>_. 0)"
instance ..
end

lemmas x_syze_def' = x_syze_def[THEN meta_eq_to_obj_eq, THEN fun_cong, THEN fun_cong, THEN fun_cong]

lemmas x_syze_simps[simp] =
  trans[OF x_syze_def' x.rec(1), folded x_syze_def']
  trans[OF x_syze_def' x.rec(2), folded x_syze_def']

lemmas syze_x_simps[simp] =
  x_syze_simps[of "\<lambda>_. 0" "\<lambda>_. 0", unfolded add_0_left add_0_right, folded syze_x_def]

thm x_syze_simps syze_x_simps

lemma x_rec_o_map:
  "rec_x f1 f2 \<circ> map_x g1 g2 = rec_x (\<lambda>y. f1 (g2 y)) (\<lambda>x xs z. f2 (g1 x) (map_x g1 g2 xs) z)"
  by (tactic {* mk_rec_o_map_tac @{context} @{thm rec_x_def} @{thms map_pre_x_def} []
    @{thm x_ctor_rec_o_map} *})

lemma x_syze_o_map: "inj f1 \<Longrightarrow> inj f2 \<Longrightarrow> x_syze g1 g2 \<circ> map_x f1 f2 = x_syze (g1 \<circ> f1) (g2 \<circ> f2)"
  by (tactic {* mk_syze_o_map_tac @{context} @{thm x_syze_def} @{thm x_rec_o_map} [] [] *})


section {* Mutual Lists and Rose Trees *}

datatype_new
  'a tl = TN | TC "'a mt" "'a tl" and
  'a mt = MT 'a "'a tl"

local_setup {* fn lthy =>
BNF_FP_Def_Sugar.fp_sugar_of lthy @{type_name tl}
|> the
|> #fp_res
|> #xtor_co_rec_o_map_thms
|> (fn [thm1, thm2] =>
  lthy
  |> Local_Theory.note ((@{binding tl_ctor_rec_o_map}, []), [thm1])
  ||>> Local_Theory.note ((@{binding mt_ctor_rec_o_map}, []), [thm2]))
|> snd
*}

definition tl_syze :: "('a \<Rightarrow> nat) \<Rightarrow> 'a tl \<Rightarrow> nat" where
  "tl_syze \<equiv> \<lambda>f. rec_tl 0 (\<lambda>_ _ y z. y + z + Suc 0) (\<lambda>x _ y. f x + y + Suc 0)"

definition mt_syze :: "('a \<Rightarrow> nat) \<Rightarrow> 'a mt \<Rightarrow> nat" where
  "mt_syze \<equiv> \<lambda>f. rec_mt 0 (\<lambda>_ _ y z. y + z + Suc 0) (\<lambda>x _ y. f x + y + Suc 0)"

instantiation tl :: (type) syze begin
definition syze_tl where "syze_tl = tl_syze (\<lambda>_. 0)"
instance ..
end

instantiation mt :: (type) syze begin
definition syze_mt where "syze_mt = mt_syze (\<lambda>_. 0)"
instance ..
end

lemmas tl_syze_def' = tl_syze_def[THEN meta_eq_to_obj_eq, THEN fun_cong, THEN fun_cong]
lemmas mt_syze_def' = mt_syze_def[THEN meta_eq_to_obj_eq, THEN fun_cong, THEN fun_cong]

lemmas tl_syze_simps[simp] =
  trans[OF tl_syze_def' tl.rec(1), folded tl_syze_def' mt_syze_def']
  trans[OF tl_syze_def' tl.rec(2), folded tl_syze_def' mt_syze_def']

lemmas mt_syze_simps[simp] =
  trans[OF mt_syze_def' mt.rec(1), folded tl_syze_def' mt_syze_def']

lemmas syze_tl_simps[simp] =
  tl_syze_simps[of "\<lambda>_. 0", unfolded add_0_left add_0_right, folded syze_tl_def syze_mt_def]

lemmas syze_mt_simps[simp] =
  mt_syze_simps[of "\<lambda>_. 0", unfolded add_0_left add_0_right, folded syze_tl_def syze_mt_def]

thm tl_syze_simps syze_tl_simps
thm mt_syze_simps syze_mt_simps

lemma tl_rec_o_map:
  "rec_tl f11 f12 f2 \<circ> map_tl g =
   rec_tl f11 (\<lambda>t ts y ys. f12 (map_mt g t) (map_tl g ts) y ys) (\<lambda>l ts ys. f2 (g l) (map_tl g ts) ys)"
  by (tactic {* mk_rec_o_map_tac @{context} @{thm rec_tl_def} @{thms map_pre_tl_def map_pre_mt_def}
    [] @{thm tl_ctor_rec_o_map} *})

lemma mt_rec_o_map:
  "rec_mt f11 f12 f2 \<circ> map_mt g =
   rec_mt f11 (\<lambda>t ts y ys. f12 (map_mt g t) (map_tl g ts) y ys) (\<lambda>l ts ys. f2 (g l) (map_tl g ts) ys)"
  by (tactic {* mk_rec_o_map_tac @{context} @{thm rec_mt_def} @{thms map_pre_tl_def map_pre_mt_def}
    [] @{thm mt_ctor_rec_o_map} *})

lemma tl_syze_o_map: "inj f \<Longrightarrow> tl_syze g \<circ> map_tl f = tl_syze (g \<circ> f)"
  by (tactic {* mk_syze_o_map_tac @{context} @{thm tl_syze_def} @{thm tl_rec_o_map} [] [] *})

lemma mt_syze_o_map: "inj f \<Longrightarrow> mt_syze g \<circ> map_mt f = mt_syze (g \<circ> f)"
  by (tactic {* mk_syze_o_map_tac @{context} @{thm mt_syze_def} @{thm mt_rec_o_map} [] [] *})


section {* Finite Sets *}

local_setup {* fn lthy =>
BNF_Def.bnf_of lthy @{type_name fset}
|> the
|> BNF_Def.inj_map_of_bnf
|> (fn thm => Local_Theory.note ((@{binding fset_inj_map}, []), [thm]) lthy)
|> snd
*}

context includes fset.lifting begin
lift_definition fset_syze :: "('a \<Rightarrow> nat) \<Rightarrow> 'a fset \<Rightarrow> nat" is "\<lambda>f. setsum (Suc \<circ> f)" .
end

instantiation fset :: (type) syze begin
definition syze_fset where "syze_fset = fset_syze (\<lambda>_. 0)"
instance ..
end

lemmas fset_syze_simps[simp] =
  fset_syze_def[THEN meta_eq_to_obj_eq, THEN fun_cong, THEN fun_cong,
    unfolded map_fun_def comp_def id_apply]

lemmas syze_fset_simps[simp] =
  fset_syze_simps[of "\<lambda>_. 0", unfolded add_0_left add_0_right, folded syze_fset_def]

thm fset_syze_simps syze_fset_simps

declare FSet.top_fset.rep_eq[simp]

lemma fset_syze_o_map: "inj f \<Longrightarrow> fset_syze g \<circ> fimage f = fset_syze (g \<circ> f)"
  unfolding fset_syze_def fimage_def
  by (auto simp: Abs_fset_inverse setsum_reindex_cong[OF subset_inj_on[OF _ top_greatest]])


section {* Rose Trees with List of Subtrees *}

datatype_new 'a t = T nat 'a "'a t l"

local_setup {* fn lthy =>
BNF_FP_Def_Sugar.fp_sugar_of lthy @{type_name t}
|> the
|> #absT_info
|> #abs_inverse
|> (fn thm => Local_Theory.note ((@{binding t_abs_inverse}, []), [thm]) lthy)
|> snd
*}

local_setup {* fn lthy =>
BNF_FP_Def_Sugar.fp_sugar_of lthy @{type_name t}
|> the
|> #fp_res
|> #xtor_co_rec_o_map_thms
|> (fn thm => Local_Theory.note ((@{binding t_ctor_rec_o_map}, []), thm) lthy)
|> snd
*}

definition t_syze :: "('a \<Rightarrow> nat) \<Rightarrow> 'a t \<Rightarrow> nat" where
  "t_syze \<equiv> \<lambda>f. rec_t (\<lambda>n x tns. f x + l_syze snd tns + Suc 0)"

instantiation t :: (type) syze begin
definition syze_t where "syze_t = t_syze (\<lambda>_. 0)"
instance ..
end

lemmas t_syze_def' = t_syze_def[THEN meta_eq_to_obj_eq, THEN fun_cong, THEN fun_cong]

lemmas t_syze_simps[simp] =
  trans[OF t_syze_def' t.rec(1),
    unfolded l_syze_o_map[THEN fun_cong, unfolded o_apply, OF inj_on_convol_id] snd_o_convol,
    folded t_syze_def']

lemmas syze_t_simps[simp] =
  t_syze_simps[of "\<lambda>_. 0", unfolded add_0_left add_0_right, folded syze_t_def]

thm t_syze_simps syze_t_simps

lemma t_rec_o_map:
  "rec_t f \<circ> map_t g = rec_t (\<lambda>n x ps. f n (g x) (map_l (map_prod (map_t g) id) ps))"
  by (tactic {* mk_rec_o_map_tac @{context} @{thm rec_t_def} @{thms map_pre_t_def}
    @{thms t_abs_inverse} @{thm t_ctor_rec_o_map} *})

lemma t_syze_o_map: "inj f \<Longrightarrow> t_syze g \<circ> map_t f = t_syze (g \<circ> f)"
  by (tactic {* mk_syze_o_map_tac @{context} @{thm t_syze_def} @{thm t_rec_o_map}
    @{thms t.inj_map} @{thms l_syze_o_map} *})


section {* Rose Trees with Finite Set of Subtrees *}

datatype_new 'a u = U 'a "'a u fset"

local_setup {* fn lthy =>
BNF_FP_Def_Sugar.fp_sugar_of lthy @{type_name u}
|> the
|> #fp_res
|> #xtor_co_rec_o_map_thms
|> (fn thm => Local_Theory.note ((@{binding u_ctor_rec_o_map}, []), thm) lthy)
|> snd
*}

definition u_syze :: "('a \<Rightarrow> nat) \<Rightarrow> 'a u \<Rightarrow> nat" where
  "u_syze \<equiv> \<lambda>f. rec_u (\<lambda>x TN. f x + fset_syze snd TN + Suc 0)"

instantiation u :: (type) syze begin
definition syze_u where "syze_u = u_syze (\<lambda>_. 0)"
instance ..
end

lemmas u_syze_def' = u_syze_def[THEN meta_eq_to_obj_eq, THEN fun_cong, THEN fun_cong]

lemmas u_syze_simps[simp] =
  trans[OF u_syze_def' u.rec(1),
    unfolded fset_syze_o_map[THEN fun_cong, unfolded o_apply, OF inj_on_convol_id] snd_o_convol,
    folded u_syze_def']

lemmas syze_u_simps[simp] =
  u_syze_simps[of "\<lambda>_. 0",
    unfolded fset_syze_o_map[THEN fun_cong, unfolded o_apply, OF inj_on_convol_id] add_0_left
      add_0_right,
    folded syze_u_def]

thm u_syze_simps syze_u_simps

lemma u_rec_o_map:
  "rec_u f \<circ> map_u g = rec_u (\<lambda>x ps. f (g x) (fimage (map_prod (map_u g) id) ps))"
  by (tactic {* mk_rec_o_map_tac @{context} @{thm rec_u_def} @{thms map_pre_u_def} []
    @{thm u_ctor_rec_o_map} *})

lemma u_syze_o_map: "inj f \<Longrightarrow> u_syze g \<circ> map_u f = u_syze (g \<circ> f)"
  by (tactic {* mk_syze_o_map_tac @{context} @{thm u_syze_def} @{thm u_rec_o_map}
    @{thms fset_inj_map u.inj_map} @{thms fset_syze_o_map} *})


section {* Complicated Mutual, Nested Types *}

datatype_new
  ('a, 'b) v = V "nat l" | V' 'a "('a, 'b) w" and
  ('a, 'b) w = W 'b "('a, 'b) v fset l"

local_setup {* fn lthy =>
BNF_FP_Def_Sugar.fp_sugar_of lthy @{type_name v}
|> the
|> #absT_info
|> #abs_inverse
|> (fn thm => Local_Theory.note ((@{binding v_abs_inverse}, []), [thm]) lthy)
|> snd
*}

local_setup {* fn lthy =>
BNF_FP_Def_Sugar.fp_sugar_of lthy @{type_name v}
|> the
|> #fp_res
|> #xtor_co_rec_o_map_thms
|> (fn [thm1, thm2] =>
  lthy
  |> Local_Theory.note ((@{binding v_ctor_rec_o_map}, []), [thm1])
  ||>> Local_Theory.note ((@{binding w_ctor_rec_o_map}, []), [thm2]))
|> snd
*}

definition v_syze :: "('a \<Rightarrow> nat) \<Rightarrow> ('b \<Rightarrow> nat) \<Rightarrow> ('a, 'b) v \<Rightarrow> nat" where
  "v_syze \<equiv> \<lambda>f g. rec_v (\<lambda>_. 0) (\<lambda>a _ y. f a + y + Suc 0) (\<lambda>b ysl. g b + l_syze (fset_syze snd) ysl)"

definition w_syze :: "('a \<Rightarrow> nat) \<Rightarrow> ('b \<Rightarrow> nat) \<Rightarrow> ('a, 'b) w \<Rightarrow> nat" where
  "w_syze \<equiv> \<lambda>f g. rec_w (\<lambda>_. 0) (\<lambda>a _ y. f a + y + Suc 0) (\<lambda>b ysl. g b + l_syze (fset_syze snd) ysl)"

instantiation v :: (type, type) syze begin
definition syze_v where "syze_v = v_syze (\<lambda>_. 0) (\<lambda>_. 0)"
instance ..
end

instantiation w :: (type, type) syze begin
definition syze_w where "syze_w = w_syze (\<lambda>_. 0) (\<lambda>_. 0)"
instance ..
end

lemmas v_syze_def' = v_syze_def[THEN meta_eq_to_obj_eq, THEN fun_cong, THEN fun_cong, THEN fun_cong]
lemmas w_syze_def' = w_syze_def[THEN meta_eq_to_obj_eq, THEN fun_cong, THEN fun_cong, THEN fun_cong]

lemmas v_syze_simps[simp] =
  trans[OF v_syze_def' v.rec(1), folded v_syze_def' w_syze_def']
  trans[OF v_syze_def' v.rec(2), folded v_syze_def' w_syze_def']
lemmas w_syze_simps[simp] =
  trans[OF w_syze_def' w.rec(1),
    simplified l_syze_o_map[THEN fun_cong, unfolded o_apply]
      fset_syze_o_map
      fset_inj_map inj_on_convol_id
      snd_o_convol,
    folded v_syze_def' w_syze_def']

lemmas syze_v_simps[simp] =
  v_syze_simps[of "\<lambda>_. 0", unfolded add_0_left add_0_right, folded syze_v_def syze_w_def]

lemmas syze_w_simps[simp] =
  w_syze_simps[of "\<lambda>_. 0", unfolded add_0_left add_0_right, folded syze_v_def syze_w_def]

thm v_syze_simps syze_v_simps
thm w_syze_simps syze_w_simps

lemma v_rec_o_map:
  "rec_v f11 f12 f2 \<circ> map_v g1 g2 =
   rec_v f11 (\<lambda>a w y. f12 (g1 a) (map_w g1 g2 w) y)
     (\<lambda>b vsl. f2 (g2 b) (map_l (fimage (map_prod (map_v g1 g2) id)) vsl))"
  by (tactic {* mk_rec_o_map_tac @{context} @{thm rec_v_def} @{thms map_pre_v_def map_pre_w_def}
    @{thms v_abs_inverse} @{thm v_ctor_rec_o_map} *})

lemma w_rec_o_map:
  "rec_w f11 f12 f2 \<circ> map_w g1 g2 =
   rec_w f11 (\<lambda>a w y. f12 (g1 a) (map_w g1 g2 w) y)
     (\<lambda>b vsl. f2 (g2 b) (map_l (fimage (map_prod (map_v g1 g2) id)) vsl))"
  by (tactic {* mk_rec_o_map_tac @{context} @{thm rec_w_def} @{thms map_pre_v_def map_pre_w_def}
    @{thms v_abs_inverse} @{thm w_ctor_rec_o_map} *})

lemma v_syze_o_map: "inj f1 \<Longrightarrow> inj f2 \<Longrightarrow> v_syze g1 g2 \<circ> map_v f1 f2 = v_syze (g1 \<circ> f1) (g2 \<circ> f2)"
  by (tactic {* mk_syze_o_map_tac @{context} @{thm v_syze_def} @{thm v_rec_o_map}
    @{thms fset_inj_map v.inj_map w.inj_map} @{thms l_syze_o_map fset_syze_o_map} *})

lemma w_syze_o_map: "inj f1 \<Longrightarrow> inj f2 \<Longrightarrow> w_syze g1 g2 \<circ> map_w f1 f2 = w_syze (g1 \<circ> f1) (g2 \<circ> f2)"
  by (tactic {* mk_syze_o_map_tac @{context} @{thm w_syze_def} @{thm w_rec_o_map}
    @{thms fset_inj_map v.inj_map w.inj_map} @{thms l_syze_o_map fset_syze_o_map} *})


section {* Additional Setup *}

lemma l_syze_estimation[termination_simp]:
  "x \<in> set_l xs \<Longrightarrow> y < f x \<Longrightarrow> y < l_syze f xs"
  by (induct xs) auto

lemma l_syze_estimation'[termination_simp]:
  "x \<in> set_l xs \<Longrightarrow> y \<le> f x \<Longrightarrow> y \<le> l_syze f xs"
  by (induct xs) auto

lemma l_syze_pointwise[termination_simp]:
  "(\<And>x. x \<in> set_l xs \<Longrightarrow> f x \<le> g x) \<Longrightarrow> l_syze f xs \<le> l_syze g xs"
  by (induct xs) force+

lemma x_syze_estimation[termination_simp]:
  "x \<in> set1_x xs \<Longrightarrow> y < f x \<Longrightarrow> y < x_syze f g xs"
  "x \<in> set2_x xs \<Longrightarrow> y < g x \<Longrightarrow> y < x_syze f g xs"
  by (induct xs) auto

lemma x_syze_estimation'[termination_simp]:
  "x \<in> set1_x xs \<Longrightarrow> y \<le> f x \<Longrightarrow> y \<le> x_syze f g xs"
  "x \<in> set2_x xs \<Longrightarrow> y \<le> g x \<Longrightarrow> y \<le> x_syze f g xs"
  by (induct xs) auto

lemma x_syze_pointwise[termination_simp]:
  "(\<And>x. x \<in> set1_x xs \<Longrightarrow> f x \<le> g x) \<Longrightarrow> x_syze f h xs \<le> x_syze g h xs"
  "(\<And>x. x \<in> set2_x xs \<Longrightarrow> f x \<le> g x) \<Longrightarrow> x_syze h f xs \<le> x_syze h g xs"
  by (induct xs) force+

lemma fset_syze_estimation[termination_simp]:
  "x \<in> fset X \<Longrightarrow> y < f x \<Longrightarrow> y < fset_syze f X"
  unfolding fset_syze_def map_fun_def o_def id_apply
  apply (erule order.strict_trans2)
  apply (drule insert_Diff[symmetric])
  apply (erule ssubst)
  apply (subst setsum.insert_remove)
  apply auto
  done

lemma fset_syze_estimation'[termination_simp]:
  "x \<in> fset xs \<Longrightarrow> y \<le> f x \<Longrightarrow> y \<le> fset_syze f xs"
  by (metis dual_order.order_iff_strict fset_syze_estimation not_le)

lemma fset_syze_pointwise[termination_simp]:
  "(\<And>x. x \<in> fset xs \<Longrightarrow> f x \<le> g x) \<Longrightarrow> fset_syze f xs \<le> fset_syze g xs"
  unfolding fset_syze_def map_fun_def o_def id_apply by (auto simp: setsum_mono)


section {* Function Examples *}

(*
lemma [termination_simp]: "z \<in> fset X \<Longrightarrow> syze z < Suc (\<Sum>x\<in>fset X. Suc (syze x))"
  by (metis fset_syze_estimation fset_syze_simps less_not_refl not_less_eq)

fun id_l where
  "id_l (N m n) = N m n"
| "id_l (C x xs) = C x (id_l xs)"

fun id_x where
  "id_x (XN y) = XN y"
| "id_x (XC x xs) = XC x (id_x xs)"

fun id_tl and id_mt where
  "id_tl TN = TN"
| "id_tl (TC t ts) = TC (id_mt t) (id_tl ts)"
| "id_mt (MT x ts) = MT x (id_tl ts)"

fun id_fset where
  "id_fset X = (if fset X = {} then X else id_fset (X |-| fUNIV))"

fun id_t where
  "id_t (T n x xs) = T n x (map_l id_t xs)"

fun id_u where
  "id_u (U x X) = U x (fimage id_u X)"
*)

end
