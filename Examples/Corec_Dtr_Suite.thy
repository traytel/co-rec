theory Corec_Dtr_Suite
imports Corec_Suite_Base
begin

primcorec f1_s1 :: "nat \<Rightarrow> s1" where
  "is_X1 (f1_s1 _)"
print_theorems

primcorec f1_s2 :: "nat \<Rightarrow> s2" where
  "n = 0 \<Longrightarrow> is_Y1 (f1_s2 n)"
print_theorems

primcorec f1_s2' :: "nat \<Rightarrow> s2'" where
  "n = 0 \<Longrightarrow> is_Y1' (f1_s2' n)"
print_theorems

primcorec f1_s3 :: "nat \<Rightarrow> s3" where
  "n = 0 \<Longrightarrow> is_A1 (f1_s3 n)"
print_theorems

primcorec f1_s3' :: "nat \<Rightarrow> s3" where
  "n = 0 \<Longrightarrow> n = 1 \<Longrightarrow> is_A1 (f1_s3' n)"
| "n = 3 \<Longrightarrow> is_A3 (f1_s3' n)"
| "_ \<Longrightarrow> is_A2 (f1_s3' n)"
print_theorems

primcorec f1_s5 :: "nat \<Rightarrow> s5" where
  "n = 1 \<Longrightarrow> is_Z1 (f1_s5 n)"
| "n = 3 \<Longrightarrow> is_Z3 (f1_s5 n)"
| "n = 4 \<Longrightarrow> is_Z4 (f1_s5 n)"
| "n = 5 \<Longrightarrow> is_Z5 (f1_s5 n)"
| "n > 500 \<Longrightarrow> is_Z2 (f1_s5 n)"
| "un_Z21 (f1_s5 n) = n"
| "un_Z22 (f1_s5 n) = Suc n"
| "un_Z4 (f1_s5 n) = ()"
print_theorems

(* FIXME for LORENZ: ugly redundant conditions in 'code' *)
primcorec (sequential) f2_s5 :: "nat \<Rightarrow> nat \<Rightarrow> s5" where
  "m = 1 \<Longrightarrow> n = 1 \<Longrightarrow> is_Z1 (f2_s5 m n)"
| "m = 3 \<Longrightarrow> n = 3 \<Longrightarrow> is_Z3 (f2_s5 m n)"
| "m = 4 \<Longrightarrow> n = 4 \<Longrightarrow> is_Z4 (f2_s5 m n)"
| "m = 5 \<Longrightarrow> n = 5 \<Longrightarrow> is_Z5 (f2_s5 m n)"
| "m > 500 \<Longrightarrow> n > 500 \<Longrightarrow> is_Z2 (f2_s5 m n)"
| "un_Z21 (f2_s5 m n) = n"
| "un_Z22 (f2_s5 m n) = Suc n"
| "un_Z4 (f2_s5 m n) = ()"
print_theorems

primcorec (sequential) f2_s5' :: "nat \<Rightarrow> s5" where
  "m = 1 \<Longrightarrow> is_Z1 (f2_s5' m)"
| "m = 3 \<Longrightarrow> is_Z3 (f2_s5' m)"
| "m = 4 \<Longrightarrow> is_Z4 (f2_s5' m)"
| "m = 5 \<Longrightarrow> is_Z5 (f2_s5' m)"
(*| "m = 2 \<Longrightarrow> is_Z2 (f2_s5' m)" (* this is *very* slow *)*)
| "_ \<Longrightarrow> is_Z2 (f2_s5' m)" (* this works fine *)
| "un_Z21 (f2_s5' m) = m"
| "un_Z22 (f2_s5' m) = Suc m"
| "un_Z4 (f2_s5' m) = ()"

primcorec f1_stream :: "'a stream \<Rightarrow> 'a stream \<Rightarrow> 'a stream" where
  "shd (f1_stream xs ys) = shd xs"
| "stl (f1_stream xs ys) = (if some_cond then ys else f1_stream (stl xs) ys)"
print_theorems

primcorec f1'_stream :: "'a stream \<Rightarrow> 'a stream \<Rightarrow> 'a stream" where
  "stl (f1'_stream xs ys) = f1'_stream (stl xs) ys"
print_theorems

primcorec f1_llist :: "nat \<Rightarrow> nat llist" where
  "\<not> lnull (f1_llist n)"
| "lhd (f1_llist n) = n"
| "ltl (f1_llist n) = f1_llist (Suc n)"
print_theorems

primcorec f2_llist :: "('a \<Rightarrow> bool) \<Rightarrow> 'a llist \<Rightarrow> 'a llist" where
  "lnull xs \<or> \<not> p (lhd xs) \<Longrightarrow> lnull (f2_llist p xs)"
| "lhd (f2_llist p ys) = lhd ys"
| "ltl (f2_llist q xs) = f2_llist q (ltl xs)"
print_theorems

primcorec f3_llist :: "nat \<Rightarrow> bool \<Rightarrow> (nat \<times> bool) llist" where
  "\<not> lnull (f3_llist n B)"
| "lhd (f3_llist n B) = (n, B)"
| "ltl (f3_llist n B) = (let g = f3_llist (Suc n) in g (\<not> B))"
print_theorems

primcorec f4_llist :: "nat llist" where
  "\<not> lnull f4_llist"
| "lhd f4_llist = 0"
| "ltl f4_llist = f4_llist"
print_theorems

primcorec f5_llist :: "'a llist \<Rightarrow> 'a llist \<Rightarrow> 'a llist" where
  "lnull xs \<Longrightarrow> lnull ys \<Longrightarrow> lnull (f5_llist xs ys)" |
  "lnull xs \<and> \<not> lnull ys \<or> \<not> lnull xs \<Longrightarrow>
   lhd (f5_llist xs ys) =
     (if lnull xs then (case ys of LNil \<Rightarrow> undefined | LCons z _ \<Rightarrow> z) else lhd xs)" |
  "ltl (f5_llist xs ys) =
     (if lnull xs then (case ys of LNil \<Rightarrow> undefined | LCons _ zs \<Rightarrow> zs) else f5_llist (ltl xs) ys)"
print_theorems

primcorec f6_llist :: "xnat \<Rightarrow> xnat llist" where
  "\<not> lnull (f6_llist n)" |
  "lhd (f6_llist n) = n" |
  "ltl (f6_llist n) = (case n of XZero \<Rightarrow> LNil | XSuc m \<Rightarrow> f6_llist m)"

primcorec f7_llist :: "xnat \<Rightarrow> xnat llist" where
  "\<not> lnull (f7_llist n)" |
  "lhd (f7_llist n) = n" |
  "ltl (f7_llist n) = (case n of XZero \<Rightarrow> LNil | XSuc m \<Rightarrow>
     (if m = XZero then f7_llist n else
        (case m of XSuc m' \<Rightarrow> f7_llist m')))"

primcorec f1_some_passive :: "nat \<Rightarrow> (nat, 'b) some_passive" where
  "n = 0 \<Longrightarrow> is_SP2 (f1_some_passive n)"
| "un_SP2 (f1_some_passive n) = n"
| "_ \<Longrightarrow> is_SP1 (f1_some_passive n)"
| "un_SP1 (f1_some_passive n) = f1_some_passive (Suc n)"
print_theorems

primcorec f1_some_dead :: "nat \<Rightarrow> ('b, nat) some_dead" where
  "is_SD1 (f1_some_dead n)"
| "un_SD1 (f1_some_dead n) = Suc n"
print_theorems

primcorec f1_tree :: "int \<Rightarrow> int tree"
      and f1_forest :: "int \<Rightarrow> bool \<Rightarrow> int forest" where
  "i = 0 \<Longrightarrow> f1_tree i = TEmpty"
| "j \<noteq> 0 \<Longrightarrow> f1_tree j \<noteq> TEmpty"
| "un_TNode1 (f1_tree k) = k"
| "un_TNode2 (f1_tree i) = f1_forest (i + 2) True"
| "\<lbrakk>i = 0; b\<rbrakk> \<Longrightarrow> is_FNil (f1_forest i b)"
| "_ \<Longrightarrow> \<not> is_FNil (f1_forest j b)"
| "fhd (f1_forest j b) = f1_tree j"
| "ftl (f1_forest i b) = f1_forest (i + 3) (\<not> b)"
print_theorems

primcorec f2_tree :: "int \<Rightarrow> int tree" where
  "i = 0 \<Longrightarrow> f2_tree i = TEmpty"
| "j \<noteq> 0 \<Longrightarrow> f2_tree j \<noteq> TEmpty"
| "un_TNode1 (f2_tree k) = k"
| "un_TNode2 (f2_tree i) = f1_forest (i + 2) True"
print_theorems

primcorec f3_forest :: "int \<Rightarrow> bool \<Rightarrow> int forest" where
  "\<lbrakk>i = 0; b\<rbrakk> \<Longrightarrow> is_FNil (f3_forest i b)"
| "_ \<Longrightarrow> \<not> is_FNil (f3_forest j b)"
| "fhd (f3_forest j b) = f1_tree j"
| "ftl (f3_forest i b) = f3_forest (i + 3) (\<not> b)"
print_theorems

primcorec f1_x :: "nat \<Rightarrow> x"
      and f1_y :: "int \<Rightarrow> int \<Rightarrow> y"
      and f1_z :: "nat \<Rightarrow> z" where
  "is_X (f1_x _)"
| "is_Y (f1_y _ _)"
| "un_Z (f1_z n) = Suc n"
print_theorems

primcorec f1_hfset :: "bool \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> hfset" where
  "un_HFset (f1_hfset b \<mu> i) = lmap (\<lambda>(n, b). f1_hfset b n 0) (f3_llist \<mu> b)"

primcorec f2_hfset :: "bool \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> hfset"
      and f2_hfset_llist :: "nat \<Rightarrow> bool \<Rightarrow> hfset llist" where
  "un_HFset (f2_hfset b \<mu> i) = f2_hfset_llist \<mu> b"
| "\<not> lnull (f2_hfset_llist n B)"
| "lhd (f2_hfset_llist n B) = f2_hfset B n 0"
| "ltl (f2_hfset_llist n B) = f2_hfset_llist (Suc n) (\<not> B)"
print_theorems

definition rest :: "hfset llist" where "rest = undefined"
primcorec f3_hfset :: "bool \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> hfset" where
  "un_HFset (f3_hfset b \<mu> i) =
    LCons (f3_hfset b \<mu> 0)
      (LCons (f3_hfset True \<mu> 1) (LCons (undefined (Suc 0)) rest))"
print_theorems

primcorec f1_btree :: "'a llist \<Rightarrow> 'a btree" where
  "lnull xs \<Longrightarrow> f1_btree xs = LNull"
| "lb (f1_btree xs) = lhd xs"
| "bsub (f1_btree xs) =
    map_xpair (\<lambda>g. f1_btree (g xs)) (\<lambda>h. f1_btree (h (h xs))) (XPair id ltl)"
print_theorems

primcorec f2_btree :: "'a llist \<Rightarrow> 'a btree" where
  "lnull xs \<Longrightarrow> xs = LNil \<Longrightarrow> f2_btree xs = LNull"
| "lb (f2_btree xs) = lhd xs"
| "bsub (f2_btree xs) = XPair (f2_btree xs) (f2_btree (ltl (ltl xs)))"
print_theorems


(* deriving C-view from K-view *)

lemma f2_btree_code: "f2_btree xs =
  (if lnull xs \<and> xs = LNil then LNull
   else LNode (lhd xs) (XPair (f2_btree xs) (f2_btree (ltl (ltl xs)))))"
by (tactic {* mk_primcorec_raw_code_tac @{context} [] [] [] [] [2, 1] @{thms f2_btree.ctr} NONE *})

lemma f2_btree_code': "f2_btree xs =
  (if lnull xs \<and> xs = LNil then LNull
   else if \<not> lnull xs then LNode (lhd xs) (XPair (f2_btree xs) (f2_btree (ltl (ltl xs))))
   else Code.abort (STR ''f2_btree'') (\<lambda>_. f2_btree xs))"
by (tactic {* mk_primcorec_raw_code_tac @{context} [] [] [] [] [2, 1, 0]
  @{thms f2_btree.ctr Code.abort_def[symmetric]} NONE *})

primcorec f3_btree :: "'a llist \<Rightarrow> 'a btree" where
  "lnull xs \<Longrightarrow> f3_btree xs = LNull"
| "lb (f3_btree xs) = lhd xs"
| "bsub (f3_btree xs) = XPair (f3_btree xs) LNull"
print_theorems

primcorec fk :: "nat \<Rightarrow> 'a \<Rightarrow> 'b \<Rightarrow> ('a, 'b) k"
      and fl :: "nat \<Rightarrow> nat \<Rightarrow> 'b \<Rightarrow> ('a, 'b) l" where
  "n = 0 \<Longrightarrow> fk n a b = K1"
| "n > 10 \<Longrightarrow> \<not> n < 20 \<Longrightarrow> is_K3 (fk n a b)"
| "un_K3 (fk n a b) = fk (Suc n) a b"
| "n = 5 \<Longrightarrow> is_K4 (fk n a b)"
| "un_K4 (fk n a b) = map (fl n (Suc n)) [b, b]"
| "un_K21 (fk _ a _) = a"
| "un_K22 (fk _ _ b) = b"
| "m > 2 \<Longrightarrow> n > 3 \<Longrightarrow> m > n \<Longrightarrow> \<not> is_L1 (fl m n b)"
| "un_L2 (fl m n b) = undefined m n b"
| "n > m \<Longrightarrow> is_L1 (fl m n b)"
| "un_L1 (fl m n b) = (if m = 0 then K1 else if n = 0 then fk n undefined b else K4 [])"
print_theorems

end
