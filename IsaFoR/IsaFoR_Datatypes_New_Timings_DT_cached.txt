generic_assm_proof (old 5s, new 36s)

Construction of BNFs: 21.52 s 
Normalization & sealing of BNFs: 2.43 s 
Extracted terms & thms: 176 ms 
Checked nonemptiness: 0 ms 
Derived simple theorems: 103 ms 
Algebra definition & thms: 99 ms 
Proved nonemptiness: 10 ms 
Morphism definition & thms: 335 ms 
Isomorphism definition & thms: 22 ms 
Copy thms: 72 ms 
Bounds: 27 ms 
min_algs definition & thms: 510 ms 
Minimal algebra definition & thms: 178 ms 
Initiality definition & thms: 676 ms 
THE TYPEDEFs & Rep/Abs thms: 284 ms 
ctor definitions & thms: 67 ms 
fold definitions & thms: 62 ms 
dtor definitions & thms: 62 ms 
rec definitions & thms: 85 ms 
induction: 13 ms 
bnf constants for the new datatypes: 564 ms 
map functions for the new datatypes: 23 ms 
set functions for the new datatypes: 126 ms 
helpers for BNF properties: 93 ms 
registered new datatypes as BNFs: 247 ms 
relator induction: 77 ms 
FP construction in total: 4.09 s 
Constructors, discriminators, selectors, etc., for the new datatype: 8.75 s
----------------------------------------------------------------------------------------------------

assm (old 3.8s, new 17s)

Construction of BNFs: 8.74 s 
Normalization & sealing of BNFs: 1.58 s 
Extracted terms & thms: 85 ms 
Checked nonemptiness: 0 ms 
Derived simple theorems: 48 ms 
Algebra definition & thms: 83 ms 
Proved nonemptiness: 4 ms 
Morphism definition & thms: 293 ms 
Isomorphism definition & thms: 11 ms 
Copy thms: 23 ms 
Bounds: 24 ms 
min_algs definition & thms: 257 ms 
Minimal algebra definition & thms: 138 ms 
Initiality definition & thms: 381 ms 
THE TYPEDEFs & Rep/Abs thms: 200 ms 
ctor definitions & thms: 60 ms 
fold definitions & thms: 72 ms 
dtor definitions & thms: 68 ms 
rec definitions & thms: 91 ms 
induction: 12 ms 
bnf constants for the new datatypes: 406 ms 
map functions for the new datatypes: 18 ms 
set functions for the new datatypes: 40 ms 
helpers for BNF properties: 44 ms 
registered new datatypes as BNFs: 49 ms 
relator induction: 50 ms 
FP construction in total: 2.53 s 
Constructors, discriminators, selectors, etc., for the new datatype: 4.57 s
----------------------------------------------------------------------------------------------------

dp_nontermination_proofs and friends (old 350s, new 289s)

Construction of BNFs: 46.56 s 
Normalization & sealing of BNFs: 12.23 s 
Extracted terms & thms: 443 ms 
Checked nonemptiness: 0 ms 
Derived simple theorems: 202 ms 
Algebra definition & thms: 266 ms 
Proved nonemptiness: 63 ms 
Morphism definition & thms: 1.07 s 
Isomorphism definition & thms: 51 ms 
Copy thms: 128 ms 
Bounds: 560 ms 
min_algs definition & thms: 112.92 s 
Minimal algebra definition & thms: 14.71 s 
Initiality definition & thms: 25.68 s 
THE TYPEDEFs & Rep/Abs thms: 11.97 s 
ctor definitions & thms: 3.15 s 
fold definitions & thms: 1.21 s 
dtor definitions & thms: 1.71 s 
rec definitions & thms: 1.88 s 
induction: 591 ms 
bnf constants for the new datatypes: 5.16 s 
map functions for the new datatypes: 160 ms 
set functions for the new datatypes: 370 ms 
helpers for BNF properties: 708 ms 
registered new datatypes as BNFs: 1.03 s 
relator induction: 1.94 s 
FP construction in total: 187.97 s 
Constructors, discriminators, selectors, etc., for the new datatype: 42.53
----------------------------------------------------------------------------------------------------

dp_termination_proofs and friends (old 895s, new 486s)

Construction of BNFs: 168.2 s 
Normalization & sealing of BNFs: 17.35 s 
Extracted terms & thms: 720 ms 
Checked nonemptiness: 0 ms 
Derived simple theorems: 468 ms 
Algebra definition & thms: 446 ms 
Proved nonemptiness: 83 ms 
Morphism definition & thms: 1.96 s 
Isomorphism definition & thms: 117 ms 
Copy thms: 471 ms 
Bounds: 605 ms 
min_algs definition & thms: 29.0 s 
Minimal algebra definition & thms: 12.89 s 
Initiality definition & thms: 49.82 s 
THE TYPEDEFs & Rep/Abs thms: 70.48 s 
ctor definitions & thms: 4.44 s 
fold definitions & thms: 1.68 s 
dtor definitions & thms: 2.99 s 
rec definitions & thms: 3.65 s 
induction: 527 ms 
bnf constants for the new datatypes: 4.52 s 
map functions for the new datatypes: 132 ms 
set functions for the new datatypes: 696 ms 
helpers for BNF properties: 679 ms 
registered new datatypes as BNFs: 845 ms 
relator induction: 1.68 s 
FP construction in total: 190.73 s 
Constructors, discriminators, selectors, etc., for the new datatype: 110.22 s
