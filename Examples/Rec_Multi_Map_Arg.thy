theory Rec_Multi_Map_Arg
imports "~~/src/HOL/BNF/BNF"
begin


declare [[bnf_note_all]]

ML {* open BNF_FP_Rec_Sugar_Tactics *}

typedecl real

hide_fact Predicate.map.id


datatype_new 'a tree = Node "('a tree \<times> 'a tree) list"
term tree_rec

consts size :: "'a tree \<Rightarrow> nat"
consts size_pair :: "'a tree \<times> 'a tree \<Rightarrow> nat"


primrec_new f_1Aa :: "'a tree \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> real" where
  "f_1Aa (Node tps) n i = undefined i n (map (map_pair f_1Aa f_1Aa) tps)"
print_theorems


(*primrec_new tree_rec_1Ab1 and tree_rec_1Ab2 where
  "tree_rec_1Ab1 f1 f2 (Node x) =
  f1 (map (map_pair (\<lambda>t. (t, tree_rec_1Ab2 f1 f2 t)) (\<lambda>t. (t, tree_rec_1Ab1 f1 f2 t))) x)" |
  "tree_rec_1Ab2 f1 f2 (Node x) =
  f2 (map (map_pair (\<lambda>t. (t, tree_rec_1Ab1 f1 f2 t)) (\<lambda>t. (t, tree_rec_1Ab1 f1 f2 t))) x)"
thm f_1Ab1_def f_1Ab2_def*)

axiomatization tree_rec_1Ab1 and tree_rec_1Ab2 where
tree_rec_1Ab1: "tree_rec_1Ab1 f1 f2 (Node x) =
  f1 (map (map_pair (\<lambda>t. (t, tree_rec_1Ab2 f1 f2 t)) (\<lambda>t. (t, tree_rec_1Ab1 f1 f2 t))) x)" and
tree_rec_1Ab2: "tree_rec_1Ab2 f1 f2 (Node x) =
  f2 (map (map_pair (\<lambda>t. (t, tree_rec_1Ab1 f1 f2 t)) (\<lambda>t. (t, tree_rec_1Ab1 f1 f2 t))) x)"

definition f_1Ab1 :: "'a tree \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> real" where
"f_1Ab1 = tree_rec_1Ab1 (\<lambda>pps. \<lambda>(n :: nat) (i :: int). undefined i n pps :: real)
   (\<lambda>pps. undefined pps :: bool)"

definition f_1Ab2 :: "'a tree \<Rightarrow> bool" where
"f_1Ab2 = tree_rec_1Ab2 (\<lambda>pps. \<lambda>(n :: nat) (i :: int). undefined i n pps :: real)
   (\<lambda>pps. undefined pps :: bool)"

lemma "f_1Ab1 (Node tps) n i = undefined i n
  (map (map_pair (\<lambda>t. (t, f_1Ab2 t)) (\<lambda>t. (t, f_1Ab1 t))) tps)"
by (tactic {* mk_primrec_tac @{context} 2 @{thms map.id[unfolded id_def] map_pair.id[unfolded id_def]} @{thms list.map_comp prod.map_comp} @{thms f_1Ab1_def f_1Ab2_def} @{thm tree_rec_1Ab1} *})

lemma "f_1Ab2 (Node tps) = undefined (map (map_pair (\<lambda>t. (t, f_1Ab1 t)) (\<lambda>t. (t, f_1Ab1 t))) tps)"
by (tactic {* mk_primrec_tac @{context} 0 @{thms map.id[unfolded id_def] map_pair.id[unfolded id_def]} @{thms list.map_comp prod.map_comp} @{thms f_1Ab1_def f_1Ab2_def} @{thm tree_rec_1Ab2} *})


primrec_new f_1B :: "'a tree \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> real" where
  "f_1B (Node tps) n i = undefined i n (map (map_pair (\<lambda> t. (t, f_1B t)) (\<lambda> t. (t, f_1B t))) tps)"
print_theorems

primrec_new f_1C :: "'a tree \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> real" where
  "f_1C (Node tps) n i = undefined i n (map (map_pair id id) tps)"
print_theorems

primrec_new f_2A :: "'a tree \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> real" where
  "f_2A (Node tps) n i = undefined i n (map (map_pair ((\<lambda>g. g (Suc 0) (-1)) o f_2A) ((\<lambda>g. g (Suc 0) (-1)) o f_2A)) tps)"
print_theorems

primrec_new f_2B :: "'a tree \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> real" where
  "f_2B (Node tps) n i = undefined i n
  (map (map_pair ((\<lambda>(t, g). g (size t) (-1)) o (\<lambda>t. (t, f_2B t)))
     ((\<lambda>(t, g). g (size t) (-1)) o (\<lambda>t. (t, f_2B t)))) tps)"
print_theorems

primrec_new f_2C :: "'a tree \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> real" where
  "f_2C (Node tps) n i = undefined i n (map (map_pair (size o (\<lambda>x. x)) (size o (\<lambda>x. x))) tps)"
print_theorems

primrec_new f_3A :: "'a tree \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> real" where
  "f_3A (Node tps) n i = undefined i n
  (map (map_pair (\<lambda>t. f_3A t (Suc 0) (-1)) (\<lambda>t. f_3A t 0 (-2))) tps)"
print_theorems

primrec_new f_3B :: "'a tree \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> real" where
  "f_3B (Node tps) n i = undefined i n
  (map (map_pair (\<lambda>t. f_3B t (size t) (-1)) (\<lambda>t. f_3B t (size t) (-2))) tps)"
print_theorems

primrec_new f_3Ca :: "'a tree \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> real" where
  "f_3Ca (Node tps) n i = undefined i n (map size_pair tps)"
print_theorems

primrec_new f_3Cb :: "'a tree \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> real" where
  "f_3Cb (Node tps) n i = undefined i n (map (map_pair size size) tps)"
print_theorems


datatype_new 'a tree3 = Node3 "('a tree3 \<times> 'a tree3 \<times> 'a tree3) list"

primrec_new f :: "'a tree3 \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> real" where
  "f (Node3 tpps) = undefined (map (map_pair f (map_pair f id)) tpps)"
print_theorems

end

