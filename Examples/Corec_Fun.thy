theory Corec_Fun
imports BNF_GFP
begin

codatatype 'a language = Lang (\<oo>: bool) (\<dd>: "'a \<Rightarrow> 'a language")
term corec_language
thm language.corec

primcorec unc_Zero0 where
  "\<oo> (unc_Zero0 u) = False" |
  "\<dd> (unc_Zero0 u) = unc_Zero0 o (\<lambda>_. ())"

primcorec unc_Zero where
  "\<oo> (unc_Zero u) = False" |
  "\<dd> (unc_Zero u) = (\<lambda>_. unc_Zero ())"

primcorec Zero where
  "\<oo> Zero = False" |
  "\<dd> Zero = (\<lambda>_. Zero)"

primcorec One where
  "\<oo> One = True"
| "\<dd> One = (\<lambda>_. Zero)"

primcorec Atom where
  "\<oo> (Atom a) = False"
| "\<dd> (Atom a) = (\<lambda>b. if a = b then One else Zero)"

primcorec unc_Plus where
  "\<oo> (unc_Plus rs) = (\<oo> (fst rs) \<or> \<oo> (snd rs))"
| "\<dd> (unc_Plus rs) = unc_Plus o (\<lambda>a. (\<dd> (fst rs) a, \<dd> (snd rs) a))"

primcorec Plus0 where
  "\<oo> (Plus0 r s) = (\<oo> r \<or> \<oo> s)"
| "\<dd> (Plus0 r s) = unc_Plus o (\<lambda>a. (\<dd> r a, \<dd> s a))"

primcorec Plus1 where
  "\<oo> (Plus1 r s) = (\<oo> r \<or> \<oo> s)"
| "\<dd> (Plus1 r s) = (\<lambda>(r, s). Plus1 r s) o (\<lambda>a. (\<dd> r a, \<dd> s a))"

primcorec Not0 where
  "\<oo> (Not0 r) = (\<not> \<oo> r)"
| "\<dd> (Not0 r) = Not0 o \<dd> r"

primcorec Not where
  "\<oo> (Not r) = (\<not> \<oo> r)"
| "\<dd> (Not r) = (\<lambda>a. Not (\<dd> r a))"

primcorec Plus where
  "\<oo> (Plus r s) = (\<oo> r \<or> \<oo> s)"
| "\<dd> (Plus r s) = (\<lambda>a. Plus (\<dd> r a) (\<dd> s a))"
print_theorems

end
