theory SCCs
imports "~~/src/HOL/BNF_Least_Fixpoint"
(* imports "~~/src/HOL/Main" *)
begin

ML {*
open BNF_FP_Rec_Sugar_Util
open BNF_FP_Util
*}

ML {*
fun mk_edges Xs ctrXs_Tsss =
  let
    fun edges i Ts = fold (fn T => fn Gs =>
      let val j = find_index (curry (op =) T) Xs in
        if j = ~1 then Gs else (i,j) :: Gs
      end) Ts
  in
    fold_index (uncurry (fold o edges)) ctrXs_Tsss []
  end;

fun mk_graph nn edges =
  Int_Graph.empty
  |> fold (fn kk => Int_Graph.new_node (kk, ())) (0 upto nn - 1)
  |> fold Int_Graph.add_edge edges;

fun str_of_scc fp fpT_names =
  co_prefix fp ^ "datatype " ^
  space_implode " and " (map (suffix " = \<dots>" o Long_Name.base_name) fpT_names);

fun warn _ _ [_] = ()
  | warn fp fpT_names sccs =
    warning ("Defined types not fully mutually " ^ co_prefix fp ^ "recursive\n\
      \Alternative specification:\n" ^
      cat_lines (map (prefix "  " o str_of_scc fp o map (nth fpT_names)) sccs));
*}

(* warning: [{a1}, {a2}] *)
(* long version:
   datatype a1 = \<dots>
   datatype a2 = \<dots>
*)
datatype
  a1 = A1 and
  a2 = A2

ML {*
val edges = mk_edges ["'a1", "'a2"] [[[]], [[]]]
val G = mk_graph 2 edges;
val sccs = rev (map (sort int_ord) (Int_Graph.strong_conn G));
val _ = warn Least_FP [@{type_name a1}, @{type_name a2}] sccs;
*}
  
(* warning: [{b2}, {b1}] *)
(* long version:
   datatype b2 = \<dots>
   datatype b1 = \<dots>
*)
datatype
  b1 = B1 b2 and
  b2 = B2

ML {*
val edges = mk_edges ["'b1", "'b2"] [[["'b2"]], [[]]]
val G = mk_graph 2 edges;
val sccs = rev (map (sort int_ord) (Int_Graph.strong_conn G));
val _ = warn Least_FP [@{type_name b1}, @{type_name b2}] sccs;
*}

(* OK *)
datatype
  c1 = C1 c2 and
  c2 = C2 c1 | C2'

ML {*
val edges = mk_edges ["'c1", "'c2"] [[["'c2"]], [["'c1"], []]]
val G = mk_graph 2 edges;
val sccs = rev (map (sort int_ord) (Int_Graph.strong_conn G));
val _ = warn Least_FP [@{type_name c1}, @{type_name c2}] sccs;
*}

(* warning: [{d1, d2}, {d3, d4}] *)
(* long version:
   datatype d1 = \<dots> and d2 = \<dots>
   datatype d3 = \<dots> and d4 = \<dots>
*)
datatype
  d1 = D1 d2 and
  d2 = D2 d1 | D2' and
  d3 = D3 d4 and
  d4 = D4 d3 | D4'

ML {*
val edges = mk_edges ["'d1", "'d2", "'d3", "'d4"] [[["'d2"]], [["'d1"], []], [["'d4"]], [["'d3"], []]]
val G = mk_graph 4 edges;
val sccs = rev (map (sort int_ord) (Int_Graph.strong_conn G));
val _ = warn Least_FP [@{type_name d1}, @{type_name d2}, @{type_name d3}, @{type_name d4}] sccs;
*}

(* warning: [{e3, e4}, {e1, e2}] *)
(* long version:
   datatype e3 = \<dots> and e4 = \<dots>
   datatype e1 = \<dots> and e2 = \<dots>
*)
datatype
  e1 = E1 e2 e4 and
  e2 = E2 e1 | E2' and
  e3 = E3 e4 and
  e4 = E4 e3 | E4'

ML {*
val edges = mk_edges ["'e1", "'e2", "'e3", "'e4"] [[["'e2", "'e4"]], [["'e1"], []], [["'e4"]], [["'e3"], []]]
val G = mk_graph 4 edges;
val sccs = rev (map (sort int_ord) (Int_Graph.strong_conn G));
val _ = warn Least_FP [@{type_name e1}, @{type_name e2}, @{type_name e3}, @{type_name e4}] sccs;
*}

(* OK *)
datatype
  f1 = F1 f2 f4 and
  f2 = F2 f1 | F2' and
  f3 = F3 f4 f2 and
  f4 = F4 f3 | F4'

ML {*
val edges = mk_edges ["'f1", "'f2", "'f3", "'f4"] [[["'f2", "'f4"]], [["'f1"], []], [["'f4", "'f2"]], [["'f3"], []]]
val G = mk_graph 4 edges;
val sccs = rev (map (sort int_ord) (Int_Graph.strong_conn G));
val _ = warn Least_FP [@{type_name f1}, @{type_name f2}, @{type_name f3}, @{type_name f4}] sccs;
*}

end
