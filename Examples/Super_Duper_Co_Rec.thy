theory Super_Duper_Co_Rec
imports "~~/src/HOL/BNF/BNF"
begin

declare [[bnf_note_all]]


datatype_new 'a lt = null: Nil | Cons (hd: 'a) (tl: "'a lt")

term lt_ctor_rec
thm lt.ctor_rec

definition lt_ctor_rec' where
"lt_ctor_rec' stop end s =
   lt_ctor_rec (\<lambda>z. let xs = lt_ctor (pre_lt_map id fst z) in if stop xs then end xs else s z)"

lemma "stop xs \<Longrightarrow> lt_ctor_rec' stop end s xs = end xs"
unfolding lt_ctor_rec'_def
apply (case_tac xs rule: lt.ctor_exhaust)
apply hypsubst
unfolding lt.ctor_rec Let_def pre_lt.map_comp fst_convol pre_lt.map_id id_o
by auto

lemma "\<not> stop (lt_ctor y) \<Longrightarrow>
  lt_ctor_rec' stop end s (lt_ctor y) = s (pre_lt_map id <id, lt_ctor_rec' stop end s> y)"
unfolding lt_ctor_rec'_def
unfolding lt.ctor_rec Let_def pre_lt.map_comp fst_convol pre_lt.map_id id_o
by auto


term lt_rec
thm lt.rec

definition lt_rec' where
"lt_rec' stop end nil cons =
   lt_rec (if stop Nil then end Nil else nil)
     (\<lambda>y ys a. let xs = Cons y ys in if stop xs then end xs else cons y ys a)"

lemma "stop xs \<Longrightarrow> lt_rec' stop end nil cons xs = end xs"
unfolding lt_rec'_def
by (case_tac xs) auto

lemma "\<not> stop Nil \<Longrightarrow> lt_rec' stop end nil cons Nil = nil"
unfolding lt_rec'_def
by auto

lemma "\<not> stop (Cons y ys) \<Longrightarrow>
  lt_rec' stop end nil cons (Cons y ys) = cons y ys (lt_rec' stop end nil cons ys)"
unfolding lt_rec'_def
by auto


codatatype 'a llt = null: Nil | Cons (hd: 'a) (tl: "'a llt")

term llt_dtor_corec
thm llt.dtor_corec

definition llt_dtor_corec' where
"llt_dtor_corec' stop end s =
   llt_dtor_corec (\<lambda>a. if stop a then pre_llt_map id Inl (llt_dtor (end a)) else s a)"

lemma "stop a \<Longrightarrow> llt_dtor_corec' stop end s a = end a"
unfolding llt_dtor_corec'_def
apply (rule iffD1[OF llt.dtor_inject])
unfolding llt.dtor_corec
apply (simp only: if_P)
unfolding pre_llt.map_comp pre_llt.map_id sum_case_o_inj id_o
by (rule refl)

lemma "\<not> stop a \<Longrightarrow>
  llt_dtor (llt_dtor_corec' stop end s a) = pre_llt_map id (sum_case id (llt_dtor_corec' stop end s)) (s a)"
unfolding llt_dtor_corec'_def
unfolding llt.dtor_corec
by (simp only: if_False if_P)

end
