theory rec_transfer
(* imports "~~/src/HOL/Transfer" *)
imports "~~/src/HOL/BNF_Greatest_Fixpoint"
begin

context lifting_syntax (* enables the ===> syntax for rel_fun *)
begin

declare [[bnf_note_all = true]]

datatype ('a, 'b) pair = Pair 'a 'b
datatype (dead 'a, 'b) dead_pair = Dead_pair 'a 'b

datatype 'a list
  = nil: Nil
  | Cons (head: 'a) (tail: "'a list")
  where
    "tail Nil = Nil"

datatype 'a big_list
  = nil: Nil
  | Cons (head: 'a) 'a (tail: "'a big_list")
  where
    "tail Nil = Nil"

codatatype 'a llist
  = LNil
  | LCons 'a "'a llist"

codatatype 'a big_llist
  = LBNil
  | LBCons 'a 'a "'a big_llist"

datatype 'a tree
  = is_node: Node (val: 'a) (children: "'a tree list")

codatatype 'a ltree
  = is_node: LNode (val: 'a) (children: "'a ltree llist")

datatype ('a, 'b) example
  = is_N1: N1 'a
  | is_N2: N2 'b
  | is_N3: N3 'a 'b 'b 'b
  | is_N4: N4
  | is_ConsA: ConsA 'a "('a, 'b) example"
  | is_ConsB: ConsB 'b "('a, 'b) example"

codatatype ('a, 'b) lexample
  = is_LN1: LN1 'a
  | is_LN2: LN2 'b
  | is_LN3: LN3 'a 'b
  | is_LN4: LN4
  | is_LConsA: LConsA 'a "('a, 'b) lexample"
  | is_LConsB: LConsB 'b "('a, 'b) lexample"

datatype 'a tree_mutual = is_TNode: TNode 'a "'a forest_mutual"
         and 'a forest_mutual = is_FNil: FNil | is_FCons: FCons "'a tree_mutual" "'a forest_mutual"

codatatype 'a ltree_mutual = is_LTNode: LTNode 'a "'a lforest_mutual"
         and 'a lforest_mutual = is_LFNil: LFNil | is_LFCons: LFCons "'a ltree_mutual" "'a lforest_mutual"

datatype ('a, 'b) F1 = is_F1: F1 "'a \<Rightarrow> 'b"
codatatype ('a, 'b) lF1 = is_lF1: lF1 "'a \<Rightarrow> 'b"
codatatype ('a, 'b) lF1' = is_lF1: lF1 "('a, 'b) dead_pair"

datatype ('a, 'b, 'c) F2 = is_F2: F2 "'a \<Rightarrow> 'b \<Rightarrow> 'c"
codatatype ('a, 'b, 'c) lF2 = is_lF2: lF2 "'a \<Rightarrow> 'b \<Rightarrow> 'c"

datatype ('t, 'id) F3 = F3 "'t \<Rightarrow> nat" | Bar 'id
codatatype ('t, 'id) lF3 = lF3 "'t \<Rightarrow> nat" | lBar 'id
codatatype ('t, 'id) lF4 = lF4 "('t, nat) dead_pair" 'id
codatatype 'a lF5 = lF5 'a "'a lF5" 'a

datatype 'a A1 = is_A1: A1 nat
         and 'a A2 = is_A2: A2 'a

codatatype 'a lA1 = is_lA1: lA1 nat
       and 'a lA2 = is_lA2: lA2 'a

codatatype 'a live_direct = LD 'a
codatatype 'a live_indirect = DI "('a, nat) pair"
codatatype ('a, 'b) live_indirect' = LI' "('a, 'b) pair"
codatatype 'a nested_corecursive = NC 'a "(nat, 'a nested_corecursive) pair"
codatatype 'a live_indirect_and_corecursive_indirect = LI_CI "('a, 'a live_indirect_and_corecursive_indirect) pair"
codatatype 'a mutually_corec_direct = MCD 'a "'a mutually_corec_direct"
codatatype 'a mutually_corec_indirect1 = MCI1 'a 'a "(nat, 'a mutually_corec_indirect2) pair"
       and 'a mutually_corec_indirect2 = MCI2 'a 'a"(nat, 'a mutually_corec_indirect2) pair"

codatatype 'a three_args = TA 'a 'a 'a
codatatype 'a five_args = FA 'a 'a 'a 'a 'a

datatype ('a, 'b) ite = ITE "'a \<Rightarrow> bool" "'a \<Rightarrow> 'b" "'a \<Rightarrow> 'b"
codatatype 'a foo = Foo nat nat
codatatype bar = X1 nat | X2 

term corec_bar

(* Hand written *)
lemma
  "((R ===> T) ===> (S ===> T) ===> rel_sum R S ===> T) case_sum case_sum"
  unfolding rel_fun_def rel_sum_def by (auto split: sum.splits)

lemma case_sum_transfer:
  "rel_fun (rel_fun R T) (rel_fun (rel_fun S T) (rel_fun (rel_sum R S) T)) case_sum case_sum"
  unfolding rel_fun_def rel_sum_def by (auto split: sum.splits)

lemma
  "((R ===> T) ===> (S ===> U) ===> rel_sum R S ===> rel_sum T U) map_sum map_sum"
  unfolding rel_fun_def rel_sum_def by (auto split: sum.splits)

lemma map_sum_transfer:
  "rel_fun (rel_fun R T) (rel_fun (rel_fun S U) (rel_fun (rel_sum R S) (rel_sum T U))) map_sum map_sum"
  unfolding rel_fun_def rel_sum_def by (auto split: sum.splits)

lemma
  "((R ===> S) ===> (R ===> T) ===> R ===> rel_prod S T) BNF_Def.convol BNF_Def.convol"
  unfolding rel_prod_def rel_fun_def convol_def by auto

lemma convol_transfer:
  "rel_fun (rel_fun R S) (rel_fun (rel_fun R T) (rel_fun R (rel_prod S T))) BNF_Def.convol BNF_Def.convol"
  unfolding rel_prod_def rel_fun_def convol_def by auto

lemma Inl_transfer:
  "(S ===> rel_sum S T) Inl Inl"
  by auto

lemma Inr_transfer:
  "(T ===> rel_sum S T) Inr Inr"
  by auto

ML {*
open Ctr_Sugar_Tactics
open BNF_Util

fun mk_ctor_rec_transfer_tac ctxt n (* # mutual *) m (* # active *) ctor_rec_defs ctor_fold_transfers pre_T_map_transfers
    ctor_rels =
  let
    val rel_funD = @{thm rel_funD};
    fun rel_funD_n n = funpow n (fn thm => thm RS rel_funD);
    val rel_funD_n_rotated = rotate_prems ~1 oo rel_funD_n;
  in
    CONJ_WRAP (fn (ctor_rec_def, ctor_fold_transfer) =>
      REPEAT_DETERM (HEADGOAL (rtac @{thm rel_funI})) THEN
      unfold_thms_tac ctxt [ctor_rec_def, o_apply] THEN
      HEADGOAL (rtac @{thm rel_funD[OF snd_transfer]} THEN'
        etac (rel_funD_n_rotated (n + 1) ctor_fold_transfer) THEN'
        EVERY' (map2 (fn pre_T_map_transfer => fn ctor_rel =>
          etac (rel_funD_n_rotated 2 @{thm convol_transfer}) THEN'
          rtac (rel_funD_n_rotated 2 @{thm comp_transfer}) THEN'
          rtac (rel_funD_n (m + n) pre_T_map_transfer) THEN'
          REPEAT_DETERM_N m o rtac @{thm id_transfer} THEN'
          REPEAT_DETERM_N n o rtac @{thm fst_transfer} THEN'
          rtac @{thm rel_funI} THEN'
          etac (ctor_rel RS iffD2)) pre_T_map_transfers ctor_rels)))
      (ctor_rec_defs ~~ ctor_fold_transfers)
  end;
*}

thm list.ctor_rec_transfer
lemma list_ctor_rec_transfer:
  "((rel_pre_list R (rel_prod (rel_list R) S) ===> S) ===> rel_list R ===> S) ctor_rec_list ctor_rec_list"
  apply (tactic {* mk_ctor_rec_transfer_tac @{context} 1 1
    @{thms ctor_rec_list_def}
    @{thms list.ctor_fold_transfer}
    @{thms pre_list.map_transfer}
    @{thms list.ctor_rel} *})
(*   apply (rule rel_funI)+
  unfolding ctor_rec_list_def o_apply
  apply (rule rel_funD[OF snd_transfer])
  apply (erule rel_funD[OF rel_funD[OF list.ctor_fold_transfer], rotated -1])
  apply (erule rel_funD[OF rel_funD[OF convol_transfer], rotated -1])
  apply (rule rel_funD[OF rel_funD[OF comp_transfer], rotated -1])
  apply (rule rel_funD[OF rel_funD[OF pre_list.map_transfer], rotated -1])
  apply (rule fst_transfer)
  apply (rule id_transfer)
  apply (rule rel_funI)
  apply (erule iffD2[OF list.ctor_rel]) *)
  done


thm big_list.ctor_rec_transfer
lemma big_list_ctor_rec_transfer:
  "((rel_pre_big_list R (rel_prod (rel_big_list R) S) ===> S) ===> rel_big_list R ===> S) ctor_rec_big_list ctor_rec_big_list"
  apply (tactic {* mk_ctor_rec_transfer_tac @{context} 1 1
    @{thms ctor_rec_big_list_def}
    @{thms big_list.ctor_fold_transfer}
    @{thms pre_big_list.map_transfer}
    @{thms big_list.ctor_rel} *})
  done

thm tree.ctor_rec_transfer
lemma tree_ctor_rec_transfer:
  "((rel_pre_tree R (rel_prod (rel_tree R) S) ===> S) ===> rel_tree R ===> S) ctor_rec_tree ctor_rec_tree"
  apply (tactic {* mk_ctor_rec_transfer_tac @{context} 1 1
    @{thms ctor_rec_tree_def}
    @{thms tree.ctor_fold_transfer}
    @{thms pre_tree.map_transfer}
    @{thms tree.ctor_rel} *})
  done

thm example.ctor_rec_transfer
lemma example_ctor_rec_transfer:
  "((rel_pre_example R1 R2 (rel_prod (rel_example R1 R2) S) ===> S) ===> rel_example R1 R2 ===> S) ctor_rec_example ctor_rec_example"
  apply (tactic {* mk_ctor_rec_transfer_tac @{context} 1 2
    @{thms ctor_rec_example_def}
    @{thms example.ctor_fold_transfer}
    @{thms pre_example.map_transfer}
    @{thms example.ctor_rel} *})
(*   apply (rule rel_funI)+
  unfolding ctor_rec_example_def o_apply
  apply (rule rel_funD[OF snd_transfer])
  apply (erule rel_funD[OF rel_funD[OF example.ctor_fold_transfer], rotated])
  apply (erule rel_funD[OF rel_funD[OF convol_transfer], rotated])
  apply (rule rel_funD[OF rel_funD[OF comp_transfer], rotated])
  apply (rule rel_funD[OF rel_funD[OF rel_funD[OF pre_example.map_transfer]], rotated -1])
  apply (rule fst_transfer)
  apply ((rule id_transfer)+) [2]
  apply (rule rel_funI)
  apply (erule iffD2[OF example.ctor_rel]) *)
  done

term"((rel_pre_tree_mutual R Q (rel_prod (rel_forest_mutual R) S) ===> S) ===>
  (rel_pre_forest_mutual R (rel_prod (rel_tree_mutual R) S) (rel_prod (rel_forest_mutual R) S) ===> S) ===>
  rel_tree_mutual R ===> S) ctor_rec_tree_mutual ctor_rec_tree_mutual"

thm tree_mutual_forest_mutual.ctor_rec_transfer
lemma tree_mutual_forest_mutual_ctor_rec_transfer:
  "((rel_pre_tree_mutual R (rel_prod (rel_tree_mutual R) S1) (rel_prod (rel_forest_mutual R) S2) ===> S1) ===>
    (rel_pre_forest_mutual R (rel_prod (rel_tree_mutual R) S1) (rel_prod (rel_forest_mutual R) S2) ===> S2) ===> rel_tree_mutual R ===> S1)
    ctor_rec_tree_mutual ctor_rec_tree_mutual \<and>
   ((rel_pre_tree_mutual R (rel_prod (rel_tree_mutual R) S1) (rel_prod (rel_forest_mutual R) S2) ===> S1) ===>
    (rel_pre_forest_mutual R (rel_prod (rel_tree_mutual R) S1) (rel_prod (rel_forest_mutual R) S2) ===> S2) ===> rel_forest_mutual R ===> S2)
    ctor_rec_forest_mutual ctor_rec_forest_mutual"
apply (tactic {* mk_ctor_rec_transfer_tac @{context} 2 1
  @{thms ctor_rec_tree_mutual_def ctor_rec_forest_mutual_def}
  @{thms tree_mutual_forest_mutual.ctor_fold_transfer}
  @{thms pre_tree_mutual.map_transfer pre_forest_mutual.map_transfer}
  @{thms tree_mutual.ctor_rel forest_mutual.ctor_rel} *})
  (* apply (rule conjI)
  apply (rule rel_funI)+
  unfolding ctor_rec_tree_mutual_def o_apply
  apply (rule rel_funD[OF snd_transfer])
  apply (erule rel_funD[OF rel_funD[OF rel_funD[OF tree_mutual_forest_mutual.ctor_fold_transfer(1)]], rotated -1])

  apply (erule rel_funD[OF rel_funD[OF convol_transfer], rotated -1])
  apply (rule rel_funD[OF rel_funD[OF comp_transfer], rotated -1])
  apply (rule rel_funD[OF rel_funD[OF rel_funD[OF pre_tree_mutual.map_transfer]]])
  apply (rule id_transfer)+
  apply (rule fst_transfer)+
  apply (rule rel_funI)
  apply (erule iffD2[OF tree_mutual.ctor_rel])

  apply (erule rel_funD[OF rel_funD[OF convol_transfer], rotated -1])
  apply (rule rel_funD[OF rel_funD[OF comp_transfer], rotated -1])
  apply (rule rel_funD[OF rel_funD[OF rel_funD[OF pre_forest_mutual.map_transfer]]])
  apply (rule id_transfer)+
  apply (rule fst_transfer)+
  apply (rule rel_funI)
  apply (erule iffD2[OF forest_mutual.ctor_rel])


  apply (rule rel_funI)+
  unfolding ctor_rec_forest_mutual_def o_apply
  apply (rule rel_funD[OF snd_transfer])
  apply (erule rel_funD[OF rel_funD[OF rel_funD[OF tree_mutual_forest_mutual.ctor_fold_transfer(2)]], rotated -1])

  apply (erule rel_funD[OF rel_funD[OF convol_transfer], rotated -1])
  apply (rule rel_funD[OF rel_funD[OF comp_transfer], rotated -1])
  apply (rule rel_funD[OF rel_funD[OF rel_funD[OF pre_tree_mutual.map_transfer]]])
  apply (rule id_transfer)+
  apply (rule fst_transfer)+
  apply (rule rel_funI)
  apply (erule iffD2[OF tree_mutual.ctor_rel])

  apply (erule rel_funD[OF rel_funD[OF convol_transfer], rotated -1])
  apply (rule rel_funD[OF rel_funD[OF comp_transfer], rotated -1])
  apply (rule rel_funD[OF rel_funD[OF rel_funD[OF pre_forest_mutual.map_transfer]]])
  apply (rule id_transfer)+
  apply (rule fst_transfer)+
  apply (rule rel_funI)
  apply (erule iffD2[OF forest_mutual.ctor_rel]) *)
  done

thm F1.ctor_rec_transfer
lemma F1_ctor_rec_transfer:
  "((rel_pre_F1 R (rel_prod (rel_F1 R :: ('d, _) F1 \<Rightarrow> _) S) ===> S) ===> (rel_F1 R  :: ('d, _) F1 \<Rightarrow> _) ===> S) ctor_rec_F1 ctor_rec_F1"
  apply (tactic {* mk_ctor_rec_transfer_tac @{context} 1 1
    @{thms ctor_rec_F1_def}
    @{thms F1.ctor_fold_transfer}
    @{thms pre_F1.map_transfer}
    @{thms F1.ctor_rel} *})
(*   apply (rule rel_funI)+
  unfolding ctor_rec_F1_def o_apply
  apply (rule rel_funD[OF snd_transfer])
  apply (erule rel_funD[OF rel_funD[OF F1.ctor_fold_transfer], rotated -1])

  apply (erule rel_funD[OF rel_funD[OF convol_transfer], rotated -1])
  apply (rule rel_funD[OF rel_funD[OF comp_transfer], rotated -1])
  apply (rule rel_funD[OF rel_funD[OF pre_F1.map_transfer]])
  apply (rule id_transfer)+
  apply (rule fst_transfer)
  apply (rule rel_funI)
  apply (erule iffD2[OF F1.ctor_rel]) *)
  done

thm F2.ctor_rec_transfer
lemma F2_ctor_rec_transfer:
  "((rel_pre_F2 R (rel_prod (rel_F2 R :: ('d, 'e, _) F2 \<Rightarrow> _) S) ===> S) ===> (rel_F2 R  :: ('d, 'e, _) F2 \<Rightarrow> _) ===> S) ctor_rec_F2 ctor_rec_F2"
  apply (tactic {* mk_ctor_rec_transfer_tac @{context} 1 1
    @{thms ctor_rec_F2_def}
    @{thms F2.ctor_fold_transfer}
    @{thms pre_F2.map_transfer}
    @{thms F2.ctor_rel} *})
done

thm A1_A2.ctor_rec_transfer
lemma A1_A2_ctor_rec_transfer:
  "((rel_pre_A1 R (rel_prod (rel_A1 R) S1) (rel_prod (rel_A2 R) S2) ===> S1) ===> (rel_pre_A2 R (rel_prod (rel_A1 R) S1) (rel_prod (rel_A2 R) S2) ===> S2) ===> rel_A1 R ===> S1) ctor_rec_A1 ctor_rec_A1 \<and>
   ((rel_pre_A1 R (rel_prod (rel_A1 R) S1) (rel_prod (rel_A2 R) S2) ===> S1) ===> (rel_pre_A2 R (rel_prod (rel_A1 R) S1) (rel_prod (rel_A2 R) S2) ===> S2) ===> rel_A2 R ===> S2) ctor_rec_A2 ctor_rec_A2"
  apply (tactic {* mk_ctor_rec_transfer_tac @{context} 2 1
    @{thms ctor_rec_A1_def ctor_rec_A2_def}
    @{thms A1_A2.ctor_fold_transfer}
    @{thms pre_A1.map_transfer pre_A2.map_transfer}
    @{thms A1.ctor_rel A2.ctor_rel} *})
oops

(**************************************************************************************************)

ML {*
open Ctr_Sugar_Tactics
open BNF_Util

fun mk_dtor_corec_transfer_tac ctxt n (* # mutual *) m (* # active *) dtor_corec_defs dtor_unfold_transfer
  pre_T_map_transfers dtor_rels =
  let
    val rel_funD = @{thm rel_funD};
    fun rel_funD_n n = funpow n (fn thm => thm RS rel_funD);
    val rel_funD_n_rotated = rotate_prems 1 oo rel_funD_n;
  in
    CONJ_WRAP (fn (dtor_corec_def, dtor_unfold_transfer) => print_tac ctxt "000" THEN
      REPEAT_DETERM (HEADGOAL (rtac @{thm rel_funI})) THEN print_tac ctxt "AAA" THEN
      unfold_thms_tac ctxt [dtor_corec_def, o_apply] THEN print_tac ctxt "BBB" THEN
      HEADGOAL (rtac (rel_funD_n (n + 1) dtor_unfold_transfer) THEN' K (print_tac ctxt "CCC") THEN'
        EVERY' (map2 (fn pre_T_map_transfer => fn dtor_rel =>
          etac (rel_funD_n_rotated 2 @{thm case_sum_transfer}) THEN' K (print_tac ctxt "DDD") THEN'
          rtac (rel_funD_n 2 @{thm comp_transfer}) THEN' K (print_tac ctxt "EEE") THEN'
          rtac (rel_funD_n (m + n) pre_T_map_transfer) THEN' K (print_tac ctxt "FFF") THEN'
          REPEAT_DETERM_N m o rtac @{thm id_transfer} THEN'
          REPEAT_DETERM_N n o rtac @{thm Inl_transfer} THEN'
          rtac @{thm rel_funI} THEN'
          etac (dtor_rel RS iffD1)) pre_T_map_transfers dtor_rels) THEN'
        etac (rel_funD_n 1 @{thm Inr_transfer})))
      (dtor_corec_defs ~~ dtor_unfold_transfer)
  end;
*}

thm llist.dtor_corec_transfer
lemma llist_dtor_corec_transfer:
  "((S ===> rel_pre_llist R (rel_sum (rel_llist R) S)) ===> S ===> rel_llist R) dtor_corec_llist dtor_corec_llist"
  apply (tactic {* mk_dtor_corec_transfer_tac @{context} 1 1
    @{thms dtor_corec_llist_def}
    @{thms llist.dtor_unfold_transfer}
    @{thms pre_llist.map_transfer}
    @{thms llist.dtor_rel} *})
(*   apply (rule rel_funI)+
  unfolding dtor_corec_llist_def o_apply
  apply (rule rel_funD[OF rel_funD[OF llist.dtor_unfold_transfer]])
  apply (erule rel_funD[OF rel_funD[OF case_sum_transfer], rotated])
  apply (rule rel_funD[OF rel_funD[OF comp_transfer]])
  apply (rule rel_funD[OF rel_funD[OF pre_llist.map_transfer]])
  apply (rule id_transfer)
  apply (rule Inl_transfer)
  apply (rule rel_funI)
  apply (erule iffD1[OF llist.dtor_rel])
  apply (erule rel_funD[OF Inr_transfer]) *)
  done

lemma big_llist_dtor_corec_transfer:
  "((S ===> rel_pre_big_llist R (rel_sum (rel_big_llist R) S)) ===> S ===> rel_big_llist R) dtor_corec_big_llist dtor_corec_big_llist"
  apply (rule rel_funI)+
  apply (tactic {* mk_dtor_corec_transfer_tac @{context} 1 1
    @{thms dtor_corec_big_llist_def}
    @{thms big_llist.dtor_unfold_transfer}
    @{thms pre_big_llist.map_transfer}
    @{thms big_llist.dtor_rel} *})
  unfolding dtor_corec_big_llist_def o_apply
(*   apply (rule rel_funD[OF rel_funD[OF big_llist.dtor_unfold_transfer]])
  apply (erule rel_funD[OF rel_funD[OF case_sum_transfer], rotated])
  apply (rule rel_funD[OF rel_funD[OF comp_transfer]])
  apply (rule rel_funD[OF rel_funD[OF pre_big_llist.map_transfer]])
  apply (rule id_transfer)
  apply (rule Inl_transfer)
  apply (rule rel_funI)
  apply (erule iffD1[OF big_llist.dtor_rel])
  apply (erule rel_funD[OF Inr_transfer]) *)
  done

lemma ltree_dtor_corec_transfer:
  "((S ===> rel_pre_ltree R (rel_sum (rel_ltree R) S)) ===> S ===> rel_ltree R) dtor_corec_ltree dtor_corec_ltree"
  apply (tactic {* mk_dtor_corec_transfer_tac @{context} 1 1
    @{thms dtor_corec_ltree_def}
    @{thms ltree.dtor_unfold_transfer}
    @{thms pre_ltree.map_transfer}
    @{thms ltree.dtor_rel} *})
(*   apply (rule rel_funI)+
  unfolding dtor_corec_ltree_def o_apply
  apply (rule rel_funD[OF rel_funD[OF ltree.dtor_unfold_transfer]])
  apply (erule rel_funD[OF rel_funD[OF case_sum_transfer], rotated])
  apply (rule rel_funD[OF rel_funD[OF comp_transfer]])
  apply (rule rel_funD[OF rel_funD[OF pre_ltree.map_transfer]])
  apply (rule id_transfer)
  apply (rule Inl_transfer)
  apply (rule rel_funI)
  apply (erule iffD1[OF ltree.dtor_rel])
  apply (erule rel_funD[OF Inr_transfer]) *)
  done

lemma lexample_dtor_corec_transfer:
  "((R ===> rel_pre_lexample S R3 (rel_sum (rel_lexample S R3) R)) ===> R ===> rel_lexample S R3) dtor_corec_lexample dtor_corec_lexample"
  apply (tactic {* mk_dtor_corec_transfer_tac @{context} 1 2
    @{thms dtor_corec_lexample_def}
    @{thms lexample.dtor_unfold_transfer}
    @{thms pre_lexample.map_transfer}
    @{thms lexample.dtor_rel} *})
(*   apply (rule rel_funI)+
  unfolding dtor_corec_lexample_def o_apply
  apply (rule rel_funD[OF rel_funD[OF lexample.dtor_unfold_transfer]])
  apply (erule rel_funD[OF rel_funD[OF case_sum_transfer], rotated])
  apply (rule rel_funD[OF rel_funD[OF comp_transfer]])
  apply (rule rel_funD[OF rel_funD[OF rel_funD[OF pre_lexample.map_transfer]]])
  apply (rule id_transfer)+
  apply (rule Inl_transfer)
  apply (rule rel_funI)
  apply (erule iffD1[OF lexample.dtor_rel])
  apply (erule rel_funD[OF Inr_transfer]) *)
  done

lemma ltree_mutual_lforest_mutual_dtor_corec_transfer:
  "((S1 ===> rel_pre_ltree_mutual R (rel_sum (rel_ltree_mutual R) S1) (rel_sum (rel_lforest_mutual R) S2)) ===>
    (S2 ===> rel_pre_lforest_mutual R (rel_sum (rel_ltree_mutual R) S1) (rel_sum (rel_lforest_mutual R) S2)) ===> S1 ===> rel_ltree_mutual R)
    dtor_corec_ltree_mutual dtor_corec_ltree_mutual \<and>
   ((S1 ===> rel_pre_ltree_mutual R (rel_sum (rel_ltree_mutual R) S1) (rel_sum (rel_lforest_mutual R) S2)) ===>
    (S2 ===> rel_pre_lforest_mutual R (rel_sum (rel_ltree_mutual R) S1) (rel_sum (rel_lforest_mutual R) S2)) ===> S2 ===> rel_lforest_mutual R)
    dtor_corec_lforest_mutual dtor_corec_lforest_mutual"
(*   apply (tactic {* mk_dtor_corec_transfer_tac @{context} 2 1
    @{thms dtor_corec_ltree_mutual_def dtor_corec_lforest_mutual_def}
    @{thms ltree_mutual_lforest_mutual.dtor_unfold_transfer}
    @{thms pre_ltree_mutual.map_transfer pre_lforest_mutual.map_transfer}
    @{thms ltree_mutual.dtor_rel lforest_mutual.dtor_rel} *}) *)
  apply (rule conjI)

  apply (rule rel_funI)+
  unfolding dtor_corec_ltree_mutual_def o_apply
  apply (rule rel_funD[OF rel_funD[OF rel_funD[OF ltree_mutual_lforest_mutual.dtor_unfold_transfer(1)]]])

  apply (erule rel_funD[OF rel_funD[OF case_sum_transfer], rotated])
  apply (rule rel_funD[OF rel_funD[OF comp_transfer]])
  apply (rule rel_funD[OF rel_funD[OF rel_funD[OF pre_ltree_mutual.map_transfer]]])
  apply (rule id_transfer)+
  apply (rule Inl_transfer)+
  apply (rule rel_funI)
  apply (erule iffD1[OF ltree_mutual.dtor_rel])

  apply (erule rel_funD[OF rel_funD[OF case_sum_transfer], rotated])
  apply (rule rel_funD[OF rel_funD[OF comp_transfer]])
  apply (rule rel_funD[OF rel_funD[OF rel_funD[OF pre_lforest_mutual.map_transfer]]])
  apply (rule id_transfer)+
  apply (rule Inl_transfer)+
  apply (rule rel_funI)
  apply (erule iffD1[OF lforest_mutual.dtor_rel])

  apply (erule rel_funD[OF Inr_transfer])

  apply (rule rel_funI)+
  unfolding dtor_corec_lforest_mutual_def o_apply
  apply (rule rel_funD[OF rel_funD[OF rel_funD[OF ltree_mutual_lforest_mutual.dtor_unfold_transfer(2)]]])

  apply (erule rel_funD[OF rel_funD[OF case_sum_transfer], rotated])
  apply (rule rel_funD[OF rel_funD[OF comp_transfer]])
  apply (rule rel_funD[OF rel_funD[OF rel_funD[OF pre_ltree_mutual.map_transfer]]])
  apply (rule id_transfer)+
  apply (rule Inl_transfer)+
  apply (rule rel_funI)
  apply (erule iffD1[OF ltree_mutual.dtor_rel])

  apply (erule rel_funD[OF rel_funD[OF case_sum_transfer], rotated])
  apply (rule rel_funD[OF rel_funD[OF comp_transfer]])
  apply (rule rel_funD[OF rel_funD[OF rel_funD[OF pre_lforest_mutual.map_transfer]]])
  apply (rule id_transfer)+
  apply (rule Inl_transfer)+
  apply (rule rel_funI)
  apply (erule iffD1[OF lforest_mutual.dtor_rel])

  apply (erule rel_funD[OF Inr_transfer])
  done

lemma lF1_dtor_corec_transfer:
  "((S ===> rel_pre_lF1 R (rel_sum (rel_lF1 R :: ('d, _) lF1 \<Rightarrow> _) S)) ===> S ===> (rel_lF1 R :: ('d, _) lF1 \<Rightarrow> _)) dtor_corec_lF1 dtor_corec_lF1"
  apply (tactic {* mk_dtor_corec_transfer_tac @{context} 1 1
    @{thms dtor_corec_lF1_def}
    @{thms lF1.dtor_unfold_transfer}
    @{thms pre_lF1.map_transfer}
    @{thms lF1.dtor_rel} *})
(*   apply (rule rel_funI)+
  unfolding dtor_corec_lF1_def o_apply
  apply (rule rel_funD[OF rel_funD[OF lF1.dtor_unfold_transfer]])
  apply (erule rel_funD[OF rel_funD[OF case_sum_transfer], rotated])
  apply (rule rel_funD[OF rel_funD[OF comp_transfer]])
  apply (rule rel_funD[OF rel_funD[OF pre_lF1.map_transfer]])
  apply (rule id_transfer)
  apply (rule Inl_transfer)
  apply (rule rel_funI)
  apply (erule iffD1[OF lF1.dtor_rel])
  apply (erule rel_funD[OF Inr_transfer]) *)
  done

lemma lF2_dtor_corec_transfer:
  "((S ===> rel_pre_lF2 R (rel_sum (rel_lF2 R :: ('d1, 'd2, _) lF2 \<Rightarrow> _) S)) ===> S ===> (rel_lF2 R :: ('d1, 'd2, _) lF2 \<Rightarrow> _)) dtor_corec_lF2 dtor_corec_lF2"
  apply (tactic {* mk_dtor_corec_transfer_tac @{context} 1 1
    @{thms dtor_corec_lF2_def}
    @{thms lF2.dtor_unfold_transfer}
    @{thms pre_lF2.map_transfer}
    @{thms lF2.dtor_rel} *})
(*   apply (rule rel_funI)+
  unfolding dtor_corec_lF2_def o_apply
  apply (rule rel_funD[OF rel_funD[OF lF2.dtor_unfold_transfer]])
  apply (rule rel_funD[OF rel_funD[OF case_sum_transfer], rotated])
  apply assumption
  apply (rule rel_funD[OF rel_funD[OF comp_transfer]])
  apply (rule rel_funD[OF rel_funD[OF pre_lF2.map_transfer]])
  apply (rule id_transfer)
  apply (rule Inl_transfer)
  apply (rule rel_funI)
  apply (erule iffD1[OF lF2.dtor_rel])
  apply (erule rel_funD[OF Inr_transfer]) *)
  done

lemma lA1_lA2_dtor_corec_transfer:
  "((S1 ===> rel_pre_lA1 R (rel_sum (rel_lA1 R) S1) (rel_sum (rel_lA2 R) S2)) ===> (S2 ===> rel_pre_lA2 R (rel_sum (rel_lA1 R) S1) (rel_sum (rel_lA2 R) S2)) ===> S1 ===> rel_lA1 R) dtor_corec_lA1 dtor_corec_lA1 \<and>
   ((S1 ===> rel_pre_lA1 R (rel_sum (rel_lA1 R) S1) (rel_sum (rel_lA2 R) S2)) ===> (S2 ===> rel_pre_lA2 R (rel_sum (rel_lA1 R) S1) (rel_sum (rel_lA2 R) S2)) ===> S2 ===> rel_lA2 R) dtor_corec_lA2 dtor_corec_lA2"
  apply (tactic {* mk_dtor_corec_transfer_tac @{context} 2 1
    @{thms dtor_corec_lA1_def dtor_corec_lA2_def}
    @{thms lA1_lA2.dtor_unfold_transfer}
    @{thms pre_lA1.map_transfer pre_lA2.map_transfer}
    @{thms lA1.dtor_rel lA2.dtor_rel} *})
(*   apply (rule conjI)

  apply (rule rel_funI)+
  unfolding dtor_corec_lA1_def o_apply
  apply (rule rel_funD[OF rel_funD[OF rel_funD[OF lA1_lA2.dtor_unfold_transfer(1)]]])

  apply (erule rel_funD[OF rel_funD[OF case_sum_transfer], rotated])
  apply (rule rel_funD[OF rel_funD[OF comp_transfer]])
  apply (rule rel_funD[OF rel_funD[OF rel_funD[OF pre_lA1.map_transfer]]])
  apply (rule id_transfer)+
  apply (rule Inl_transfer)+
  apply (rule rel_funI)
  apply (erule iffD1[OF lA1.dtor_rel])

  apply (erule rel_funD[OF rel_funD[OF case_sum_transfer], rotated])
  apply (rule rel_funD[OF rel_funD[OF comp_transfer]])
  apply (rule rel_funD[OF rel_funD[OF rel_funD[OF pre_lA2.map_transfer]]])
  apply (rule id_transfer)+
  apply (rule Inl_transfer)+
  apply (rule rel_funI)
  apply (erule iffD1[OF lA2.dtor_rel])

  apply (erule rel_funD[OF Inr_transfer])

  apply (rule rel_funI)+
  unfolding dtor_corec_lA2_def o_apply
  apply (rule rel_funD[OF rel_funD[OF rel_funD[OF lA1_lA2.dtor_unfold_transfer(2)]]])

  apply (erule rel_funD[OF rel_funD[OF case_sum_transfer], rotated])
  apply (rule rel_funD[OF rel_funD[OF comp_transfer]])
  apply (rule rel_funD[OF rel_funD[OF rel_funD[OF pre_lA1.map_transfer]]])
  apply (rule id_transfer)+
  apply (rule Inl_transfer)+
  apply (rule rel_funI)
  apply (erule iffD1[OF lA1.dtor_rel])

  apply (erule rel_funD[OF rel_funD[OF case_sum_transfer], rotated])
  apply (rule rel_funD[OF rel_funD[OF comp_transfer]])
  apply (rule rel_funD[OF rel_funD[OF rel_funD[OF pre_lA2.map_transfer]]])
  apply (rule id_transfer)+
  apply (rule Inl_transfer)+
  apply (rule rel_funI)
  apply (erule iffD1[OF lA2.dtor_rel])

  apply (erule rel_funD[OF Inr_transfer]) *)
  done

(**************************************************************************************************)

ML {*
fun mk_rec_transfer_tac ctxt nb_ctrs actives passives rec_defs ctor_rec_transfers rel_pre_T_defs
    rel_eqs =
  let
    val rel_funI = @{thm rel_funI};
    val rel_funD = @{thm rel_funD};
    fun rel_funD_n n = funpow n (fn thm => thm RS rel_funD);
    val rel_funD_n_rotated = rotate_prems ~1 oo rel_funD_n;
    val ctor_rec_transfers' =
      map (cterm_instantiate_pos (map SOME passives @ map SOME actives)) ctor_rec_transfers;
    val nb_mutual = length actives;
    val nb_ctrs' = fold (curry (op +)) nb_ctrs 0;
    val my_print_tac = if false then print_tac ctxt else K all_tac;
  in
    HEADGOAL Goal.conjunction_tac THEN
    EVERY (map (fn ctor_rec_transfer => my_print_tac "AAA" THEN
      REPEAT_DETERM (HEADGOAL (rtac rel_funI)) THEN my_print_tac "BBB" THEN
      unfold_thms_tac ctxt rec_defs THEN my_print_tac "CCC" THEN
      HEADGOAL (etac (rel_funD_n_rotated (nb_mutual + 1) ctor_rec_transfer)) THEN my_print_tac "DDD" THEN
      unfold_thms_tac ctxt rel_pre_T_defs THEN my_print_tac "EEE" THEN
      EVERY (fst (fold_map (fn nb_ctr => fn nb_ctr_acc => rpair (nb_ctr + nb_ctr_acc) (my_print_tac "FFF" THEN
        HEADGOAL (rtac (rel_funD_n_rotated 2 @{thm comp_transfer})) THEN my_print_tac "GGG" THEN
        HEADGOAL (rtac @{thm vimage2p_rel_fun}) THEN my_print_tac "HHH" THEN
        unfold_thms_tac ctxt rel_eqs THEN
        EVERY (map (fn n => my_print_tac "III" THEN
          REPEAT_DETERM (HEADGOAL
            (rtac (rel_funD_n 2 @{thm case_sum_transfer[of "op =" _ "op =", unfolded sum.rel_eq]}) ORELSE'
             rtac (rel_funD_n 2 @{thm case_sum_transfer}))) THEN my_print_tac "JJJ" THEN
          HEADGOAL (select_prem_tac nb_ctrs' (dtac asm_rl) (nb_ctr_acc + n)) THEN my_print_tac "KKK" THEN
          HEADGOAL (SELECT_GOAL (HEADGOAL
            (( K (my_print_tac "LLL") THEN' REPEAT_DETERM o ((atac ORELSE'
                rtac (rel_funD_n 1 @{thm case_prod_transfer[of "op =" "op =", unfolded prod.rel_eq]}) ORELSE'
                rtac (rel_funD_n 1 @{thm case_prod_transfer}) ORELSE'
                rtac rel_funI) THEN' K (my_print_tac "MMM"))) THEN_ALL_NEW
             (REPEAT_ALL_NEW (K (my_print_tac "NNN") THEN' dtac @{thm rel_funD} THEN' K (my_print_tac "OOO")) THEN_ALL_NEW atac)))))
          (1 upto nb_ctr))))
        nb_ctrs 0)))
      ctor_rec_transfers')
  end;
*}

lemma list_rec_transfer:
  "(S ===> (R ===> rel_list R ===> S ===> S) ===> rel_list R ===> S) rec_list rec_list"
  apply (tactic {* mk_rec_transfer_tac @{context} [2]
    [@{cterm S}] [@{cterm R}]
    @{thms rec_list_def}
    @{thms list_ctor_rec_transfer}
    @{thms rel_pre_list_def}
    @{thms }*})
(*   apply (rule rel_funI)+
  unfolding rec_list_def
  apply (erule rel_funD[OF rel_funD[OF list_ctor_rec_transfer], rotated])
  unfolding rel_pre_list_def
  apply (rule rel_funD[OF rel_funD[OF comp_transfer], rotated])
  apply (rule vimage2p_rel_fun)
  apply (rule rel_funD[OF rel_funD[OF case_sum_transfer]])
  apply (rule rel_funI)
  apply assumption
  apply (rule rel_funD[OF case_prod_transfer])
  apply (rule rel_funI)
  apply (rule rel_funD[OF case_prod_transfer])
  apply (rule rel_funI)
  apply (rule rel_funI)
  apply (tactic {* (REPEAT_ALL_NEW (dtac @{thm rel_funD}) THEN_ALL_NEW atac) 1 *}) *)
  done

lemma big_list_rec_transfer:
  "(S ===> (R ===> R ===> rel_big_list R ===> S ===> S) ===> rel_big_list R ===> S) rec_big_list rec_big_list"
  apply (tactic {* mk_rec_transfer_tac @{context} [2]
    [@{cterm S}] [@{cterm R}]
    @{thms rec_big_list_def}
    @{thms big_list_ctor_rec_transfer}
    @{thms rel_pre_big_list_def}
    @{thms }*})
(*   apply (rule rel_funI)+
  unfolding rec_big_list_def
  apply (erule rel_funD[OF rel_funD[OF big_list_ctor_rec_transfer], rotated])
  unfolding rel_pre_big_list_def
  apply (rule rel_funD[OF rel_funD[OF comp_transfer], rotated])
  apply (rule vimage2p_rel_fun)
  apply (rule rel_funD[OF rel_funD[OF case_sum_transfer]])
  apply (rule rel_funI)
  apply assumption
  apply (rule rel_funD[OF case_prod_transfer])
  apply (rule rel_funI)
  apply (rule rel_funD[OF case_prod_transfer])
  apply (rule rel_funI)
  apply (rule rel_funD[OF case_prod_transfer])
  apply (rule rel_funI)
  apply (rule rel_funI)
  apply (tactic {* (REPEAT_ALL_NEW (dtac @{thm rel_funD}) THEN_ALL_NEW atac) 1 *}) *)
  done

lemma tree_rec_transfer:
  "((R ===> rel_list (rel_prod (rel_tree R) S) ===> S) ===> rel_tree R ===> S) rec_tree rec_tree"
  apply (tactic {* mk_rec_transfer_tac @{context} [1]
    [@{cterm S}] [@{cterm R}]
    @{thms rec_tree_def}
    @{thms tree_ctor_rec_transfer}
    @{thms rel_pre_tree_def}
    @{thms }*})
(*   apply (rule rel_funI)+
  unfolding rec_tree_def
  apply (erule rel_funD[OF rel_funD[OF tree_ctor_rec_transfer], rotated])
  unfolding rel_pre_tree_def
  apply (rule rel_funD[OF rel_funD[OF comp_transfer], rotated])
  apply (rule vimage2p_rel_fun)
  apply (rule rel_funD[OF case_prod_transfer])
  apply (rule rel_funI)
  apply (rule rel_funI)
  apply (tactic {* (REPEAT_ALL_NEW (dtac @{thm rel_funD}) THEN_ALL_NEW atac) 1 *}) *)
  done

term rec_example
lemma example_rec_transfer:
  "((R1 ===> S) ===>
    (R2 ===> S) ===>
    (R1 ===> R2 ===> R2 ===> R2 ===> S) ===>
    S ===>
    (R1 ===> rel_example R1 R2 ===> S ===> S) ===>
    (R2 ===> rel_example R1 R2 ===> S ===> S) ===>
    rel_example R1 R2 ===>
    S) rec_example rec_example"
  apply (tactic {* mk_rec_transfer_tac @{context} [6]
    [@{cterm S}] [@{cterm R1}, @{cterm R2}]
    @{thms rec_example_def}
    @{thms example.ctor_rec_transfer}
    @{thms rel_pre_example_def}
    @{thms } *})
   (* apply (rule rel_funI)+
   unfolding rec_example_def
   apply (erule rel_funD[OF rel_funD[OF example_ctor_rec_transfer], rotated])
   unfolding rel_pre_example_def
   apply (rule rel_funD[OF rel_funD[OF comp_transfer], rotated])
   apply (rule vimage2p_rel_fun)

   apply ((rule rel_funD[OF rel_funD[OF case_sum_transfer]])+)?
   (* select_prems first *)  apply (drule asm_rl, erule thin_rl, erule thin_rl, erule thin_rl, erule thin_rl, erule thin_rl)
     apply ((assumption | rule rel_funD[OF case_prod_transfer] | rule rel_funI)+) []

   apply ((rule rel_funD[OF rel_funD[OF case_sum_transfer]])+)?
   (* select_prems 2nd *)  apply (erule thin_rl, drule asm_rl, erule thin_rl, erule thin_rl, erule thin_rl, erule thin_rl)
     apply ((assumption | rule rel_funD[OF case_prod_transfer] | rule rel_funI)+) []

   apply ((rule rel_funD[OF rel_funD[OF case_sum_transfer]])+)?
   (* select_prems 3nd *) apply (erule thin_rl, erule thin_rl, drule asm_rl, erule thin_rl, erule thin_rl, erule thin_rl)
     apply ((assumption | rule rel_funD[OF case_prod_transfer] | rule rel_funI)+) []
     apply (tactic {* (REPEAT_ALL_NEW (dtac @{thm rel_funD}) THEN_ALL_NEW atac) 1 *})

   apply ((rule rel_funD[OF rel_funD[OF case_sum_transfer]])+)?
   (* select_prems 4nd *) apply (erule thin_rl, erule thin_rl, erule thin_rl, drule asm_rl, erule thin_rl, erule thin_rl)
     apply ((assumption | rule rel_funD[OF case_prod_transfer] | rule rel_funI)+) []

   apply ((rule rel_funD[OF rel_funD[OF case_sum_transfer]])+)?
   (* select_prems 5nd *) apply (erule thin_rl, erule thin_rl, erule thin_rl, erule thin_rl, drule asm_rl, erule thin_rl)
     apply ((assumption | rule rel_funD[OF case_prod_transfer] | rule rel_funI)+) []
     apply (tactic {* (REPEAT_ALL_NEW (dtac @{thm rel_funD}) THEN_ALL_NEW atac) 1 *})

   apply ((rule rel_funD[OF rel_funD[OF case_sum_transfer]])+)?
   (* select_prems 6nd *) apply (erule thin_rl, erule thin_rl, erule thin_rl, erule thin_rl, erule thin_rl, drule asm_rl)
     apply ((assumption | rule rel_funD[OF case_prod_transfer] | rule rel_funI)+) []
     apply (tactic {* (REPEAT_ALL_NEW (dtac @{thm rel_funD}) THEN_ALL_NEW atac) 1 *})  *)
  done

term rec_tree_mutual
term rec_forest_mutual
lemma tree_mutual_rec_transfer:
  "((S ===> rel_forest_mutual S ===> R2 ===> R1) ===>
    R2 ===>
    (rel_tree_mutual S ===> rel_forest_mutual S ===> R1 ===> R2 ===> R2) ===>
    rel_tree_mutual S ===>
    R1) rec_tree_mutual rec_tree_mutual"
  "((S ===> rel_forest_mutual S ===> R2 ===> R1) ===>
    R2 ===>
    (rel_tree_mutual S ===> rel_forest_mutual S ===> R1 ===> R2 ===> R2) ===>
    rel_forest_mutual S ===>
    R2) rec_forest_mutual rec_forest_mutual"
  apply (tactic {* mk_rec_transfer_tac @{context} [1, 2]
    [@{cterm R1}, @{cterm R2}] [@{cterm S}]
    @{thms rec_tree_mutual_def rec_forest_mutual_def}
    @{thms tree_mutual_forest_mutual.ctor_rec_transfer}
    @{thms rel_pre_tree_mutual_def rel_pre_forest_mutual_def}
    @{thms }*})
  done

term rec_F1
lemma F1_rec_transfer:
  "(((op = ===> R) ===> S) ===>
    rel_F1 R ===>
    S) rec_F1 rec_F1"
  apply (tactic {* mk_rec_transfer_tac @{context} [1]
    [@{cterm S}] [@{cterm R}]
    @{thms rec_F1_def}
    @{thms F1.ctor_rec_transfer}
    @{thms rel_pre_F1_def}
    @{thms }*})
  done

thm F2.ctor_rec_transfer
term rec_F2
lemma F2_rec_transfer:
  "(((op = ===> op = ===> R) ===> S) ===>
    rel_F2 R ===>
    S) rec_F2 rec_F2"
  apply (tactic {* mk_rec_transfer_tac @{context} [1]
    [@{cterm S}] [@{cterm R}]
    @{thms rec_F2_def}
    @{thms F2.ctor_rec_transfer}
    @{thms rel_pre_F2_def}
    @{thms }*})
  done

term rec_F3
lemma F3_rec_transfer:
  "(((op = ===> op =) ===> S) ===>
    (R ===> S) ===>
    rel_F3 R ===>
    S) rec_F3 rec_F3"
  apply (tactic {* mk_rec_transfer_tac @{context} [2]
    [@{cterm S}] [@{cterm R}]
    @{thms rec_F3_def}
    @{thms F3.ctor_rec_transfer}
    @{thms rel_pre_F3_def}
    @{thms }*})
oops

term rec_A1
term rec_A2
lemma A1_rec_transfer:
  "((op = ===> S1) ===>
    (R ===> S2) ===>
    rel_A1 R ===>
    S1) rec_A1 rec_A1"
  "((op = ===> S1) ===>
    (R ===> S2) ===>
    rel_A2 R ===>
    S2) rec_A2 rec_A2"
  apply (tactic {* mk_rec_transfer_tac @{context} [1, 1]
    [@{cterm S1}, @{cterm S2}] [@{cterm R}]
    @{thms rec_A1_def rec_A2_def}
    @{thms A1_A2.ctor_rec_transfer}
    @{thms rel_pre_A1_def rel_pre_A2_def}
    @{thms }*})
  done

(**************************************************************************************************)

lemma Abs_transfer:
  assumes type_copy1: "type_definition Rep1 Abs1 UNIV"
  assumes type_copy2: "type_definition Rep2 Abs2 UNIV"
  shows "rel_fun R (vimage2p Rep1 Rep2 R) Abs1 Abs2"
  unfolding vimage2p_def rel_fun_def
    type_definition.Abs_inverse[OF type_copy1 UNIV_I]
    type_definition.Abs_inverse[OF type_copy2 UNIV_I] by simp

thm rel_funD[OF Inl_transfer[of "op =" "op =", unfolded sum.rel_eq]]

ML {*
fun flat_corec_predss_getterss qss gss = maps (op @) (qss ~~ gss);

fun flat_corec_preds_predsss_gettersss [] [qss] [gss] = flat_corec_predss_getterss qss gss
  | flat_corec_preds_predsss_gettersss (p :: ps) (qss :: qsss) (gss :: gsss) =
    p :: flat_corec_predss_getterss qss gss @ flat_corec_preds_predsss_gettersss ps qsss gsss;

fun mk_corec_transfer_tac ctxt actives passives type_definitions corec_defs dtor_corec_transfers
    rel_pre_T_defs rel_eqs pgs pss qssss gssss =
  let
    val num_pgs = length pgs;
    fun prem_no_of x = let
    in 1 + find_index (curry (op =) x) pgs end;

    val Inl_Inr_Pair_tac = K (print_tac ctxt "Inl_Inr_Pair") THEN' REPEAT_DETERM o (resolve_tac
      [mk_rel_funDN 1 @{thm Inl_transfer},
       mk_rel_funDN 1 @{thm Inl_transfer[of "op =" "op =", unfolded sum.rel_eq]},
       mk_rel_funDN 1 @{thm Inr_transfer},
       mk_rel_funDN 1 @{thm Inr_transfer[of "op =" "op =", unfolded sum.rel_eq]},
       mk_rel_funDN 2 @{thm Pair_transfer},
       mk_rel_funDN 2 @{thm Pair_transfer[of "op =" "op =", unfolded prod.rel_eq]}]);

    fun mk_unfold_If_tac total pos =
      HEADGOAL (Inl_Inr_Pair_tac THEN'
        rtac (mk_rel_funDN 3 @{thm If_transfer}) THEN'
        select_prem_tac total (dtac asm_rl) pos THEN'
        dtac rel_funD THEN' atac THEN' atac);

    fun mk_unfold_Inl_Inr_Pair_tac total pos =
      HEADGOAL (K (print_tac ctxt "J") THEN' Inl_Inr_Pair_tac THEN'
        K (print_tac ctxt ("K " ^ @{make_string} total ^ " " ^ @{make_string} pos)) THEN' select_prem_tac total (dtac asm_rl) pos THEN'
        K (print_tac ctxt "L") THEN' dtac rel_funD THEN' atac THEN' atac);

    fun mk_unfold_arg_tac qs gs =
      print_tac ctxt "H" THEN EVERY (map (mk_unfold_If_tac num_pgs o prem_no_of) qs) THEN
      print_tac ctxt "I" THEN EVERY (map (mk_unfold_Inl_Inr_Pair_tac num_pgs o prem_no_of) gs);

    fun mk_unfold_ctr_tac type_definition qss gss =
      print_tac ctxt "E" THEN 
      HEADGOAL (rtac (mk_rel_funDN 1 (@{thm Abs_transfer} OF
        [type_definition, type_definition])) THEN' Inl_Inr_Pair_tac) THEN
      print_tac ctxt "F" THEN 
      (case (qss, gss) of
        ([], []) => print_tac ctxt "G1" THEN HEADGOAL (rtac refl)
      | _ => print_tac ctxt ("G2 (length qss) = " ^ @{make_string} (length qss)) THEN
        EVERY (map2 mk_unfold_arg_tac qss gss));

    fun mk_unfold_type_tac type_definition ps qsss gsss =
      let
        val p_tacs = map (mk_unfold_If_tac num_pgs o prem_no_of) ps;
        val qg_tacs = map2 (mk_unfold_ctr_tac type_definition) qsss gsss;
        fun mk_unfold_ty [] [qg_tac] = print_tac ctxt "D1" THEN qg_tac
          | mk_unfold_ty (p_tac :: p_tacs) (qg_tac :: qg_tacs) =
            print_tac ctxt "D2" THEN 
            p_tac THEN print_tac ctxt "D2a" THEN
            qg_tac THEN print_tac ctxt "D2b" THEN
            mk_unfold_ty p_tacs qg_tacs
      in
        HEADGOAL (rtac rel_funI) THEN mk_unfold_ty p_tacs qg_tacs
      end;

    fun mk_unfold_corec_type_tac dtor_corec_transfer corec_def =
      let
        val active :: actives' = actives;
        val dtor_corec_transfer' = cterm_instantiate_pos
          (SOME active :: map SOME passives @ map SOME actives') dtor_corec_transfer;
      in
        HEADGOAL Goal.conjunction_tac THEN
        REPEAT_DETERM (HEADGOAL (rtac rel_funI)) THEN
        unfold_thms_tac ctxt [corec_def] THEN
        HEADGOAL (etac (mk_rel_funDN_rotated (1 + length actives) dtor_corec_transfer')) THEN
        unfold_thms_tac ctxt (rel_pre_T_defs @ rel_eqs)
      end;

    fun mk_unfold_prop_tac dtor_corec_transfer corec_def =
      print_tac ctxt "A" THEN
      mk_unfold_corec_type_tac dtor_corec_transfer corec_def THEN
      print_tac ctxt "B" THEN
      EVERY (@{map 4} mk_unfold_type_tac type_definitions pss qssss gssss) THEN
      print_tac ctxt "C";
  in
    HEADGOAL Goal.conjunction_tac THEN
    EVERY (map2 mk_unfold_prop_tac dtor_corec_transfers corec_defs)
  end;

fun nth' n xs = nth xs n
fun W f x = f x x

val pss_llist = [["Free ('p', ''c \<Rightarrow> HOL.bool')"]]
val qssss_llist = [[[], [[], ["Free ('q22', ''c \<Rightarrow> HOL.bool')"]]]]
val gssss_llist = [[[], [["Free ('g21', ''c \<Rightarrow> 'a')"], ["Free ('g221', ''c \<Rightarrow> 'a rec_transfer.lifting_syntax.llist')", "Free ('g222', ''c \<Rightarrow> 'c')"]]]]
val pgs_llist = flat (@{map 3} flat_corec_preds_predsss_gettersss pss_llist qssss_llist gssss_llist)

val pss_big_llist = [["Free ('p', ''c \<Rightarrow> HOL.bool')"]]
val qssss_big_llist = [[[], [[], [], ["Free ('q23', ''c \<Rightarrow> HOL.bool')"]]]]
val gssss_big_llist = [[[], [["Free ('g21', ''c \<Rightarrow> 'a')"], ["Free ('g22', ''c \<Rightarrow> 'a')"], ["Free ('g231', ''c \<Rightarrow> 'a rec_transfer.lifting_syntax.big_llist')", "Free ('g232', ''c \<Rightarrow> 'c')"]]]]
val pgs_big_llist = flat (@{map 3} flat_corec_preds_predsss_gettersss pss_big_llist qssss_big_llist gssss_big_llist)

val pss_ltree = [[]]
val qssss_ltree = [[[[], []]]]
val gssss_ltree = [[[["Free ('g1', ''c \<Rightarrow> 'a')"], ["Free ('g2', ''c \<Rightarrow> ('a rec_transfer.lifting_syntax.ltree, 'c) Sum_Type.sum rec_transfer.lifting_syntax.llist')"]]]]
val pgs_ltree = flat (@{map 3} flat_corec_preds_predsss_gettersss pss_ltree qssss_ltree gssss_ltree)

val pss_lexample = [["Free ('p1', ''e \<Rightarrow> HOL.bool')", "Free ('p2', ''e \<Rightarrow> HOL.bool')", "Free ('p3', ''e \<Rightarrow> HOL.bool')", "Free ('p4', ''e \<Rightarrow> HOL.bool')", "Free ('p5', ''e \<Rightarrow> HOL.bool')"]]
val qssss_lexample = [[[[]], [[]], [[], []], [], [[], ["Free ('q52', ''e \<Rightarrow> HOL.bool')"]], [[], ["Free ('q62', ''e \<Rightarrow> HOL.bool')"]]]]
val gssss_lexample = [[[["Free ('g1', ''e \<Rightarrow> 'a')"]], [["Free ('g2', ''e \<Rightarrow> 'b')"]], [["Free ('g31', ''e \<Rightarrow> 'a')"], ["Free ('g32', ''e \<Rightarrow> 'b')"]], [], [["Free ('g51', ''e \<Rightarrow> 'a')"], ["Free ('g521', ''e \<Rightarrow> ('a, 'b) rec_transfer.lifting_syntax.lexample')", "Free ('g522', ''e \<Rightarrow> 'e')"]],
          [["Free ('g61', ''e \<Rightarrow> 'b')"], ["Free ('g621', ''e \<Rightarrow> ('a, 'b) rec_transfer.lifting_syntax.lexample')", "Free ('g622', ''e \<Rightarrow> 'e')"]]]]
val pgs_lexample = flat (@{map 3} flat_corec_preds_predsss_gettersss pss_lexample qssss_lexample gssss_lexample)

val pss_ltree_mutual_lforest_mutual = [[], ["Free ('p2', ''d \<Rightarrow> HOL.bool')"]]
val qssss_ltree_mutual_lforest_mutual = [[[[], ["Free ('q12', ''c \<Rightarrow> HOL.bool')"]]], [[], [["Free ('q221', ''d \<Rightarrow> HOL.bool')"], ["Free ('q222', ''d \<Rightarrow> HOL.bool')"]]]]
val gssss_ltree_mutual_lforest_mutual = [[[["Free ('g11', ''c \<Rightarrow> 'a')"], ["Free ('g121', ''c \<Rightarrow> 'a rec_transfer.lifting_syntax.lforest_mutual')", "Free ('g122', ''c \<Rightarrow> 'd')"]]],
         [[], [["Free ('g2211', ''d \<Rightarrow> 'a rec_transfer.lifting_syntax.ltree_mutual')", "Free ('g2212', ''d \<Rightarrow> 'c')"], ["Free ('g2221', ''d \<Rightarrow> 'a rec_transfer.lifting_syntax.lforest_mutual')", "Free ('g2222', ''d \<Rightarrow> 'd')"]]]]
val pgs_ltree_mutual_lforest_mutual = flat (@{map 3} flat_corec_preds_predsss_gettersss pss_ltree_mutual_lforest_mutual qssss_ltree_mutual_lforest_mutual gssss_ltree_mutual_lforest_mutual)

val pss_lF1 = [[]]
val qssss_lF1 = [[[[]]]]
val gssss_lF1 = [[[["Free ('g', ''e \<Rightarrow> 'a \<Rightarrow> 'b')"]]]]
val pgs_lF1 = flat (@{map 3} flat_corec_preds_predsss_gettersss pss_lF1 qssss_lF1 gssss_lF1)

val pss_lF1' = [[]]
val qssss_lF1' = [[[[]]]]
val gssss_lF1' = [[[["Free ('g', ''e \<Rightarrow> ('a, 'b) rec_transfer.lifting_syntax.dead_pair')"]]]]
val pgs_lF1' = flat (@{map 3} flat_corec_preds_predsss_gettersss pss_lF1' qssss_lF1' gssss_lF1')

val pss_lF2 = [[]]
val qssss_lF2 = [[[[]]]]
val gssss_lF2 = [[[["Free ('g', ''g \<Rightarrow> 'a \<Rightarrow> 'b \<Rightarrow> 'c')"]]]]
val pgs_lF2 = flat (@{map 3} flat_corec_preds_predsss_gettersss pss_lF2 qssss_lF2 gssss_lF2)

val pss_lF3 = [["Free ('p', ''c \<Rightarrow> HOL.bool')"]]
val qssss_lF3 = [[[[]], [[]]]]
val gssss_lF3 = [[[["Free ('g1', ''c \<Rightarrow> 't \<Rightarrow> Nat.nat')"]], [["Free ('g2', ''c \<Rightarrow> 'id')"]]]]
val pgs_lF3 = flat (@{map 3} flat_corec_preds_predsss_gettersss pss_lF3 qssss_lF3 gssss_lF3)

val pss_lF4 = [[]] 
val qssss_lF4 = [[[[], []]]] 
val gssss_lF4 = [[[["Free ('g1', ''c \<Rightarrow> ('t, Nat.nat) rec_transfer.lifting_syntax.dead_pair')"], ["Free ('g2', ''c \<Rightarrow> 'id')"]]]] 
val pgs_lF4 = flat (@{map 3} flat_corec_preds_predsss_gettersss pss_lF4 qssss_lF4 gssss_lF4)

val pss_lF5 = [[]] 
val qssss_lF5 = [[[[], ["Free ('q2', ''c \<Rightarrow> HOL.bool')"], []]]] 
val gssss_lF5 = [[[["Free ('g1', ''c \<Rightarrow> 'a')"], ["Free ('g21', ''c \<Rightarrow> 'a rec_transfer.lifting_syntax.lF5')", "Free ('g22', ''c \<Rightarrow> 'c')"], ["Free ('g3', ''c \<Rightarrow> 'a')"]]]] 
val pgs_lF5 = flat (@{map 3} flat_corec_preds_predsss_gettersss pss_lF5 qssss_lF5 gssss_lF5)

val pss_lA1_lA2 = [[], []]
val qssss_lA1_lA2 = [[[[]]], [[[]]]]
val gssss_lA1_lA2 = [[[["Free ('g1', ''c \<Rightarrow> Nat.nat')"]]], [[["Free ('g2', ''d \<Rightarrow> 'a')"]]]]
val pgs_lA1_lA2 = flat (@{map 3} flat_corec_preds_predsss_gettersss pss_lA1_lA2 qssss_lA1_lA2 gssss_lA1_lA2)

val pss_live_direct = [[]]
val qssss_live_direct = [[[[]]]]
val gssss_live_direct = [[[["Free ('g', ''c \<Rightarrow> 'a')"]]]]
val pgs_live_direct = flat (@{map 3} flat_corec_preds_predsss_gettersss pss_live_direct qssss_live_direct gssss_live_direct)

val pss_live_indirect = [[]]
val qssss_live_indirect = [[[[]]]]
val gssss_live_indirect = [[[["Free ('g', ''c \<Rightarrow> ('a, Nat.nat) rec_transfer.lifting_syntax.pair')"]]]]
val pgs_live_indirect = flat (@{map 3} flat_corec_preds_predsss_gettersss pss_live_indirect qssss_live_indirect gssss_live_indirect)

val pss_live_indirect' = [[]]
val qssss_live_indirect' = [[[[]]]]
val gssss_live_indirect' = [[[["Free ('g', ''e \<Rightarrow> ('a, 'b) rec_transfer.lifting_syntax.pair')"]]]]
val pgs_live_indirect' = flat (@{map 3} flat_corec_preds_predsss_gettersss pss_live_indirect' qssss_live_indirect' gssss_live_indirect')

val pss_nested_corecursive = [[]]
val qssss_nested_corecursive = [[[[], []]]]
val gssss_nested_corecursive = [[[["Free ('g1', ''c \<Rightarrow> 'a')"], ["Free ('g2', ''c \<Rightarrow> (Nat.nat, ('a rec_transfer.lifting_syntax.nested_corecursive, 'c) Sum_Type.sum) rec_transfer.lifting_syntax.pair')"]]]]
val pgs_nested_corecursive = flat (@{map 3} flat_corec_preds_predsss_gettersss pss_nested_corecursive qssss_nested_corecursive gssss_nested_corecursive)

val pss_live_indirect_and_corecursive_indirect = [[]]
val qssss_live_indirect_and_corecursive_indirect = [[[[]]]]
val gssss_live_indirect_and_corecursive_indirect = [[[["Free ('g', ''c \<Rightarrow> ('a, ('a rec_transfer.lifting_syntax.live_indirect_and_corecursive_indirect, 'c) Sum_Type.sum) rec_transfer.lifting_syntax.pair')"]]]]
val pgs_live_indirect_and_corecursive_indirect = flat (@{map 3} flat_corec_preds_predsss_gettersss pss_live_indirect_and_corecursive_indirect qssss_live_indirect_and_corecursive_indirect gssss_live_indirect_and_corecursive_indirect)

val pss_mutually_corec_direct = [[]]
val qssss_mutually_corec_direct = [[[[], ["Free ('q2', ''c \<Rightarrow> HOL.bool')"]]]]
val gssss_mutually_corec_direct = [[[["Free ('g1', ''c \<Rightarrow> 'a')"], ["Free ('g21', ''c \<Rightarrow> 'a rec_transfer.lifting_syntax.mutually_corec_direct')", "Free ('g22', ''c \<Rightarrow> 'c')"]]]]
val pgs_mutually_corec_direct = flat (@{map 3} flat_corec_preds_predsss_gettersss pss_mutually_corec_direct qssss_mutually_corec_direct gssss_mutually_corec_direct)

val pss_mutually_corec_indirect1_mutually_corec_indirect2 = [[], []] 
val qssss_mutually_corec_indirect1_mutually_corec_indirect2 = [[[[], [], []]], [[[], [], []]]] 
val gssss_mutually_corec_indirect1_mutually_corec_indirect2 = [[[["Free ('g11', ''c \<Rightarrow> 'a')"], ["Free ('g12', ''c \<Rightarrow> 'a')"], ["Free ('g13', ''c \<Rightarrow> (Nat.nat, ('a rec_transfer.lifting_syntax.mutually_corec_indirect2, 'd) Sum_Type.sum) rec_transfer.lifting_syntax.pair')"]]],
         [[["Free ('g21', ''d \<Rightarrow> 'a')"], ["Free ('g22', ''d \<Rightarrow> 'a')"], ["Free ('g23', ''d \<Rightarrow> (Nat.nat, ('a rec_transfer.lifting_syntax.mutually_corec_indirect2, 'd) Sum_Type.sum) rec_transfer.lifting_syntax.pair')"]]]] 
val pgs_mutually_corec_indirect1_mutually_corec_indirect2 = flat (@{map 3} flat_corec_preds_predsss_gettersss pss_mutually_corec_indirect1_mutually_corec_indirect2 qssss_mutually_corec_indirect1_mutually_corec_indirect2 gssss_mutually_corec_indirect1_mutually_corec_indirect2)

val pss_three_args = [[]]
val qssss_three_args = [[[[], [], []]]]
val gssss_three_args = [[[["Free ('g1', ''c \<Rightarrow> 'a')"], ["Free ('g2', ''c \<Rightarrow> 'a')"], ["Free ('g3', ''c \<Rightarrow> 'a')"]]]]
val pgs_three_args = flat (@{map 3} flat_corec_preds_predsss_gettersss pss_three_args qssss_three_args gssss_three_args)

val pss_five_args = [[]]
val qssss_five_args = [[[[], [], [], [], []]]]
val gssss_five_args = [[[["Free ('g1', ''c \<Rightarrow> 'a')"], ["Free ('g2', ''c \<Rightarrow> 'a')"], ["Free ('g3', ''c \<Rightarrow> 'a')"], ["Free ('g4', ''c \<Rightarrow> 'a')"], ["Free ('g5', ''c \<Rightarrow> 'a')"]]]]
val pgs_five_args = flat (@{map 3} flat_corec_preds_predsss_gettersss pss_five_args qssss_five_args gssss_five_args)

val pss_foo = [[]]
val qssss_foo = [[[[], []]]]
val gssss_foo = [[[["Free ('g1', ''c \<Rightarrow> Nat.nat')"], ["Free ('g2', ''c \<Rightarrow> Nat.nat')"]]]]
val pgs_foo = flat (@{map 3} flat_corec_preds_predsss_gettersss pss_foo qssss_foo gssss_foo)

val pss_bar = [["Free ('p', ''a \<Rightarrow> HOL.bool')"]]
val qssss_bar = [[[[]], []]]
val gssss_bar = [[[["Free ('g1', ''a \<Rightarrow> Nat.nat')"]], []]]
val pgs_bar = flat (@{map 3} flat_corec_preds_predsss_gettersss pss_bar qssss_bar gssss_bar)
*}

term corec_llist
lemma llist_corec_transfer:
  "((S ===> op =) ===>
    (S ===> R) ===> (S ===> op =) ===> (S ===> rel_llist R) ===> (S ===> S) ===>
    S ===> rel_llist R) corec_llist corec_llist"
  apply (tactic {* mk_corec_transfer_tac @{context} [@{cterm S}] [@{cterm R}] @{thms BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms corec_llist_def} @{thms llist_dtor_corec_transfer} @{thms rel_pre_llist_def} @{thms }
    pgs_llist pss_llist qssss_llist gssss_llist *})
done

term corec_big_llist
lemma big_llist_corec_transfer:
  "((S ===> op =) ===>
    (S ===> R) ===> (S ===> R) ===> (S ===> op =) ===> (S ===> rel_big_llist R) ===> (S ===> S) ===>
    S ===> rel_big_llist R) corec_big_llist corec_big_llist"
  apply (tactic {* mk_corec_transfer_tac @{context} [@{cterm S}] [@{cterm R}] @{thms type_definition_big_llist_pre_big_llist}
    @{thms corec_big_llist_def}@{thms big_llist_dtor_corec_transfer} @{thms rel_pre_big_llist_def} @{thms }
    pgs_big_llist pss_big_llist qssss_big_llist gssss_big_llist *})
done

term corec_ltree
lemma ltree_corec_transfer:
  "((S ===> R) ===>
    (S ===> rel_llist (rel_sum (rel_ltree R) S)) ===>
    S ===> rel_ltree R) corec_ltree corec_ltree"
  apply (tactic {* mk_corec_transfer_tac @{context} [@{cterm S}] [@{cterm R}] @{thms BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms corec_ltree_def} @{thms ltree_dtor_corec_transfer} @{thms rel_pre_ltree_def} @{thms }
    pgs_ltree pss_ltree qssss_ltree gssss_ltree *})
done

term corec_lexample
lemma lexample_corec_transfer:
  "((S ===> op =) ===> (S ===> R1) ===>
    (S ===> op =) ===> (S ===> R2) ===>
    (S ===> op =) ===> (S ===> R1) ===> (S ===> R2) ===>
    (S ===> op =) ===>
    (S ===> op =) ===> (S ===> R1) ===> (S ===> op =) ===> (S ===> rel_lexample R1 R2) ===> (S ===> S) ===>
                       (S ===> R2) ===> (S ===> op =) ===> (S ===> rel_lexample R1 R2) ===> (S ===> S) ===>
    S ===> rel_lexample R1 R2) corec_lexample corec_lexample"
  apply (tactic {* mk_corec_transfer_tac @{context} [@{cterm S}] [@{cterm R1}, @{cterm R2}] @{thms type_definition_lexample_pre_lexample}
    @{thms corec_lexample_def} @{thms lexample_dtor_corec_transfer} @{thms rel_pre_lexample_def} @{thms }
    pgs_lexample pss_lexample qssss_lexample gssss_lexample *})
done

term corec_ltree_mutual
term corec_lforest_mutual
lemma ltree_mutual_lforest_mutual_corec_transfer:
  "(                    (S1 ===> R) ===> (S1 ===> op =) ===> (S1 ===> rel_lforest_mutual R) ===> (S1 ===> S2) ===>
    (S2 ===> op =) ===>
                        (S2 ===> op =) ===> (S2 ===> rel_ltree_mutual R) ===> (S2 ===> S1) ===> (S2 ===> op =) ===> (S2 ===> rel_lforest_mutual R) ===> (S2 ===> S2) ===>
    S1 ===> rel_ltree_mutual R) corec_ltree_mutual corec_ltree_mutual"
"((S1 ===> R) ===> (S1 ===> op =) ===> (S1 ===> rel_lforest_mutual R) ===> (S1 ===> S2) ===>
    (S2 ===> op =) ===>
    (S2 ===> op =) ===> (S2 ===> rel_ltree_mutual R) ===> (S2 ===> S1) ===> (S2 ===> op =) ===> (S2 ===> rel_lforest_mutual R) ===> (S2 ===> S2) ===>
    S2 ===> rel_lforest_mutual R) corec_lforest_mutual corec_lforest_mutual"

apply (tactic {* mk_corec_transfer_tac @{context} [@{cterm S1}, @{cterm S2}] [@{cterm R}] @{thms BNF_Composition.type_definition_id_bnf_UNIV BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms corec_ltree_mutual_def corec_lforest_mutual_def} @{thms ltree_mutual.dtor_corec_transfer lforest_mutual.dtor_corec_transfer} @{thms rel_pre_ltree_mutual_def rel_pre_lforest_mutual_def} @{thms }
    pgs_ltree_mutual_lforest_mutual pss_ltree_mutual_lforest_mutual qssss_ltree_mutual_lforest_mutual gssss_ltree_mutual_lforest_mutual *})
done

term corec_lF1
lemma lF1_corec_transfer:
  "((S ===> rel_fun (op =) R) ===>
    S ===> rel_lF1 R) corec_lF1 corec_lF1"
  apply (tactic {* mk_corec_transfer_tac @{context} [@{cterm S}] [@{cterm R}] @{thms BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms corec_lF1_def} @{thms lF1.dtor_corec_transfer} @{thms rel_pre_lF1_def} @{thms }
    pgs_lF1 pss_lF1 qssss_lF1 gssss_lF1 *})
done


term corec_lF1'
lemma lF1'_corec_transfer:
  "((S ===> rel_dead_pair R) ===>
    S ===> rel_lF1' R) corec_lF1' corec_lF1'"
  apply (tactic {* mk_corec_transfer_tac @{context} [@{cterm S}] [@{cterm R}] @{thms BNF_Composition.type_definition_id_bnf_UNIV }
    @{thms corec_lF1'_def} @{thms lF1'.dtor_corec_transfer} @{thms rel_pre_lF1'_def} @{thms }
    pgs_lF1' pss_lF1' qssss_lF1' gssss_lF1' *})
done

term corec_lF2
lemma lF2_corec_transfer:
  "((S ===> op =  ===> op = ===> R) ===>
    S ===> rel_lF2 R) corec_lF2 corec_lF2"
  apply (tactic {* mk_corec_transfer_tac @{context} [@{cterm S}] [@{cterm R}] @{thms BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms corec_lF2_def} @{thms lF2.dtor_corec_transfer} @{thms rel_pre_lF2_def} @{thms }
    pgs_lF2 pss_lF2 qssss_lF2 gssss_lF2 *})
done

term corec_lF3
lemma lF3_corec_transfer:
  "((S ===> op =) ===> (S ===> op =) ===> (S ===> R) ===>
    S ===> rel_lF3 R) corec_lF3 corec_lF3"
  apply (tactic {* mk_corec_transfer_tac @{context} [@{cterm S}] [@{cterm R}] @{thms BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms corec_lF3_def} @{thms lF3.dtor_corec_transfer} @{thms rel_pre_lF3_def} @{thms fun.rel_eq}
    pgs_lF3 pss_lF3 qssss_lF3 gssss_lF3 *})
done

term corec_lF4
lemma lF4_corec_transfer:
  "((S ===> rel_dead_pair op =) ===> (S ===> R) ===>
    S ===> rel_lF4 R) corec_lF4 corec_lF4"
  apply (tactic {* mk_corec_transfer_tac @{context} [@{cterm S}] [@{cterm R}] @{thms BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms corec_lF4_def} @{thms lF4.dtor_corec_transfer} @{thms rel_pre_lF4_def} @{thms dead_pair.rel_eq}
    pgs_lF4 pss_lF4 qssss_lF4 gssss_lF4 *})
done

term corec_lF5
lemma lF5_corec_transfer:
  "((S ===> R) ===> (S ===> op =) ===> (S ===> rel_lF5 R) ===> (S ===> S) ===> (S ===> R) ===>
    S ===> rel_lF5 R) corec_lF5 corec_lF5 "
  apply (tactic {* mk_corec_transfer_tac @{context} [@{cterm S}] [@{cterm R}] @{thms BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms corec_lF5_def} @{thms lF5.dtor_corec_transfer} @{thms rel_pre_lF5_def} @{thms }
    pgs_lF5 pss_lF5 qssss_lF5 gssss_lF5 *})
done

term corec_lA1
term corec_lA2
lemma lA1_lA2_corec_transfer:
  "((S1 ===> op =) ===>
    (S2 ===> R) ===>
    S1 ===> rel_lA1 R) corec_lA1 corec_lA1"
  "((S1 ===> op =) ===>
    (S2 ===> R) ===>
    S2 ===> rel_lA2 R) corec_lA2 corec_lA2"
  apply (tactic {* mk_corec_transfer_tac @{context} [@{cterm S1}, @{cterm S2}] [@{cterm R}] @{thms BNF_Composition.type_definition_id_bnf_UNIV BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms corec_lA1_def corec_lA2_def} @{thms lA1.dtor_corec_transfer lA2.dtor_corec_transfer} @{thms rel_pre_lA1_def rel_pre_lA2_def} @{thms }
    pgs_lA1_lA2 pss_lA1_lA2 qssss_lA1_lA2 gssss_lA1_lA2 *})
done

term corec_live_direct
lemma live_direct_corec_transfer:
  "((S ===> R) ===>
    S ===> rel_live_direct R) corec_live_direct corec_live_direct"
  apply (tactic {* mk_corec_transfer_tac @{context} [@{cterm S}] [@{cterm R}] @{thms BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms corec_live_direct_def} @{thms live_direct.dtor_corec_transfer} @{thms rel_pre_live_direct_def} @{thms }
    pgs_live_direct pss_live_direct qssss_live_direct gssss_live_direct *})
done

term corec_live_indirect
lemma live_indirect_corec_transfer:
  "((S ===> rel_pair R op =) ===>
    S ===> rel_live_indirect R) corec_live_indirect corec_live_indirect"
  apply (tactic {* mk_corec_transfer_tac @{context} [@{cterm S}] [@{cterm R}] @{thms BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms corec_live_indirect_def} @{thms live_indirect.dtor_corec_transfer} @{thms rel_pre_live_indirect_def} @{thms }
    pgs_live_indirect pss_live_indirect qssss_live_indirect gssss_live_indirect *})
done

term corec_live_indirect'
lemma live_indirect'_corec_transfer:
  "((S ===> rel_pair R1 R2) ===>
    S ===> rel_live_indirect' R1 R2) corec_live_indirect' corec_live_indirect'"
  apply (tactic {* mk_corec_transfer_tac @{context} [@{cterm S}] [@{cterm R1}, @{cterm R2}] @{thms BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms corec_live_indirect'_def} @{thms live_indirect'.dtor_corec_transfer} @{thms rel_pre_live_indirect'_def} @{thms }
    pgs_live_indirect' pss_live_indirect' qssss_live_indirect' gssss_live_indirect' *})
done

term corec_nested_corecursive
lemma nested_corecursive_corec_transfer:
  "((S ===> R) ===> (S ===> rel_pair op = (rel_sum (rel_nested_corecursive R) S)) ===>
    S ===> rel_nested_corecursive R) corec_nested_corecursive corec_nested_corecursive"
  apply (tactic {* mk_corec_transfer_tac @{context} [@{cterm S}] [@{cterm R}] @{thms BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms corec_nested_corecursive_def} @{thms nested_corecursive.dtor_corec_transfer} @{thms rel_pre_nested_corecursive_def} @{thms }
    pgs_nested_corecursive pss_nested_corecursive qssss_nested_corecursive gssss_nested_corecursive *})
done

term corec_live_indirect_and_corecursive_indirect
lemma live_indirect_and_corecursive_indirect_corec_transfer:
  "((S ===> rel_pair R (rel_sum (rel_live_indirect_and_corecursive_indirect R) S)) ===>
    S ===> rel_live_indirect_and_corecursive_indirect R) corec_live_indirect_and_corecursive_indirect corec_live_indirect_and_corecursive_indirect"
  apply (tactic {* mk_corec_transfer_tac @{context} [@{cterm S}] [@{cterm R}] @{thms BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms corec_live_indirect_and_corecursive_indirect_def} @{thms live_indirect_and_corecursive_indirect.dtor_corec_transfer} @{thms rel_pre_live_indirect_and_corecursive_indirect_def} @{thms }
    pgs_live_indirect_and_corecursive_indirect pss_live_indirect_and_corecursive_indirect qssss_live_indirect_and_corecursive_indirect gssss_live_indirect_and_corecursive_indirect *})
done

term corec_mutually_corec_direct
lemma mutually_corec_direct_corec_transfer:
  "((S ===> R) ===> (S ===> op =) ===> (S ===> rel_mutually_corec_direct R) ===> (S ===> S) ===>
    S ===> rel_mutually_corec_direct R) corec_mutually_corec_direct corec_mutually_corec_direct"
  apply (tactic {* mk_corec_transfer_tac @{context} [@{cterm S}] [@{cterm R}] @{thms BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms corec_mutually_corec_direct_def} @{thms mutually_corec_direct.dtor_corec_transfer} @{thms rel_pre_mutually_corec_direct_def} @{thms }
    pgs_mutually_corec_direct pss_mutually_corec_direct qssss_mutually_corec_direct gssss_mutually_corec_direct *})
done

term corec_mutually_corec_indirect1
term corec_mutually_corec_indirect2
lemma mutually_corec_indirect1_mutually_corec_indirect2_corec_transfer:
  "((S1 ===> R) ===> (S1 ===> R) ===> (S1 ===> rel_pair op = (rel_sum (rel_mutually_corec_indirect2 R) S2)) ===>
    (S2 ===> R) ===> (S2 ===> R) ===> (S2 ===> rel_pair op = (rel_sum (rel_mutually_corec_indirect2 R) S2)) ===>
    S1 ===> rel_mutually_corec_indirect1 R) corec_mutually_corec_indirect1 corec_mutually_corec_indirect1"

  "((S1 ===> R) ===> (S1 ===> R) ===> (S1 ===> rel_pair op = (rel_sum (rel_mutually_corec_indirect2 R) S2)) ===>
    (S2 ===> R) ===> (S2 ===> R) ===> (S2 ===> rel_pair op = (rel_sum (rel_mutually_corec_indirect2 R) S2)) ===>
    S2 ===> rel_mutually_corec_indirect2 R) corec_mutually_corec_indirect2 corec_mutually_corec_indirect2"
  apply (tactic {* mk_corec_transfer_tac @{context} [@{cterm S1}, @{cterm S2}] [@{cterm R}] @{thms type_definition_mutually_corec_indirect1_pre_mutually_corec_indirect1 type_definition_mutually_corec_indirect2_pre_mutually_corec_indirect2}
    @{thms corec_mutually_corec_indirect1_def corec_mutually_corec_indirect2_def} @{thms mutually_corec_indirect1.dtor_corec_transfer mutually_corec_indirect2.dtor_corec_transfer} @{thms rel_pre_mutually_corec_indirect1_def rel_pre_mutually_corec_indirect2_def} @{thms }
    pgs_mutually_corec_indirect1_mutually_corec_indirect2 pss_mutually_corec_indirect1_mutually_corec_indirect2 qssss_mutually_corec_indirect1_mutually_corec_indirect2 gssss_mutually_corec_indirect1_mutually_corec_indirect2 *})
done

term corec_three_args
lemma three_args_corec_transfer:
  "((S ===> R) ===> (S ===> R) ===> (S ===> R) ===>
    S ===> rel_three_args R) corec_three_args corec_three_args"
  apply (tactic {* mk_corec_transfer_tac @{context} [@{cterm S}] [@{cterm R}] @{thms BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms corec_three_args_def} @{thms three_args.dtor_corec_transfer} @{thms rel_pre_three_args_def} @{thms }
    pgs_three_args pss_three_args qssss_three_args gssss_three_args *})
done

term corec_five_args
lemma five_args_corec_transfer:
  "((S ===> R) ===> (S ===> R) ===> (S ===> R) ===> (S ===> R) ===> (S ===> R) ===>
    S ===> rel_five_args R) corec_five_args corec_five_args"
  apply (tactic {* mk_corec_transfer_tac @{context} [@{cterm S}] [@{cterm R}] @{thms type_definition_five_args_pre_five_args}
    @{thms corec_five_args_def} @{thms five_args.dtor_corec_transfer} @{thms rel_pre_five_args_def} @{thms }
    pgs_five_args pss_five_args qssss_five_args gssss_five_args *})
done

lemma foo_corec_transfer:
  "((S ===> op =) ===> (S ===> op =) ===> S ===> rel_foo R) corec_foo corec_foo"
  apply (tactic {* mk_corec_transfer_tac @{context} [@{cterm S}] [@{cterm R}] @{thms type_definition_foo_pre_foo}
    @{thms corec_foo_def} @{thms foo.dtor_corec_transfer} @{thms rel_pre_foo_def} @{thms }
    pgs_foo pss_foo qssss_foo gssss_foo *})
done

thm bar.corec_transfer[no_vars]
lemma bar_corec_transfer:
  "((S ===> op =) ===> (S ===> op =) ===> S ===> op =) corec_bar corec_bar"
  apply (tactic {* mk_corec_transfer_tac @{context} [@{cterm S}] [] @{thms BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms corec_bar_def} @{thms bar.dtor_corec_transfer} @{thms rel_pre_bar_def} @{thms }
    pgs_bar pss_bar qssss_bar gssss_bar *})
done
end
