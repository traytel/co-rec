theory Corec_Ctr_Suite
imports Corec_Suite_Base
begin

primcorec f1_s2' :: "nat \<Rightarrow> s2'" where
  "n = 0 \<Longrightarrow> f1_s2' n = Y1' ()"
| "_ \<Longrightarrow> f1_s2' n = Y2' ()"
print_theorems

primcorec f1_s5 :: "nat \<Rightarrow> s5" where
  "n = 1 \<Longrightarrow> f1_s5 n = Z1"
| "n = 3 \<Longrightarrow> f1_s5 n = Z3"
| "n = 4 \<Longrightarrow> f1_s5 n = Z4 ()"
| "n = 5 \<Longrightarrow> f1_s5 n = Z5"
| "n > 500 \<Longrightarrow> f1_s5 n = Z2 n (Suc n)"
print_theorems

primcorec (sequential) f2_s5 :: "nat \<Rightarrow> nat \<Rightarrow> s5" where
  "m = 1 \<Longrightarrow> n = 1 \<Longrightarrow> f2_s5 m n = Z1"
| "m = 3 \<Longrightarrow> n = 3 \<Longrightarrow> f2_s5 m n = Z3"
| "m = 4 \<Longrightarrow> n = 4 \<Longrightarrow> f2_s5 m n = Z4 ()"
| "m = 5 \<Longrightarrow> n = 5 \<Longrightarrow> f2_s5 m n = Z5"
| "m > 500 \<Longrightarrow> n > 500 \<Longrightarrow> f2_s5 m n = Z2 n (Suc n)"
print_theorems

primcorec f1_stream :: "'a stream \<Rightarrow> 'a stream \<Rightarrow> 'a stream" where
  "f1_stream xs ys = SCons (shd xs) (if some_cond then ys else f1_stream (stl xs) ys)"
print_theorems

primcorec f2_stream :: "xnat stream \<Rightarrow> xnat stream \<Rightarrow> xnat stream" where
  "f2_stream xs ys =
     SCons (shd xs) (case shd xs of XZero \<Rightarrow> xs | XSuc _ \<Rightarrow> f2_stream (stl xs) ys)"
print_theorems

primcorec f1_llist :: "nat \<Rightarrow> nat llist" where
  "f1_llist n = LCons n (f1_llist (Suc n))"
print_theorems

primcorec f2_llist :: "('a \<Rightarrow> bool) \<Rightarrow> 'a llist \<Rightarrow> 'a llist" where
  "lnull xs \<or> \<not> p (lhd xs) \<Longrightarrow> f2_llist p xs = LNil"
| "_ \<Longrightarrow> f2_llist q ys = LCons (lhd ys) (f2_llist q (ltl ys))"
print_theorems

primcorec f3_llist :: "nat \<Rightarrow> bool \<Rightarrow> (nat \<times> bool) llist" where
  "f3_llist n B = LCons (n, B) (let g = f3_llist (Suc n) in g (\<not> B))"
print_theorems

primcorec f4_llist :: "nat llist" where
  "f4_llist = LCons 0 f4_llist"
print_theorems

primcorec f5_llist :: "'a llist \<Rightarrow> 'a llist \<Rightarrow> 'a llist" where
  "lnull xs \<Longrightarrow> lnull ys \<Longrightarrow> f5_llist xs ys = LNil" |
  "lnull xs \<and> \<not> lnull ys \<or> \<not> lnull xs \<Longrightarrow>
   f5_llist xs ys = LCons
     (if lnull xs then (case ys of LNil \<Rightarrow> undefined | LCons z _ \<Rightarrow> z) else lhd xs)
     (if lnull xs then (case ys of LNil \<Rightarrow> undefined | LCons _ zs \<Rightarrow> zs) else f5_llist (ltl xs) ys)"
print_theorems

primcorec f6_llist :: "xnat \<Rightarrow> xnat llist" where
  "f6_llist n = LCons n (case n of XZero \<Rightarrow> LNil | XSuc m \<Rightarrow> f6_llist m)"
print_theorems

primcorec f7_llist :: "xnat \<Rightarrow> xnat llist" where
  "f7_llist n = LCons n (case n of XZero \<Rightarrow> LNil | XSuc m \<Rightarrow>
     (if m = XZero then f7_llist n else
        (case m of XSuc m' \<Rightarrow> f7_llist m')))"
print_theorems

primcorec f8_llist :: "xnat \<Rightarrow> xnat llist" where
  "f8_llist n = LCons n (case n of XZero \<Rightarrow> f8_llist n | XSuc m \<Rightarrow> LCons m LNil)"
print_theorems

primcorec f9_llist :: "xnat \<Rightarrow> xnat llist" where
  "f9_llist n = LCons n (if n = XZero then
       case n of XZero \<Rightarrow> f9_llist n
       | XSuc m \<Rightarrow> (if m = XZero then LCons m LNil else f9_llist m)
     else
       case n of XZero \<Rightarrow> f9_llist n | XSuc m \<Rightarrow> LCons m LNil)"
print_theorems

primcorec f10_llist :: "nat \<Rightarrow> nat llist" where
  "f10_llist n = LCons n (case n of 0 \<Rightarrow> LNil | Suc m \<Rightarrow> LCons m LNil)"
print_theorems

primcorec f1_some_passive :: "nat \<Rightarrow> (nat, 'b) some_passive" where
  "n = 0 \<Longrightarrow> f1_some_passive n = SP2 n"
| "_ \<Longrightarrow> f1_some_passive n = SP1 (f1_some_passive (Suc n))"
print_theorems

primcorec f1_some_dead :: "nat \<Rightarrow> ('b, nat) some_dead" where
  "f1_some_dead n = SD1 (Suc n)"
print_theorems

primcorec f1_tree :: "int \<Rightarrow> int tree"
      and f1_forest :: "int \<Rightarrow> bool \<Rightarrow> int forest" where
  "i = 0 \<Longrightarrow> f1_tree i = TEmpty"
| "j \<noteq> 0 \<Longrightarrow> f1_tree j = TNode j (f1_forest (j + 2) True)"
| "\<lbrakk>i = 0; b\<rbrakk> \<Longrightarrow> f1_forest i b = FNil"
| "_ \<Longrightarrow> f1_forest j b = FCons (f1_tree j) (f1_forest (j + 3) (\<not> b))"
print_theorems

primcorec f2_tree :: "int \<Rightarrow> int tree" where
  "i = 0 \<Longrightarrow> f2_tree i = TEmpty"
| "j \<noteq> 0 \<Longrightarrow> f2_tree j = TNode j (f1_forest (j + 2) True)"
print_theorems

primcorec f3_forest :: "int \<Rightarrow> bool \<Rightarrow> int forest" where
  "\<lbrakk>i = 0; b\<rbrakk> \<Longrightarrow> f3_forest i b = FNil"
| "_ \<Longrightarrow> f3_forest j b = FCons (f1_tree j) (f3_forest (j + 3) (\<not> b))"
print_theorems

primcorec f1_x :: "nat \<Rightarrow> x"
      and f1_y :: "int \<Rightarrow> int \<Rightarrow> y"
      and f1_z :: "nat \<Rightarrow> z" where
  "f1_x _ = X"
| "f1_y _ _ = Y"
| "f1_z n = Z (Suc n)"
print_theorems

primcorec f1_hfset :: "bool \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> hfset" where
  "f1_hfset b \<mu> i = HFset (lmap (\<lambda>(n, b). f1_hfset b n 0) (f3_llist \<mu> b))"
print_theorems

primcorec f2_hfset :: "bool \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> hfset"
      and f2_hfset_llist :: "nat \<Rightarrow> bool \<Rightarrow> hfset llist" where
  "f2_hfset b \<mu> i = HFset (f2_hfset_llist \<mu> b)"
| "f2_hfset_llist n B = LCons (f2_hfset B n 0) (f2_hfset_llist (Suc n) (\<not> B))"
print_theorems

definition rest :: "hfset llist" where "rest = undefined"
primcorec f3_hfset :: "bool \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> hfset" where
  "f3_hfset b \<mu> i =
     HFset (LCons (f3_hfset b \<mu> 0) (LCons (f3_hfset True \<mu> 1)
       (LCons (undefined (Suc 0)) rest)))"
print_theorems

primcorec f1_btree :: "'a llist \<Rightarrow> 'a btree" where
  "lnull xs \<Longrightarrow> f1_btree xs = LNull"
| "_ \<Longrightarrow> f1_btree xs =
    LNode (lhd xs) (map_xpair (\<lambda>g. f1_btree (g xs)) (\<lambda>h. f1_btree (h (h xs))) (XPair id ltl))"
print_theorems

primcorec f2_btree :: "'a llist \<Rightarrow> 'a btree" where
  "lnull xs \<Longrightarrow> f2_btree xs = LNull"
| "_ \<Longrightarrow> f2_btree xs = LNode (lhd xs) (XPair (f2_btree xs) (f2_btree (ltl (ltl xs))))"
print_theorems

primcorec f3_btree :: "'a llist \<Rightarrow> 'a btree" where
  "lnull xs \<Longrightarrow> f3_btree xs = LNull"
| "_ \<Longrightarrow> f3_btree xs = LNode (lhd xs) (XPair (f3_btree xs) LNull)"
print_theorems

primcorec
  f1_ctree :: "nat \<Rightarrow> nat ctree"
where
  "f1_ctree n = CNode n (map_copt f1_ctree (CSome (Suc n))) (map_copt f1_ctree (CSome (Suc n)))"
print_theorems

primcorec
  f2_ctree :: "nat \<Rightarrow> nat ctree" and
  f2_ctree_copt :: "nat \<Rightarrow> nat ctree copt"
where
  "f2_ctree n = CNode n (f2_ctree_copt (Suc n)) (f2_ctree_copt (Suc n))" |
  "f2_ctree_copt n = CSome (f2_ctree n)"
print_theorems

primcorec
  f3_ctree :: "nat \<Rightarrow> nat ctree" and
  f3_ctree_copt :: "nat \<Rightarrow> nat ctree copt"
where
  "f3_ctree n = CNode n (f3_ctree_copt (Suc n)) (map_copt f3_ctree (CSome (Suc n)))" |
  "f3_ctree_copt n = CSome (f3_ctree n)"
print_theorems

primcorec
  f4_ctree :: "nat \<Rightarrow> nat ctree" and
  f4_ctree_copt :: "nat \<Rightarrow> nat ctree copt"
where
  "f4_ctree n = CNode n (map_copt f4_ctree (CSome (Suc n))) (f4_ctree_copt (Suc n))" |
  "f4_ctree_copt n = CSome (f4_ctree n)"
print_theorems

primcorec f1_copt :: "nat \<Rightarrow> nat copt" where
  "n = 0 \<Longrightarrow> f1_copt n = CNone" |
  "_ \<Longrightarrow> f1_copt n = (let no = CSome n in no)"
print_theorems

primcorec f2_copt :: "nat \<Rightarrow> nat copt" where
  "n = 0 \<Longrightarrow> f2_copt n = CNone" |
  "_ \<Longrightarrow> f2_copt n = (let csome = CSome in csome n)"
print_theorems

primcorec fk :: "nat \<Rightarrow> 'a \<Rightarrow> 'b \<Rightarrow> ('a, 'b) k"
      and fl :: "nat \<Rightarrow> nat \<Rightarrow> 'b \<Rightarrow> ('a, 'b) l" where
  "n = 0 \<Longrightarrow> fk n a b = K1"
| "n > 10 \<Longrightarrow> \<not> n < 20 \<Longrightarrow> fk n a b = K3 (fk (Suc n) a b)"
| "n = 5 \<Longrightarrow> fk n a b = K4 (map (fl n (Suc n)) [b, b])"
| "_ \<Longrightarrow> fk _ a b = K2 a b"
| "m > 2 \<Longrightarrow> n > 3 \<Longrightarrow> m > n \<Longrightarrow> fl m n b = L2 (undefined m n b)"
| "n > m \<Longrightarrow> fl m n b = L1 (if m = 0 then K1 else if n = 0 then fk n undefined b else K4 [])"
print_theorems

end
