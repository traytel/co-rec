theory Corec_Scratch_Dtr
imports Corec_Scratch_Ctr
begin

(*
primcorecursive f_simple "nt \<Rightarrow> simple" where
  "is_X1 (f_simple n) \<longleftrightarrow> n = Zo"
*)

(*
primcorecursive f_simple "nt \<Rightarrow> simple" where
  "f_simple n = X1 \<longleftrightarrow> n = Zo"
*)

(*
primcorecursive f_simple "nt \<Rightarrow> simple" where
  "f_simple n = X2 \<longleftrightarrow> n \<noteq> Zo"
*)

lemma not_trans: "\<lbrakk>r = t; s \<noteq> t\<rbrakk> \<Longrightarrow> r \<noteq> s" by metis

lemma is_X1_f_simple: "is_X1 (f_simple n) \<longleftrightarrow> n = Zo"
unfolding f_simple_def
apply (rule iffI)
 apply (erule contrapos_pp)
 apply (erule simple.disc_corec(2))
apply (erule simple.disc_corec(1))
done

lemma f_simple_eq_X1: "f_simple n = X1 \<longleftrightarrow> n = Zo"
unfolding f_simple_def
apply (rule iffI)
 apply (erule contrapos_pp)
 apply (erule simple.corec(2)[THEN not_trans])
 apply (rule simple.distinct(1))
apply (erule simple.corec(1))
done

lemma f_simple_eq_X2: "f_simple n = X2 \<longleftrightarrow> n \<noteq> Zo"
unfolding f_simple_def
apply (rule iffI)
 apply (erule contrapos_pp)
 apply (rule simple.corec(1)[THEN not_trans])
  apply (simp only: not_not)
 apply (rule simple.distinct(2))
apply (erule simple.corec(2))
done

(*
primcorecursive "from" :: "nt \<Rightarrow> nt llist" where
"lnull (from n) \<longleftrightarrow> False" |
"lhd (from n) = n" (* for LCons *) |
"ltl (from n) = from (Sc n)" (* for LCons *)
*)

lemma lnull_from: "lnull (from n) \<longleftrightarrow> False"
unfolding from_def
apply (rule iffI)
 apply (erule contrapos_pp)
 apply (erule llist.disc_corec(2))
apply (erule llist.disc_corec(1))
done

lemma lhd_from: "\<not> lnull (from n) \<Longrightarrow> lhd (from n) = n"
apply (subst from_def)
apply (rule llist.sel_corec(1))
apply (rule notI)
apply assumption
done

lemma ltl_from: "\<not> lnull (from n) \<Longrightarrow> ltl (from n) = from (Sc n)"
apply (subst from_def)+
apply (rule llist.sel_corec(2)[THEN trans])
 apply (rule notI)
 apply assumption
apply (simp only: if_False)
done


(*
primcorecursive ltake_while :: "('a \<Rightarrow> bool) \<Rightarrow> 'a llist \<Rightarrow> 'a llist" where
"lnull (ltake_while p xs) \<longleftrightarrow> lnull xs \<or> \<not> p (lhd xs)"
"lhd (ltake_while p xs) = lhd xs" (* for LCons *) |
"ltl (ltake_while p xs) = ltake_while p (ltl xs)" (* for LCons *)
*)

definition ltake_while :: "('a \<Rightarrow> bool) \<Rightarrow> 'a llist \<Rightarrow> 'a llist" where
"ltake_while p xs =
   llist_corec (\<lambda>(p, xs). lnull xs \<or> \<not> p (lhd xs)) (\<lambda>(p, xs) \<Rightarrow> lhd xs) (\<lambda>_. False) undefined
       (\<lambda>(p, xs). (p, ltl xs)) (p, xs)"

lemma lnull_ltake_while: "lnull (ltake_while p xs) \<longleftrightarrow> lnull xs \<or> \<not> p (lhd xs)"
unfolding ltake_while_def
apply rule
 apply (erule contrapos_pp)
 apply (rule llist.disc_corec(2))
 apply (simp only: split not_False_eq_True)
apply (rule llist.disc_corec(1))
apply (simp only: split not_False_eq_True)
done

lemma lhd_ltake_while: "\<not> lnull (ltake_while p xs) \<Longrightarrow> lhd (ltake_while p xs) = lhd xs"
apply (subst ltake_while_def)
apply (rule llist.sel_corec(1)[THEN trans])
 apply (simp only: lnull_ltake_while split not_False_eq_True)
apply (rule split)
done

lemma ltl_ltake_while:
"\<not> lnull (ltake_while p xs) \<Longrightarrow> ltl (ltake_while p xs) = ltake_while p (ltl xs)"
apply (subst ltake_while_def)+
apply (rule llist.sel_corec(2)[THEN trans])
 apply (simp only: split lnull_ltake_while not_False_eq_True)
apply (simp only: split if_False)
done

end
