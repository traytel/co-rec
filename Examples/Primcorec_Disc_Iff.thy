theory Primcorec_Disc_Iff
imports Main
begin

ML {*
Variable.variant_frees @{context} [@{term "P"}] [("P", ())]
*}

codatatype (lset: 'a) llist =
    lnull: LNil
  | LCons (lhd: 'a) (ltl: "'a llist")
for
  map: lmap
  rel: llist_all2
where
  "lhd LNil = undefined"
| "ltl LNil = LNil"

primcorec ltakeWhile :: "('a \<Rightarrow> bool) \<Rightarrow> 'a llist \<Rightarrow> 'a llist"
where
  "lnull xs \<or> \<not> P (lhd xs) \<Longrightarrow> lnull (ltakeWhile P xs)"
| "lhd (ltakeWhile P xs) = lhd xs"
| "ltl (ltakeWhile P xs) = ltakeWhile P (ltl xs)"

print_theorems

codatatype ('b, 'c, 'd, 'e) some_passive =
  SP1 "('b, 'c, 'd, 'e) some_passive" | SP2 'b | SP3 'c | SP4 'd | SP5 'e

primcorec shuffle_sp :: "('a, 'b, 'c, 'd) some_passive \<Rightarrow> ('d, 'a, 'b, 'c) some_passive" where
  "shuffle_sp sp =
     (case sp of
       SP1 sp' \<Rightarrow> SP1 (shuffle_sp sp')
     | SP2 a \<Rightarrow> SP3 a
     | SP3 b \<Rightarrow> SP4 b
     | SP4 c \<Rightarrow> SP5 c
     | SP5 d \<Rightarrow> SP2 d)"

thm shuffle_sp.exhaust

codatatype u = U
codatatype simple = X1 | X2 | X3 | X4

primcorec simple_of_bools :: "bool \<Rightarrow> bool \<Rightarrow> simple" where
  "simple_of_bools b b' = (if b then if b' then X1 else X2 else if b' then X3 else X4)"

primcorec rename_lam :: "(string \<Rightarrow> string) \<Rightarrow> lambda \<Rightarrow> lambda" where
  "rename_lam f l =
     (case l of
       Var s \<Rightarrow> Var (f s)
     | App l l' \<Rightarrow> App (rename_lam f l) (rename_lam f l')
     | Abs s l \<Rightarrow> Abs (f s) (rename_lam f l)
     | Let SL l \<Rightarrow> Let (fimage (map_pair f (rename_lam f)) SL) (rename_lam f l))"

primcorec
  j1_sum :: "('a\<Colon>{zero,one,plus}) \<Rightarrow> 'a J1" and
  j2_sum :: "'a \<Rightarrow> 'a J2"
where
  "n = 0 \<Longrightarrow> is_J11 (j1_sum n)" |
  "un_J111 (j1_sum _) = 0" |
  "un_J112 (j1_sum _) = j1_sum 0" |
  "un_J121 (j1_sum n) = n + 1" |
  "un_J122 (j1_sum n) = j2_sum (n + 1)" |
  "n = 0 \<Longrightarrow> is_J21 (j2_sum n)" |
  "un_J221 (j2_sum n) = j1_sum (n + 1)" |
  "un_J222 (j2_sum n) = j2_sum (n + 1)"

primcorec forest_of_mylist :: "'a tree mylist \<Rightarrow> 'a forest" where
  "forest_of_mylist ts =
     (case ts of
       MyNil \<Rightarrow> FNil
     | MyCons t ts \<Rightarrow> FCons t (forest_of_mylist ts))"

primcorec mylist_of_forest :: "'a forest \<Rightarrow> 'a tree mylist" where
  "mylist_of_forest f =
     (case f of
       FNil \<Rightarrow> MyNil
     | FCons t ts \<Rightarrow> MyCons t (mylist_of_forest ts))"

primcorec semi_stream :: "'a stream \<Rightarrow> 'a stream" where
  "semi_stream s = Stream (shd s) (semi_stream (stl (stl s)))"

primcorec
  tree'_of_stream :: "'a stream \<Rightarrow> 'a tree'" and
  branch_of_stream :: "'a stream \<Rightarrow> 'a branch"
where
  "tree'_of_stream s =
     TNode' (branch_of_stream (semi_stream s)) (branch_of_stream (semi_stream (stl s)))" |
  "branch_of_stream s = (case s of Stream h t \<Rightarrow> Branch h (tree'_of_stream t))"

primcorec
  freeze_exp :: "('b \<Rightarrow> 'a) \<Rightarrow> ('a, 'b) exp \<Rightarrow> ('a, 'b) exp" and
  freeze_trm :: "('b \<Rightarrow> 'a) \<Rightarrow> ('a, 'b) trm \<Rightarrow> ('a, 'b) trm" and
  freeze_factor :: "('b \<Rightarrow> 'a) \<Rightarrow> ('a, 'b) factor \<Rightarrow> ('a, 'b) factor"
where
  "freeze_exp g e =
     (case e of
       Term t \<Rightarrow> Term (freeze_trm g t)
     | Sum t e \<Rightarrow> Sum (freeze_trm g t) (freeze_exp g e))" |
  "freeze_trm g t =
     (case t of
       Factor f \<Rightarrow> Factor (freeze_factor g f)
     | Prod f t \<Rightarrow> Prod (freeze_factor g f) (freeze_trm g t))" |
  "freeze_factor g f =
     (case f of
       C a \<Rightarrow> C a
     | V b \<Rightarrow> C (g b)
     | Paren e \<Rightarrow> Paren (freeze_exp g e))"

primcorec poly_unity :: "'a poly_unit" where
  "poly_unity = U (\<lambda>_. poly_unity)"

primcorec build_cps :: "('a \<Rightarrow> 'a) \<Rightarrow> ('a \<Rightarrow> bool stream) \<Rightarrow> 'a \<Rightarrow> bool stream \<Rightarrow> 'a cps" where
  "shd b \<Longrightarrow> build_cps f g a b = CPS1 a" |
  "_ \<Longrightarrow> build_cps f g a b = CPS2 (\<lambda>a. build_cps f g (f a) (g a))"



codatatype simple = X1 | X2

primcorec simple_of_bools :: "bool \<Rightarrow> bool \<Rightarrow> simple" where
  "simple_of_bools b b' = (if b then X1 else X2)"


ML {* List.app tracing (replicate 1000 "\n\n\n\n\n\n\n\n\n\n") *}

codatatype (lset: 'a) llist (map: lmap rel: llist_all2) =
  lnull: LNil (defaults ltl: LNil)
| LCons (lhd: 'a) (ltl: "'a llist")

    primcorec lappend :: "'a llist \<Rightarrow> 'a llist \<Rightarrow> 'a llist" where
      "lappend xs ys =
         (case xs of
            LNil \<Rightarrow> ys
          | LCons x xs' \<Rightarrow> LCons x (lappend xs' ys))"


    codatatype (sset: 'a) stream (map: smap rel: stream_all2) =
      SCons (shd: 'a) (stl: "'a stream")

    codatatype 'a process =
      Fail
    | Skip (cont: "'a process")
    | Action (prefix: 'a) (cont: "'a process")
    | Choice (left: "'a process") (right: "'a process")

    consts every_snd :: "'a stream \<Rightarrow> 'a stream"

    primcorec
      random_process :: "'a stream \<Rightarrow> (int \<Rightarrow> int) \<Rightarrow> int \<Rightarrow> 'a process"
    where
      "random_process s f n =
         (if n mod 4 = 0 then
            Fail
          else if n mod 4 = 1 then
            Skip (random_process s f (f n))
          else if n mod 4 = 2 then
            Action (shd s) (random_process (stl s) f (f n))
          else
            Choice (random_process (every_snd s) (f \<circ> f) (f n))
              (random_process (every_snd (stl s)) (f \<circ> f) (f (f n))))"





codatatype 'a llist = LNil | LCons 'a "'a llist"

primcorec lappend :: "'a llist \<Rightarrow> 'a llist \<Rightarrow> 'a llist" where
  "lappend xs ys =
     (case xs of
        LNil \<Rightarrow> ys
      | LCons x xs' \<Rightarrow> LCons x (lappend xs' ys))"


codatatype 'a treeFI = is_Tree: Tree (lab: 'a)

(* Tree reverse:*)
primcorecursive (exhaustive) trev where
  "trev t = Tree (lab t)"
.

print_theorems

thm trev.disc_iff

codatatype 'a t = T1 | is_T2: T2

primcorecursive
  g :: "'a t"
where
  "g = T2"
.

print_theorems





codatatype 'a k = C1 'a | C2 'a | C3

primcorecursive g where
"x = 0 \<Longrightarrow> g x = C1 x" |
"x > (2::nat) \<Longrightarrow> g x = C2 x"
apply auto done

thm g.disc_iff
thm g.exhaust
thm g.code

print_theorems


codatatype 'a k = C1 'a | C2 'a | C3 'a | C4 'a

primcorec g where
"x = 0 \<Longrightarrow> g x = C1 x" |
"x > (0::nat) \<Longrightarrow> g x = C3 x"

primcorec g where
  "x = 0 \<or> x = 1 - 1 \<Longrightarrow> g x = C1 x"
| "x > 2 \<Longrightarrow> g x = C2 x"
| "x = Suc 0 \<Longrightarrow> x = 1 \<Longrightarrow> x = 2 - 1 \<Longrightarrow> g x = C4 x"
| "_ \<Longrightarrow> g x = C3 x"

thm g.code
thm g.disc_iff
thm g.exhaust g.nchotomy


oops
(* TODO: generate disc_iff also in syntactically exclusive case *)


thm g.disc_iff


primcorecursive (exhaustive) g where
  "x = 0 \<or> x = 1 - 1 \<Longrightarrow> g x = C1 x"
| "x > 2 \<Longrightarrow> g x = C2 x"
| "x = Suc 0 \<Longrightarrow> x = 1 \<Longrightarrow> x = 2 - 1 \<Longrightarrow> g x = C4 x"
| "x = 2 \<Longrightarrow> x = Suc 1 \<Longrightarrow> g x = C3 x"
by auto

thm g.disc_iff

thm g.disc

lemma "is_C3 (g x) = (x = 2 \<and> x = Suc 1)"
apply (rule iffI)
 apply (rule g.exhaust)
    apply (drule g.disc(1), drule k.disc_exclude(3), erule notE, assumption)
   apply (drule g.disc(2), drule k.disc_exclude(7), erule notE, assumption)
  apply (rule conjI, assumption)+
  apply assumption
 apply (drule g.disc(4), assumption+, drule k.disc_exclude(12), erule notE, assumption)
apply (erule g.disc(3)[unfolded atomize_conjL])
done

ML {* open Ctr_Sugar_Util BNF_Util *}

ML {*
val atomize_conjL = @{thm atomize_conjL};

fun mk_primcorec_disc_iff_tac ctxt k exhaust discs disc_excludess =
  HEADGOAL (rtac iffI THEN'
    rtac exhaust THEN'
    EVERY' (map2 (fn disc => fn [] => REPEAT_DETERM o (atac ORELSE' etac conjI)
        | [disc_exclude] =>
          dtac disc THEN' (REPEAT_DETERM o atac) THEN' dtac disc_exclude THEN' etac notE THEN' atac)
      discs disc_excludess) THEN'
    etac (unfold_thms ctxt [atomize_conjL] (nth discs (k - 1))));
*}

ML {*
val k = 3
val exhaust = @{thm g.exhaust};
val discs = @{thms g.disc};
val disc_excludess =
  [@{thms k.disc_exclude(3)}, @{thms k.disc_exclude(7)}, [], @{thms k.disc_exclude(12)}];
*}

lemma "is_C3 (g x) = (x = 2 \<and> x = Suc 1)"
by (tactic {* mk_primcorec_disc_iff_tac @{context} k exhaust discs disc_excludess *})

end
