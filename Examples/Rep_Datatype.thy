theory Rep_Datatype
imports "~~/src/HOL/BNF/BNF"
begin

ML {*
open Datatype_Aux
open Datatype_Data
*}


text {* Mutual Recursion *}

datatype 'a tree0 = Node0 'a "'a tree_list0"
and 'a tree_list0 = TNil0 | TCons0 "'a tree0" "'a tree_list0"

datatype 'a tree = Node 'a "'a tree_list"
and 'a tree_list = TNil | TCons "'a tree" "'a tree_list"

ML {*
val info0 = get_info @{theory} @{type_name tree};
val info1 = get_info @{theory} @{type_name tree_list};
*}

ML {*
val descr =
     [(0, ("Rep_Datatype.tree0", [DtTFree ("'a", ["HOL.type"])],
           [("Rep_Datatype.tree0.Node0", [DtTFree ("'a", ["HOL.type"]), DtRec 1])])),
      (1, ("Rep_Datatype.tree_list0", [DtTFree ("'a", ["HOL.type"])],
           [("Rep_Datatype.tree_list0.TNil0", []),
            ("Rep_Datatype.tree_list0.TCons0", [DtRec 0, DtRec 1])]))] : descr;

val info0 =
  {index = 0,
   descr = descr,
   inject = @{thms tree0.inject},
   distinct = [],
   induct = @{thm tree0_tree_list0.induct},
   inducts =
     @{thms tree0_tree_list0.induct[THEN conjunct1]
            tree0_tree_list0.induct[THEN conjunct2]},
   exhaust = @{thm tree0.exhaust},
   nchotomy = @{thm tree0.nchotomy},
   rec_names = ["Rep_Datatype.tree0.tree0_rec", "Rep_Datatype.tree_list0.tree_list0_rec"],
   rec_rewrites = @{thms tree0.rec tree_list0.rec},
   case_name = "Rep_Datatype.tree0.tree0_case",
   case_rewrites = @{thms tree0.case},
   case_cong = @{thm tree0.case_cong},
   weak_case_cong = @{thm tree0.weak_case_cong},
   split = @{thm tree0.split},
   split_asm = @{thm tree0.split_asm}} : info;

val info1 =
  {index = 1,
   descr = descr,
   inject = @{thms tree_list0.inject},
   distinct = @{thms tree_list0.distinct},
   induct = @{thm tree0_tree_list0.induct},
   inducts =
     @{thms tree0_tree_list0.induct[THEN conjunct1]
            tree0_tree_list0.induct[THEN conjunct2]},
   exhaust = @{thm tree_list0.exhaust},
   nchotomy = @{thm tree_list0.nchotomy},
   rec_names = ["Rep_Datatype.tree0.tree0_rec", "Rep_Datatype.tree_list0.tree_list0_rec"],
   rec_rewrites = @{thms tree0.rec tree_list0.rec},
   case_name = "Rep_Datatype.tree_list0.tree_list0_case",
   case_rewrites = @{thms tree_list0.case},
   case_cong = @{thm tree_list0.case_cong},
   weak_case_cong = @{thm tree_list0.weak_case_cong},
   split = @{thm tree_list0.split},
   split_asm = @{thm tree_list0.split_asm}} : info;
*}

setup {*
register [(@{type_name tree0}, info0), (@{type_name tree_list0}, info1)]
*}

setup {*
interpretation_data (default_config, [@{type_name tree0}, @{type_name tree_list0}])
*}

term tree0_rec
term tree_tree_list_rec_1

term tree_list0_rec
term tree_tree_list_rec_2

ML {*
val info0 = get_info @{theory} @{type_name tree0};
val info1 = get_info @{theory} @{type_name tree_list0};
*}

fun set_tree :: "'a tree0 \<Rightarrow> 'a set"
and set_trees :: "'a tree_list0 \<Rightarrow> 'a set" where
"set_tree (Node0 a ts) = insert a (set_trees ts)" |
"set_trees TNil0 = {}" |
"set_trees (TCons0 t ts) = set_tree t \<union> set_trees ts"

datatype funky0 = Funky0 "funky0 tree0" | Funky0'
datatype funky = Funky "funky tree" | Funky'


text {* Nested Recursion *}

data 'a tre0 = Nod0 'a "'a tre0 list"

datatype 'a tre = Nod 'a "'a tre list"

axiomatization tre0_rec_1 and tre0_rec_2 where
tr1: "tre0_rec_1 f1 f2 f3 (Nod0 a list) = f1 a list (tre0_rec_2 f1 f2 f3 list)" and
tr2a: "tre0_rec_2 f1 f2 f3 [] = f2" and
tr2b: "tre0_rec_2 f1 f2 f3 (tre # list) = f3 tre list (tre0_rec_1 f1 f2 f3 tre) (tre0_rec_2 f1 f2 f3 list)"

axiomatization where
tre0_induct_mult:
"\<lbrakk>\<And>a list. P2 list \<Longrightarrow> P1 (Nod0 a list); P2 []; \<And>tre list. \<lbrakk>P1 tre; P2 list\<rbrakk> \<Longrightarrow> P2 (tre # list)\<rbrakk> \<Longrightarrow>
 P1 tre \<and> P2 list"

lemmas tre0_induct = tre0_induct_mult
lemmas tre0_inducts = tre0_induct[THEN conjunct1] tre0_induct[THEN conjunct2]

(*
lemmas tre0_induct = tre0.induct
lemmas tre0_inducts = tre0_induct
*)

ML {*
val info0 = get_info @{theory} @{type_name tre};
*}

ML {*
val descr =
     [(0, ("Rep_Datatype.tre0", [DtTFree ("'a", ["HOL.type"])],
           [("Rep_Datatype.tre0.Nod0", [DtTFree ("'a", ["HOL.type"]), DtRec 1])])),
      (1, ("List.list", [DtRec 0],
           [("List.list.Nil", []), ("List.list.Cons", [DtRec 0, DtRec 1])]))] : descr;

val info0 =
  {index = 0,
   descr = descr,
   inject = @{thms tre0.inject},
   distinct = [],
   induct = @{thm tre0_induct},
   inducts =
     @{thms tre0_induct[THEN conjunct1]
            tre0_induct[THEN conjunct2]},
   exhaust = @{thm tre0.exhaust},
   nchotomy = @{thm tre0.nchotomy},
   rec_names = ["Rep_Datatype.tre0_rec_1", "Rep_Datatype.tre0_rec_2"],
   rec_rewrites = @{thms tr1 tr2a tr2b},
   case_name = "Rep_Datatype.tre0.tre0_case",
   case_rewrites = @{thms tre0.case},
   case_cong = @{thm tre0.case_cong},
   weak_case_cong = @{thm tre0.weak_case_cong},
   split = @{thm tre0.split},
   split_asm = @{thm tre0.split_asm}} : info;

val info1 = (* BAD *)
  {index = 1,
   descr = descr,
   inject = @{thms list.inject},
   distinct = [],
   induct = @{thm tre0_induct},
   inducts =
     @{thms tre0_induct[THEN conjunct1]
            tre0_induct[THEN conjunct2]},
   exhaust = @{thm list.exhaust},
   nchotomy = @{thm list.nchotomy},
   rec_names = ["Rep_Datatype.tre0_rec_1", "Rep_Datatype.tre0_rec_2"],
   rec_rewrites = @{thms tr1 tr2a tr2b},
   case_name = @{const_name list_case},
   case_rewrites = @{thms list.cases},
   case_cong = @{thm list.case_cong},
   weak_case_cong = @{thm list.weak_case_cong},
   split = @{thm list.split},
   split_asm = @{thm list.split_asm}} : info;

val info1 = the_info @{theory} @{type_name list};
*}

setup {*
register [(@{type_name tre0}, info0), (@{type_name list}, info1)]
*}

setup {*
interpretation_data (default_config, [@{type_name tre0}])
*}

term tre0_rec_1
term tre0_rec_2

primrec set_tre :: "'a tre0 \<Rightarrow> 'a set"
and set_tres :: "'a tre0 list \<Rightarrow> 'a set" where
"set_tre (Nod0 a ts) = insert a (set_tres ts)" |
"set_tres [] = {}" |
"set_tres (t # ts) = set_tre t \<union> set_tres ts"

ML {*
val info0 = get_info @{theory} @{type_name tre0};
*}

datatype fnky0 = Fnky0 "nat tre0"
datatype fnky = Fnky "nat tre"

end
