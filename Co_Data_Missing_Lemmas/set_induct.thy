theory set_induct
 imports "~~/src/HOL/BNF_Greatest_Fixpoint"
(* imports "~~/src/HOL/Library/FSet" *)

begin

declare [[bnf_note_all]]

codatatype 'a llist
  = LNil
  | LCons (lhead: 'a) (ltail: "'a llist")
  where
    "ltail LNil = LNil"

codatatype 'a maybe
  = Nothing
  | Just (just: 'a)

codatatype 'a ltree
  = LNode (lval: 'a) (lchildren: "'a ltree llist")

codatatype ('a, 'b) example
  = N1 'a
  | N2 'b
  | N3 'a 'b
  | N4
  | ConsA 'a "('a, 'b) example"
  | ConsB 'b "('a, 'b) example"

codatatype 'a tree = Node 'a "'a forest"
       and 'a forest = FNil | FCons "'a tree" "'a forest"

codatatype 'a A = A1 'a | A2 'a
codatatype ('a, 'b) B = B1 'a | B2 'b
codatatype ('a, 'b) C = C1 "'a A" | C2 "('a, 'b) B"
codatatype 'a D1 = D1 "('a, unit) B A"
codatatype 'a D2 = D2 "('a * unit) A"
codatatype 'a D3 = D3 "('a, (unit, nat) B) B A"
codatatype 'a D4 = D4 "('a, ('a, nat) B) B A"
codatatype 'a D5 = D5 "('a, ('a D5, nat) B) B A"
codatatype 'a D6 = D6 "('a, ('a, ('a, ('a, ('a, nat) B) B) B) B) B"
codatatype 'a D = D "('a * 'a D * nat) A" "('a * nat) A"

codatatype ('a, 'b, 'c, 'd) E = E "'a * ('b, ('c, ('d * nat)) C A) B"

codatatype ('a, 'b) F = F "'a \<Rightarrow> 'b"
find_theorems name: id_bnf
(*codatatype only*)
theorem set_induct0_0:
  assumes
    "\<And>x xs. P x (LCons x xs)"
    "\<And>x x' xs. \<lbrakk>x' \<in> set_llist xs; P x' xs\<rbrakk> \<Longrightarrow> P x' (LCons x xs)"
  shows "\<forall>x \<in> set_llist xs. P x xs"
apply (rule llist.dtor_set_induct)
apply (tactic {* ALLGOALS (rtac @{thm llist.exhaust} THEN_ALL_NEW
  (rtac @{thm iffD2[OF arg_cong2[of _ _ _ _ P, OF refl]]} THEN' atac THEN' hyp_subst_tac @{context})) *})
apply (unfold set1_pre_llist_def set2_pre_llist_def BNF_Composition.id_bnf_def o_apply LNil_def LCons_def
  llist.dtor_ctor sum_set_simps prod_set_simps UN_empty UN_insert Un_empty_right empty_iff singleton_iff)
apply (tactic {* ALLGOALS (etac FalseE ORELSE'
  (TRY o hyp_subst_tac @{context} THEN'
   resolve_tac @{thms assms[unfolded LCons_def BNF_Composition.id_bnf_def]} THEN'
   REPEAT_DETERM o atac)) *})
done

term set2_pre_llist

(*codatatype only*)
(* only for the non-mutual case: *)
theorems set_induct_0[consumes 1, case_names lhd ltl, induct set: "set_llist"] =
  bspec[OF set_induct0_0, rotated -1]

(*
lemma magic: "(b \<in> (\<Union>x\<in>A. B x)) \<Longrightarrow> ((\<exists>x\<in>A. b \<in> B x) \<Longrightarrow> Q) \<Longrightarrow> Q"
  by simp
thm bexE
lemma magic': "(b \<in> (\<Union>x\<in>A. B x)) \<Longrightarrow> (\<And>x. x \<in> A \<Longrightarrow> b \<in> B x \<Longrightarrow> Q) \<Longrightarrow> Q"
  by auto
*)

thm
  UN_iff
  UN_iff[THEN iffD1]
  UN_iff[THEN iffD1, unfolded atomize_imp]
  UN_iff[THEN iffD1, unfolded atomize_imp, THEN impE]

lemmas magic = UN_iff[THEN iffD1, unfolded atomize_imp, THEN impE]

ML {*
open Ctr_Sugar_Tactics
open Ctr_Sugar_Util

fun mk_set_induct0_tac ctxt cts assms dtor_set_inducts exhausts set_pre_defs ctor_defs dtor_ctors Abs_pre_inverse =
  let
    val assms_ctor_defs = map (unfold_thms ctxt (@{thm BNF_Comp.id_bnf_comp_def} :: ctor_defs)) assms;
    val foo = map (fn thm => thm RS @{thm asm_rl[of "P x y" for P x y]}) exhausts
    val bar =  flat (map (fn thm => map (try (fn ct => cterm_instantiate_pos [NONE, SOME ct] thm)) cts) foo)
    val baz = map the (filter is_some bar)
  in
    ALLGOALS (resolve_tac dtor_set_inducts) THEN
    TRYALL (resolve_tac baz THEN_ALL_NEW
      (resolve_tac (map (fn ct =>
        refl RS (cterm_instantiate_pos (replicate 4 NONE @ [SOME ct]) @{thm arg_cong2})RS iffD2) cts)
        THEN' atac THEN' hyp_subst_tac ctxt)) THEN
    unfold_thms_tac ctxt (Abs_pre_inverse :: dtor_ctors @ set_pre_defs @ ctor_defs @
      @{thms BNF_Comp.id_bnf_comp_def o_apply sum_set_simps prod_set_simps UN_empty UN_insert
        Un_empty_left Un_empty_right empty_iff singleton_iff}) THEN
    REPEAT_DETERM (HEADGOAL
      (TRY o etac UnE THEN'
       TRY o (fn _ => unfold_thms_tac ctxt @{thms singleton_iff}) THEN'
       TRY o hyp_subst_tac ctxt THEN'
       REPEAT_DETERM o
         (etac @{thm UN_iff[THEN iffD1, unfolded atomize_imp, THEN impE]} THEN' etac bexE) THEN'
       TRY o etac UnE THEN'
       fold (curry (op ORELSE')) (map (fn thm =>
         funpow (length (prems_of thm)) (fn f => f THEN' atac) (rtac thm)) assms_ctor_defs)
         (etac FalseE)))
  end;
*}
thm UN_iff

find_theorems "_ \<in> (_ \<union> _)"

theorem set_induct0_1:
  assumes
    "\<And>x xs. P x (LCons x xs)"
    "\<And>x x' xs. \<lbrakk>x' \<in> set_llist xs; P x' xs\<rbrakk> \<Longrightarrow> P x' (LCons x xs)"
  shows "\<forall>x \<in> set_llist xs. P x xs"
apply (tactic {* mk_set_induct0_tac @{context}
  [@{cterm P}]
  @{thms assms}
  @{thms llist.dtor_set_induct}
  @{thms llist.exhaust}
  @{thms set1_pre_llist_def set2_pre_llist_def}
  @{thms LNil_def LCons_def}
  @{thms llist.dtor_ctor}
  @{thm refl}
*})
done

theorem set_induct0_2:
  assumes
    "\<And>x. P x (Just x)"
  shows "\<forall>x \<in> set_maybe xs. P x xs"
apply (tactic {* mk_set_induct0_tac @{context}
  [@{cterm P}]
  @{thms assms}
  @{thms maybe.dtor_set_induct}
  @{thms maybe.exhaust}
  @{thms set1_pre_maybe_def set2_pre_maybe_def}
  @{thms Nothing_def Just_def}
  @{thms maybe.dtor_ctor}
  @{thm refl}
*})
done

theorem set_induct0_3:
  assumes
    "\<And>x xs. P x (LNode x xs)"
    "\<And>x x' x'' xs. \<lbrakk>x' \<in> set_llist xs; x'' \<in> set_ltree x'; P x'' x'\<rbrakk> \<Longrightarrow> P x'' (LNode x xs)"
  shows "\<forall>x \<in> set_ltree xs. P x xs"
apply (tactic {* mk_set_induct0_tac @{context}
  [@{cterm P}]
  @{thms assms}
  @{thms ltree.dtor_set_induct}
  @{thms ltree.exhaust}
  @{thms set1_pre_ltree_def set2_pre_ltree_def}
  @{thms LNode_def}
  @{thms ltree.dtor_ctor}
  @{thm refl}
*})
done

theorem set_induct0_4:
  assumes
    "\<And>x. P x (N1 x)"
    "\<And>x y. P x (N3 x y)"
    "\<And>x xs. P x (ConsA x xs)"
    "\<And>x x' xs. \<lbrakk>x' \<in> set1_example xs; P x' xs\<rbrakk> \<Longrightarrow> P x' (ConsA x xs)"
    "\<And>x x' xs. \<lbrakk>x' \<in> set1_example xs; P x' xs\<rbrakk> \<Longrightarrow> P x' (ConsB x xs)"
  shows "\<forall>x \<in> set1_example xs. P x xs"
apply (tactic {* mk_set_induct0_tac @{context}
  [@{cterm P}]
  @{thms assms}
  @{thms example.dtor_set1_induct example.dtor_set2_induct}
  @{thms example.exhaust}
  @{thms set1_pre_example_def set2_pre_example_def set3_pre_example_def}
  @{thms N1_def N2_def N3_def N4_def ConsA_def ConsB_def}
  @{thms example.dtor_ctor}
  @{thm Abs_example_pre_example_inverse[OF UNIV_I]}
*})
done

theorem set_induct0_5:
  assumes
    "\<And>x. P x (N2 x)"
    "\<And>x y. P y (N3 x y)"
    "\<And>x x' xs. \<lbrakk>x' \<in> set2_example xs; P x' xs\<rbrakk> \<Longrightarrow> P x' (ConsA x xs)"
    "\<And>x xs. P x (ConsB x xs)"
    "\<And>x x' xs. \<lbrakk>x' \<in> set2_example xs; P x' xs\<rbrakk> \<Longrightarrow> P x' (ConsB x xs)"
  shows "\<forall>x \<in> set2_example xs. P x xs"
apply (tactic {* mk_set_induct0_tac @{context}
  [@{cterm P}]
  @{thms assms}
  @{thms example.dtor_set1_induct example.dtor_set2_induct}
  @{thms example.exhaust}
  @{thms set1_pre_example_def set2_pre_example_def set3_pre_example_def}
  @{thms N1_def N2_def N3_def N4_def ConsA_def ConsB_def}
  @{thms example.dtor_ctor}
  @{thm Abs_example_pre_example_inverse[OF UNIV_I]}
*})
done

print_attributes

declare [[goals_limit = 100]]

theorem set_induct0_6:
  assumes
    "\<And>x ts. P x (Node x ts)"
    "\<And>x x' ts. \<lbrakk>x' \<in> set_forest ts; Q x' ts\<rbrakk> \<Longrightarrow> P x' (Node x ts)"
    "\<And>t x' ts. \<lbrakk>x' \<in> set_tree t; P x' t\<rbrakk> \<Longrightarrow> Q x' (FCons t ts)"
    "\<And>t x' ts. \<lbrakk>x' \<in> set_forest ts; Q x' ts\<rbrakk> \<Longrightarrow> Q x' (FCons t ts)"
  shows "(\<forall>x \<in> set_tree t. P x t) \<and> (\<forall>x \<in> set_forest ts. Q x ts)"
apply (tactic {* mk_set_induct0_tac @{context}
  [@{cterm P}, @{cterm Q}]
  @{thms assms}
  @{thms tree_forest.dtor_set_induct}
  @{thms tree.exhaust forest.exhaust}
  @{thms set1_pre_tree_def set2_pre_tree_def set3_pre_tree_def set1_pre_forest_def set2_pre_forest_def set3_pre_forest_def}
  @{thms Node_def FNil_def FCons_def}
  @{thms tree.dtor_ctor forest.dtor_ctor}
  @{thm refl}
*})
done

theorem set_induct0_7:
  assumes
    "\<And>x. P x (A1 x)"
    "\<And>x. P x (A2 x)"
  shows "(\<forall>x \<in> set_A xs. P x xs)"
apply (tactic {* mk_set_induct0_tac @{context}
  [@{cterm P}]
  @{thms assms}
  @{thms A.dtor_set_induct}
  @{thms A.exhaust}
  @{thms set1_pre_A_def set2_pre_A_def}
  @{thms A1_def A2_def}
  @{thms A.dtor_ctor}
  @{thm refl}
*})
done

theorem set_induct0_8:
  assumes
    "\<And>x. P x (B1 x)"
  shows "\<forall>x \<in> set1_B xs. P x xs"
apply (tactic {* mk_set_induct0_tac @{context}
  [@{cterm P}]
  @{thms assms}
  @{thms B.dtor_set1_induct B.dtor_set2_induct}
  @{thms B.exhaust}
  @{thms set1_pre_B_def set2_pre_B_def set3_pre_B_def}
  @{thms B1_def B2_def}
  @{thms B.dtor_ctor}
  @{thm refl}
*})
done

theorem set_induct0_9:
  assumes
    "\<And>x. P x (B2 x)"
  shows "\<forall>x \<in> set2_B xs. P x xs"
apply (tactic {* mk_set_induct0_tac @{context}
  [@{cterm P}]
  @{thms assms}
  @{thms B.dtor_set1_induct B.dtor_set2_induct}
  @{thms B.exhaust}
  @{thms set1_pre_B_def set2_pre_B_def set3_pre_B_def}
  @{thms B1_def B2_def}
  @{thms B.dtor_ctor}
  @{thm refl}
*})
done

theorem set_induct0_10:
  assumes
    "\<And>x x' xs. \<lbrakk>x' \<in> set_A xs\<rbrakk> \<Longrightarrow> P x' (C1 xs)"
    "\<And>x x' xs. \<lbrakk>x' \<in> set1_B xs\<rbrakk> \<Longrightarrow> P x' (C2 xs)"
  shows "\<forall>x \<in> set1_C xs. P x xs"
apply (tactic {* mk_set_induct0_tac @{context}
  [@{cterm P}]
  @{thms assms}
  @{thms C.dtor_set1_induct C.dtor_set2_induct}
  @{thms C.exhaust}
  @{thms set1_pre_C_def set2_pre_C_def set3_pre_C_def}
  @{thms C1_def C2_def}
  @{thms C.dtor_ctor}
  @{thm Abs_C_pre_C_inverse[OF UNIV_I]}
*})
done

theorem set_induct0_11:
  assumes
    "\<And>x x' xs. \<lbrakk>x' \<in> set2_B xs\<rbrakk> \<Longrightarrow> P x' (C2 xs)"
  shows "\<forall>x \<in> set2_C xs. P x xs"
apply (tactic {* mk_set_induct0_tac @{context}
  [@{cterm P}]
  @{thms assms}
  @{thms C.dtor_set1_induct C.dtor_set2_induct}
  @{thms C.exhaust}
  @{thms set1_pre_C_def set2_pre_C_def set3_pre_C_def}
  @{thms C1_def C2_def}
  @{thms C.dtor_ctor}
  @{thm Abs_C_pre_C_inverse[OF UNIV_I]}
*})
done

thm D.sel_set (* Ho no, there seems to be a problem with 'xc'*)
term Basic_BNFs.fsts
term set

theorem set_induct0_13:
  assumes
    "\<And>x x' xs. \<lbrakk>x \<in> set_A xs; x' \<in> set1_B x\<rbrakk> \<Longrightarrow> P x' (D1 xs)"
  shows "\<forall>x \<in> set_D1 xs. P x xs"
apply (tactic {* mk_set_induct0_tac @{context}
  [@{cterm P}]
  @{thms assms}
  @{thms D1.dtor_set_induct}
  @{thms D1.exhaust}
  @{thms set1_pre_D1_def set2_pre_D1_def}
  @{thms D1_def}
  @{thms D1.dtor_ctor}
  @{thm refl}
*})
done

theorem set_induct0_14:
  assumes
    "\<And>x x' xs. \<lbrakk>x \<in> set_A xs; x' \<in> Basic_BNFs.fsts x\<rbrakk> \<Longrightarrow> P x' (D2 xs)"
  shows "\<forall>x \<in> set_D2 xs. P x xs"
apply (tactic {* mk_set_induct0_tac @{context}
  [@{cterm P}]
  @{thms assms}
  @{thms D2.dtor_set_induct}
  @{thms D2.exhaust}
  @{thms set1_pre_D2_def set2_pre_D2_def}
  @{thms D2_def}
  @{thms D2.dtor_ctor}
  @{thm refl}
*})
done

theorem set_induct0_15:
  assumes
    "\<And>x x' xs. \<lbrakk>x \<in> set_A xs; x' \<in> set1_B x\<rbrakk> \<Longrightarrow> P x' (D3 xs)"
  shows "\<forall>x \<in> set_D3 xs. P x xs"
apply (tactic {* mk_set_induct0_tac @{context}
  [@{cterm P}]
  @{thms assms}
  @{thms D3.dtor_set_induct}
  @{thms D3.exhaust}
  @{thms set1_pre_D3_def set2_pre_D3_def}
  @{thms D3_def}
  @{thms D3.dtor_ctor}
  @{thm Abs_D3_pre_D3_inverse[OF UNIV_I]}
*})
done

theorem set_induct0_16:
  assumes
    "\<And>x x' xs. \<lbrakk>x \<in> set_A xs; x' \<in> set1_B x\<rbrakk> \<Longrightarrow> P x' (D4 xs)"
    "\<And>x x' x'' xs. \<lbrakk>x \<in> set_A xs; x' \<in> set2_B x; x'' \<in> set1_B x'\<rbrakk> \<Longrightarrow> P x'' (D4 xs)"
  shows "\<forall>x \<in> set_D4 xs. P x xs"
apply (tactic {* mk_set_induct0_tac @{context}
  [@{cterm P}]
  @{thms assms}
  @{thms D4.dtor_set_induct}
  @{thms D4.exhaust}
  @{thms set1_pre_D4_def set2_pre_D4_def}
  @{thms D4_def}
  @{thms D4.dtor_ctor}
  @{thm Abs_D4_pre_D4_inverse[OF UNIV_I]}
*})
done

theorem set_induct0_17:
  assumes
    "\<And>x x' xs. \<lbrakk>x \<in> set_A xs; x' \<in> set1_B x\<rbrakk> \<Longrightarrow> P x' (D5 xs)"
    "\<And>x x' x'' x'' x''' xs. \<lbrakk>x \<in> set_A xs; x' \<in> set2_B x; x'' \<in> set1_B x'; x''' \<in> set_D5 x''; P x''' x''\<rbrakk> \<Longrightarrow> P x''' (D5 xs)"
  shows "\<forall>x \<in> set_D5 xs. P x xs"
apply (tactic {* mk_set_induct0_tac @{context}
  [@{cterm P}]
  @{thms assms}
  @{thms D5.dtor_set_induct}
  @{thms D5.exhaust}
  @{thms set1_pre_D5_def set2_pre_D5_def}
  @{thms D5_def}
  @{thms D5.dtor_ctor}
  @{thm Abs_D5_pre_D5_inverse[OF UNIV_I]}
*})
done

theorem set_induct0_18:
  assumes
    "\<And>x xs. \<lbrakk>x \<in> set1_B xs\<rbrakk> \<Longrightarrow> P x (D6 xs)"
    "\<And>x x' xs. \<lbrakk>x \<in> set2_B xs; x' \<in> set1_B x\<rbrakk> \<Longrightarrow> P x' (D6 xs)"
    "\<And>x x' x'' xs. \<lbrakk>x \<in> set2_B xs; x' \<in> set2_B x; x'' \<in> set1_B x'\<rbrakk> \<Longrightarrow> P x'' (D6 xs)"
    "\<And>x x' x'' x''' xs. \<lbrakk>x \<in> set2_B xs; x' \<in> set2_B x; x'' \<in> set2_B x'; x''' \<in> set1_B x''\<rbrakk> \<Longrightarrow> P x''' (D6 xs)"
    "\<And>x x' x'' x''' x'''' xs. \<lbrakk>x \<in> set2_B xs; x' \<in> set2_B x; x'' \<in> set2_B x'; x''' \<in> set2_B x''; x'''' \<in> set1_B x'''\<rbrakk> \<Longrightarrow> P x'''' (D6 xs)"
  shows "\<forall>x \<in> set_D6 xs. P x xs"
apply (tactic {* mk_set_induct0_tac @{context}
  [@{cterm P}]
  @{thms assms}
  @{thms D6.dtor_set_induct}
  @{thms D6.exhaust}
  @{thms set1_pre_D6_def set2_pre_D6_def}
  @{thms D6_def}
  @{thms D6.dtor_ctor}
  @{thm Abs_D6_pre_D6_inverse[OF UNIV_I]}
*})
done

theorem set_induct0_19:
  assumes
    "\<And>x x' xs ys. \<lbrakk>x \<in> set_A xs; x' \<in> Basic_BNFs.fsts x\<rbrakk> \<Longrightarrow> P x' (D xs ys)"
    "\<And>x x' x'' x'' x''' xs ys. \<lbrakk>x \<in> set_A xs; x' \<in> Basic_BNFs.snds x; x'' \<in> Basic_BNFs.fsts x'; x''' \<in> set_D x''; P x''' x''\<rbrakk> \<Longrightarrow> P x''' (D xs ys)"
    "\<And>x x' xs ys. \<lbrakk>x \<in> set_A xs; x' \<in> Basic_BNFs.fsts x\<rbrakk> \<Longrightarrow> P x' (D ys xs)"
  shows "\<forall>x \<in> set_D xs. P x xs"
apply (tactic {* mk_set_induct0_tac @{context}
  [@{cterm P}]
  @{thms assms}
  @{thms D.dtor_set_induct}
  @{thms D.exhaust}
  @{thms set1_pre_D_def set2_pre_D_def}
  @{thms D_def}
  @{thms D.dtor_ctor}
  @{thm Abs_D_pre_D_inverse[OF UNIV_I]}
*})
done

theorem set_induct0_20:
  assumes
    "\<And>x xs. \<lbrakk>x \<in> Basic_BNFs.fsts xs\<rbrakk> \<Longrightarrow> P x (E xs)"
  shows "(\<forall>x \<in> set1_E xs. P x xs)"
apply (tactic {* mk_set_induct0_tac @{context}
  [@{cterm P}]
  @{thms assms}
  @{thms E.dtor_set1_induct E.dtor_set2_induct E.dtor_set3_induct E.dtor_set4_induct}
  @{thms E.exhaust}
  @{thms set1_pre_E_def set2_pre_E_def set3_pre_E_def set4_pre_E_def set5_pre_E_def}
  @{thms E_def}
  @{thms E.dtor_ctor}
  @{thm Abs_E_pre_E_inverse[OF UNIV_I]}
*})
done

theorem set_induct0_21:
  assumes
    "\<And>x x' xs. \<lbrakk>x \<in> Basic_BNFs.snds xs; x' \<in> set1_B x\<rbrakk> \<Longrightarrow> P x' (E xs)"
  shows "(\<forall>x \<in> set2_E xs. P x xs)"
apply (tactic {* mk_set_induct0_tac @{context}
  [@{cterm P}]
  @{thms assms}
  @{thms E.dtor_set1_induct E.dtor_set2_induct E.dtor_set3_induct E.dtor_set4_induct}
  @{thms E.exhaust}
  @{thms set1_pre_E_def set2_pre_E_def set3_pre_E_def set4_pre_E_def set5_pre_E_def}
  @{thms E_def}
  @{thms E.dtor_ctor}
  @{thm Abs_E_pre_E_inverse[OF UNIV_I]}
*})
done

theorem set_induct0_22:
  assumes
    "\<And>x x' x'' x''' xs. \<lbrakk>x \<in> Basic_BNFs.snds xs; x' \<in> set2_B x; x'' \<in> set_A x'; x''' \<in> set1_C x''\<rbrakk> \<Longrightarrow> P x''' (E xs)"
  shows "(\<forall>x \<in> set3_E xs. P x xs)"
apply (tactic {* mk_set_induct0_tac @{context}
  [@{cterm P}]
  @{thms assms}
  @{thms E.dtor_set1_induct E.dtor_set2_induct E.dtor_set3_induct E.dtor_set4_induct}
  @{thms E.exhaust}
  @{thms set1_pre_E_def set2_pre_E_def set3_pre_E_def set4_pre_E_def set5_pre_E_def}
  @{thms E_def}
  @{thms E.dtor_ctor}
  @{thm Abs_E_pre_E_inverse[OF UNIV_I]}
*})
done

theorem set_induct0_23:
  assumes
    "\<And>x x' x'' x''' x'''' xs. \<lbrakk>x \<in> Basic_BNFs.snds xs; x' \<in> set2_B x; x'' \<in> set_A x'; x''' \<in> set2_C x''; x'''' \<in> Basic_BNFs.fsts x'''\<rbrakk> \<Longrightarrow> P x'''' (E xs)"
  shows "(\<forall>x \<in> set4_E xs. P x xs)"
apply (tactic {* mk_set_induct0_tac @{context}
  [@{cterm P}]
  @{thms assms}
  @{thms E.dtor_set1_induct E.dtor_set2_induct E.dtor_set3_induct E.dtor_set4_induct}
  @{thms E.exhaust}
  @{thms set1_pre_E_def set2_pre_E_def set3_pre_E_def set4_pre_E_def set5_pre_E_def}
  @{thms E_def}
  @{thms E.dtor_ctor}
  @{thm Abs_E_pre_E_inverse[OF UNIV_I]}
*})
done

theorem set_induct0_24:
  assumes
    "\<And>x f. \<lbrakk>x \<in> range f\<rbrakk> \<Longrightarrow> P x (F f)"
  shows "(\<forall>x \<in> set_F xs. P x xs)"
apply (tactic {* mk_set_induct0_tac @{context}
  [@{cterm P}]
  @{thms assms}
  @{thms F.dtor_set_induct}
  @{thms F.exhaust}
  @{thms set1_pre_F_def set2_pre_F_def}
  @{thms F_def}
  @{thms F.dtor_ctor}
  @{thm refl}
*})
done

codatatype 'a par_lambda =
  PVar 'a |
  PApp "'a par_lambda" "'a par_lambda" |
  PAbs 'a "'a par_lambda" |
  PLet "('a \<times> 'a par_lambda) fset" "'a par_lambda"

theorem set_induct0_25:
  assumes
    "(\<And>arg. P arg (PVar arg))"
    "(\<And>arg1 arg2 xa. (xa \<in> set_par_lambda arg1 \<Longrightarrow> P xa arg1) \<Longrightarrow> P xa (PApp arg1 arg2))"
    "(\<And>arg1 arg2 xb. (xb \<in> set_par_lambda arg2 \<Longrightarrow> P xb arg2) \<Longrightarrow> P xb (PApp arg1 arg2))"
    "(\<And>arg1a arg2a. P arg1a (PAbs arg1a arg2a))"
    "(\<And>arg1a arg2a xc. (xc \<in> set_par_lambda arg2a \<Longrightarrow> P xc arg2a) \<Longrightarrow> P xc (PAbs arg1a arg2a))"
    "(\<And>arg1b arg2b xd xe. xd \<in> fset arg1b \<Longrightarrow> xe \<in> Basic_BNFs.fsts xd \<Longrightarrow> P xe (PLet arg1b arg2b))"
    "(\<And>arg1b arg2b xd xf xg. xd \<in> fset arg1b \<Longrightarrow> xf \<in> Basic_BNFs.snds xd \<Longrightarrow> (xg \<in> set_par_lambda xf \<Longrightarrow> P xg xf) \<Longrightarrow> P xg (PLet arg1b arg2b))"
    "(\<And>arg1b arg2b xh. (xh \<in> set_par_lambda arg2b \<Longrightarrow> P xh arg2b) \<Longrightarrow> P xh (PLet arg1b arg2b))"
  shows "\<forall>x \<in> set_par_lambda a. P x a"
apply (tactic {* mk_set_induct0_tac @{context}
  [@{cterm P}]
  @{thms assms}
  @{thms par_lambda.dtor_set_induct}
  @{thms par_lambda.exhaust}
  @{thms set1_pre_par_lambda_def set2_pre_par_lambda_def}
  @{thms PVar_def PApp_def PAbs_def PLet_def}
  @{thms par_lambda.dtor_ctor}
  @{thm Abs_par_lambda_pre_par_lambda_inverse[OF UNIV_I]}
*})

(* apply ((erule UnE)? , (unfold singleton_iff)? , (hypsubst | erule UN_iff[THEN iffD1, unfolded atomize_imp, THEN impE], erule bexE))
apply (rule assms[unfolded PVar_def PApp_def PAbs_def PLet_def BNF_Comp.id_bnf_comp_def], assumption+)

apply ((erule UnE)? , (unfold singleton_iff)? , (hypsubst | erule UN_iff[THEN iffD1, unfolded atomize_imp, THEN impE], erule bexE))
apply (rule assms[unfolded PVar_def PApp_def PAbs_def PLet_def BNF_Comp.id_bnf_comp_def], assumption)

apply ((erule UnE)? , (unfold singleton_iff)? , (hypsubst | erule UN_iff[THEN iffD1, unfolded atomize_imp, THEN impE], erule bexE))
apply (rule assms[unfolded PVar_def PApp_def PAbs_def PLet_def BNF_Comp.id_bnf_comp_def], assumption)

apply ((erule UnE)? , (unfold singleton_iff)? , (hypsubst | erule UN_iff[THEN iffD1, unfolded atomize_imp, THEN impE], erule bexE))
apply (rule assms[unfolded PVar_def PApp_def PAbs_def PLet_def BNF_Comp.id_bnf_comp_def], assumption, assumption, assumption)

apply ((erule UnE)? , (unfold singleton_iff)? , (hypsubst | erule UN_iff[THEN iffD1, unfolded atomize_imp, THEN impE], erule bexE))
apply (rule assms[unfolded PVar_def PApp_def PAbs_def PLet_def BNF_Comp.id_bnf_comp_def], assumption) *)

(*
apply (erule UnE)
apply (unfold singleton_iff)
apply (hypsubst)
apply (rule assms[unfolded PVar_def PApp_def PAbs_def PLet_def BNF_Comp.id_bnf_comp_def], assumption)

apply (hypsubst)
apply (rule assms[unfolded PVar_def PApp_def PAbs_def PLet_def BNF_Comp.id_bnf_comp_def], assumption)

apply (hypsubst)
apply (rule assms[unfolded PVar_def PApp_def PAbs_def PLet_def BNF_Comp.id_bnf_comp_def], assumption)

apply (erule UnE)
apply (erule UN_iff[THEN iffD1, unfolded atomize_imp, THEN impE])
apply (erule bexE)
apply (rule assms[unfolded PVar_def PApp_def PAbs_def PLet_def BNF_Comp.id_bnf_comp_def], assumption, assumption, assumption)

apply (unfold singleton_iff)
apply (hypsubst)
apply (rule assms[unfolded PVar_def PApp_def PAbs_def PLet_def BNF_Comp.id_bnf_comp_def], assumption)
*)
done
end
