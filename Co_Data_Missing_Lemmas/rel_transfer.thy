theory rel_transfer
imports "~~/src/HOL/Transfer"
begin

context lifting_syntax (* enables the ===> syntax for rel_fun *)
begin

declare [[bnf_note_all = true]]

datatype_new 'a list
  = nil: Nil
  | Cons (head: 'a) (tail: "'a list")
  where
    "tail Nil = Nil"

lemma
  "((S1a ===> S1c ===> op =) ===>
   (S2a ===> S2c ===> op =) ===>
   rel_sum op = (rel_prod S1a S2a) ===>
   rel_sum op = (rel_prod S1c S2c) ===>
   op =) rel_pre_list rel_pre_list "
thm rel_funI
  apply (rule rel_funI)+
  apply (erule iffD1[OF pre_list.in_rel[unfolded rel_pre_list_def BNF_Comp.id_bnf_comp_def vimage2p_def], elim_format] exE conjE CollectE)+
  apply hypsubst
  apply (unfold pre_list.rel_map)
  apply (rule iffI)
  apply (erule pre_list.rel_mono_strong)
  apply (erule iffD1[OF predicate2_transferD], assumption+)+
  apply (erule pre_list.rel_mono_strong)
  apply (erule iffD2[OF predicate2_transferD], assumption+)+
done

thm iffD1[OF predicate2_transferD] predicate2_transferD
thm pre_list.in_rel[unfolded rel_pre_list_def BNF_Comp.id_bnf_comp_def vimage2p_def]

lemmas foo = rel_pre_list_def[THEN symmetric, unfolded BNF_Comp.id_bnf_comp_def vimage2p_def]
thm fun_eq_iff ext

datatype_new 'a option
  = is_none: None
  | is_some: Some (the: 'a)

datatype_new 'a tree
  = is_node: Node (val: 'a) (children: "'a tree list")

datatype_new ('a, 'b) example
  = is_N1: N1 'a
  | is_N2: N2 'b
  | is_N3: N3 'a 'b
  | is_N4: N4
  | is_ConsA: ConsA 'a "('a, 'b) example"
  | is_ConsB: ConsB 'b "('a, 'b) example"

datatype_new 'a tree_mutual = is_TNode: TNode 'a "'a forest_mutual"
         and 'a forest_mutual = is_FNil: FNil | is_FCons: FCons "'a tree_mutual" "'a forest_mutual"

datatype_new ('a, 'b) F1 = is_F1: F1 "'a \<Rightarrow> 'b"

datatype_new ('a, 'b, 'c) F2 = is_F2: F2 "'a \<Rightarrow> 'b \<Rightarrow> 'c"

datatype_new 'a A1 = is_A1: A1 nat
         and 'a A2 = is_A2: A2 'a

datatype_new 'a T = T 'a

lemma predicate2_transferD:
   "\<lbrakk>(R1 ===> R2 ===> op =) P Q; a \<in> A; b \<in> B; A \<subseteq> {(x, y). R1 x y}; B \<subseteq> {(x, y). R2 x y}\<rbrakk> \<Longrightarrow>
   P (fst a) (fst b) \<longleftrightarrow> Q (snd a) (snd b)"
  unfolding rel_fun_def by (blast dest!: Collect_splitD)

ML {*
open BNF_Tactics

fun mk_rel_transfer_tac ctxt in_rel rel_map rel_mono_strong =
  let
    fun handle_last iffD =
      HEADGOAL (etac rel_mono_strong) THEN
      REPEAT_DETERM (HEADGOAL (etac (@{thm predicate2_transferD} RS iffD) THEN' REPEAT_DETERM o atac))
  in
    REPEAT_DETERM (HEADGOAL (rtac @{thm rel_funI})) THEN
      REPEAT_DETERM (HEADGOAL (eresolve_tac (Tactic.make_elim (in_rel RS iffD1) ::
        @{thms exE conjE CollectE}))) THEN
      HEADGOAL (hyp_subst_tac ctxt) THEN
      unfold_thms_tac ctxt rel_map THEN
      HEADGOAL (rtac iffI) THEN
      handle_last iffD1 THEN
      handle_last iffD2 ORELSE
    HEADGOAL (rtac refl) ORELSE
    REPEAT_DETERM (HEADGOAL (rtac @{thm rel_funI})) THEN
      unfold_thms_tac ctxt @{thms BNF_Comp.id_bnf_comp_def} THEN
      REPEAT (HEADGOAL (dtac @{thm rel_funD} THEN' atac)) THEN
      HEADGOAL atac
  end;
*}

lemma T_rel_transfer: "((R1 ===> R2 ===> op =) ===> rel_T R1 ===> rel_T R2 ===> op =) rel_T rel_T"
  (* apply (tactic {* mk_rel_transfer_tac @{context} @{thm list.in_rel} @{thms list.rel_map} @{thm list.rel_mono_strong} *}) *)
  apply (rule rel_funI)+
  apply (erule iffD1[OF T.in_rel, elim_format] exE conjE CollectE)+
  apply hypsubst
  apply (unfold T.rel_map)
  apply (rule iffI)
  apply (erule T.rel_mono_strong)
  apply (erule iffD1[OF predicate2_transferD], assumption+)+
  apply (erule T.rel_mono_strong)
  apply (erule iffD2[OF predicate2_transferD], assumption+)+
done

thm list.rel_transfer
lemma list_rel_transfer: "((R1 ===> R2 ===> op =) ===> rel_list R1 ===> rel_list R2 ===> op =) rel_list rel_list"
  (* apply (tactic {* mk_rel_transfer_tac @{context} @{thm list.in_rel} @{thms list.rel_map} @{thm list.rel_mono_strong} *}) *)
  apply (rule rel_funI)+
  apply (erule iffD1[OF list.in_rel, elim_format] exE conjE CollectE)+
  apply hypsubst
  apply (unfold list.rel_map)
  apply (rule iffI)
  apply (erule list.rel_mono_strong)
  apply (erule iffD1[OF predicate2_transferD], assumption+)+
  apply (erule list.rel_mono_strong)
  apply (erule iffD2[OF predicate2_transferD], assumption+)+
done

thm option.rel_transfer
lemma option_rel_transfer: "((R ===> R ===> op =) ===> rel_option R ===> rel_option R ===> op =) rel_option rel_option"
  apply (tactic {* mk_rel_transfer_tac @{context} @{thm option.in_rel} @{thms option.rel_map} @{thm option.rel_mono_strong} *})
done

thm tree.rel_transfer
lemma tree_rel_transfer: "((R ===> R ===> op =) ===> rel_tree R ===> rel_tree R ===> op =) rel_tree rel_tree"
  apply (tactic {* mk_rel_transfer_tac @{context} @{thm tree.in_rel} @{thms tree.rel_map} @{thm tree.rel_mono_strong} *})
done

thm example.rel_transfer
lemma example_rel_transfer: "((R1 ===> R1 ===> op =) ===> (R2 ===> R2 ===> op =) ===> rel_example R1 R2 ===> rel_example R1 R2 ===> op =) rel_example rel_example"
  apply (tactic {* mk_rel_transfer_tac @{context} @{thm example.in_rel} @{thms example.rel_map} @{thm example.rel_mono_strong} *})
done

thm tree_mutual.rel_transfer
lemma tree_mutual_rel_transfer: "((R ===> R ===> op =) ===> rel_tree_mutual R ===> rel_tree_mutual R ===> op =) rel_tree_mutual rel_tree_mutual"
  apply (tactic {* mk_rel_transfer_tac @{context} @{thm tree_mutual.in_rel} @{thms tree_mutual.rel_map} @{thm tree_mutual.rel_mono_strong} *})
done

thm forest_mutual.rel_transfer
lemma forest_mutual_rel_transfer: "((R ===> R ===> op =) ===> rel_forest_mutual R ===> rel_forest_mutual R ===> op =) rel_forest_mutual rel_forest_mutual"
  apply (tactic {* mk_rel_transfer_tac @{context} @{thm forest_mutual.in_rel} @{thms forest_mutual.rel_map} @{thm forest_mutual.rel_mono_strong} *})
done

thm F1.rel_transfer
lemma F1_rel_transfer: "((R ===> R ===> op =) ===> rel_F1 R ===> rel_F1 R ===> op =) rel_F1 rel_F1"
  apply (tactic {* mk_rel_transfer_tac @{context} @{thm F1.in_rel} @{thms F1.rel_map} @{thm F1.rel_mono_strong} *})
done

thm F2.rel_transfer
lemma F2_rel_transfer: "((R ===> R ===> op =) ===> rel_F2 R ===> rel_F2 R ===> op =) rel_F2 rel_F2"
  apply (tactic {* mk_rel_transfer_tac @{context} @{thm F2.in_rel} @{thms F2.rel_map} @{thm F2.rel_mono_strong} *})
done

thm A1.rel_transfer
lemma A1_rel_transfer: "((R ===> R ===> op =) ===> rel_A1 R ===> rel_A1 R ===> op =) rel_A1 rel_A1"
  apply (tactic {* mk_rel_transfer_tac @{context} @{thm A1.in_rel} @{thms A1.rel_map} @{thm A1.rel_mono_strong} *})
done

thm A2.rel_transfer
lemma A2_rel_transfer: "((R ===> R ===> op =) ===> rel_A2 R ===> rel_A2 R ===> op =) rel_A2 rel_A2"
  apply (tactic {* mk_rel_transfer_tac @{context} @{thm A2.in_rel} @{thms A2.rel_map} @{thm A2.rel_mono_strong} *})
done

end

end
