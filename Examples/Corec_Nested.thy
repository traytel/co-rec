theory Corec_Nested
imports "~~/src/HOL/BNF/BNF"
begin

consts
  g :: 'a
  g' :: 'a
  g'' :: 'a
  h :: 'a
  h' :: 'a
  q :: 'a
  q' :: 'a


codatatype tr0 = Node0 nat "tr0"

term tr0_corec
thm tr0.corec[no_vars]


definition f0 where
"f0 = tr0_corec g q g' h"

lemma f0_charact: "f0 x = Node0 (g x) (if q x then g' x else f0 (h (x :: 'a) :: 'a))"
unfolding f0_def
by (rule tr0.corec)


codatatype tr = Node nat "tr list"

term tr_corec
thm tr.corec[no_vars]

definition k0 where
"k0 = tr_corec g (\<lambda>x. map (\<lambda>y. if q y then Inl (g' y) else Inr (h' y)) (h (x :: 'a) :: nat list))"

lemma k0_charact: "k0 x = Node (g x) (map (\<lambda>z. case z of Inl t \<Rightarrow> t | Inr (x' :: 'a) \<Rightarrow> k0 x')
  (map (\<lambda>y. if q y then Inl (g' y) else Inr (h' y)) (h (x :: 'a) :: nat list)))"
unfolding k0_def
by (rule tr.corec)

lemma k0_user: "k0 x = Node (g x) (map (\<lambda>y. if q y then g' y else k0 (h' y :: 'a)) (h (x :: 'a) :: nat list))"
apply (rule trans[OF k0_charact])
apply (unfold list.map_comp o_def)
apply simp
done


definition k1 where
"k1 = tr_corec g (\<lambda>x. map (\<lambda>y. Inr (h' y)) (h (x :: 'a) :: nat list))"

lemma k1_charact: "k1 x = Node (g x) (map (\<lambda>z. case z of Inl t \<Rightarrow> t | Inr (x' :: 'a) \<Rightarrow> k1 x')
  (map (\<lambda>y. Inr (h' y)) (h (x :: 'a) :: nat list)))"
unfolding k1_def
by (rule tr.corec)

lemma k1_user: "k1 x = Node (g x) (map (\<lambda>y. k1 (h' y :: 'a)) (h (x :: 'a) :: nat list))"
apply (rule trans[OF k1_charact])
apply (unfold list.map_comp o_def)
apply simp
done


definition k2 where
"k2 = tr_corec g (\<lambda>x. map (\<lambda>y. Inl (g' y)) (h (x :: 'a) :: nat list))"

lemma k2_charact: "k2 x = Node (g x) (map (\<lambda>z. case z of Inl t \<Rightarrow> t | Inr (x' :: 'a) \<Rightarrow> k2 x')
  (map (\<lambda>y. Inl (g' y)) (h (x :: 'a) :: nat list)))"
unfolding k2_def
by (rule tr.corec)

lemma k2_user: "k2 x = Node (g x) (map g' (h (x :: 'a) :: nat list))"
apply (rule trans[OF k2_charact])
apply (unfold list.map_comp o_def)
apply simp
done


definition k3 where
"k3 = tr_corec g (\<lambda>x. map Inl (h (x :: 'a)))"

lemma k3_charact: "k3 x = Node (g x) (map (\<lambda>z. case z of Inl t \<Rightarrow> t | Inr (x' :: 'a) \<Rightarrow> k3 x')
  (map Inl (h (x :: 'a))))"
unfolding k3_def
by (rule tr.corec)

lemma k3_user: "k3 x = Node (g x) (h (x :: 'a))"
apply (rule trans[OF k3_charact])
apply (unfold list.map_comp o_def)
apply simp
done


end
