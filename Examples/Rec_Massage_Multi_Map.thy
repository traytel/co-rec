theory Rec_Massage_Multi_Map
imports Complex_Main "~~/src/HOL/BNF/BNF"
begin

datatype_new 'a tree = Node "('a tree \<times> 'a tree) list"

consts size :: "'a tree \<Rightarrow> nat"
consts size_pair :: "'a tree \<times> 'a tree \<Rightarrow> nat"

ML {*
open BNF_Util BNF_Def BNF_FP_Def_Sugar BNF_FP_Util BNF_FP_Rec_Sugar_Util
val fnames = ["f", "fa", "fb", "g", "ga", "gb"];
fun get_kks t =
  map_index (fn (i, s) =>
    if exists_subterm (fn Free (s', _) => s' = s | _ => false) t then SOME i else NONE) fnames
  |> map_filter I;
fun massage check y y' t =
  massage_indirect_rec_call
    @{context}
    (not o null o get_kks)
    (fn T1 => fn U1 => fn t =>
       let val T2 = range_type (fastype_of t) in
         Const (@{const_name undefined}, (T1 --> T2) --> HOLogic.mk_prodT (T1, U1) --> T2) $ t
       end)
    [] y y' t
  |> tap (Syntax.check_term @{context})
  |> tap (Output.urgent_message o Syntax.string_of_term @{context})
  handle ERROR msg => if check then error msg else (warning ("Error: " ^ msg); Term.dummy)
       | Fail msg => if check then raise Fail msg else (warning ("Fail: " ^ msg); Term.dummy)
*}

lemma
  fixes fa :: "'a tree \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> real"
  fixes fb :: "nat \<Rightarrow> 'a tree \<Rightarrow> int \<Rightarrow> real"
  fixes ga :: "'a tree \<Rightarrow> nat \<Rightarrow> int \<Rightarrow> real"
  fixes gb :: "nat \<Rightarrow> 'a tree \<Rightarrow> int \<Rightarrow> real"
  fixes tps :: "('a tree \<times> 'a tree) list"
  fixes pps :: "(('a tree \<times> (nat \<Rightarrow> int \<Rightarrow> real)) \<times> ('a tree \<times> (nat \<Rightarrow> int \<Rightarrow> real))) list"
  shows True
proof -

ML_val {* massage false @{term tps} @{term pps}
  @{term "map (\<lambda>tp. (tp, map_pair fa ga tp)) tps"} *}


txt {* 1Aa *}

ML_val {* massage true @{term tps} @{term pps} @{term "map (map_pair fa ga) tps"} *}
ML_val {* massage true @{term tps} @{term pps} @{term "map (map_pair (\<lambda>t. fa t) (\<lambda>t. ga t)) tps"} *}
ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair (\<lambda>t n. fa t n) (\<lambda>t n. ga t n)) tps"} *}
ML_val {* massage false @{term tps} @{term pps} @{term "mymap fa ga tps"} *}
ML_val {* massage false @{term tps} @{term pps} @{term "fa (fst (hd tps)) "} *}

txt {* 1Ab *}

ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair (\<lambda>n t. fb t n) (\<lambda>n t. gb t n)) tps"} *}
ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair (\<lambda>n t i. fb t n i) (\<lambda>n t i. gb t n i)) tps"} *}

txt {* 1Ba *}

ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair (\<lambda>t. (t, fa t)) (\<lambda>t. (t, ga t))) tps"} *}
ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair (\<lambda>t. (t, \<lambda>n. fa t n)) (\<lambda>t. (t, \<lambda>n. ga t n))) tps"} *}
ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair (\<lambda>t. (t, \<lambda>n i. fa t n i)) (\<lambda>t. (t, \<lambda>n i. ga t n i))) tps"} *}

txt {* 1Bb *}

ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair (\<lambda>t. (t, \<lambda>n. fb n t)) (\<lambda>t. (t, \<lambda>n. gb n t))) tps"} *}

txt {* 1C *}

ML_val {* massage true @{term tps} @{term pps} @{term tps} *}
ML_val {* massage true @{term tps} @{term pps} @{term "map (\<lambda>tp. tp) tps"} *}
ML_val {* massage true @{term tps} @{term pps} @{term "map (map_pair (\<lambda>t. t) (\<lambda>t. t)) tps"} *}

txt {* 2Aa *}

ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair ((\<lambda>g. g (Suc 0) (-1)) o fa) ((\<lambda>g. g (Suc 0) (-1)) o ga)) tps"} *}
ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair (h o (\<lambda>t. fa t)) (h o (\<lambda>t. ga t))) tps"} *}
ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair (k o h o (\<lambda>t. fa t)) (k o h o (\<lambda>t. ga t))) tps"} *}

txt {* 2Ab *}

ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair ((\<lambda>g. g (Suc 0) (-1)) o (\<lambda>t n. fb n t))
      ((\<lambda>g. g (Suc 0) (-1)) o (\<lambda>t n. gb n t))) tps"} *}
ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair (h o (\<lambda>t n. fb n t)) (h o (\<lambda>t n. gb n t))) tps"} *}
ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair (k o h o (\<lambda>t n. fb n t)) (k o h o (\<lambda>t n. gb n t))) tps"} *}

txt {* 2Ba *}

ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair ((\<lambda>(t, g). g (size t) (-1)) o (\<lambda>t. (t, ga t)))
      ((\<lambda>(t, g). g (size t) (-1)) o (\<lambda>t. (t, ga t)))) tps"} *}
ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair (h o (\<lambda>t. (t, fa t))) (h o (\<lambda>t. (t, ga t)))) tps"} *}
ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair (k o h o (\<lambda>t. (t, fa t))) (k o h o (\<lambda>t. (t, ga t)))) tps"} *}

txt {* 2Bb *}

ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair ((\<lambda>(t, g). g (size t) (-1)) o (\<lambda>t. (t, \<lambda>n. fb n t)))
      ((\<lambda>(t, g). g (size t) (-1)) o (\<lambda>t. (t, \<lambda>n. gb n t)))) tps"} *}

txt {* 2C *}

ML_val {* massage true @{term tps} @{term pps} @{term "map (size_pair o (\<lambda>tp. tp)) tps"} *}
ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair (size o (\<lambda>t. t)) (size o (\<lambda>t. t))) tps"} *}

txt {* 3Aa *}

ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair (\<lambda>t. fa t (Suc 0) (-1)) (\<lambda>t. ga t (Suc 0) (-1))) tps"} *}
ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair (\<lambda>t. fa t (Suc 0)) (\<lambda>t. ga t (Suc 0))) tps"} *}
ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair (\<lambda>t. h (fa t (Suc 0) (-1) + fa t 0 (-2)))
      (\<lambda>t. h (ga t (Suc 0) (-1) + ga t 0 (-2)))) tps"} *}

txt {* 3Ab *}

ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair (\<lambda>t. fb (Suc 0) t (-1)) (\<lambda>t. gb (Suc 0) t (-1))) tps"} *}
ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair (\<lambda>t. fb (Suc 0) t) (\<lambda>t. gb (Suc 0) t)) tps"} *}
ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair (\<lambda>t. h (fb (Suc 0) t (-1) + fb 0 t (-2)))
      (\<lambda>t. h (gb (Suc 0) t (-1) + gb 0 t (-2)))) tps"} *}

txt {* 3Ba *}

ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair (\<lambda>t. fa t (size t) (-1)) (\<lambda>t. ga t (size t) (-1))) tps"} *}
ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair (\<lambda>t. fa t (size t)) (\<lambda>t. ga t (size t))) tps"} *}
ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair (\<lambda>t. h t (fa t (Suc 0) (-1) + fa t 0 (-2)))
      (\<lambda>t. h t (ga t (Suc 0) (-1) + ga t 0 (-2)))) tps"} *}

txt {* 3Bb *}

ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair (\<lambda>t. fb (size t) t (-1)) (\<lambda>t. gb (size t) t (-1))) tps"} *}
ML_val {* massage true @{term tps} @{term pps}
  @{term "map (map_pair (\<lambda>t. fb (size t) t) (\<lambda>t. gb (size t) t)) tps"} *}

txt {* 3C *}

ML_val {* massage true @{term tps} @{term pps} @{term "map size_pair tps"} *}
ML_val {* massage true @{term tps} @{term pps} @{term "map (map_pair size size) tps"} *}

txt {* Misc *}

ML_val {* massage false @{term tps} @{term pps} @{term "map size_pair (id tps)"} *}
ML_val {* massage false @{term tps} @{term pps} @{term "map (map_pair fa ga) (map id tps)"} *}

oops

end
