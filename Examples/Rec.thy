theory Rec
imports "~~/src/HOL/BNF/BNF"
begin

datatype 'a tree = Empty 'a | Node "'a tree" "'a tree list"

primrec qset and qset_list where
  "qset (Empty l) = {l}"
| "qset (Node t ts) = qset t \<union> qset_list ts"
| "qset_list [] = {}"
| "qset_list (x # xs) = qset x \<union> qset_list xs"

datatype_new 'a list2 = N | C "'a" "'a list2"
datatype_new 'a tree2 = Empty 'a | Node "'a tree2" "'a tree2 list2"

datatype_new 'a tree3 = Empty 'a | Node "'a tree3" "'a tree_list3"
and  'a tree_list3 = N | C "'a tree3" "'a tree_list3"

definition "tree2_list2_fold t1 t2 l1 l2 =
  tree2_fold t1 (\<lambda>l Ss. t2 l (list2_fold l1 l2 Ss))"
definition "list2_tree2_fold t1 t2 l1 l2 =
  list2_fold l1 (\<lambda>t S. l2 (tree2_fold t1 (\<lambda>l Ss. t2 l (list2_fold l1 l2 Ss)) t) S)"

term "\<lambda>t. (tree3_fold t, tree_list3_fold t)"
term "\<lambda>t. (tree2_list2_fold t, list2_tree2_fold t)"

definition "qset2 =
  tree2_list2_fold (\<lambda>l. {l}) (\<lambda>S S'. S \<union> S') {} (\<lambda>S S'. S \<union> S')"
definition "qset_list2 =
  list2_tree2_fold (\<lambda>l. {l}) (\<lambda>S S'. S \<union> S') {} (\<lambda>S S'. S \<union> S')"

definition "qset2' =
  tree2_fold (\<lambda>l. {l}) (\<lambda>S Ss. S \<union> list2_fold {} (\<lambda>S S'. S \<union> S') Ss)"
definition "qset_list2' =
  list2_fold {} (\<lambda>t S. tree2_fold  (\<lambda>l. {l}) (\<lambda>S Ss. S \<union> list2_fold {} (\<lambda>S S'. S \<union> S') Ss) t \<union> S)"

definition "qset3 =
  tree2_fold (\<lambda>l. {l}) (\<lambda>S Ss. S \<union> \<Union>list2_set Ss)"

end
