theory Nested_To_Mutual_Co
imports "~~/src/HOL/BNF/BNF"
begin

declare [[bnf_note_all]]
bnf_decl ('a, 'b) F0
bnf_decl ('a, 'b) G0

(*nested version*)
codatatype 'a F = CF "('a, 'a F) F0"
codatatype 'a G = CG "('a, ('a G) F) G0"
term "dtor_corec_F :: ('b \<Rightarrow> ('a, 'a F + 'b) F0) \<Rightarrow> 'b \<Rightarrow> 'a F"
term "dtor_corec_G :: ('b \<Rightarrow> ('a, ('a G + 'b) F) G0) \<Rightarrow> 'b \<Rightarrow> 'a G"
thm G.rel_coinduct[unfolded rel_pre_G_def id_apply]
(*pseudomutual version*)
codatatype 'a G' = CG "('a, 'a GF') G0"
and  'a GF' = CF "('a G', 'a GF') F0"
term "G'_dtor_corec ::
  ('c \<Rightarrow> ('a, 'a GF' + 'b) G0) \<Rightarrow> ('b \<Rightarrow> ('a G' + 'c, 'a GF' + 'b) F0) \<Rightarrow> 'c \<Rightarrow> 'a G'"
term "GF'_dtor_corec ::
  ('c \<Rightarrow> ('a, 'a GF' + 'b) G0) \<Rightarrow> ('b \<Rightarrow> ('a G' + 'c, 'a GF' + 'b) F0) \<Rightarrow> 'b \<Rightarrow> 'a GF'"

definition G_pseudomutual_dtor_unfold :: 
    "('c \<Rightarrow> ('a, 'b) G0) \<Rightarrow> ('b \<Rightarrow> ('c, 'b) F0) \<Rightarrow> 'c \<Rightarrow> 'a G"
  where "G_pseudomutual_dtor_unfold s1 s2 =
    dtor_unfold_G (map_G0 id (dtor_unfold_F s2) o s1)"

definition G_pseudomutual_dtor_corec :: 
    "('c \<Rightarrow> ('a, 'a G F + 'b) G0) \<Rightarrow> ('b \<Rightarrow> ('a G + 'c, 'a G F + 'b) F0) \<Rightarrow> 'c \<Rightarrow> 'a G"
  where "G_pseudomutual_dtor_corec s1 s2 =
    dtor_corec_G (map_G0 id (sum_case (map_F Inl) (dtor_corec_F (map_F0 id (sum_map (map_F Inl) id) o s2))) o s1)"

definition F_pseudomutual_dtor_corec ::
    "('c \<Rightarrow> ('a, 'a G F + 'b) G0) \<Rightarrow> ('b \<Rightarrow> ('a G + 'c, 'a G F + 'b) F0) \<Rightarrow> 'b \<Rightarrow> 'a G F"
  where "F_pseudomutual_dtor_corec s1 s2 =
    dtor_corec_F (map_F0 (sum_case id (G_pseudomutual_dtor_corec s1 s2)) id o s2)"

lemma G_pseudomutual_dtor_corec:
  "dtor_G (G_pseudomutual_dtor_corec s1 s2 x) =
     map_G0 id (sum_case id (F_pseudomutual_dtor_corec s1 s2)) (s1 x)"
  unfolding G_pseudomutual_dtor_corec_def F_pseudomutual_dtor_corec_def G.dtor_corec
    o_apply o_sum_case id_apply id_o sum_case_o_inj(1)
    G0.map_comp G0.map_id map_pre_G_def
    F.map_comp0[symmetric] F.map_id0
  apply (rule G0.map_cong0) (*arg_cong*)
  apply (rule refl)
  apply (rule fun_cong[OF arg_cong2[of _ _ _ _ sum_case]])
  apply (rule refl)
  apply (rule F.dtor_corec_unique)
  apply (rule ext)
  unfolding F0.map_comp
    o_apply id_o o_id id_apply o_sum_case sum_case_o_sum_map sum_case_o_inj(1)
    F.dtor_map F.dtor_corec F.map_comp0[symmetric] F.map_id0
  ..

lemma F_pseudomutual_dtor_corec:
  "dtor_F (F_pseudomutual_dtor_corec s1 s2 x) =
     map_F0 (sum_case id (G_pseudomutual_dtor_corec s1 s2))
            (sum_case id (F_pseudomutual_dtor_corec s1 s2)) (s2 x)"
  unfolding G_pseudomutual_dtor_corec_def F_pseudomutual_dtor_corec_def F.dtor_corec F0.map_comp o_def id_apply
  ..

thm G'_GF'.dtor_coinduct[no_vars]

lemma G_pseudomutual_rel_coinduct:
  assumes CIH1: "\<forall>z1 z1b. P2 z1 z1b \<longrightarrow> rel_pre_G' P1 P2 P3 (dtor_G z1) (dtor_G z1b)"
  and     CIH2: "\<forall>z2 z2b. P3 z2 z2b \<longrightarrow> rel_pre_GF' P1 P2 P3 (dtor_F z2) (dtor_F z2b)"
  shows "P2 \<le> rel_G P1"
apply (rule G.rel_coinduct[unfolded rel_pre_G_def id_apply])
apply (erule mp[OF spec2[OF CIH1[unfolded rel_pre_G'_def id_apply]], THEN rev_predicate2D[OF _ G0.rel_mono]])
apply (rule order_refl)
apply (rule F.rel_coinduct[unfolded id_apply])
apply (erule mp[OF spec2[OF CIH2[unfolded rel_pre_GF'_def id_apply]]])
done

lemma G_F_pseudomutual_rel_coinduct:
  assumes CIH1: "\<forall>z1 z1b. P2 z1 z1b \<longrightarrow> rel_pre_G' P1 P2 P3 (dtor_G z1) (dtor_G z1b)"
  and     CIH2: "\<forall>z2 z2b. P3 z2 z2b \<longrightarrow> rel_pre_GF' P1 P2 P3 (dtor_F z2) (dtor_F z2b)"
  shows "P2 \<le> rel_G P1 \<and> P3 \<le> rel_F (rel_G P1)"
apply (rule conjI)
apply (rule G_pseudomutual_rel_coinduct[OF CIH1 CIH2])

apply (rule F.rel_coinduct)
apply (drule mp[OF spec2[OF CIH2]])
unfolding rel_pre_GF'_def
apply (erule predicate2D[OF F0.rel_mono, rotated -1])
apply (rule G_pseudomutual_rel_coinduct[OF CIH1 CIH2])
apply (rule order_refl)
done

thm G.rel_coinduct[unfolded rel_pre_G_def id_apply]
thm F.rel_coinduct
thm G_F_pseudomutual_rel_coinduct[unfolded rel_pre_G'_def rel_pre_GF'_def]

lemmas G_pseudomutual_coinduct =
  spec[OF spec[OF G_pseudomutual_rel_coinduct[of _ "op =", unfolded G.rel_eq le_fun_def le_bool_def]]]

lemmas G_F_pseudomutual_coinduct =
  spec[OF spec[OF spec[OF spec[OF G_F_pseudomutual_rel_coinduct[of _ "op =",
    unfolded F.rel_eq G.rel_eq le_fun_def le_bool_def all_simps(1,2)[symmetric]]]]]]

thm G_F_pseudomutual_coinduct

end
