theory Primrec_Transfer
imports "~~/src/HOL/Library/FSet"
begin

datatype (discs_sels) simple = X1 | X2 | X3 | X4

datatype (discs_sels) simple' = X1' unit | X2' unit | X3' unit | X4' unit

datatype (discs_sels) simple'' = X1'' nat int | X2''

datatype (discs_sels) ('b, 'c :: ord, 'd, 'e) some_passive =
  SP1 "('b, 'c, 'd, 'e) some_passive" | SP2 'b | SP3 'c | SP4 'd | SP5 'e

datatype (discs_sels) 'a mylist = MyNil | MyCons (myhd: 'a) (mytl: "'a mylist")

datatype (discs_sels) hfset = HFset "hfset fset"

datatype (discs_sels) 'a unord_tree = Leaf 'a | Node "'a unord_tree fset"

datatype (discs_sels) 'a par_lambda =
  PVar 'a |
  PApp "'a par_lambda" "'a par_lambda" |
  PAbs 'a "'a par_lambda" |
  PLet "('a \<times> 'a par_lambda) fset" "'a par_lambda"

datatype (discs_sels) 'a I1 = I11 'a "'a I1" | I12 'a "'a I2"
and 'a I2 = I21 | I22 "'a I1" "'a I2"

datatype (discs_sels) 'a tree = TEmpty | TNode 'a "'a forest"
and 'a forest = FNil | FCons "'a tree" "'a forest"

datatype (discs_sels) 'a tree' = TEmpty' | TNode' "'a branch" "'a branch"
and 'a branch = Branch 'a "'a tree'"

datatype (discs_sels) 'a bin_rose_tree = BRTree 'a "'a bin_rose_tree mylist" "'a bin_rose_tree mylist"

datatype (discs_sels) ('a, 'b) exp = Term "('a, 'b) trm" | Sum "('a, 'b) trm" "('a, 'b) exp"
and ('a, 'b) trm = Factor "('a, 'b) factor" | Prod "('a, 'b) factor" "('a, 'b) trm"
and ('a, 'b) factor = Const 'a | Var 'b | Paren "('a, 'b) exp"

declare [[bnf_note_all]]
datatype (discs_sels) 'a ftree = FTLeaf 'a | FTNode "'a \<Rightarrow> 'a ftree"

context lifting_syntax
begin

primrec nat_of_simple :: "simple \<Rightarrow> nat" where
  "nat_of_simple X1 = 1" |
  "nat_of_simple X2 = 2" |
  "nat_of_simple X3 = 3" |
  "nat_of_simple X4 = 4"

lemma nat_of_simple_transfer: "(op = ===> op =) nat_of_simple nat_of_simple"
  unfolding nat_of_simple_def by transfer_prover

primrec simple_of_simple' :: "simple' \<Rightarrow> simple" where
  "simple_of_simple' (X1' _) = X1" |
  "simple_of_simple' (X2' _) = X2" |
  "simple_of_simple' (X3' _) = X3" |
  "simple_of_simple' (X4' _) = X4"

lemma simple_of_simple'_transfer: "(op = ===> op =) simple_of_simple' simple_of_simple'"
  unfolding simple_of_simple'_def by transfer_prover

primrec inc_simple'' :: "nat \<Rightarrow> simple'' \<Rightarrow> simple''" where
  "inc_simple'' k (X1'' n i) = X1'' (n + k) (i + int k)" |
  "inc_simple'' _ X2'' = X2''"

lemma inc_simple''_transfer: "(op = ===> op = ===> op =) inc_simple'' inc_simple''"
  unfolding inc_simple''_def by transfer_prover

primrec (transfer) myapp :: "'a mylist \<Rightarrow> 'a mylist \<Rightarrow> 'a mylist" where
  "myapp MyNil ys = ys" |
  "myapp (MyCons x xs) ys = MyCons x (myapp xs ys)"

declare myapp.transfer[transfer_rule]

lemma myapp_transfer: "(rel_mylist R ===> rel_mylist R ===> rel_mylist R) myapp myapp"
  unfolding myapp_def by transfer_prover

primrec (transfer) myrev :: "'a mylist \<Rightarrow> 'a mylist" where
  "myrev MyNil = MyNil" |
  "myrev (MyCons x xs) = myapp (myrev xs) (MyCons x MyNil)"

lemma myrev_transfer: "(rel_mylist R ===> rel_mylist R) myrev myrev"
  unfolding myrev_def by transfer_prover

primrec shuffle_sp :: "('a \<Colon> ord, 'b \<Colon> ord, 'c, 'd) some_passive \<Rightarrow> ('d, 'a, 'b, 'c) some_passive" where
  "shuffle_sp (SP1 sp) = SP1 (shuffle_sp sp)" |
  "shuffle_sp (SP2 a) = SP3 a" |
  "shuffle_sp (SP3 b) = SP4 b" |
  "shuffle_sp (SP4 c) = SP5 c" |
  "shuffle_sp (SP5 d) = SP2 d"

lemma shuffle_sp_transfer:
  "(rel_some_passive A B C D ===> rel_some_passive D A B C) shuffle_sp shuffle_sp"
  unfolding shuffle_sp_def by transfer_prover

primrec
  hf_size :: "hfset \<Rightarrow> nat"
where
  "hf_size (HFset X) = 1 + setsum id (fset (fimage hf_size X))"

lemma hf_size_transfer:
  "(op = ===> op =) hf_size hf_size"
  by transfer_prover

primrec
  unord_tree_size :: "('a \<Rightarrow> nat) \<Rightarrow> 'a unord_tree \<Rightarrow> nat"
where
  "unord_tree_size f (Leaf a) = f a"
| "unord_tree_size f (Node X) = 1 + setsum id (fset (fimage (unord_tree_size f) X))"

lemma unord_tree_size_transfer:
  "((A ===> op =) ===> rel_unord_tree A ===> op =) unord_tree_size unord_tree_size"
  unfolding unord_tree_size_def by transfer_prover

primrec rename_lam :: "('a \<Rightarrow> 'b) \<Rightarrow> 'a par_lambda \<Rightarrow> 'b par_lambda" where
  "rename_lam f (PVar s) = PVar (f s)" |
  "rename_lam f (PApp l l') = PApp (rename_lam f l) (rename_lam f l')" |
  "rename_lam f (PAbs s l) = PAbs (f s) (rename_lam f l)" |
  "rename_lam f (PLet SL l) = PLet (fimage (map_prod f (rename_lam f)) SL) (rename_lam f l)"

lemma rename_lam_transfer:
  "((A ===> B) ===> rel_par_lambda A ===> rel_par_lambda B) rename_lam rename_lam"
  unfolding rename_lam_def by transfer_prover

primrec
  sum_i1 :: "('a\<Colon>{zero,plus}) I1 \<Rightarrow> 'a" and
  sum_i2 :: "'a I2 \<Rightarrow> 'a"
where
  "sum_i1 (I11 n i) = n + sum_i1 i" |
  "sum_i1 (I12 n i) = n + sum_i2 i" |
  "sum_i2 I21 = 0" |
  "sum_i2 (I22 i j) = sum_i1 i + sum_i2 j"

(*not parametric, 0!*)
lemma sum_i1_transfer:
  "(rel_I1 A ===> A) sum_i1 sum_i1"
  oops
lemma sum_i2_transfer:
  "(rel_I2 A ===> A) sum_i2 sum_i2"
  oops

primrec forest_of_mylist :: "'a tree mylist \<Rightarrow> 'a forest" where
  "forest_of_mylist MyNil = FNil" |
  "forest_of_mylist (MyCons t ts) = FCons t (forest_of_mylist ts)"

lemma forest_of_mylist_transfer[transfer_rule]:
  "(rel_mylist (rel_tree A) ===> rel_forest A) forest_of_mylist forest_of_mylist"
  unfolding forest_of_mylist_def by transfer_prover

primrec mylist_of_forest :: "'a forest \<Rightarrow> 'a tree mylist" where
  "mylist_of_forest FNil = MyNil" |
  "mylist_of_forest (FCons t ts) = MyCons t (mylist_of_forest ts)"

lemma mylist_of_forest_transfer:
  "(rel_forest A ===> rel_mylist (rel_tree A)) mylist_of_forest mylist_of_forest"
  unfolding mylist_of_forest_def (*by transfer_prover*) oops

primrec unused_id_tree :: "'a tree \<Rightarrow> 'a tree" and mylist_of_forest2 :: "'a forest \<Rightarrow> 'a tree mylist" where
  "unused_id_tree TEmpty = TEmpty" |
  "unused_id_tree (TNode a ts) = TNode a (map_forest id ts)" |
  "mylist_of_forest2 FNil = MyNil" |
  "mylist_of_forest2 (FCons t ts) = MyCons t (mylist_of_forest2 ts)"

lemma mylist_of_forest2_transfer[transfer_rule]:
  "(rel_forest A ===> rel_mylist (rel_tree A)) mylist_of_forest2 mylist_of_forest2"
  unfolding mylist_of_forest2_def by transfer_prover

definition frev :: "'a forest \<Rightarrow> 'a forest" where
  "frev = forest_of_mylist \<circ> myrev \<circ> mylist_of_forest2"

lemma frev_transfer[transfer_rule]: "(rel_forest A ===> rel_forest A) frev frev"
  unfolding frev_def by transfer_prover

primrec
  mirror_tree :: "'a tree \<Rightarrow> 'a tree" and
  mirror_forest :: "'a forest \<Rightarrow> 'a forest"
where
  "mirror_tree TEmpty = TEmpty" |
  "mirror_tree (TNode x ts) = TNode x (mirror_forest ts)" |
  "mirror_forest FNil = FNil" |
  "mirror_forest (FCons t ts) = frev (FCons (mirror_tree t) (mirror_forest ts))"

lemma mirror_tree_transfer: "(rel_tree A ===> rel_tree A) mirror_tree mirror_tree"
  unfolding mirror_tree_def by transfer_prover

lemma mirror_forest_transfer: "(rel_forest A ===> rel_forest A) mirror_forest mirror_forest"
  unfolding mirror_forest_def by transfer_prover

primrec
  mylist_of_tree' :: "'a tree' \<Rightarrow> 'a mylist" and
  mylist_of_branch :: "'a branch \<Rightarrow> 'a mylist"
where
  "mylist_of_tree' TEmpty' = MyNil" |
  "mylist_of_tree' (TNode' b b') = myapp (mylist_of_branch b) (mylist_of_branch b')" |
  "mylist_of_branch (Branch x t) = MyCons x (mylist_of_tree' t)"

lemma mylist_of_tree'_transfer: "(rel_tree' A ===> rel_mylist A) mylist_of_tree' mylist_of_tree'"
  unfolding mylist_of_tree'_def by transfer_prover

lemma mylist_of_branch_transfer: "(rel_branch A ===> rel_mylist A) mylist_of_branch mylist_of_branch"
  unfolding mylist_of_branch_def by transfer_prover

(*
primrec
  id_tree :: "'a bin_rose_tree \<Rightarrow> 'a bin_rose_tree" and
  id_trees1 :: "'a bin_rose_tree mylist \<Rightarrow> 'a bin_rose_tree mylist" and
  id_trees2 :: "'a bin_rose_tree mylist \<Rightarrow> 'a bin_rose_tree mylist"
where
  "id_tree (BRTree a ts ts') = BRTree a (id_trees1 ts) (id_trees2 ts')" |
  "id_trees1 MyNil = MyNil" |
  "id_trees1 (MyCons t ts) = MyCons (id_tree t) (id_trees1 ts)" |
  "id_trees2 MyNil = MyNil" |
  "id_trees2 (MyCons t ts) = MyCons (id_tree t) (id_trees2 ts)"
thm id_tree_def
thm id_tree.rec_n2m_bin_rose_tree_def
thm id_tree.n2m_bin_rose_tree_ctor_rec_def


primrec
  trunc_tree :: "'a bin_rose_tree \<Rightarrow> 'a bin_rose_tree" and
  trunc_trees1 :: "'a bin_rose_tree mylist \<Rightarrow> 'a bin_rose_tree mylist" and
  trunc_trees2 :: "'a bin_rose_tree mylist \<Rightarrow> 'a bin_rose_tree mylist"
where
  "trunc_tree (BRTree a ts ts') = BRTree a (trunc_trees1 ts) (trunc_trees2 ts')" |
  "trunc_trees1 MyNil = MyNil" |
  "trunc_trees1 (MyCons t ts) = MyCons (id_tree t) MyNil" |
  "trunc_trees2 MyNil = MyNil" |
  "trunc_trees2 (MyCons t ts) = MyCons (id_tree t) MyNil"
*)

primrec
  is_ground_exp :: "('a, 'b) exp \<Rightarrow> bool" and
  is_ground_trm :: "('a, 'b) trm \<Rightarrow> bool" and
  is_ground_factor :: "('a, 'b) factor \<Rightarrow> bool"
where
  "is_ground_exp (Term t) \<longleftrightarrow> is_ground_trm t" |
  "is_ground_exp (Sum t e) \<longleftrightarrow> is_ground_trm t \<and> is_ground_exp e" |
  "is_ground_trm (Factor f) \<longleftrightarrow> is_ground_factor f" |
  "is_ground_trm (Prod f t) \<longleftrightarrow> is_ground_factor f \<and> is_ground_trm t" |
  "is_ground_factor (Const _) \<longleftrightarrow> True" |
  "is_ground_factor (Var _) \<longleftrightarrow> False" |
  "is_ground_factor (Paren e) \<longleftrightarrow> is_ground_exp e"

lemma is_ground_exp_transfer: "(rel_exp A B ===> op =) is_ground_exp is_ground_exp"
  unfolding is_ground_exp_def by transfer_prover

lemma is_ground_trm_transfer: "(rel_trm A B ===> op =) is_ground_trm is_ground_trm"
  unfolding is_ground_trm_def by transfer_prover

lemma is_ground_factor_transfer: "(rel_factor A B ===> op =) is_ground_factor is_ground_factor"
  unfolding is_ground_factor_def by transfer_prover

primrec map_ftreeA :: "('a \<Rightarrow> 'a) \<Rightarrow> 'a ftree \<Rightarrow> 'a ftree" where
  "map_ftreeA f (FTLeaf x) = FTLeaf (f x)" |
  "map_ftreeA f (FTNode g) = FTNode (map_ftreeA f \<circ> g)"

lemma map_ftreeA_transfer:
  "((op = ===> op =) ===> op = ===> op =) map_ftreeA map_ftreeA"
  by transfer_prover

primrec map_ftreeB :: "('a \<Rightarrow> 'b) \<Rightarrow> 'a ftree \<Rightarrow> 'b ftree" where
  "map_ftreeB f (FTLeaf x) = FTLeaf (f x)" |
  "map_ftreeB f (FTNode g) = FTNode (map_ftreeB f \<circ> g \<circ> the_inv f)"

lemma map_ftreeB_transfer:
  "((op = ===> op =) ===> op = ===> op =) map_ftreeB map_ftreeB"
  by transfer_prover

end
