theory Cabs_Experiment
imports Main
begin 

declare [[ML_print_depth = 100]]

ML {*
val raw_deps =
 [("cabs_expression",
   ["cabs_expression0", "cabs_generic_association", "designator", "initializer", "type_name"]),
  ("cabs_expression0",
   ["cabs_expression"]),
  ("cabs_generic_association",
   ["type_name", "cabs_expression0"]),
  ("declaration0",
   ["specifiers", "init_declarator", "static_assert_declaration"]),
  ("specifiers",
   ["cabs_type_specifier", "alignment_specifier"]),
  ("init_declarator",
   ["declarator", "initializer"]),
  ("cabs_type_specifier",
   ["type_name", "struct_declaration", "enumerator"]),
  ("struct_declaration",
   ["cabs_type_specifier", "struct_declarator", "static_assert_declaration"]),
  ("struct_declarator",
   ["declarator", "cabs_expression0"]),
  ("enumerator",
   ["cabs_expression0"]),
  ("alignment_specifier",
   ["type_name", "cabs_expression0"]),
  ("declarator",
   ["pointer_declarator", "direct_declarator"]),
  ("direct_declarator",
   ["declarator", "direct_declarator", "array_declarator", "parameter_type_list"]),
  ("array_declarator",
   ["array_declarator_size"]),
  ("array_declarator_size",
   ["cabs_expression0"]),
  ("pointer_declarator",
   ["pointer_declarator"]),
  ("parameter_type_list",
   ["parameter_declaration"]),
  ("parameter_declaration",
   ["specifiers", "declarator", "abstract_declarator"]),
  ("type_name",
   ["cabs_type_specifier", "abstract_declarator"]),
  ("abstract_declarator",
   ["pointer_declarator", "direct_abstract_declarator"]),
  ("direct_abstract_declarator",
   ["abstract_declarator", "direct_abstract_declarator", "array_declarator", "parameter_type_list"]),
  ("initializer",
   ["cabs_expression0", "designator", "initializer"]),
  ("designator",
   ["cabs_expression0"]),
  ("static_assert_declaration",
   ["cabs_expression0"])];
*}

ML {*
val deps = map (fn (x, xs) => (x, remove (op =) x xs)) raw_deps;
*}

ML {*
val G = Graph.make (map (apfst (rpair ())) deps);
val sccs = map (sort string_ord) (Graph.strong_conn G);
*}

ML {*
val normals = maps (fn x :: xs => map (fn y => (y, x)) xs) sccs;

fun normal s = AList.lookup (op =) normals s |> the_default s;

val normal_deps = map (fn (x, xs) => (normal x, fold (insert (op =) o normal) xs [])) deps
  |> AList.group (op =)
  |> map (apsnd (fn xss => fold (union (op =)) xss []));

val normal_G = Graph.make (map (apfst (rpair ())) normal_deps);

val ordered_normals = rev (Graph.topological_order normal_G);
val ordered_sccs = map (fn x => the (find_first (fn (y :: _) => y = x) sccs)) ordered_normals;
*}

end
