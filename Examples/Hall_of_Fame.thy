theory Hall_of_Fame
imports Main
begin

codatatype (lset: 'a) llist =
    lnull: LNil
  | LCons (lhd: 'a) (ltl: "'a llist")
for
  map: lmap
  rel: llist_all2
where
  "lhd LNil = undefined"
| "ltl LNil = LNil"

primcorec lappend2 :: "'a llist \<Rightarrow> 'a llist \<Rightarrow> 'a llist" where
  "lappend2 xs ys = (if lnull xs \<and> lnull xs then if lnull ys then LNil else LCons (lhd ys) (ltl ys)
     else LCons (lhd xs) (lappend2 (ltl xs) ys))"


locale deads
begin
datatype ('a, 'k, 'b) x1 = F "'a \<Rightarrow> 'k \<Rightarrow> 'b"
datatype ('a, 'k, 'b) x2 = F "'k \<Rightarrow> 'a \<Rightarrow> 'b"
datatype ('a, 'k) y1 = Y "('a, 'k, ('a, 'k) y1 option) x1"
datatype ('a, 'k) y2 = Y "('a, 'k, ('a, 'k) y2 option) x2"
end

codatatype
  'a l_mut = C 'a "'a l_mut" and
  'a m_mut = D 'a "'a m_mut"

codatatype
  t_mut = T "t_mut l_mut" and
  u_mut = U "t_mut"

primcorec
  f_u :: "nat \<Rightarrow> u_mut" and
  f_t :: "nat \<Rightarrow> t_mut" and
  f_tm :: "nat \<Rightarrow> t_mut m_mut"
where
  "f_tm n = D (f_t n) undefined" |
  "f_t n = T undefined" |
  "f_u n = U (f_t n)"

codatatype 'a s0 = SC (shd: 'a) (stl: "'a s0")
codatatype t0 = N "t0 list"

primcorec mkT :: "nat s0 \<Rightarrow> nat \<Rightarrow> t0" where
  "mkT s i = (case s of SC x s' \<Rightarrow> N (map (mkT s') [i]))"

datatype 'a fun_with_deads = X "'a \<Rightarrow> nat" "unit \<Rightarrow> 'a"

datatype 'a l = N | C 'a "'a l"
datatype 'a t = T 'a "'a \<Rightarrow> 'a t" | T'

primrec (nonexhaustive) ff where
"ff (T x g) = g"

primrec (nonexhaustive) ff' where
"ff' (T x g) = g x"

primrec (nonexhaustive) ff'' where
"ff'' (T x g) = id g"

primrec (nonexhaustive) ff''' where
"ff''' (T x g) = id (g x)"

primrec (nonexhaustive) ff'''' where
"ff'''' (T x g) = id g x"

datatype n = Z | S n

locale A = fixes f :: "n \<Rightarrow> bool" begin
primrec r :: "n \<Rightarrow> bool" where
  "r Z = True"
| "r (S x) = f x"
end

primrec r' :: "(n \<Rightarrow> bool) \<Rightarrow> n \<Rightarrow> bool" where
  "r' fff Z = True"
| "r' fff (S x) = fff x"


locale z4
begin

datatype 'a l = N | C 'a "'a l"
datatype 'a t = T 'a "'a \<Rightarrow> 'a t l" "'a \<Rightarrow> 'a t l" "'a \<Rightarrow> 'a t l" | T'

primrec (nonexhaustive) ff :: "'a t \<Rightarrow> 'a t" and gg :: "'a t l \<Rightarrow> 'a t l" where
  "ff (T x g1 g2 g3) = T x (%y. gg (g1 y)) (gg \<circ> g2) (%y. map_l ff (g3 y))" |
  "gg N = N"

end


datatype ('a, 'b, 'c) SP = GET "'a \<Rightarrow> ('a, 'b, 'c) SP" | PUT "'b" "'c"
datatype_compat SP
consts OUT :: "'c \<Rightarrow> ('a, 'b, 'c) SP"

codatatype 'a stream = SCons (shd: 'a) (stl: "'a stream")

primrec run\<^sub>\<mu> :: "('a, 'b, 'c) SP \<Rightarrow> 'a stream \<Rightarrow> ('b \<times> 'c) \<times> 'a stream" where
  "run\<^sub>\<mu> (GET f) s = (run\<^sub>\<mu> o f) (shd s) (stl s)"
| "run\<^sub>\<mu> (PUT b sp) s = ((b, sp), s)"

primrec run :: "('a, 'b, 'c) SP \<Rightarrow> 'a stream \<Rightarrow> ('b \<times> 'c) \<times> 'a stream" where
  "run (GET f) s = run (f (shd s)) (stl s)"
| "run (PUT b sp) s = ((b, sp), s)"

primcorec mystery :: "'a stream \<Rightarrow> 'a stream" where
  "mystery s = (let (a, s') = dtor_stream s in SCons a (mystery s'))"

primcorec mystery2 :: "'a stream \<Rightarrow> 'a stream" where
  "mystery2 s = (let as' = dtor_stream s in SCons (fst as') (mystery2 (snd as')))"

datatype ('a, 'b, 'c) FSP = FGet "'a \<Rightarrow> ('a, 'b, 'c) FSP" | FPut "'b" "'c"
codatatype ('a, 'b) SPx = SP (unSP: "('a, 'b, ('a, 'b) SPx) FSP")

primcorec copy :: "('a, 'b) SPx" where
  "copy = SP undefined"

primcorec copy2 where
  "copy2 = SP (FGet (\<lambda>b. FPut b copy2))"


codatatype 'a language = Lang (\<oo>: bool) (\<dd>: "'a \<Rightarrow> 'a language")

primcorec Zero :: "'a language" where
  "\<oo> Zero = False"
| "\<dd> Zero = (\<lambda>_. Zero)"

primcorec One :: "'a language" where
  "\<oo> One = True"
| "\<dd> One = (\<lambda>_. Zero)"

primcorec TimesLR :: "'a language \<Rightarrow> 'a language \<Rightarrow> bool language" where
  "\<oo> (TimesLR r s) = (\<oo> r \<and> \<oo> s)"
| "\<dd> (TimesLR r s) = (\<lambda>b. if b then undefined else TimesLR One Zero)"

primcorec TimesLR2 :: "'a language \<Rightarrow> 'a language \<Rightarrow> ('a \<times> bool) language" where
  "\<oo> (TimesLR2 r s) = (\<oo> r \<and> \<oo> s)"
| "\<dd> (TimesLR2 r s) = (\<lambda>(a, b).
   (if b then TimesLR2 (\<dd> r a) s else if \<oo> r then TimesLR2 (\<dd> s a) One else TimesLR2 Zero One))"

primcorec TimesLR3 :: "'a language \<Rightarrow> 'a language \<Rightarrow> ('a \<times> bool) language" where
  "\<oo> (TimesLR3 r s) = (\<oo> r \<and> \<oo> s)"
| "\<dd> (TimesLR3 r s) = (\<lambda>(r, s). TimesLR3 r s) o (\<lambda>(a, b).
   (if b then (\<dd> r a, s) else if \<oo> r then (\<dd> s a, One) else (Zero, One)))"

locale z1
begin

datatype 'a dopt = DSome 'a
datatype dtree = DNode (cright: "nat \<Rightarrow> dtree dopt") | DNil

primrec (nonexhaustive)
  f4_dtree :: "dtree \<Rightarrow> nat" and
  f4_dtree_dopt :: "dtree dopt \<Rightarrow> nat"
where
  "f4_dtree (DNode fff) = undefined (\<lambda>y. map_dopt f4_dtree (fff y))" |
  "f4_dtree_dopt (DSome _) = undefined"

end

locale z2
begin

codatatype exp = E and trm = T1 | T2

primcorec
  freeze_exp :: "exp \<Rightarrow> exp"
where
  "freeze_exp e = E"

end

locale d2
begin

codatatype d = D1 | D2 | D3

primcorec d123 where
"d = D1 \<Longrightarrow> d123 d = D2" |
"d = D2 \<Longrightarrow> d123 d = D3" |
"_ \<Longrightarrow> d123 d = D1"

(* Due to "_", the specification is a priori complete. *)
thm d123.code

end

locale d3
begin

codatatype d = D1 | D2 | D3

primcorec d123 where
"d = D1 \<Longrightarrow> d123 d = D2" |
"d = D2 \<Longrightarrow> d123 d = D3"

(* Same as previous example. *)
thm d123.code

end



locale d4
begin

codatatype d = D1 | D2 | D3 | D4

primcorec d123 where
"d = D1 \<Longrightarrow> d123 d = D1" |
"d = D2 \<Longrightarrow> d123 d = D2"

(* "code" is generated: (if d = d1 then D1 else if d = d2 then D2 else abort ...) *)
thm d123.code

end

term " False"

locale d5
begin

codatatype d = D1 nat

primcorec d1 where
"d1 n = D1 n"

(* "d1.code" is identical to "d1.ctr" *)
thm d1.code

end



(* let is preserved in characteristic equations*)
codatatype s = S s
datatype 'a x = X (un_X: 'a)
codatatype y = Y "y x"

primcorec ctr :: "y \<Rightarrow> s" where
 "ctr y = (let y = un_X (un_Y y) in S (ctr y))"
print_theorems



(* (hopefully) no more weird failures à la "wrong constructor in goal" *)
codatatype 'a k = C1 'a | C2 'a | C3
primcorec gg :: "nat \<Rightarrow> nat k" where
"x = 0 \<Longrightarrow> gg x = C1 x" |
"x > (2::nat) \<Longrightarrow> gg x = C2 x"

codatatype (lset: 'a) llist =
  lnull: LNil
| LCons (lhd: 'a) (ltl: "'a llist")
| f_LNil2
for
  map: lmap rel: llist_all2
where
  "ltl LNil = LNil"

primcorec literate :: "('a \<Rightarrow> 'a) \<Rightarrow> 'a \<Rightarrow> 'a llist" where
  "literate f x = (if x = x then LNil else LCons x (literate f (f x)))"

codatatype 'a ll = LN | LC 'a "'a ll"
codatatype 'a lt = LT (val: 'a) (sub: "'a lt ll")

primcorec ll1 where
  "ll1 = LC (0 :: nat) (let X = range ((\<lambda>(a::nat, b). a + b))
       in if undefined \<in> X then LN else ll1)"

primcorec lapp :: "'a ll \<Rightarrow> 'a ll \<Rightarrow> 'a ll" where
  "lapp xs ys = (case xs of LN \<Rightarrow> ys | LC x xs' \<Rightarrow> LC x (lapp xs' ys))"

primcorec
  bind_tree :: "'a lt \<Rightarrow> ('a \<Rightarrow> 'b lt) \<Rightarrow> 'b lt"
where
  "bind_tree t f = (case f (val t) of LT b ts \<Rightarrow> 
     LT b (map_ll (\<lambda>t. case t of Inl l \<Rightarrow> l | Inr r \<Rightarrow> bind_tree r f) (lapp (map_ll Inl ts) (map_ll Inr (sub t)))))"

primcorec fp :: "('a \<Rightarrow> 'a) \<Rightarrow> 'a \<Rightarrow> 'a llist" where
  "f a = a \<Longrightarrow> is_LCons (fp f a)"
| "lhd (fp f a) = f a"
| "ltl (fp f a) = fp f (f a)"

codatatype yy = Y bool
definition "hideY = Y"
primcorec f :: "bool \<Rightarrow> yy" where
  "f b = (if b then hideY True else hideY False)"
thm f.code

codatatype s = S s
consts gs :: "'a \<Rightarrow> 'a"
primcorec fs where
  "fs x = S (fs (gs x))"
primcorec fs' where (* could be made to work *)
  "fs' x = S ((fs' o gs) x)"
primcorec fs'' where (* too crazy *)
  "fs'' = S o fs'' o gs"

codatatype t = T "t list"
consts gt :: "'a \<Rightarrow> 'a list"
primcorec ft where
  "ft x = T (map ft (gt x))"
primcorec ft' where (* could be made to work *)
  "ft' x = T ((map ft' o gt) x)"
primcorec ft'' where (* too crazy *)
  "ft'' = T o map ft' o gt"

codatatype xyz = C nat "nat \<Rightarrow> xyz"
primcorec f where "f x = C x (f o id)"
primcorec g where "g x = C x g"

(* FIXME for LORENZ or JASMIN *)
codatatype 'a llist = lnull: LNil | LCons (lhd: 'a) (ltl: "'a llist")

(* should work *)
primcorec zeroes :: "nat llist" where
  "lhd zeroes = 0"
| "ltl zeroes = zeroes"

(* the message could be improved (corecursive call as a function argument) *)
primcorec nats :: "nat llist" where
  "\<not> lnull nats"
| "lhd nats = 0"
| "ltl nats = map_llist Suc nats"


codatatype z = IN z

primcorec merge where
  "todo = [] \<Longrightarrow> merge done todo = IN (merge todo done)"
| "todo \<noteq> [] \<Longrightarrow> merge done todo = IN (merge (hd todo # done) (tl todo))"

primcorec merge2 where
  "merge2 done todo =
     (if todo = [] then IN (merge2 todo done) else IN (merge2 (hd todo # done) (tl todo)))"
print_theorems


(* the message could be more graceful *)
codatatype
  'a col1 = N1 | C1 'a "'a col2" and
  'a col2 = N2 | C2 'a "'a col1"

codatatype
  ('a, 'b) cot1 = T1 'a 'b "('a, 'b) cot1 col1" and
  ('a, 'b) cot2 = T2 "('a, 'b) cot1"

primcorec
  j3_cot2 :: "nat \<Rightarrow> (nat, 'a) cot2" and
  j3_cot1 :: "nat \<Rightarrow> (nat, 'a) cot1" and
  j3_cotcol2 :: "nat \<Rightarrow> (nat, 'a) cot1 col2"
where
  "j3_cotcol2 n = C2 (j3_cot1 n) (C1 (j3_cot1 n) N2)" |
  "j3_cot1 n = T1 n undefined (C1 (j3_cot1 n) N2)" |
  "j3_cot2 n = T2 (j3_cot1 n)"


codatatype x = A | B x

consts p :: bool q :: bool

primcorec f :: x where
  "f = (if p & q then A else B f)"


codatatype 'a ll = LN | LC 'a "'a ll"

primcorec fp_code :: "('a \<Rightarrow> 'a) \<Rightarrow> 'a \<Rightarrow> 'a ll" where
  "fp_code f a = (let fa = f a in if fa = a then LN else LC fa (fp_code f fa))"
thm fp_code.sel fp_code.ctr (* spurious "if _ then undefined else _" in the equations *)


codatatype d = D1 | D2 | D3

primcorec d123 where
"d = D1 \<Longrightarrow> d123 d = D2" |
"d = D2 \<Longrightarrow> d123 d = D3" |
"d = D3 \<Longrightarrow> d123 d = D1"

(* Ideally the constructors would appear in the order in which the user specified them (D2, D3, D1) *)
thm d123.code

end
