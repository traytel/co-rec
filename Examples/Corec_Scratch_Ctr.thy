theory Corec_Scratch_Ctr
imports "~~/src/HOL/BNF/BNF"
begin

declare [[bnf_note_all]]

datatype_new nt = Zo | Sc nt

codatatype simple = X1 | X2
term simple_corec

(*
primcorecursive f_simple "nt \<Rightarrow> simple" where
  "f_simple n = (case n of Zo \<Rightarrow> X1 | Sc _ \<Rightarrow> X2)"
*)

definition f_simple :: "nt \<Rightarrow> simple" where
"f_simple = simple_corec (\<lambda>n. n = Zo)"

lemma f_simple_unfold: "f_simple n = (if n = Zo then X1 else X2)"
unfolding f_simple_def
apply (case_tac "n = Zo")
 apply (simp only: if_True refl)
 apply (rule simple.corec(1))
 apply (rule refl)
apply (simp only: if_False)
apply (erule simple.corec(2))
done

lemma f_simple_unfold': "f_simple n = (case n of Zo \<Rightarrow> X1 | Sc _ \<Rightarrow> X2)"
unfolding nt.case_conv is_Zo_def
by (rule f_simple_unfold)


codatatype 'a llist (map: lmap) = lnull: LNil | LCons (lhd: 'a) (ltl: "'a llist")


(*
primcorecursive "from" :: "nt \<Rightarrow> nt llist" where
"from n = LCons n (from (Sc n))"
*)

thm llist.corec

definition "from" :: "nt \<Rightarrow> nt llist" where
"from = llist_corec (\<lambda>n. False) (\<lambda>n. n) (\<lambda>n. False) undefined Sc"

lemma from_unfold: "from n = LCons n (from (Sc n))"
unfolding from_def
apply (case_tac False)
 apply (erule FalseE)
 apply (erule llist.corec(2)[of _ _ _ "\<lambda>_. False", unfolded if_False])
done


(*
primcorecursive ltake_while :: "('a \<Rightarrow> bool) \<Rightarrow> 'a llist \<Rightarrow> 'a llist" where
"ltake_while p xs =
   (if lnull xs \<or> \<not> p (lhd xs) then
      LNil
    else
      LCons (lhd xs) (ltake_while p (ltl xs)))"
*)

(*
primcorecursive ltake_while :: "('a \<Rightarrow> bool) \<Rightarrow> 'a llist \<Rightarrow> 'a llist" where
"ltake_while _ LNil = LNil" |
"ltake_while p (LCons x xs) = (if p x then LCons x (ltake_while p xs) else LNil)"
*)

definition ltake_while :: "('a \<Rightarrow> bool) \<Rightarrow> 'a llist \<Rightarrow> 'a llist" where
"ltake_while p xs =
   llist_corec (\<lambda>(p, xs). lnull xs \<or> \<not> p (lhd xs)) (\<lambda>(p, xs) \<Rightarrow> lhd xs) (\<lambda>_. False) undefined
       (\<lambda>(p, xs). (p, ltl xs)) (p, xs)"

lemma ltake_while_unfold:
"ltake_while p xs =
   (if lnull xs \<or> \<not> p (lhd xs) then
      LNil
    else
      LCons (lhd xs) (ltake_while p (ltl xs)))"
unfolding ltake_while_def
apply (rule iffD2[OF split_if_eq2])
apply (rule conjI)
 apply (rule impI)
 apply (rule llist.corec(1))
 apply (unfold split)
 apply assumption
apply (rule impI)
apply (rule trans[OF llist.corec(2)])
 apply (unfold split)
 apply assumption
apply (unfold if_False)
apply (rule refl)
done


codatatype x = A | B nt x | C x x




(* mutually rec *)

(* nested rec *)

end
