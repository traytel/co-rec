theory Shame_On_Nitpick
imports Main
begin

codatatype 'a language = Lang (\<oo>: bool) (\<dd>: "'a \<Rightarrow> 'a language")

declare language.coinduct[unfolded rel_fun_def, simplified, case_names Lang, coinduct type: language]

primcorec Plus :: "'a language \<Rightarrow> 'a language \<Rightarrow> 'a language" where
  "\<oo> (Plus r s) = (\<oo> r \<or> \<oo> s)"
| "\<dd> (Plus r s) = (\<lambda>a. Plus (\<dd> r a) (\<dd> s a))"

primcorec Not :: "'a language \<Rightarrow> 'a language" where
  "\<oo> (Not r) = (\<not> \<oo> r)"
| "\<dd> (Not r) = (\<lambda>a. Not (\<dd> r a))"

definition "le r s = (Plus r s = s)"

lemma Not_antimono: "le r s \<Longrightarrow> le (Not s) (Not r)"
(* TODO for JASMIN: implement *)
nitpick [expect=genuine]
unfolding le_def proof (coinduction arbitrary: r s)
  case Lang
  then have "\<oo> (Plus r s) = \<oo> s" "\<forall>a. \<dd> (Plus r s) a = \<dd> s a" by simp_all
  then show ?case by auto
qed

end
