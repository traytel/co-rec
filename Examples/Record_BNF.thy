theory Record_BNF
imports
  "~~/src/HOL/Library/Cardinal_Notations"
  "~~/src/HOL/Library/BNF_Axiomatization"
begin

lemma disj_split_meta:
  "((\<And>x. P x \<or> Q x \<Longrightarrow> R x) \<Longrightarrow> PROP S) \<equiv>
   ((\<And>x. P x \<Longrightarrow> R x) \<Longrightarrow> (\<And>x. Q x \<Longrightarrow> R x) \<Longrightarrow> PROP S)"
  by default force+

lemma hypsubst_meta:
  "((\<And>x. x = y \<Longrightarrow> P x) \<Longrightarrow> PROP S) \<equiv> (P y \<Longrightarrow> PROP S)"
  by default force+

lemma Cfinite_singleton: "Cfinite |{x}|"
  unfolding cfinite_def by (rule conjI[OF _ card_of_Card_order]) (simp add: Field_card_of)

declare [[goals_limit = 50]]

record ('a, 'b) foo =
  bla1 :: 'a
  bla2 :: 'b
  bla12 :: "'a \<times> 'b"

term Rep_foo_ext

definition map_foo_ext where
  "map_foo_ext f1 f2 f3 x =
    foo_ext (f1 (bla1 x)) (f2 (bla2 x)) (map_prod f1 f2 (bla12 x)) (f3 (foo.more x))"

definition set1_foo_ext where
  "set1_foo_ext x = {bla1 x} \<union> {} \<union> Basic_BNFs.fsts (bla12 x)"

definition set2_foo_ext where
  "set2_foo_ext x = {bla2 x} \<union> {} \<union> Basic_BNFs.snds (bla12 x)"

definition set3_foo_ext where
  "set3_foo_ext x = {foo.more x}"

definition rel_foo_ext where
  "rel_foo_ext R1 R2 R3 x y \<longleftrightarrow>
    R1 (bla1 x) (bla1 y) \<and>
    R2 (bla2 x) (bla2 y) \<and> 
    rel_prod R1 R2 (bla12 x) (bla12 y) \<and>
    R3 (more x) (more y)"

bnf "('a, 'b, 'c) foo_scheme"
  map: map_foo_ext
  sets: set1_foo_ext set2_foo_ext set3_foo_ext
  bd: "(natLeq +c natLeq) +c natLeq"
  rel: rel_foo_ext
apply -
(*Get rid of the easy stuff*)
apply (unfold map_foo_ext_def[abs_def] rel_foo_ext_def[abs_def]
  set1_foo_ext_def[abs_def] set2_foo_ext_def[abs_def] set3_foo_ext_def[abs_def]
  id_apply o_apply fun_eq_iff OO_Grp_alt mem_Collect_eq image_Un image_insert image_empty
    Un_iff insert_iff empty_iff simp_thms
  prod.map_id prod.map_comp[symmetric] prod.set_map prod.rel_compp
  foo.surjective[symmetric] foo.iffs foo.simps disj_split_meta hypsubst_meta)
apply (rule conjI refl prod.map_cong TrueI
  card_order_csum natLeq_card_order cinfinite_csum disjI1 natLeq_cinfinite | assumption)+

(*Cardinality bounds*)
apply (rule ordLeq_transitive[OF Un_csum])
apply (rule csum_mono)
apply (rule ordLeq_transitive[OF Un_csum])
apply (rule csum_mono)
apply (simp add: Card_order_singl_ordLeq Field_natLeq natLeq_card_order) (*FIXME*)
apply (simp add: card_of_empty1 natLeq_Well_order) (*FIXME*)
apply (simp add: prod_set_defs Card_order_singl_ordLeq Field_natLeq natLeq_card_order) (*FIXME*)
apply (rule ordLeq_transitive[OF Un_csum])
apply (rule csum_mono)
apply (rule ordLeq_transitive[OF Un_csum])
apply (rule csum_mono)
apply (simp add: Card_order_singl_ordLeq Field_natLeq natLeq_card_order) (*FIXME*)
apply (simp add: card_of_empty1 natLeq_Well_order) (*FIXME*)
apply (simp add: prod_set_defs Card_order_singl_ordLeq Field_natLeq natLeq_card_order) (*FIXME*)
apply (rule ordLeq_transitive[OF _ ordLeq_csum2[OF natLeq_Card_order]])
apply (rule Card_order_singl_ordLeq[OF natLeq_Card_order, unfolded Field_natLeq, OF UNIV_not_empty])

(*Relator composition*)
apply (rule predicate2I)
apply (erule relcomppE conversepE GrpE CollectE exE conjE)+
apply (intro conjI)
apply (elim relcomppI, assumption)+

(*Relator in terms of map+set*)
apply (intro allI iffI)
apply (elim conjE prod.in_rel[THEN iffD1, elim_format] exE CollectE)
apply (intro exI[of _ "foo_ext a b c d" for a b c d])
apply (unfold foo.simps) []
apply (rule conjI Un_least insert_subsetI empty_subsetI CollectI splitI | assumption)+
apply (unfold fst_conv snd_conv)
apply (simp_all only: foo.surjective[symmetric]) [2]
apply (elim exE conjE)+
apply hypsubst
apply (unfold foo.simps prod.rel_map Un_subset_iff
  prod.in_rel mem_Collect_eq insert_subset split_beta eqTrueI[OF empty_subsetI] simp_thms)
apply (erule conjE)+
apply (rule conjI exI refl | assumption)+
done

declare [[bnf_note_all]]
bnf_axiomatization (dead 'd1, 'a, 'b) F
bnf_axiomatization (dead 'd2, 'a, 'b) G
bnf_axiomatization (dead 'd3, 'a, 'b) H

record ('d1, 'd2, 'd3, 'a, 'b) bar =
  bla1 :: "('d1, 'a, 'b) F"
  bla2 :: "('d2, 'a, 'b) G"
  bla3 :: "('d3, 'a, 'b) H"

definition map_bar_ext where
  "map_bar_ext f1 f2 f3 x =
    bar_ext (map_F f1 f2 (bla1 x)) (map_G f1 f2 (bla2 x)) (map_H f1 f2 (bla3 x)) (f3 (bar.more x))"

definition set1_bar_ext where
  "set1_bar_ext x = set1_F (bla1 x) \<union> set1_G (bla2 x) \<union> set1_H (bla3 x)"

definition set2_bar_ext where
  "set2_bar_ext x = set2_F (bla1 x) \<union> set2_G (bla2 x) \<union> set2_H (bla3 x)"

definition set3_bar_ext where
  "set3_bar_ext x = {bar.more x}"

definition rel_bar_ext where
  "rel_bar_ext R1 R2 R3 x y \<longleftrightarrow>
    rel_F R1 R2 (bla1 x) (bla1 y) \<and>
    rel_G R1 R2 (bla2 x) (bla2 y) \<and> 
    rel_H R1 R2 (bla3 x) (bla3 y) \<and>
    R3 (more x) (more y)"

bnf "('d1, 'd2, 'd3, 'a, 'b, 'c) bar_scheme"
  map: map_bar_ext
  sets: set1_bar_ext set2_bar_ext set3_bar_ext
  bd: "((bd_F :: 'd1 bd_type_F rel) +c (bd_G :: 'd2 bd_type_G rel)) +c (bd_H :: 'd3 bd_type_H rel)"
  rel: rel_bar_ext
apply -
(*Get rid of the easy stuff*)
apply (unfold map_bar_ext_def[abs_def] rel_bar_ext_def[abs_def]
  set1_bar_ext_def[abs_def] set2_bar_ext_def[abs_def] set3_bar_ext_def[abs_def]
  id_apply o_apply fun_eq_iff OO_Grp_alt mem_Collect_eq image_Un image_insert image_empty
    Un_iff insert_iff empty_iff simp_thms
  F.map_id F.map_comp0 F.set_map F.rel_compp
  G.map_id G.map_comp0 G.set_map G.rel_compp
  H.map_id H.map_comp0 H.set_map H.rel_compp
  bar.surjective[symmetric] bar.iffs bar.simps disj_split_meta hypsubst_meta)
apply (rule conjI refl F.map_cong G.map_cong H.map_cong TrueI
  F.bd_card_order G.bd_card_order H.bd_card_order
  card_order_csum cinfinite_csum disjI2 H.bd_cinfinite | assumption)+

(*Cardinality bounds*)
apply (rule ordLeq_transitive[OF Un_csum])
apply (rule csum_mono)
apply (rule ordLeq_transitive[OF Un_csum])
apply (rule csum_mono)
apply (rule F.set_bd)
apply (rule G.set_bd)
apply (rule H.set_bd)
apply (rule ordLeq_transitive[OF Un_csum])
apply (rule csum_mono)
apply (rule ordLeq_transitive[OF Un_csum])
apply (rule csum_mono)
apply (rule F.set_bd)
apply (rule G.set_bd)
apply (rule H.set_bd)
apply (rule ordLess_imp_ordLeq)
apply (rule Cfinite_ordLess_Cinfinite[OF Cfinite_singleton])
apply (rule Cinfinite_csum disjI2 H.bd_Cinfinite)+

(*Relator composition*)
apply (rule predicate2I)
apply (erule relcomppE conversepE GrpE CollectE exE conjE)+
apply (intro conjI)
apply (elim relcomppI, assumption)+

(*Relator in terms of map+set*)
apply (intro allI iffI)
apply (elim conjE exE CollectE
  F.in_rel[THEN iffD1, elim_format] G.in_rel[THEN iffD1, elim_format] H.in_rel[THEN iffD1, elim_format])
apply (intro exI[of _ "bar_ext a b c d" for a b c d])
apply (unfold bar.simps) []
apply (rule conjI Un_least insert_subsetI empty_subsetI CollectI splitI | assumption)+
apply (unfold fst_conv snd_conv)
apply (simp_all only: bar.surjective[symmetric]) [2]
apply (elim exE conjE)+
apply hypsubst
apply (unfold bar.simps prod.rel_map Un_subset_iff
  F.in_rel G.in_rel H.in_rel mem_Collect_eq insert_subset split_beta eqTrueI[OF empty_subsetI] simp_thms)
apply (erule conjE)+
apply (rule conjI exI refl | assumption)+
done

free_constructors case_bar for bar_ext using bar.surjective by blast+

term "case x of \<lparr>bla1=x, bla2 = y, bla3 = z\<rparr> \<Rightarrow> y"

end