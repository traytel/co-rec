generic_assm_proof

Construction of BNFs: 3.42 s 
Normalization & sealing of BNFs: 1.37 s 
Extracted terms & thms: 171 ms 
Checked nonemptiness: 0 ms 
Derived simple theorems: 95 ms 
Algebra definition & thms: 112 ms 
Proved nonemptiness: 5 ms 
Morphism definition & thms: 341 ms 
Isomorphism definition & thms: 14 ms 
Copy thms: 34 ms 
Bounds: 46 ms 
min_algs definition & thms: 757 ms 
Minimal algebra definition & thms: 379 ms 
Initiality definition & thms: 877 ms 
THE TYPEDEFs & Rep/Abs thms: 360 ms 
ctor definitions & thms: 151 ms 
fold definitions & thms: 70 ms 
dtor definitions & thms: 64 ms 
rec definitions & thms: 140 ms 
induction: 17 ms 
bnf constants for the new datatypes: 558 ms 
map functions for the new datatypes: 26 ms 
set functions for the new datatypes: 112 ms 
helpers for BNF properties: 123 ms 
registered new datatypes as BNFs: 273 ms 
relator induction: 115 ms 
FP construction in total: 5.13 s 
Constructors, discriminators, selectors, etc., for the new datatype: 9.37 s
----------------------------------------------------------------------------------------------------

assm

Construction of BNFs: 0 ms 
Normalization & sealing of BNFs: 604 ms 
Extracted terms & thms: 59 ms 
Checked nonemptiness: 0 ms 
Derived simple theorems: 44 ms 
Algebra definition & thms: 210 ms 
Proved nonemptiness: 3 ms 
Morphism definition & thms: 176 ms 
Isomorphism definition & thms: 11 ms 
Copy thms: 27 ms 
Bounds: 44 ms 
min_algs definition & thms: 521 ms 
Minimal algebra definition & thms: 3.48 s 
Initiality definition & thms: 759 ms 
THE TYPEDEFs & Rep/Abs thms: 277 ms 
ctor definitions & thms: 76 ms 
fold definitions & thms: 76 ms 
dtor definitions & thms: 77 ms 
rec definitions & thms: 103 ms 
induction: 18 ms 
relator induction: 91 ms 
FP construction in total: 6.14 s 
Constructors, discriminators, selectors, etc., for the new datatype: 3.85 s
----------------------------------------------------------------------------------------------------

dp_nontermination_proofs and friends

Construction of BNFs: 17.43 s 
Normalization & sealing of BNFs: 6.58 s 
Extracted terms & thms: 595 ms 
Checked nonemptiness: 0 ms 
Derived simple theorems: 197 ms 
Algebra definition & thms: 284 ms 
Proved nonemptiness: 56 ms 
Morphism definition & thms: 904 ms 
Isomorphism definition & thms: 66 ms 
Copy thms: 118 ms 
Bounds: 1.27 s 
min_algs definition & thms: 435.43 s 
Minimal algebra definition & thms: 191.54 s 
Initiality definition & thms: 304.18 s 
THE TYPEDEFs & Rep/Abs thms: 116.9 s 
ctor definitions & thms: 8.59 s 
fold definitions & thms: 1.83 s 
dtor definitions & thms: 1.52 s 
rec definitions & thms: 1.41 s 
induction: 10.18 s 
relator induction: 1.31 s 
FP construction in total: 1077.51 s 
Constructors, discriminators, selectors, etc., for the new datatype: 35.91 s
----------------------------------------------------------------------------------------------------

dp_termination_proofs and friends

Construction of BNFs: 92.82 s 
Normalization & sealing of BNFs: 13.04 s 
Extracted terms & thms: 539 ms 
Checked nonemptiness: 0 ms 
Derived simple theorems: 309 ms 
Algebra definition & thms: 424 ms 
Proved nonemptiness: 85 ms 
Morphism definition & thms: 2.22 s 
Isomorphism definition & thms: 77 ms 
Copy thms: 203 ms 
Bounds: 1.39 s 
min_algs definition & thms: 401.28 s 
Minimal algebra definition & thms: 51.11 s 
Initiality definition & thms: 441.98 s 
THE TYPEDEFs & Rep/Abs thms: 62.97 s 
ctor definitions & thms: 44.74 s 
fold definitions & thms: 12.57 s 
dtor definitions & thms: 24.36 s 
rec definitions & thms: 12.56 s 
induction: 6.05 s 
relator induction: 17.71 s 
FP construction in total: 1084.68 s 
Constructors, discriminators, selectors, etc., for the new datatype: 172.01 s
