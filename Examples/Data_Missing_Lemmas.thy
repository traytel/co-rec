theory Data_Missing_Lemmas
imports Main
begin

declare [[bnf_note_all]]

hide_const (open) tl

datatype_new (s: 'a) l = n: N | C (hd: 'a) (tl: "'a l")
  for map: m rel: r
  where "tl N = N"

end
