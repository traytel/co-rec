Phase one:

    print_bnfs
    bnf
    datatype_new
    codatatype
    primrec_new
    primcorec
    wrap_free_constructors
    wrap_datatype_new
    wrap_codatatype

    datatype
    primrec
    rep_datatype


Phase two:

    Add "compat" option to "datatype_new"
    Kill "rep_compat" option


Phase three:

    print_bnfs
    bnf
    datatype               (<- datatype_new)
    codatatype
    primrec
    primcorec
    wrap_free_constructors
    wrap_datatype
    wrap_codatatype

    legacy_datatype        (<- datatype)
    legacy_primrec         (<- primrec)
    legacy_rep_datatype    (<- rep_datatype)

    Rename "compat" option to "legacy_compat"
