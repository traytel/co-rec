theory Rec_Fun
imports BNF_LFP
begin


datatype_new 'a tree = Leaf 'a | Node "'a \<Rightarrow> 'a tree"
term rec_tree
thm tree.rec

primrec tree_map1 :: "('a \<Rightarrow> 'a) \<Rightarrow> 'a tree \<Rightarrow> 'a tree" where
  "tree_map1 f (Leaf x) = Leaf (f x)" |
  "tree_map1 f (Node g) = Node (tree_map1 f \<circ> g)"

primrec tree_map2 :: "('a \<Rightarrow> 'a) \<Rightarrow> 'a tree \<Rightarrow> 'a tree" where
  "tree_map2 f (Leaf x) = Leaf (f x)" |
  "tree_map2 f (Node g) = Node (\<lambda>x. (tree_map2 f \<circ> g) x)"

primrec tree_map3 :: "('a \<Rightarrow> 'a) \<Rightarrow> 'a tree \<Rightarrow> 'a tree" where
  "tree_map3 f (Leaf x) = Leaf (f x)" |
  "tree_map3 f (Node g) = Node (\<lambda>x. id (tree_map3 f \<circ> g) x)"

(* I've changed my mind about five times about whether we should support the following.
   Right now I'm against, since "function" is there anyway for cases like this. *)
(* Oops, it seems we do. *)

primrec tree_map4 :: "('a \<Rightarrow> 'a) \<Rightarrow> 'a tree \<Rightarrow> 'a tree" where
  "tree_map4 f (Leaf x) = Leaf (f x)" |
  "tree_map4 f (Node g) = Node (\<lambda>x. tree_map4 f (g x))"

primrec tree_map5 :: "('a \<Rightarrow> 'a) \<Rightarrow> 'a tree \<Rightarrow> 'a tree" where
  "tree_map5 f (Leaf x) = Leaf (f x)" |
  "tree_map5 f (Node g) = Node (\<lambda>x. id (tree_map5 f (g x)))"

end
