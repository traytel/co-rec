theory Corec_Mutual
imports BNF
begin

codatatype x = C1 | C2 x | C3 x y | C4 nat int and
  y = D1 | D2 x | D3

;function f :: "nat \<Rightarrow> int \<Rightarrow> x" and g :: "nat list \<Rightarrow> y" where
"f n i =
   (if n = 0 then
      C1
    else if n = 1 then
      C2 (if i = 0 then C1 else f 0 0)
    else if i = 2 then
      C3 (if i = 1 then C1 else f n i) (if n = 2 then D1 else g [])
    else
      C4 n i)" |
"g ns =
  (if ns = [] then
     D1
   else if length ns = 1 then
     D2 (if ns = [0] then C1 else f 0 0)
   else
     D3)"
oops

definition "p11 \<equiv> (\<lambda>(n::nat, i::int). n = 0)"
definition "p12 \<equiv> (\<lambda>(n::nat, i::int). n = 1)"
definition "q12 \<equiv> (\<lambda>(n::nat, i::int). i = 0)"
definition "g12 \<equiv> (\<lambda>(n::nat, i::int). C1)"
definition "h12 \<equiv> (\<lambda>(n::nat, i::int). (0, 0))"
definition "p13 \<equiv> (\<lambda>(n::nat, i::int). i = 2)"
definition "q131 \<equiv> (\<lambda>(n::nat, i::int). i = 1)"
definition "g131 \<equiv> (\<lambda>(n::nat, i::int). C1)"
definition "h131 \<equiv> (\<lambda>(n::nat, i::int). (n, i))"
definition "q132 \<equiv> (\<lambda>(n::nat, i::int). n = 2)"
definition "g132 \<equiv> (\<lambda>(n::nat, i::int). D1)"
definition "h132 \<equiv> (\<lambda>(n::nat, i::int). [])"
definition "g141 \<equiv> (\<lambda>(n::nat, i::int). n)"
definition "g142 \<equiv> (\<lambda>(n::nat, i::int). i)"
definition "p21 \<equiv> (\<lambda>ns::nat list. ns = [])"
definition "p22 \<equiv> (\<lambda>ns::nat list. length ns = 1)"
definition "q22 \<equiv> (\<lambda>ns::nat list. ns = [0])"
definition "g22 \<equiv> (\<lambda>ns::nat list. C1)"
definition "h22 \<equiv> (\<lambda>ns::nat list. (0, 0))"

definition f :: "nat \<Rightarrow> int \<Rightarrow> x" where
"f n i = x_corec p11 p12 q12 g12 h12 p13 q131 g131 h131 q132 g132 h132 g141 g142 p21
   p22 q22 g22 h22 (n, i)"

definition g :: "nat list \<Rightarrow> y" where
"g ns = y_corec p11 p12 q12 g12 h12 p13 q131 g131 h131 q132 g132 h132 g141 g142 p21
   p22 q22 g22 h22 ns"

lemma
"f n i =
   (if n = 0 then
      C1
    else if n = 1 then
      C2 (if i = 0 then C1 else f 0 0)
    else if i = 2 then
      C3 (if i = 1 then C1 else f n i) (if n = 2 then D1 else g [])
    else
      C4 n i)"
unfolding f_def
apply (rule iffD2[OF split_if_eq2])
apply (rule conjI)
 apply (rule impI)
 apply (rule x.corec(1))
 apply (simp only: p11_def split)
apply (rule impI)
apply (rule iffD2[OF split_if_eq2])
apply (rule conjI)
 apply (rule impI)
 apply (rule trans[OF x.corec(2)])
   apply (simp add: p11_def split)
  apply (simp add: p12_def split)
 apply (simp only: q12_def g12_def h12_def split)
apply (rule impI)
apply (rule iffD2[OF split_if_eq2])
apply (rule conjI)
 apply (rule impI)
 apply (rule trans[OF x.corec(3)])
    apply (simp add: p11_def split)
   apply (simp add: p12_def split)
  apply (simp add: p13_def split)
 apply (simp only: q131_def g131_def h131_def q132_def g132_def h132_def g_def split)
apply (rule impI)
apply (rule trans[OF x.corec(4)])
   apply (simp add: p11_def split)
  apply (simp add: p12_def split)
 apply (simp add: p13_def split)
apply (simp only: g141_def g142_def split)
done

lemma
"g ns =
  (if ns = [] then
     D1
   else if length ns = 1 then
     D2 (if ns = [0] then C1 else f 0 0)
   else
     D3)"
unfolding g_def
apply (rule iffD2[OF split_if_eq2])
apply (rule conjI)
 apply (rule impI)
 apply (rule y.corec(1))
 apply (simp only: p21_def split)
apply (rule impI)
apply (rule iffD2[OF split_if_eq2])
apply (rule conjI)
 apply (rule impI)
 apply (rule trans[OF y.corec(2)])
   apply (simp add: p21_def split)
  apply (simp add: p22_def split)
 apply (simp only: q22_def g22_def h22_def f_def split)
apply (rule impI)
apply (rule trans[OF y.corec(3)])
  apply (simp add: p21_def split)
 apply (simp add: p22_def split)
apply (simp only: split)
done

end
