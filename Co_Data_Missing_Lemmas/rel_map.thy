theory rel_map
imports "~~/src/HOL/BNF_LFP"
begin

declare [[bnf_note_all]]

datatype_new 'a llist
  = LNil
  | LCons (lhead: 'a) (ltail: "'a llist")
  where
    "ltail LNil = LNil"

datatype_new 'a maybe
  = Nothing
  | Just (just: 'a)

datatype_new 'a ltree
  = LNode (lval: 'a) (lchildren: "'a ltree llist")

datatype_new ('a, 'b) example
  = N1 'a
  | N2 'b
  | N3 'a 'b
  | N4
  | ConsA 'a "('a, 'b) example"
  | ConsB 'b "('a, 'b) example"

datatype_new 'a tree = Node 'a "'a forest"
       and 'a forest = FNil | FCons "'a tree" "'a forest"

datatype_new 'a A = A1 'a | A2 'a
datatype_new ('a, 'b) B = B1 'a | B2 'b
datatype_new ('a, 'b) C = C1 "'a A" | C2 "('a, 'b) B"
datatype_new 'a D1 = D1 "('a, unit) B A"
datatype_new 'a D2 = D2 "('a * unit) A"
datatype_new 'a D3 = D3 "('a, (unit, nat) B) B A"
datatype_new 'a D4 = D4 "('a, ('a, nat) B) B A"
datatype_new 'a D5 = D5 "('a, ('a D5, nat) B) B A"
datatype_new 'a D6 = D6 "('a, ('a, ('a, ('a, ('a, nat) B) B) B) B) B"

datatype_new ('a, 'b, 'c, 'd) E = E "'a * ('b, ('c, ('d * nat)) C A) B"

datatype_new ('a, 'b) F = F "'a \<Rightarrow> 'b"

ML {*
open BNF_Util
open BNF_Tactics

fun mk_rel_map_map_tac live rel_compp rel_conversep rel_Grp map_id
  {context = ctxt, prems = _} =
  let val ks = 1 upto live;
  in
    Goal.conjunction_tac 1 THEN
    unfold_thms_tac ctxt [rel_compp, rel_conversep, rel_Grp, @{thm vimage2p_Grp}] THEN
    TRYALL (EVERY' [rtac iffI, rtac @{thm relcomppI}, rtac @{thm GrpI},
      resolve_tac [map_id, refl], rtac CollectI,
      CONJ_WRAP' (K (rtac @{thm subset_UNIV})) ks, rtac @{thm relcomppI}, atac,
      rtac @{thm conversepI}, rtac @{thm GrpI}, resolve_tac [map_id, refl], rtac CollectI,
      CONJ_WRAP' (K (rtac @{thm subset_UNIV})) ks,
      REPEAT_DETERM o eresolve_tac @{thms relcomppE conversepE GrpE},
      dtac (trans OF [sym, map_id]), hyp_subst_tac ctxt, atac])
  end;
*}

thm llist.rel_map
thm llist.rel_mono_strong
lemma llist_rel_map0:
  "rel_llist P (map_llist f a) b \<longleftrightarrow> rel_llist (vimage2p f id P) a b"
  "rel_llist Q a (map_llist g b) \<longleftrightarrow> rel_llist (vimage2p id g Q) a b"
  apply (tactic {* mk_rel_map_map_tac 1
    @{thm llist.rel_compp}
    @{thm llist.rel_conversep}
    @{thm llist.rel_Grp}
    @{thm llist.map_id}
    {context = @{context}, prems = []}*})
done
theorems ll_rel_map = llist_rel_map0[unfolded vimage2p_def id_apply]

thm maybe.rel_map
lemma maybe_rel_map0:
  "rel_maybe P (map_maybe f a) b \<longleftrightarrow> rel_maybe (vimage2p f id P) a b"
  "rel_maybe Q a (map_maybe g b) \<longleftrightarrow> rel_maybe (vimage2p id g Q) a b"
  apply (tactic {* mk_rel_map_map_tac 1
    @{thm maybe.rel_compp}
    @{thm maybe.rel_conversep}
    @{thm maybe.rel_Grp}
    @{thm maybe.map_id}
    {context = @{context}, prems = []}*})
done

thm ltree.rel_map
lemma ltree_rel_map0:
  "rel_ltree P (map_ltree f a) b \<longleftrightarrow> rel_ltree (vimage2p f id P) a b"
  "rel_ltree Q a (map_ltree g b) \<longleftrightarrow> rel_ltree (vimage2p id g Q) a b"
  apply (tactic {* mk_rel_map_map_tac 1
    @{thm ltree.rel_compp}
    @{thm ltree.rel_conversep}
    @{thm ltree.rel_Grp}
    @{thm ltree.map_id}
    {context = @{context}, prems = []}*})
done

thm example.rel_map
lemma example_rel_map0:
  "rel_example P1 P2 (map_example f1 f2 a) b \<longleftrightarrow> rel_example (vimage2p f1 id P1) (vimage2p f2 id P2) a b"
  "rel_example Q1 Q2 a (map_example g1 g2 b) \<longleftrightarrow> rel_example (vimage2p id g1 Q1) (vimage2p id g2 Q2) a b"
  (* apply (unfold example.rel_compp example.rel_conversep example.rel_Grp vimage2p_Grp)

  apply (rule iffI)
  apply (rule relcomppI)
  apply (rule GrpI)
  apply (rule refl)
  apply (rule CollectI)
  apply (rule conjI)
  apply (rule subset_UNIV)
  apply (rule subset_UNIV)
  apply (rule relcomppI)
  apply (assumption)
  apply (rule conversepI)
  apply (rule GrpI)
  apply (rule example.map_id)
  apply (rule CollectI)
  apply (rule conjI)
  apply (rule subset_UNIV)
  apply (rule subset_UNIV)
  apply (tactic {* REPEAT_DETERM (HEADGOAL (eresolve_tac @{thms relcomppE conversepE GrpE})) *})
  apply (drule trans[OF sym example.map_id])
  apply hypsubst
  apply assumption

  apply (rule iffI)
  apply (rule relcomppI)
  apply (rule GrpI)
  apply (rule example.map_id)
  apply (rule CollectI)
  apply (rule conjI)
  apply (rule subset_UNIV)
  apply (rule subset_UNIV)
  apply (rule relcomppI)
  apply (assumption)
  apply (rule conversepI)
  apply (rule GrpI)
  apply (rule refl)
  apply (rule CollectI)
  apply (rule conjI)
  apply (rule subset_UNIV)
  apply (rule subset_UNIV)
  apply (tactic {* REPEAT_DETERM (HEADGOAL (eresolve_tac @{thms relcomppE conversepE GrpE})) *})
  apply (drule trans[OF sym example.map_id])
  apply hypsubst
  apply assumption *)

  apply (tactic {* mk_rel_map_map_tac 2
    @{thm example.rel_compp}
    @{thm example.rel_conversep}
    @{thm example.rel_Grp}
    @{thm example.map_id}
    {context = @{context}, prems = []}*})
done

thm example.rel_compp example.rel_conversep example.rel_Grp example.map_id
thm vimage2p_Grp

term "op OO"

thm tree.rel_map
lemma tree_rel_map0:
  "rel_tree P (map_tree f a) b \<longleftrightarrow> rel_tree (vimage2p f id P) a b"
  "rel_tree Q a (map_tree g b) \<longleftrightarrow> rel_tree (vimage2p id g Q) a b"
  apply (tactic {* mk_rel_map_map_tac 1
    @{thm tree.rel_compp}
    @{thm tree.rel_conversep}
    @{thm tree.rel_Grp}
    @{thm tree.map_id}
    {context = @{context}, prems = []}*})
done

thm forest.rel_map
lemma forest_rel_map0:
  "rel_forest P (map_forest f a) b \<longleftrightarrow> rel_forest (vimage2p f id P) a b"
  "rel_forest Q a (map_forest g b) \<longleftrightarrow> rel_forest (vimage2p id g Q) a b"
  apply (tactic {* mk_rel_map_map_tac 1
    @{thm forest.rel_compp}
    @{thm forest.rel_conversep}
    @{thm forest.rel_Grp}
    @{thm forest.map_id}
    {context = @{context}, prems = []}*})
done

thm A.rel_map
lemma A_rel_map0:
  "rel_A P (map_A f a) b \<longleftrightarrow> rel_A (vimage2p f id P) a b"
  "rel_A Q a (map_A g b) \<longleftrightarrow> rel_A (vimage2p id g Q) a b"
  apply (tactic {* mk_rel_map_map_tac 1
    @{thm A.rel_compp}
    @{thm A.rel_conversep}
    @{thm A.rel_Grp}
    @{thm A.map_id}
    {context = @{context}, prems = []}*})
done

thm B.rel_map
lemma B_rel_map0:
  "rel_B P1 P2 (map_B f1 f2 a) b \<longleftrightarrow> rel_B (vimage2p f1 id P1) (vimage2p f2 id P2) a b"
  "rel_B Q1 Q2 a (map_B g1 g2 b) \<longleftrightarrow> rel_B (vimage2p id g1 Q1) (vimage2p id g2 Q2) a b"
  apply (tactic {* mk_rel_map_map_tac 2
    @{thm B.rel_compp}
    @{thm B.rel_conversep}
    @{thm B.rel_Grp}
    @{thm B.map_id}
    {context = @{context}, prems = []}*})
done

thm C.rel_map
lemma C_rel_map0:
  "rel_C P1 P2 (map_C f1 f2 a) b \<longleftrightarrow> rel_C (vimage2p f1 id P1) (vimage2p f2 id P2) a b"
  "rel_C Q1 Q2 a (map_C g1 g2 b) \<longleftrightarrow> rel_C (vimage2p id g1 Q1) (vimage2p id g2 Q2) a b"
  apply (tactic {* mk_rel_map_map_tac 2
    @{thm C.rel_compp}
    @{thm C.rel_conversep}
    @{thm C.rel_Grp}
    @{thm C.map_id}
    {context = @{context}, prems = []}*})
done

thm D1.rel_map
lemma D1_rel_map0:
  "rel_D1 P (map_D1 f a) b \<longleftrightarrow> rel_D1 (vimage2p f id P) a b"
  "rel_D1 Q a (map_D1 g b) \<longleftrightarrow> rel_D1 (vimage2p id g Q) a b"
  apply (tactic {* mk_rel_map_map_tac 1
    @{thm D1.rel_compp}
    @{thm D1.rel_conversep}
    @{thm D1.rel_Grp}
    @{thm D1.map_id}
    {context = @{context}, prems = []}*})
done

thm D2.rel_map
lemma D2_rel_map0:
  "rel_D2 P (map_D2 f a) b \<longleftrightarrow> rel_D2 (vimage2p f id P) a b"
  "rel_D2 Q a (map_D2 g b) \<longleftrightarrow> rel_D2 (vimage2p id g Q) a b"
  apply (tactic {* mk_rel_map_map_tac 1
    @{thm D2.rel_compp}
    @{thm D2.rel_conversep}
    @{thm D2.rel_Grp}
    @{thm D2.map_id}
    {context = @{context}, prems = []}*})
done

thm D3.rel_map
lemma D3_rel_map0:
  "rel_D3 P (map_D3 f a) b \<longleftrightarrow> rel_D3 (vimage2p f id P) a b"
  "rel_D3 Q a (map_D3 g b) \<longleftrightarrow> rel_D3 (vimage2p id g Q) a b"
  apply (tactic {* mk_rel_map_map_tac 1
    @{thm D3.rel_compp}
    @{thm D3.rel_conversep}
    @{thm D3.rel_Grp}
    @{thm D3.map_id}
    {context = @{context}, prems = []}*})
done

thm D4.rel_map
lemma D4_rel_map0:
  "rel_D4 P (map_D4 f a) b \<longleftrightarrow> rel_D4 (vimage2p f id P) a b"
  "rel_D4 Q a (map_D4 g b) \<longleftrightarrow> rel_D4 (vimage2p id g Q) a b"
  apply (tactic {* mk_rel_map_map_tac 1
    @{thm D4.rel_compp}
    @{thm D4.rel_conversep}
    @{thm D4.rel_Grp}
    @{thm D4.map_id}
    {context = @{context}, prems = []}*})
done

thm D5.rel_map
lemma D5_rel_map0:
  "rel_D5 P (map_D5 f a) b \<longleftrightarrow> rel_D5 (vimage2p f id P) a b"
  "rel_D5 Q a (map_D5 g b) \<longleftrightarrow> rel_D5 (vimage2p id g Q) a b"
  apply (tactic {* mk_rel_map_map_tac 1
    @{thm D5.rel_compp}
    @{thm D5.rel_conversep}
    @{thm D5.rel_Grp}
    @{thm D5.map_id}
    {context = @{context}, prems = []}*})
done

thm D6.rel_map
lemma D6_rel_map0:
  "rel_D6 P (map_D6 f a) b \<longleftrightarrow> rel_D6 (vimage2p f id P) a b"
  "rel_D6 Q a (map_D6 g b) \<longleftrightarrow> rel_D6 (vimage2p id g Q) a b"
  apply (tactic {* mk_rel_map_map_tac 1
    @{thm D6.rel_compp}
    @{thm D6.rel_conversep}
    @{thm D6.rel_Grp}
    @{thm D6.map_id}
    {context = @{context}, prems = []}*})
done

thm E.rel_map
lemma E_rel_map0:
  "rel_E P1 P2 P3 P4 (map_E f1 f2 f3 f4 a) b \<longleftrightarrow> rel_E (vimage2p f1 id P1) (vimage2p f2 id P2) (vimage2p f3 id P3) (vimage2p f4 id P4) a b"
  "rel_E Q1 Q2 Q3 Q4 a (map_E g1 g2 g3 g4 b) \<longleftrightarrow> rel_E (vimage2p id g1 Q1) (vimage2p id g2 Q2) (vimage2p id g3 Q3) (vimage2p id g4 Q4) a b"
  apply (tactic {* mk_rel_map_map_tac 4
    @{thm E.rel_compp}
    @{thm E.rel_conversep}
    @{thm E.rel_Grp}
    @{thm E.map_id}
    {context = @{context}, prems = []}*})
done

thm F.rel_map
lemma F_rel_map0:
  "rel_F P (map_F f a) b \<longleftrightarrow> rel_F (vimage2p f id P) a b"
  "rel_F Q a (map_F g b) \<longleftrightarrow> rel_F (vimage2p id g Q) a b"
  apply (tactic {* mk_rel_map_map_tac 1
    @{thm F.rel_compp}
    @{thm F.rel_conversep}
    @{thm F.rel_Grp}
    @{thm F.map_id}
    {context = @{context}, prems = []}*})
done

end
