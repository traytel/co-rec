theory Bug_Dmi
imports "~~/src/HOL/BNF/BNF"
begin

typedecl ('a, 'b, 'c) G0
consts
  G0_map :: "('a1 \<Rightarrow> 'a2) \<Rightarrow> ('b1 \<Rightarrow> 'b2) \<Rightarrow> ('c1 \<Rightarrow> 'c2) \<Rightarrow> ('a1, 'b1, 'c1) G0 \<Rightarrow> ('a2, 'b2, 'c2) G0"
  G0_set1 :: "('a, 'b, 'c) G0 \<Rightarrow> 'a set"
  G0_set2 :: "('a, 'b, 'c) G0 \<Rightarrow> 'b set"
  G0_set3 :: "('a, 'b, 'c) G0 \<Rightarrow> 'c set"
  G0_wit :: "('a, 'b, 'c) G0"

declare [[bnf_note_all]]

bnf G0_map [G0_set1, G0_set2, G0_set3] "\<lambda>_::('a, 'b, 'c) G0. natLeq" [G0_wit] sorry

datatype_new 'a N = CN "'a list"
datatype_new ('a, 'b) K = CK "('a * ('a, 'b) K) list"
datatype_new 'a G = CG "('a, ('a G, 'a G N) K, ('a G N, 'a G) K) G0"
