theory case_transfer
imports "~~/src/HOL/Transfer"
begin

context lifting_syntax (* enables the ===> syntax for rel_fun *)
begin

declare [[bnf_note_all]]

datatype 'a list
  = Nil
  | Cons (head: 'a) (tail: "'a list")
  where
    "tail Nil = Nil"

datatype 'a option
  = None
  | Some (the: 'a)

datatype 'a tree
  = Node (val: 'a) (children: "'a tree list")

datatype ('a, 'b) example
  = N1 'a
  | N2 'b
  | N3 'a 'b
  | N4
  | ConsA 'a "('a, 'b) example"
  | ConsB 'b 'b 'b 'b "('a, 'b) example"

datatype 'a tree_mutual = TNode 'a "'a forest_mutual"
       and 'a forest_mutual = FNil | FCons "'a tree_mutual" "'a forest_mutual"

datatype ('a, 'b) F1 = F1 "'a \<Rightarrow> 'b"

datatype ('a, 'b, 'c) F2 = F2 "'a \<Rightarrow> 'b \<Rightarrow> 'c"

datatype 'a A1 = A1 nat
         and 'a A2 = A2 'a

thm list.rel_induct

ML {*
open Ctr_Sugar_General_Tactics

fun mk_case_transfer_tac ctxt rel_cases cases =
  let
    val n = length (tl (prems_of rel_cases));
  in
    REPEAT_DETERM (HEADGOAL (rtac @{thm rel_funI})) THEN
    HEADGOAL (etac rel_cases) THEN
    ALLGOALS (hyp_subst_tac ctxt) THEN
    unfold_thms_tac ctxt cases THEN
    ALLGOALS (fn k => (select_prem_tac n (dtac asm_rl) k) k) THEN
    TRYALL (REPEAT_DETERM o (rotate_tac ~1 THEN' dtac @{thm rel_funD} THEN' (atac THEN' etac thin_rl ORELSE' rtac refl)) THEN' atac)
  end;
*}

term case_list
thm list.case_transfer
lemma list_case_transfer:
  "(R1 ===> (R2 ===> rel_list R2 ===> R1) ===> rel_list R2 ===> R1) case_list case_list"
  apply (tactic {* mk_case_transfer_tac @{context} @{thm list.rel_cases} @{thms list.case} *})
(*   apply (rule rel_funI)+
  (* apply (rotate_tac -1) *)
  apply (erule list.rel_cases)
  apply (tactic {* ALLGOALS (hyp_subst_tac @{context}) *})
  unfolding list.case
  apply (tactic {* ALLGOALS (fn k => (select_prem_tac 2 (dtac asm_rl) k) k) *})
apply (((rotate_tac -1 , drule rel_funD, assumption, erule thin_rl)+)? , assumption)+ *)

done

term case_option
thm option.case_transfer
lemma option_case_transfer:
  "(R ===>
    (S ===> R) ===>
    rel_option S ===>
    R) case_option case_option"
  apply (tactic {* mk_case_transfer_tac @{context} @{thm option.rel_cases} @{thms option.case} *})
(*   apply (rule rel_funI)+
  apply (erule option.rel_cases)
  apply (tactic {* ALLGOALS (hyp_subst_tac @{context}) *})
  unfolding option.case
  apply (tactic {* ALLGOALS (fn k => (select_prem_tac 2 (dtac asm_rl) k) k) *})
apply (((rotate_tac -1 , drule rel_funD, assumption, erule thin_rl)+)? , assumption)+ *)
done

term case_tree
thm tree.case_transfer
lemma tree_case_transfer:
  "((R1 ===> rel_list (rel_tree R1) ===> R2) ===>
    rel_tree R1 ===>
    R2) case_tree case_tree"
  apply (tactic {* mk_case_transfer_tac @{context} @{thm tree.rel_cases} @{thms tree.case} *})
(*   apply (rule rel_funI)+
  apply (erule tree.rel_cases)
  apply (tactic {* ALLGOALS (hyp_subst_tac @{context}) *})
  unfolding tree.case
  apply (tactic {* ALLGOALS (fn k => (select_prem_tac 1 (dtac asm_rl) k) k) *})
  apply (((rotate_tac -1 , drule rel_funD, assumption, erule thin_rl)+)? , assumption)+ *)
done

term case_example
thm example.case_transfer
lemma example_case_transfer:
  "((R1 ===> R2) ===>
    (R3 ===> R2) ===>
    (R1 ===> R3 ===> R2) ===>
    R2 ===>
    (R1 ===> rel_example R1 R3 ===> R2) ===>
    (R3 ===> R3 ===> R3 ===> R3 ===> rel_example R1 R3 ===> R2) ===>
    rel_example R1 R3 ===>
    R2) case_example case_example"
  apply (tactic {* mk_case_transfer_tac @{context} @{thm example.rel_cases} @{thms example.case} *})
(*   apply (rule rel_funI)+
  apply (erule example.rel_cases)
  apply (tactic {* ALLGOALS (hyp_subst_tac @{context}) *})
  apply (unfold example.case)
  apply (tactic {* ALLGOALS (fn k => (select_prem_tac 6 (dtac asm_rl) k) k) *})
  apply (((rotate_tac -1 , drule rel_funD, assumption, erule thin_rl)+)? , assumption)+ *)
done

lemma "PROP V \<Longrightarrow> PROP W \<Longrightarrow> PROP V"
  by simp

term case_tree_mutual
thm tree_mutual.case_transfer
lemma tree_mutual_case_transfer:
  "((R1 ===> rel_forest_mutual R1 ===> R2) ===>
    rel_tree_mutual R1 ===>
    R2) case_tree_mutual case_tree_mutual"
  apply (tactic {* mk_case_transfer_tac @{context} @{thm tree_mutual.rel_cases} @{thms tree_mutual.case} *})
(*   apply (rule rel_funI)+
  apply (erule tree_mutual.rel_cases)
  apply (tactic {* ALLGOALS (hyp_subst_tac @{context}) *})
  apply (unfold tree_mutual.case)
  apply (tactic {* ALLGOALS (fn k => (select_prem_tac 1 (dtac asm_rl) k THEN' rotate_tac (~1)) k) *})
  apply (tactic {* (TRY o REPEAT_ALL_NEW (dtac @{thm rel_funD}) THEN_ALL_NEW atac) 1 *})+ *)
done

term case_forest_mutual
thm forest_mutual.case_transfer
lemma forest_mutual_case_transfer:
  "(R1 ===>
    (rel_tree_mutual R2 ===> rel_forest_mutual R2 ===> R1) ===>
    rel_forest_mutual R2 ===>
    R1) case_forest_mutual case_forest_mutual"
  apply (tactic {* mk_case_transfer_tac @{context} @{thm forest_mutual.rel_cases} @{thms forest_mutual.case} *})
(*   apply (rule rel_funI)+
  apply (erule forest_mutual.rel_cases)
  apply (tactic {* ALLGOALS (hyp_subst_tac @{context}) *})
  apply (unfold forest_mutual.case)
  apply (tactic {* ALLGOALS (fn k => (select_prem_tac 2 (dtac asm_rl) k THEN' rotate_tac (~1)) k) *})
  apply (tactic {* (TRY o REPEAT_ALL_NEW (dtac @{thm rel_funD}) THEN_ALL_NEW atac) 1 *})+ *)
done

term case_F1
thm F1.case_transfer
lemma F1_case_transfer:
  "(((op = ===> R1) ===> R2) ===>
    rel_F1 R1 ===>
    R2) case_F1 case_F1"
  apply (tactic {* mk_case_transfer_tac @{context} @{thm F1.rel_cases} @{thms F1.case} *})
(*   apply (rule rel_funI)+
  apply (erule F1.rel_cases)
  apply (tactic {* ALLGOALS (hyp_subst_tac @{context}) *})
  apply (unfold F1.case)
  apply (tactic {* ALLGOALS (fn k => (select_prem_tac 1 (dtac asm_rl) k THEN' rotate_tac (~1)) k) *})
  apply (tactic {* (TRY o REPEAT_ALL_NEW (atac ORELSE' dtac @{thm rel_funD})) 1 *}) *)
done

term case_F2
thm F2.case_transfer
lemma F2_case_transfer:
  "(((op = ===> op = ===> R1) ===> R2) ===>
    rel_F2 R1 ===>
    R2) case_F2 case_F2"
  apply (tactic {* mk_case_transfer_tac @{context} @{thm F2.rel_cases} @{thms F2.case} *})
(*   apply (rule rel_funI)+
  apply (erule F2.rel_cases)
  apply (tactic {* ALLGOALS (hyp_subst_tac @{context}) *})
  apply (unfold F2.case)
  apply (tactic {* ALLGOALS (fn k => (select_prem_tac 1 (dtac asm_rl) k THEN' rotate_tac (~1)) k) *})
  apply (tactic {* (TRY o REPEAT_ALL_NEW (atac ORELSE' dtac @{thm rel_funD})) 1 *}) *)
done



term case_A1
thm A1.case_transfer
lemma A1_case_transfer:
  "((op = ===> R1) ===> rel_A1 R2 ===> R1) case_A1 case_A1"
  apply (tactic {* mk_case_transfer_tac @{context} @{thm A1.rel_cases} @{thms A1.case} *})
(*   apply (rule rel_funI)+
  apply (erule A1.rel_cases)
  apply (tactic {* ALLGOALS (hyp_subst_tac @{context}) *})
  apply (unfold A1.case)
  apply (tactic {* ALLGOALS (fn k => (select_prem_tac 1 (dtac asm_rl) k) k) *})
  apply (((rotate_tac -1 , drule rel_funD, (assumption, erule thin_rl | rule refl))+)? , assumption)+ *)
done

end

end
