theory Rec_thru_Fun
imports
  "~~/src/HOL/BNF/BNF_LFP"
  "~~/src/HOL/BNF/More_BNFs"
  "~~/src/HOL/BNF/Examples/Stream"
begin

datatype_new ('a, 'b, 'c) SP = GET "'a \<Rightarrow> ('a, 'b, 'c) SP" | PUT "'b" "'c"
datatype_new_compat SP
consts OUT :: "'c \<Rightarrow> ('a, 'b, 'c) SP"

primrec_new run\<^sub>\<mu> :: "('a, 'b, 'c) SP \<Rightarrow> 'a stream \<Rightarrow> ('b \<times> 'c) \<times> 'a stream" where
  "run\<^sub>\<mu> (GET f) s = (run\<^sub>\<mu> o f) (shd s) (stl s)"
| "run\<^sub>\<mu> (PUT b sp) s = ((b, sp), s)"

(*would work if SP were an old-style datatype and we used old primrec *)
primrec_new run :: "('a, 'b, 'c) SP \<Rightarrow> 'a stream \<Rightarrow> ('b \<times> 'c) \<times> 'a stream" where
  "run (GET f) s = run (f (shd s)) (stl s)"
| "run (PUT b sp) s = ((b, sp), s)"







datatype_new 'a l1 = N1 | C1 "'a \<Rightarrow> 'a l1"

primrec_new r100 where
"r100 N1 = (0::nat)" |
"r100 (C1 f) = r100 (f 0)"

primrec_new r101 where
"r101 N1 (n::nat) = (0::nat)" |
"r101 (C1 f) n = r101 (f 0) n"

primrec_new r110 where
"r110 (n::nat) N1 = (0::nat)" |
"r110 n (C1 f) = r110 n (f 0)"


datatype_new 'a l2 = N2 | C2 "'a \<Rightarrow> 'a \<Rightarrow> 'a l2"

primrec_new r200 where
"r200 N2 = (0::nat)" |
"r200 (C2 f) = r200 (f 0 0)"

primrec_new r201 where
"r201 N2 (n::nat) = (0::nat)" |
"r201 (C2 f) n = r201 (f 0 0) n"

primrec_new r210 where
"r210 (n::nat) N2 = (0::nat)" |
"r210 n (C2 f) = r210 n (f 0 0)"


datatype_new 'a ll1 = NN1 | CC1 "'a \<Rightarrow> 'a ll1 list"

primrec_new rr100 :: "nat ll1 \<Rightarrow> nat" where
"rr100 NN1 = (0::nat)" |
"rr100 (CC1 f) = hd (map rr100 (f (0::nat)))"

primrec_new rr100' :: "nat ll1 \<Rightarrow> nat" where
"rr100' NN1 = (0::nat)" |
"rr100' (CC1 f) = hd (id (%x. map rr100' (f (x::nat))) 0)"

primrec_new rr101 :: "nat ll1 \<Rightarrow> nat \<Rightarrow> nat" where
"rr101 NN1 n = (0::nat)" |
"rr101 (CC1 f) n = hd (map (%l. rr101 l n) (f (0::nat)))"

primrec_new rr101' :: "nat ll1 \<Rightarrow> nat \<Rightarrow> nat" where
"rr101' NN1 n = (0::nat)" |
"rr101' (CC1 f) n = hd (id (%x. map (%l. rr101' l n) (f (x::nat))) 0)"

primrec_new rr110 :: "nat \<Rightarrow> nat ll1 \<Rightarrow> nat" where
"rr110 n NN1 = (0::nat)" |
"rr110 n (CC1 f) = hd (map (rr110 n) (f (0::nat)))"

primrec_new rr110' :: "nat \<Rightarrow> nat ll1 \<Rightarrow> nat" where
"rr110' n NN1 = (0::nat)" |
"rr110' n (CC1 f) = hd (id (%x. map (rr110' n) (f (x::nat))) 0)"


datatype_new 'a ll2 = NN2 | CC2 "'a \<Rightarrow> 'a \<Rightarrow> 'a ll2 list"

primrec_new rr200 :: "nat ll2 \<Rightarrow> nat" where
"rr200 NN2 = (0::nat)" |
"rr200 (CC2 f) = hd (map rr200 (f (0::nat) (0::nat)))"

primrec_new rr201 :: "nat ll2 \<Rightarrow> nat \<Rightarrow> nat" where
"rr201 NN2 n = (0::nat)" |
"rr201 (CC2 f) n = hd (map (%l. rr201 l n) (f (0::nat) (0::nat)))"

primrec_new rr210 :: "nat \<Rightarrow> nat ll2 \<Rightarrow> nat" where
"rr210 n NN2 = (0::nat)" |
"rr210 n (CC2 f) = hd (map (rr210 n) (f (0::nat) (0::nat)))"

end
