theory Record_Sugar
imports Main
begin

record point =
  xpos :: int
  ypos :: int

setup {*
Ctr_Sugar.default_register_ctr_sugar_global @{type_name point_ext}
  {ctrs = [@{term point_ext}],
   casex = Term.dummy,
   discs = [],
   selss = [[@{term xpos}, @{term ypos}]],
   exhaust = @{thm point.cases_scheme},
   nchotomy = refl,
   injects = [@{thm point.ext_inject}],
   distincts = [],
   case_thms = [],
   case_cong = refl,
   weak_case_cong = refl,
   split = refl,
   split_asm = refl,
   disc_defs = [],
   disc_thmss = [],
   discIs = [],
   sel_defs = [@{thm xpos_def}, @{thm ypos_def}],
   sel_thmss = [@{thms point.select_convs}],
   disc_excludesss = [],
   disc_exhausts = [],
   sel_exhausts = [],
   collapses = [],
   expands = [],
   sel_splits = [],
   sel_split_asms = [],
   case_eq_ifs = []}
*}

free_constructors point_ext for
  point_ext
 apply atomize_elim
 apply (induct_tac y rule: point.ext_induct)
 apply (intro exI)
 apply (rule refl)
apply (rule point.ext_inject)
done

print_theorems

fun dest_point where
  "dest_point \<lparr>xpos = x, ypos = y\<rparr> = (x, y)"

fun dest_point_scheme where
  "dest_point_scheme \<lparr>xpos = x, ypos = y, \<dots> = m\<rparr> = (x, y, m)"

term "case p of \<lparr>xpos = x, ypos = y\<rparr> \<Rightarrow> (x, y)"
term "case p of \<lparr>xpos = x, ypos = y, \<dots> = m\<rparr> \<Rightarrow> (x, y, m)"


datatype colour = Red | Green | Blue

record cpoint = point +
  col :: colour

setup {*
Ctr_Sugar.default_register_ctr_sugar_global @{type_name cpoint_ext}
  {ctrs = [@{term cpoint_ext}],
   casex = Term.dummy,
   discs = [],
   selss = [[@{term col}]],
   exhaust = @{thm cpoint.cases_scheme},
   nchotomy = refl,
   injects = [@{thm cpoint.ext_inject}],
   distincts = [],
   case_thms = [],
   case_cong = refl,
   weak_case_cong = refl,
   split = refl,
   split_asm = refl,
   disc_defs = [],
   disc_thmss = [],
   discIs = [],
   sel_defs = [@{thm col_def}],
   sel_thmss = [@{thms cpoint.select_convs}],
   disc_excludesss = [],
   disc_exhausts = [],
   sel_exhausts = [],
   collapses = [],
   expands = [],
   sel_splits = [],
   sel_split_asms = [],
   case_eq_ifs = []}
*}

free_constructors cpoint_ext for
  cpoint_ext
 apply atomize_elim
 apply (induct_tac y rule: cpoint.ext_induct)
 apply (intro exI)
 apply (rule refl)
apply (rule cpoint.ext_inject)
done

fun dest_cpoint where
  "dest_cpoint \<lparr>xpos = x, ypos = y, col = c\<rparr> = (x, y, c)"

fun dest_cpoint_scheme where
  "dest_cpoint_scheme \<lparr>xpos = x, ypos = y, col = c, \<dots> = m\<rparr> = (x, y, c, m)"

term "case p of \<lparr>xpos = x, ypos = y, col = c\<rparr> \<Rightarrow> (x, y, c)"
term "case p of \<lparr>xpos = x, ypos = y, col = c, \<dots> = m\<rparr> \<Rightarrow> (x, y, c, m)"

end
