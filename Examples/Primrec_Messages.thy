theory Primrec_Messages
imports "~~/src/HOL/Main"
begin

datatype_new nt = Z | S nt
datatype_new bt = X | N bt bt
datatype_new rt = T nt "rt list"
datatype_new rtt = TT nt "rtt list list"
datatype_new xx = XX and yy = YY

consts Z' :: nt
       mymap :: "('a \<Rightarrow> 'b) \<Rightarrow> 'a list \<Rightarrow> 'b list"
       u' :: rt


text {* Good *}

primrec f1 :: nt where
"f1 = S Z"

primrec f2 :: "nt \<Rightarrow> nt \<Rightarrow> nt" where
"f2 Z x = x" |
"f2 x (S y) = y"

primrec f3 :: "nt \<Rightarrow> nt \<Rightarrow> nt \<Rightarrow> nt \<Rightarrow> nt" where
"f3 x Z x x = x" |
"f3 x (S x) x x = x"

primrec f4 :: "bt \<Rightarrow> bt" where
"f4 (N t t) = t"

primrec f5 :: "(bt \<Rightarrow> bt) \<Rightarrow> bt" where
"f5 (N t) = t"

primrec f6 :: "bt \<Rightarrow> bt" where
"f6 X = X" |
"f6 X = X"

primrec f7 :: "bt \<Rightarrow> bt" where
"f7 (N (N t u) v) = u"

primrec f8 :: "nt \<Rightarrow> nt \<Rightarrow> nt" where
"f8 (S m) (S n) = m"

primrec f9 :: "nt \<Rightarrow> nt" where
"f9 Z = n"

primrec f10 :: "rt \<Rightarrow> rt" where
"f10 (T l ts) = T (S l) (mymap f10 ts)"

primrec f11 :: "rt \<Rightarrow> rt" where
"f11 (T l ts) = T (S l) (map (f11 o id) ts)"

primrec f12 :: "rt \<Rightarrow> rt" where
"f12 (T l ts) = T (S l) (map (\<lambda>t. f12 u') ts)"

primrec f13 :: "nt \<Rightarrow> nt" where
"f13 Z \<equiv> Z"

primrec f14 :: "nt \<Rightarrow> nt" where
"f14 Z = Z \<Longrightarrow> Z = Z"

primrec f15 :: "nt \<Rightarrow> nt" where
"f15 Z' = Z"

primrec f16 :: "int \<Rightarrow> nt" where
"f16 0 = Z"

primrec f17 :: "nt \<Rightarrow> nt \<Rightarrow> nt" where
"f17 m (S n) = (let j = f17 n in m)"

primrec f18 :: "rt \<Rightarrow> rt \<Rightarrow> rt" where
"f18 s (T _ ts) = (let j = map f18 ts in s)"

primrec f18' :: "rt \<Rightarrow> rt \<Rightarrow> rt \<Rightarrow> rt" where
"f18' r s (T _ ts) = (let j = map (f18' r) ts in s)"

primrec f19 :: "rtt \<Rightarrow> rtt" where
"f19 (TT l tss) = TT (S l) (map (mymap f19) tss)"

primrec f20 :: "rtt \<Rightarrow> rtt" where
"f20 (TT l tss) = TT (S l) (mymap (map f20) tss)"

primrec f21 :: "rt \<Rightarrow> rt" where
"f21 (T l ts) = T (S l) [f21 (hd ts)]"

primrec f22 :: "rt \<Rightarrow> rt" where
"f22 (T l ts) = T (S l) (map f22 [hd ts])"

primrec f22' :: "rt \<Rightarrow> rt" where
"f22' (T l ts) = T (S l) (map f22' [])"

primrec f23 :: "nt \<Rightarrow> nt" where
"f23 (S n) = S (f23 (S n))"

primrec f24 :: "rt \<Rightarrow> rt" where
"f24 (T l ts) = T (S l) (map (f24 o f24) ts)"

primrec f26 :: "rt \<Rightarrow> rt" where
"f26 (T l ts) = T (S l) (map (undefined f26 \<Colon> rt \<Rightarrow> rt) ts)"

primrec f26' :: "rt \<Rightarrow> rt \<Rightarrow> rt" where
"f26' t (T l ts) = T l (map (undefined (f26' (T Z [])) \<Colon> rt \<Rightarrow> rt) ts)"

primrec f28 :: "nt \<Rightarrow> nt" where
"f28 Z = Z" |
"f28 (S n) = (undefined f28 \<Colon> nt \<Rightarrow> nt) n"

primrec f31 :: "nt \<Rightarrow> nt" where
"f32 Z = Z" |
"f32 (S n) = S (f32 n)"


text {* Suboptimal (i.e. Bad) *}

(* FIXME for LORENZ *)
(* more graceful error (not "exception Primrec_Error") *)
primrec f27 :: "nt \<Rightarrow> nt" and f27 :: "bt \<Rightarrow> bt" where
"f27 n = n"

(* OLD PACKAGE:
datatype x = X and y = Y
primrec fx :: "x \<Rightarrow> x" and fy :: "y \<Rightarrow> y" where
"fx X = X"
*)

(* TODO for LORENZ (perhaps) *)
(* "f26" is missing -- ideally this should be a warning, not an error (cf. old package)
   and the function f26 could be simply ignored *)
primrec f25 :: "nt \<Rightarrow> nt" and f26 :: "bt \<Rightarrow> bt" where
"f25 Z = Z"

(* "f30" is missing and occurs on the right-hand side -- this should definitely be
   an error, like it is right now *)
primrec f29 :: "nt \<Rightarrow> bt" and f30 :: "bt \<Rightarrow> bt" where
"f29 Z = f30 undefined"

locale l =
  fixes x :: nt
begin
  (* this should fail, because the "x" is fixed in the context *)
  primrec f33 where
    "f33 (S x) = x"
  thm f33.simps (* here we see that "x" is fixed by the lack of "?" prefix *)

  (* this works fine right now and should stay that way *)
  primrec f34 where
    "f34 (S y) = x"
  thm f34.simps
end

end

