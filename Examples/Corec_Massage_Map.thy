theory Corec_Massage_Map
imports Complex_Main "~~/src/HOL/BNF/BNF"
begin

no_translations
  "[x, xs]" == "x # [xs]"
  "[x]" == "x # []"

no_notation
  Nil ("[]") and
  Cons (infixr "#" 65)

hide_const Nil Cons hd tl map

datatype_new (set: 'a) list (map: map rel: list_all2) =
  null: Nil ("[]")
| Cons (hd: 'a) (tl: "'a list") (infixr "#" 65)

syntax "_list" :: "args \<Rightarrow> 'a list" ("[(_)]")

translations
  "[x, xs]" == "x # [xs]"
  "[x]" == "x # []"

codatatype 'a tree = Node "'a tree list"

consts size :: "'a tree \<Rightarrow> nat"

ML {* open BNF_Util BNF_Def BNF_FP_Def_Sugar BNF_FP_Util BNF_FP_Rec_Sugar_Util *}

ML {*
val fnames = ["f", "fa", "fb"];
fun get_kks t =
  map_index (fn (i, s) =>
    if exists_subterm (fn Free (s', _) => s' = s | _ => false) t then SOME i else NONE) fnames
  |> map_filter I;
fun massage check res_U t =
  massage_indirect_corec_call
    @{context}
    (not o null o get_kks)
    (fn T1 => fn U1 => fn t =>
       Const (@{const_name undefined}, T1 --> mk_sumT (T1, U1)) $ t)
    [] res_U t
  |> tap (Syntax.check_term @{context})
  |> tap (Output.urgent_message o Syntax.string_of_term @{context})
  handle ERROR msg => if check then error msg else (warning ("Error: " ^ msg); Term.dummy)
       | Fail msg => if check then raise Fail msg else (warning ("Fail: " ^ msg); Term.dummy)
*}

type_synonym 'a T0 = "'a tree list"
type_synonym 'a U0 = "('a tree + nat) list"

type_synonym 'a T = "'a tree list"
type_synonym 'a U = "('a tree + (nat \<times> int \<times> real)) list"

type_synonym 'a TT0 = "'a tree"
type_synonym 'a UU0 = "'a tree + nat"

type_synonym 'a TT = "'a tree"
type_synonym 'a UU = "'a tree + (nat \<times> int \<times> real)"

lemma
  fixes f :: "nat \<Rightarrow> 'a tree"
  fixes fa :: "nat \<Rightarrow> int \<Rightarrow> real \<Rightarrow> 'a tree"
  fixes fb :: "nat \<times> int \<Rightarrow> real \<Rightarrow> 'a tree"
  fixes g0 :: "nat \<Rightarrow> 'a tree"
  fixes g :: "nat \<Rightarrow> real \<Rightarrow> 'a tree"
  fixes p :: "nat \<Rightarrow> bool"
  fixes xs0 :: "nat list"
  fixes xs :: "(nat \<times> int \<times> real) list"
  shows True
proof -

txt {* 1Aa *}

ML_val {* massage true @{typ "'a UU0"} @{term "fa n i r :: 'a TT0"} *}
ML_val {* massage true @{typ "'a U0"} @{term "map f xs0 :: 'a T0"} *}
ML_val {* massage false @{typ "'a U"} @{term "mymap (\<lambda>(n, i, r). fa n i r) xs :: 'a T"} *}
ML_val {* massage false @{typ "'a U"} @{term "mymap id (map (\<lambda>(n, i, r). fa n i r) xs) :: 'a T"} *}

txt {* 1Ab *}

ML_val {* massage true @{typ "'a UU0"}
  @{term "if n = 0 then fa 0 i r else fa (Suc n) i r :: 'a TT0"} *}
ML_val {* massage true @{typ "'a U"} @{term "[fa n i r] :: 'a T"} *}
ML_val {* massage true @{typ "'a U"}
  @{term "[fa (fst (hd xs)) (fst (snd (hd xs))) (snd (snd (hd xs)))] :: 'a T"} *}
ML_val {* massage true @{typ "'a U"} @{term "if n = 0 then [fa n i r] else [] :: 'a T"} *}
ML_val {* massage true @{typ "'a U"} @{term "let m = Suc n in [fa m i r] :: 'a T"} *}
ML_val {* massage true @{typ "'a U"}
  @{term "let m = Suc n in if n = 0 then [fa m i r] else [] :: 'a T"} *}
ML_val {* massage true @{typ "'a U"}
  @{term "let m = Suc n in if n = 0 then let g = (\<lambda>i. fa m i r) in [g i] else [] :: 'a T"} *}

txt {* 1Ca *}

ML_val {* massage true @{typ "'a U0"} @{term "map g0 xs0 :: 'a T0"} *}
ML_val {* massage true @{typ "'a U"} @{term "map (\<lambda>(n, i, r). g n r) xs :: 'a T"} *}
ML_val {* massage false @{typ "'a U"} @{term "mymap (\<lambda>(n, i, r). g n r) xs :: 'a T"} *}

txt {* 1Cb *}

ML_val {* massage true @{typ "'a U"} @{term "[g n r] :: 'a T"} *}

txt {* 2Aa *}

ML_val {* massage true @{typ "'a U"}
  @{term "map ((\<lambda>(n, i, r). fa n i r) o map_pair Suc id) xs :: 'a T"} *}

txt {* 2Ba *}

ML_val {* massage true @{typ "'a U"}
  @{term "map ((\<lambda>(n, i, r). if p n then g n r else fa n i r) o map_pair Suc id) xs :: 'a T"} *}
ML_val {* massage true @{typ "'a U"}
  @{term "map ((\<lambda>(n, i, r). if p n then fa n i r else g n r)
      o map_pair Suc id o map_pair Suc id) xs :: 'a T"} *}

txt {* 2Ca *}

ML_val {* massage true @{typ "'a U"} @{term "map ((\<lambda>(n, i, r). g n r) o map_pair Suc id)xs :: 'a T"} *}

txt {* 3Aa *}

ML_val {* massage true @{typ "'a U"} @{term "map (\<lambda>(n, i, r). fa (Suc n) i r) xs :: 'a T"} *}

txt {* 3Ba *}

ML_val {* massage true @{typ "'a U"}
  @{term "map (\<lambda>(n, i, r). if p n then g n r else fa n i r) xs :: 'a T"} *}
ML_val {* massage true @{typ "'a U"}
  @{term "map (\<lambda>(n, i, r). if p n then fa n i r else g n r) xs :: 'a T"} *}

txt {* 3Bb *}

ML_val {* massage true @{typ "'a U"} @{term "[if p n then g n r else fa (Suc n) i r] :: 'a T"} *}
ML_val {* massage true @{typ "'a U"} @{term "[if p n then fa (Suc n) i r else g n r] :: 'a T"} *}
ML_val {* massage true @{typ "'a U"}
  @{term "if n = 0 then [if p n then fa (Suc n) i r else g n r] else [] :: 'a T"} *}

txt {* 3Ca *}

ML_val {* massage true @{typ "'a U"} @{term "map (\<lambda>(n, i, r). g (Suc n) r) xs :: 'a T"} *}
ML_val {* massage false @{typ "'a U"} @{term "mymap (\<lambda>(n, i, r). g (Suc n) r) xs :: 'a T"} *}

txt {* 3Cb *}

ML_val {* massage true @{typ "'a U"} @{term "[g (Suc n) r] :: 'a T"} *}
ML_val {* massage true @{typ "'a U"} @{term "if n = 0 then [g (Suc n) r] else [] :: 'a T"} *}

oops

end
