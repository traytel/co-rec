theory Old_N2M_Tests
imports Main
begin

declare [[bnf_note_all]]
declare [[bnf_timing]]

datatype_new 'a lst = Nl | Cns 'a "'a lst"
datatype_new 'a tre = Tre 'a "'a tre lst"

primrec tre0_rec_2 and tre0_rec_1 where
tr1: "tre0_rec_1 f1 f2 f3 (Tre a list) = f1 a list (tre0_rec_2 f1 f2 f3 list)" |
tr2a: "tre0_rec_2 f1 f2 f3 Nl = f2" |
tr2b: "tre0_rec_2 f1 f2 f3 (Cns tre list) = f3 tre list (tre0_rec_1 f1 f2 f3 tre) (tre0_rec_2 f1 f2 f3 list)"

thm tre0_rec_1.induct tre0_rec_2.induct tre0_rec_2_tre0_rec_1.induct


datatype_new 'a y = Cy "'a y lst lst"

primrec fy and fliy and floy where
  "fy (Cy xss) = Cy (floy xss)"
| "floy Nl = Nl"
| "floy (Cns xs xss) = Cns (fliy xs) (floy xss)"
| "fliy Nl = Nl"
| "fliy (Cns x xs) = Cns (fy x) (fliy xs)"

thm fy.induct fliy.induct floy.induct fy_fliy_floy.induct

datatype_new 'a x = Cx 'a "'a x lst lst"

primrec_new fx and flix and flox where
  "fx (Cx x xss) = Cx (- x) (flox xss)"
| "flox Nl = Nl"
| "flox (Cns xs xss) = Cns (flix xs) (flox xss)"
| "flix Nl = Nl"
| "flix (Cns x xs) = Cns (fx x) (flix xs)"

thm fx.induct flix.induct flox.induct fx_flix_flox.induct

datatype_new 'a listx = N | C 'a "'a listx"
datatype_new 'a lista = Na | Ca 'a "'a listb"
         and 'a listb = Nb | Cb 'a "'a lista"

theorem
  assumes
    IH1: "\<And>y1a xa.
        sum_rel op = (prod_rel R IR2) y1a xa \<Longrightarrow>
        IR1 (listx_ctor y1a) (listx_ctor xa)"
    and
    IH2: "\<And>y2a xa.
        sum_rel op = (prod_rel R IR1) y2a xa \<Longrightarrow>
        IR2 (listx_ctor y2a) (listx_ctor xa)"
  shows "listx_rel R \<le> IR1 \<and> listx_rel R \<le> IR2"
proof -
  { fix x have "listx_rel R \<le> IR1 \<sqinter> IR2"
    apply (rule listx.rel_induct[unfolded pre_listx_rel_def id_apply])
    apply (rule inf2I)
    apply (erule IH1[OF rev_predicate2D[OF _ pre_lista.rel_mono[unfolded pre_lista_rel_def]]])
    apply (rule order_refl)
    apply (rule order_refl)
    apply (rule inf_le2)
    apply (erule IH2[OF rev_predicate2D[OF _ pre_listb.rel_mono[unfolded pre_listb_rel_def]]])
    apply (rule order_refl)
    apply (rule inf_le1)
    apply (rule order_refl)
    done
  }
  thus ?thesis by auto
qed


datatype_new nt = Zo | Sc nt

datatype_new nta = AZo | ASc ntb
and ntb = BZo | BSc nta


lemma nt_2induct:
  assumes "P Zo" "P (Sc Zo)" "\<And>x. P x \<Longrightarrow> P (Sc (Sc x))"
  shows "P x \<and> P (Sc x)"
proof (induct x)
  case Zo thus ?case by (intro conjI assms(1,2))
next
  case (Sc n)
  thus ?case by (elim conjE) (intro conjI assms(3))
qed

lemma nt_nt_induct:
 assumes "P1 Zo" "(\<And>x. P2 x \<Longrightarrow> P1 (Sc x))" "P2 Zo" "(\<And>x. P1 x \<Longrightarrow> P2 (Sc x))" 
 shows "P1 z1"
 apply (induct z1 rule: conjunct1[OF nt_2induct])
   apply (intro assms(1))
  apply (intro assms(2) assms(3))
 apply (intro assms(2) assms(4))
 .

definition "ctor = sum_case (\<lambda>_::unit. 0 :: nat) Suc"

lemma [simp]: "(0::nat) = ctor (Inl ())" "Suc n = ctor (Inr n)"
  unfolding ctor_def by auto

lemma
  assumes IH1: "\<And>x1 y1. sum_rel op = S2 x1 y1 \<Longrightarrow> S1 (ctor x1) (ctor y1)"
      and IH2: "\<And>x1 y1. sum_rel op = S1 x1 y1 \<Longrightarrow> S2 (ctor x1) (ctor y1)"
  shows "op = \<le> S1 \<and> op = \<le> S2"
proof -
  { fix x have "S1 x x \<and> S2 x x \<and> S2 (Suc x) (Suc x) \<and> S1 (Suc x) (Suc x)"
    by (induct x) (auto simp: sum_rel_def intro!: IH1 IH2)}
  thus ?thesis by auto
qed

lemma
  assumes IH1: "\<And>x1 y1. sum_rel op = S2 x1 y1 \<Longrightarrow> S1 (nt_ctor x1) (nt_ctor y1)"
      and IH2: "\<And>x1 y1. sum_rel op = S1 x1 y1 \<Longrightarrow> S2 (nt_ctor x1) (nt_ctor y1)"
  shows "op = \<le> S1 \<and> op = \<le> S2"
proof -
  { fix x have "op = \<le> S1 \<sqinter> S2"
    apply (rule nt.rel_induct[unfolded pre_nt_rel_def id_apply])
    apply (rule inf2I)
    apply (erule IH1[OF rev_predicate2D[OF _ sum.rel_mono]])
    apply (rule order_refl)
    apply (rule inf_le2)
    apply (erule IH2[OF rev_predicate2D[OF _ sum.rel_mono]])
    apply (rule order_refl)
    apply (rule inf_le1)
    done
  }
  thus ?thesis by auto
qed

axiomatization where
nat_nat_induct0:
"\<lbrakk>\<And>x1b. \<lbrakk>\<And>z1. z1 \<in> pre_nta_set1 x1b \<Longrightarrow> P1 z1; \<And>z2. z2 \<in> pre_nta_set2 x1b \<Longrightarrow> P2 z2\<rbrakk> \<Longrightarrow> P1 (nt_ctor x1b);
 \<And>x2b. \<lbrakk>\<And>z1. z1 \<in> pre_ntb_set1 x2b \<Longrightarrow> P1 z1; \<And>z2. z2 \<in> pre_ntb_set2 x2b \<Longrightarrow> P2 z2\<rbrakk> \<Longrightarrow> P2 (nt_ctor x2b)\<rbrakk>
\<Longrightarrow> P1 z1 \<and> P2 z2"

lemmas nat_nat_induct = nat_nat_induct0[unfolded pre_nta_set1_def pre_nta_set2_def pre_ntb_set1_def
  pre_ntb_set2_def]

axiomatization nat_nat_fold1 :: "(unit + 'a \<Rightarrow> 'b) \<Rightarrow> (unit + 'b \<Rightarrow> 'a) \<Rightarrow> nt \<Rightarrow> 'b" and
nat_nat_fold2 :: "(unit + 'a \<Rightarrow> 'b) \<Rightarrow> (unit + 'b \<Rightarrow> 'a) \<Rightarrow> nt \<Rightarrow> 'a" where
nat_nat_fold0:
"nat_nat_fold1 s'1 s'2 (nt_ctor x1) = s'1 (pre_nta_map (nat_nat_fold1 s'1 s'2) (nat_nat_fold2 s'1 s'2) x1)"
"nat_nat_fold2 s'1 s'2 (nt_ctor x2) = s'2 (pre_ntb_map (nat_nat_fold1 s'1 s'2) (nat_nat_fold2 s'1 s'2) x2)"

lemmas nat_nat_fold = nat_nat_fold0[unfolded pre_nta_map_def pre_ntb_map_def]

axiomatization nat_nat_rec1 :: "(unit + (nt * 'a) \<Rightarrow> 'b) \<Rightarrow> (unit + (nt * 'b) \<Rightarrow> 'a) \<Rightarrow> nt \<Rightarrow> 'b" and
nat_nat_rec2 :: "(unit + (nt * 'a) \<Rightarrow> 'b) \<Rightarrow> (unit + (nt * 'b) \<Rightarrow> 'a) \<Rightarrow> nt \<Rightarrow> 'a" where
nat_nat_rec0:
"nat_nat_rec1 s1a s2a (nt_ctor x1b) = s1a (pre_nta_map (BNF_Def.convol id (nat_nat_rec1 s1a s2a)) (BNF_Def.convol id (nat_nat_rec2 s1a s2a)) x1b)"
"nat_nat_rec2 s1a s2a (nt_ctor x2b) = s2a (pre_ntb_map (BNF_Def.convol id (nat_nat_rec1 s1a s2a)) (BNF_Def.convol id (nat_nat_rec2 s1a s2a)) x2b)"

lemmas nat_nat_rec = nat_nat_rec0[unfolded pre_nta_map_def pre_ntb_map_def]

term nt_ctor_fold
term "\<lambda>(s1::unit + 'a \<times> 'b \<Rightarrow> 'a \<times>'b).
  nt_ctor_fold s1"
term "\<lambda>(s1::unit + 'a \<Rightarrow> 'b) (s2::unit + 'b \<Rightarrow> 'a).
  nt_ctor_fold <s2 o pre_ntb_map snd id, s1 o pre_nta_map id fst>"

(*
primrec_new evn :: "nt \<Rightarrow> bool" and odd :: "nt \<Rightarrow> bool" where
  "evn Zo = True" |
  "odd Zo = False" |
  "evn (Sc n) = odd n" |
  "odd (Sc n) = evn n"
*)

end
