theory Sel_Transfer
(* imports "~~/src/HOL/BNF_Least_Fixpoint" *)
imports "~~/src/HOL/Main"
begin

notation rel_fun (infixr "===>" 55)

ML {*
val selss = [["s1", "s2", "s3"], ["s4", "s2", "s3"], ["s1", "s2", "s5'"]];
*}

ML {*
foldl1 (uncurry (inter (op =))) selss
*}

declare [[ML_exception_trace]]

datatype 'a k =
  K1 (s1: 'a) (s2: 'a) (s3: 'a)
| K2 (s4: 'a) (s2: 'a) (s3: 'a)
| K3 (s1: 'a) (s2: 'a) (s5': 'a)


datatype 'a k' =
  K1' (s1': 'a) (s2': 'a) (s3': 'a)
| K2' (s1': 'a) (s2': 'a) (s3': 'a)
| K3' (s1': 'a) (s2': 'a) (s5': 'a)
| K4' (s1': 'a) (s2': 'a) (s5': 'a)

thm k.sel_transfer
thm k'.sel_transfer

thm k.case_transfer
thm s2_def

term case_k
term s2

thm rel_funD[OF rel_funD[OF rel_funD[OF k.case_transfer]]]

term "(\<lambda>k. case k of K1 x11 x12 x13 \<Rightarrow> x12 | K2 x21 x22 x23 \<Rightarrow> x22 | K3 x31 x32 x33 \<Rightarrow> x32)"

term "case_k (%x y z. y) (%x y z. y) (%x y z. y)"

ML \<open>
open Ctr_Sugar_General_Tactics
open BNF_Util
\<close>

thm s2_def s2_def[abs_def]

lemma "(rel_k R ===> R) s2 s2"
  apply (tactic \<open>unfold_thms_tac @{context} @{thms s2_def[abs_def]} THEN
    HEADGOAL (rtac @{thm rel_funD[OF rel_funD[OF rel_funD[OF k.case_transfer]]]} THEN'
    REPEAT_DETERM o (REPEAT_DETERM o (rtac rel_funI) THEN' atac))\<close>)
  (* unfolding s2_def[abs_def]
  apply (rule rel_funD[OF rel_funD[OF rel_funD[OF k.case_transfer]]])
  apply (intro rel_funI, assumption)+ *)
  done

ML \<open>
val _ = op THEN_ALL_NEW
\<close>

lemma
  "(rel_k' R ===> R) s1' s1'"
  "(rel_k' R ===> R) s2' s2'"
  apply (tactic \<open>TRYALL Goal.conjunction_tac\<close>)
  apply (tactic \<open>unfold_thms_tac @{context} @{thms s1'_def[abs_def] s2'_def[abs_def]}\<close>)
  apply (tactic \<open>ALLGOALS (rtac @{thm rel_funD[OF rel_funD[OF rel_funD[OF rel_funD[OF k'.case_transfer]]]]} THEN_ALL_NEW
     REPEAT_DETERM o (REPEAT_DETERM o (rtac rel_funI) THEN' atac))\<close>)
  (* unfolding s2_def[abs_def]
  apply (rule rel_funD[OF rel_funD[OF rel_funD[OF k.case_transfer]]])
  apply (intro rel_funI, assumption)+ *)
  done

end