theory Custom_Thm_Names
imports "~~/src/HOL/BNF/BNF"
begin

locale data
begin

datatype_new 'a l = N | C 'a "'a l"

primrec_new s where
s_N: "s N = 0" |
s_C: "s (C _ xs) = Suc (s xs)"

print_theorems

thm s_N s_C s.simps

end

locale codata
begin

codatatype 'a l = _: N | _: C 'a "'a l"

primcorecursive s where
is_N_s: "n = 0 \<Longrightarrow> s n = N" |
is_C_s: "_ \<Longrightarrow> is_C (s n)" |
"un_C1 (s n) = undefined n n n" |
un_C2_s: "un_C2 (s n) = (if n = (0\<Colon>nat) then N else s (n - 1))"
.

print_theorems

thm is_N_s is_C_s un_C2_s s.sels s.discs

lemma "n = 0 \<Longrightarrow> s n = N" by (rule is_N_s)
lemma "n \<noteq> 0 \<Longrightarrow> is_C (s n)" by (rule is_N_s)
lemma "n \<noteq> 0 \<Longrightarrow> un_C2 (s n) = (if n = (0\<Colon>nat) then N else s (n - 1))" by (rule un_C2_s)

end

end
