theory set_transfer
imports "~~/src/HOL/Transfer"
begin

context lifting_syntax (* enables the ===> syntax for rel_fun *)
begin

declare [[bnf_note_all = true]]

datatype_new 'a list
  = nil: Nil
  | Cons (head: 'a) (tail: "'a list")
  where
    "tail Nil = Nil"

datatype_new 'a option
  = is_none: None
  | is_some: Some (the: 'a)

datatype_new 'a tree
  = is_node: Node (val: 'a) (children: "'a tree list")

datatype_new ('a, 'b) example
  = is_N1: N1 'a
  | is_N2: N2 'b
  | is_N3: N3 'a 'b
  | is_N4: N4
  | is_ConsA: ConsA 'a "('a, 'b) example"
  | is_ConsB: ConsB 'b "('a, 'b) example"

datatype_new 'a tree_mutual = is_TNode: TNode 'a "'a forest_mutual"
         and 'a forest_mutual = is_FNil: FNil | is_FCons: FCons "'a tree_mutual" "'a forest_mutual"

datatype_new ('a, 'b) F1 = is_F1: F1 "'a \<Rightarrow> 'b"

datatype_new ('a, 'b, 'c) F2 = is_F2: F2 "'a \<Rightarrow> 'b \<Rightarrow> 'c"

datatype_new 'a A1 = is_A1: A1 nat
         and 'a A2 = is_A2: A2 'a

ML {*
fun mk_set_transfer_tac ctxt in_rel set_map =
  HEADGOAL (rtac @{thm rel_funI}) THEN
  REPEAT_DETERM (HEADGOAL (eresolve_tac (Tactic.make_elim (in_rel RS iffD1) ::
    @{thms exE conjE CollectE}))) THEN
  HEADGOAL (hyp_subst_tac ctxt THEN'
    rtac (@{thm iffD2[OF arg_cong2]} OF [set_map, set_map]) THEN'
    rtac @{thm rel_setI}) THEN
  REPEAT (HEADGOAL (etac imageE THEN'
    dtac @{thm set_mp} THEN'
    atac THEN'
    REPEAT_DETERM o (eresolve_tac @{thms CollectE case_prodE}) THEN'
    hyp_subst_tac ctxt THEN'
    rtac bexI THEN'
    etac @{thm subst_Pair[OF _ refl]} THEN'
    etac imageI))
*}

thm list.set_transfer
lemma list_set_transfer: "(rel_list R ===> rel_set R) set_list set_list"
  apply (tactic {* mk_set_transfer_tac @{context} @{thm list.in_rel} @{thm list.set_map} *})
  (* apply (rule rel_funI)
  apply (erule iffD1[OF list.in_rel, elim_format] exE conjE CollectE)+
  apply hypsubst
  apply (rule iffD2[OF arg_cong2[of _ _ _ _ "rel_set R" for R], OF list.set_map list.set_map])
  apply (rule rel_setI)
  apply (erule imageE)
  apply (drule set_mp)
  apply assumption
  apply (erule CollectE case_prodE)+
  apply hypsubst
  apply (rule bexI)
  apply (erule subst_Pair[OF _ refl])
  apply (erule imageI)

  apply (erule imageE)
  apply (drule set_mp)
  apply assumption
  apply (erule CollectE case_prodE)+
  apply hypsubst
  apply (rule bexI)
  apply (erule subst_Pair[OF _ refl])
  apply (erule imageI) *)
  done

thm option.set_transfer
lemma option_set_transfer: "(rel_option R ===> rel_set R) set_option set_option"
  apply (tactic {* mk_set_transfer_tac @{context} @{thm option.in_rel} @{thm option.set_map} *})
  (* apply (rule rel_funI)
  apply (erule iffD1[OF option.in_rel, elim_format] exE conjE CollectE)+
  apply hypsubst
  apply (rule iffD2[OF arg_cong2[of _ _ _ _ "rel_set R" for R], OF option.set_map option.set_map])
  apply (rule rel_setI)
  apply (erule imageE, drule set_mp, assumption, (erule CollectE case_prodE)+, hypsubst, rule bexI, erule subst_Pair[OF _ refl], erule imageI)+ *)
  done

thm tree.set_transfer
lemma tree_set_transfer: "(rel_tree R ===> rel_set R) set_tree set_tree"
  apply (tactic {* mk_set_transfer_tac @{context} @{thm tree.in_rel} @{thm tree.set_map} *})
  (* apply (rule rel_funI)
  apply (erule iffD1[OF tree.in_rel, elim_format] exE conjE CollectE)+
  apply hypsubst
  apply (rule iffD2[OF arg_cong2[of _ _ _ _ "rel_set R" for R], OF tree.set_map tree.set_map])
  apply (rule rel_setI)
  apply (erule imageE, drule set_mp, assumption, (erule CollectE case_prodE)+, hypsubst, rule bexI, erule subst_Pair[OF _ refl], erule imageI)+ *)
  done

thm example.set_transfer
lemma example_set_transfer_1: "(rel_example R1 R2 ===> rel_set R1) set1_example set1_example"
  apply (tactic {* mk_set_transfer_tac @{context} @{thm example.in_rel} @{thm example.set_map(1)} *})
  (* apply (rule rel_funI)
  apply (erule iffD1[OF example.in_rel, elim_format] exE conjE CollectE)+
  apply hypsubst
  apply (rule iffD2[OF arg_cong2[of _ _ _ _ "rel_set R1" for R1], OF example.set_map(1) example.set_map(1)])
  apply (rule rel_setI)
  apply (erule imageE, drule set_mp, assumption, (erule CollectE case_prodE)+, hypsubst, rule bexI, erule subst_Pair[OF _ refl], erule imageI)+ *)
  done

thm example.set_transfer
lemma example_set_transfer_2: "(rel_example R1 R2 ===> rel_set R2) set2_example set2_example"
  apply (tactic {* mk_set_transfer_tac @{context} @{thm example.in_rel} @{thm example.set_map(2)} *})
  (* apply (rule rel_funI)
  apply (erule iffD1[OF example.in_rel, elim_format] exE conjE CollectE)+
  apply hypsubst
  apply (rule iffD2[OF arg_cong2[of _ _ _ _ "rel_set R2" for R2], OF example.set_map(2) example.set_map(2)])
  apply (rule rel_setI)
  apply (erule imageE, drule set_mp, assumption, (erule CollectE case_prodE)+, hypsubst, rule bexI, erule subst_Pair[OF _ refl], erule imageI)+ *)
  done

thm tree_mutual.set_transfer
lemma tree_mutual_set_transfer: "(rel_tree_mutual R ===> rel_set R) set_tree_mutual set_tree_mutual"
  apply (tactic {* mk_set_transfer_tac @{context} @{thm tree_mutual.in_rel} @{thm tree_mutual.set_map} *})
  (* apply (rule rel_funI)
  apply (erule iffD1[OF option.in_rel, elim_format] exE conjE CollectE)+
  apply hypsubst
  apply (rule iffD2[OF arg_cong2[of _ _ _ _ "rel_set R" for R], OF option.set_map option.set_map])
  apply (rule rel_setI)
  apply (erule imageE, drule set_mp, assumption, (erule CollectE case_prodE)+, hypsubst, rule bexI, erule subst_Pair[OF _ refl], erule imageI)+ *)
  done

thm forest_mutual.set_transfer
lemma forest_mutual_set_transfer: "(rel_forest_mutual R ===> rel_set R) set_forest_mutual set_forest_mutual"
  apply (tactic {* mk_set_transfer_tac @{context} @{thm forest_mutual.in_rel} @{thm forest_mutual.set_map} *})
  (* apply (rule rel_funI)
  apply (erule iffD1[OF forest_mutual.in_rel, elim_format] exE conjE CollectE)+
  apply hypsubst
  apply (rule iffD2[OF arg_cong2[of _ _ _ _ "rel_set R" for R], OF forest_mutual.set_map forest_mutual.set_map])
  apply (rule rel_setI)
  apply (erule imageE, drule set_mp, assumption, (erule CollectE case_prodE)+, hypsubst, rule bexI, erule subst_Pair[OF _ refl], erule imageI)+ *)
  done

thm F1.set_transfer
lemma F1_set_transfer: "(rel_F1 R ===> rel_set R) set_F1 set_F1"
  apply (tactic {* mk_set_transfer_tac @{context} @{thm F1.in_rel} @{thm F1.set_map} *})
  (* apply (rule rel_funI)
  apply (erule iffD1[OF F1.in_rel, elim_format] exE conjE CollectE)+
  apply hypsubst
  apply (rule iffD2[OF arg_cong2[of _ _ _ _ "rel_set R" for R], OF F1.set_map F1.set_map])
  apply (rule rel_setI)
  apply (erule imageE, drule set_mp, assumption, (erule CollectE case_prodE)+, hypsubst, rule bexI, erule subst_Pair[OF _ refl], erule imageI)+ *)
  done

thm F2.set_transfer
lemma F2_set_transfer: "(rel_F2 R ===> rel_set R) set_F2 set_F2"
  apply (tactic {* mk_set_transfer_tac @{context} @{thm F2.in_rel} @{thm F2.set_map} *})
  (* apply (rule rel_funI)
  apply (erule iffD1[OF F2.in_rel, elim_format] exE conjE CollectE)+
  apply hypsubst
  apply (rule iffD2[OF arg_cong2[of _ _ _ _ "rel_set R" for R], OF F2.set_map F2.set_map])
  apply (rule rel_setI)
  apply (erule imageE, drule set_mp, assumption, (erule CollectE case_prodE)+, hypsubst, rule bexI, erule subst_Pair[OF _ refl], erule imageI)+ *)
  done

thm A1.set_transfer
lemma A1_set_transfer: "(rel_A1 R ===> rel_set R) set_A1 set_A1"
  apply (tactic {* mk_set_transfer_tac @{context} @{thm A1.in_rel} @{thm A1.set_map} *})
  (* apply (rule rel_funI)
  apply (erule iffD1[OF A1.in_rel, elim_format] exE conjE CollectE)+
  apply hypsubst
  apply (rule iffD2[OF arg_cong2[of _ _ _ _ "rel_set R" for R], OF A1.set_map A1.set_map])
  apply (rule rel_setI)
  apply (erule imageE, drule set_mp, assumption, (erule CollectE case_prodE)+, hypsubst, rule bexI, erule subst_Pair[OF _ refl], erule imageI)+ *)
  done

thm A2.set_transfer
lemma A2_set_transfer: "(rel_A2 R ===> rel_set R) set_A2 set_A2"
  apply (tactic {* mk_set_transfer_tac @{context} @{thm A2.in_rel} @{thm A2.set_map} *})
  (* apply (rule rel_funI)
  apply (erule iffD1[OF A2.in_rel, elim_format] exE conjE CollectE)+
  apply hypsubst
  thm arg_cong2
  apply (rule iffD2[OF arg_cong2, OF A2.set_map A2.set_map])
  apply (rule rel_setI)
  apply (erule imageE, drule set_mp, assumption, (erule CollectE case_prodE)+, hypsubst, rule bexI, erule subst_Pair[OF _ refl], erule imageI)+ *)
  done

end

end
