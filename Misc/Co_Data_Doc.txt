  * (Co)datatypes

    * datatype_new
    * codatatype
      * defaults

    * set function names
    * discriminator names
      * including defaults and "= LNil"
    * selector names

    * global options:
      * no_dests
      * rep_compat

    * per-type options:
      * map, rel

    * compatibility with old function package etc.


  * Primitive (Co)recursion

    * primrec_new
    * primcorec

  * Bounded Natural Functors

    * bnf
    * print_bnfs

  * Free Constructors

    * wrap_free_constructors
      * no_dests, rep_compat
