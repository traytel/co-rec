theory Primcorec_Transfer
imports "~~/src/HOL/Datatype_Examples/Misc_Codatatype"
begin

notation rel_fun (infixr "===>" 55)

codatatype 'a llist = LNil | LCons (lhd: 'a) (ltl: "'a llist")

primcorec (transfer) lapp2 :: "'a llist \<Rightarrow> 'a llist \<Rightarrow> 'a llist" where
  "lapp2 xs ys = (case xs of LNil \<Rightarrow> ys | LCons x xs \<Rightarrow> LCons x (lapp2 xs ys))"

primcorec lapp :: "'a llist \<Rightarrow> 'a llist \<Rightarrow> 'a llist" where
  "lapp xs ys =
     (if xs = LNil then ys
      else if ys = LNil then xs
      else LCons (lhd xs) (lapp (ltl xs) ys))"

lemma lapp_LNil[simp]: "lapp LNil xs = xs"
  by (coinduction rule: llist.coinduct_strong) auto

lemma lapp_LNil2[simp]: "lapp xs LNil = xs"
  by (coinduction rule: llist.coinduct_strong) auto

lemma lapp_LCons[simp]: "lapp (LCons x xs) ys = LCons x (lapp xs ys)"
  by (coinduction rule: llist.coinduct_strong) auto

lemma lapp_map_llist: "lapp (map_llist f x) (map_llist f y) = map_llist f (lapp x y)"
  by (coinduction arbitrary: x y rule: llist.coinduct_strong)
    (auto 4 4 simp: llist.map_sel)

lemma set_llist_lapp: "set_llist (lapp x y) \<subseteq> set_llist x \<union> set_llist y"
proof (intro equalityI subsetI)
  fix z assume "z \<in> set_llist (lapp x y)"
  then show "z \<in> set_llist x \<union> set_llist y"
  proof (induct "lapp x y" arbitrary: x y)
    case (LCons1 z zs)
    then show ?case by (cases x y rule: llist.exhaust[case_product llist.exhaust]) auto
  next
    case (LCons2 z' zs z)
    then show ?case
    proof (cases x y rule: llist.exhaust[case_product llist.exhaust])
      case (LCons_LCons xh xs yh ys)
      with LCons2(1,3) LCons2(2)[of xs "LCons yh ys"] show ?thesis by auto
    qed auto
  qed
qed

lemma "(rel_llist R ===> rel_llist R ===> rel_llist R) lapp lapp"
  by (force simp: rel_fun_def llist.in_rel lapp_map_llist dest: set_mp[OF set_llist_lapp])

hide_type p stream
hide_const P corec_stream Stream rel_pre_stream rel_stream dtor_corec_stream

codatatype (discs_sels) 'a stream = Stream (shd: 'a) (stl: "'a stream")
codatatype (discs_sels) 'a llist = LNil | LCons (lhd: 'a) (ltl: "'a llist")
codatatype (discs_sels) 'a non_empty_llist = NELHd 'a | NELCons 'a "'a non_empty_llist"

notation rel_fun (infixr "===>" 55)

lemma if_conn:
  "(if P \<and> Q then t else e) = (if P then if Q then t else e else e)"
  "(if P \<or> Q then t else e) = (if P then t else if Q then t else e)"
  "(if P \<longrightarrow> Q then t else e) = (if P then if Q then t else e else t)"
  "(if \<not> P then t else e) = (if P then e else t)"
  by auto

(* declare Abs_transfer[OF BNF_Composition.type_definition_id_bnf_UNIV BNF_Composition.type_definition_id_bnf_UNIV, transfer_rule] *)
(* declare mylist.dtor_corec_transfer[unfolded rel_pre_mylist_def, transfer_rule] *)
(*
ML {*
open Ctr_Sugar_Tactics
open Ctr_Sugar_Util
open BNF_FP_Util

fun mk_primcorec_transfer_tac apply_transfer ctxt f_def corec_def type_definitions
  dtor_corec_transfers rel_pre_defs disc_eq_cases cases case_distribs case_congs =
  let
    val _ = @{print} f_def
    val _ = @{print} corec_def
    val _ = @{print} type_definitions
    val _ = @{print} dtor_corec_transfers
    val _ = @{print} rel_pre_defs
    val _ = @{print} disc_eq_cases
    val _ = @{print} cases
    val _ = @{print} case_distribs
    val _ = @{print} case_congs

    fun instantiate_with_lambda thm =
      let
        val prop = Thm.prop_of thm;
        val @{const Trueprop} $
          (Const (@{const_name HOL.eq}, _) $
            (Var (_, fT) $ _) $ _) = prop;
        val T = range_type fT;
        val idx = Term.maxidx_of_term prop + 1;
        val bool_expr = Var (("x", idx), HOLogic.boolT);
        val then_expr = Var (("t", idx), T);
        val else_expr = Var (("e", idx), T);
        val lambda = Term.lambda bool_expr (mk_If bool_expr then_expr else_expr);
      in
        cterm_instantiate_pos [SOME (certify ctxt lambda)] thm
      end;

    val transfer_rules =
      @{thm Abs_transfer[OF
        BNF_Composition.type_definition_id_bnf_UNIV
        BNF_Composition.type_definition_id_bnf_UNIV]} ::
      map (fn thm => @{thm Abs_transfer} OF [thm, thm]) type_definitions @
      map (Local_Defs.unfold ctxt rel_pre_defs) dtor_corec_transfers;
    val add_transfer_rule = Thm.attribute_declaration Transfer.transfer_add
    val ctxt' = Context.proof_map (fold add_transfer_rule transfer_rules) ctxt

    val case_distribs = map instantiate_with_lambda case_distribs;
    val simps = case_distribs @ disc_eq_cases @ cases @ @{thms if_True if_False};
    val simp_ctxt = put_simpset (simpset_of (ss_only simps ctxt)) ctxt';
  in
    unfold_thms_tac ctxt ([f_def, corec_def] @ @{thms split_beta if_conn}) THEN
    HEADGOAL (simp_tac (fold Simplifier.add_cong case_congs simp_ctxt)) THEN
    (if apply_transfer then HEADGOAL (Transfer.transfer_prover_tac ctxt') else all_tac)
  end;
*}

primcorec replace :: "'a llist \<Rightarrow> 'a stream \<Rightarrow> 'a stream" where
  "replace xs ys =
     (case xs of LNil \<Rightarrow> ys | LCons x xs' \<Rightarrow>
        (case ys of Stream y ys' \<Rightarrow> Stream x (replace xs' ys')))"

(* declare llist.dtor_corec_transfer[unfolded rel_pre_llist_def, transfer_rule] *)
(* declare stream.dtor_corec_transfer[unfolded rel_pre_stream_def, transfer_rule] *)

lemma "(rel_llist R ===> rel_stream R ===> rel_stream R) replace replace"
  (* unfolding replace_def corec_stream_def split_beta if_conn
  apply (simp only: stream.disc_eq_case llist.disc_eq_case
    stream.case_distrib[of "\<lambda>x. If x t e" for t e] llist.case_distrib[of "\<lambda>x. If x t e" for t e]
    if_True if_False stream.case llist.case cong: stream.case_cong llist.case_cong)
  by transfer_prover *)
  apply (tactic \<open>mk_primcorec_transfer_tac true @{context}
    @{thm replace_def}
    @{thm corec_stream_def}
    @{thms BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms stream.dtor_corec_transfer}
    @{thms rel_pre_stream_def}
    @{thms llist.disc_eq_case stream.disc_eq_case}
    @{thms llist.case stream.case}
    @{thms llist.case_distrib stream.case_distrib}
    @{thms llist.case_cong stream.case_cong}
  \<close>)
done

(*
We need to provide t.disc_eq_case t.case_distrib t.case t.case_cong
for every t in the type of the arguments of the function.
*)

primcorec flat :: "'a non_empty_llist stream \<Rightarrow> 'a stream" where
  "flat xs =
     (case xs of Stream y ys \<Rightarrow>
        (case y of NELHd z \<Rightarrow> Stream z (flat ys)
                 | NELCons z zs \<Rightarrow> Stream z (flat (Stream zs ys))))"

lemma "(rel_stream (rel_non_empty_llist R) ===> rel_stream R) flat flat"
(*   unfolding flat_def corec_stream_def split_beta if_conn
  apply (simp only: stream.disc_eq_case non_empty_llist.disc_eq_case
    stream.case_distrib[of "\<lambda>x. If x t e" for t e] non_empty_llist.case_distrib[of "\<lambda>x. If x t e" for t e]
    if_True if_False stream.case non_empty_llist.case
    cong: stream.case_cong non_empty_llist.case_cong)
  by transfer_prover *)
  apply (tactic \<open>mk_primcorec_transfer_tac true @{context}
    @{thm flat_def}
    @{thm corec_stream_def}
    @{thms BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms stream.dtor_corec_transfer non_empty_llist.dtor_corec_transfer}
    @{thms rel_pre_stream_def rel_pre_non_empty_llist_def}
    @{thms stream.disc_eq_case non_empty_llist.disc_eq_case}
    @{thms stream.case non_empty_llist.case}
    @{thms stream.case_distrib non_empty_llist.case_distrib}
    @{thms stream.case_cong non_empty_llist.case_cong}
  \<close>)
done

primcorec simple_of_bools :: "bool \<Rightarrow> bool \<Rightarrow> simple" where
  "simple_of_bools b b' = (if b then if b' then X1 else X2 else if b' then X3 else X4)"

thm simple_of_bools.transfer
lemma "(op = ===> op = ===> op =) simple_of_bools simple_of_bools"
(*   unfolding simple_of_bools_def corec_simple_def split_beta if_conn
  apply (simp only: simple.disc_eq_case simple.case_distrib[of "\<lambda>x. If x t e" for t e]
    if_True if_False simple.case cong: simple.case_cong)?
  by transfer_prover *)
  apply (tactic \<open>mk_primcorec_transfer_tac true @{context}
    @{thm simple_of_bools_def}
    @{thm corec_simple_def}
    @{thms BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms }
    @{thms }
    @{thms }
    @{thms }
    @{thms }
    @{thms }
  \<close>)
done

primcorec simple'_of_bools :: "bool \<Rightarrow> bool \<Rightarrow> simple'" where
  "simple'_of_bools b b' =
     (if b then if b' then X1' () else X2' () else if b' then X3' () else X4' ())"

thm simple'_of_bools.transfer
lemma "(op = ===> op = ===> op =) simple'_of_bools simple'_of_bools"
  (* unfolding simple'_of_bools_def by transfer_prover *)
  apply (tactic \<open>mk_primcorec_transfer_tac true @{context}
    @{thm simple'_of_bools_def}
    @{thm corec_simple'_def}
    @{thms BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms }
    @{thms }
    @{thms }
    @{thms }
    @{thms }
    @{thms }
  \<close>)
done

primcorec inc_simple'' :: "nat \<Rightarrow> simple'' \<Rightarrow> simple''" where
  "inc_simple'' k s = (case s of X1'' n i \<Rightarrow> X1'' (n + k) (i + int k) | X2'' \<Rightarrow> X2'')"

thm inc_simple''.transfer
lemma "(op = ===> op = ===> op =) inc_simple'' inc_simple''"
  (* unfolding inc_simple''_def by transfer_prover *)
  apply (tactic \<open>mk_primcorec_transfer_tac true @{context}
    @{thm inc_simple''_def}
    @{thm corec_simple''_def}
    @{thms BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms }
    @{thms }
    @{thms }
    @{thms }
    @{thms }
    @{thms }
  \<close>)
done
primcorec sinterleave :: "'a stream \<Rightarrow> 'a stream \<Rightarrow> 'a stream" where
  "sinterleave s s' = Stream (shd s) (sinterleave s' (stl s))"

(* declare stream.dtor_corec_transfer[unfolded rel_pre_stream_def, transfer_rule] *)

lemma "(rel_stream R ===> rel_stream R ===> rel_stream R) sinterleave sinterleave"
  (* unfolding sinterleave_def corec_stream_def split_beta if_conn
  apply (simp only: if_True if_False stream.case cong: stream.case_cong)
  by transfer_prover *)
  apply (tactic \<open>mk_primcorec_transfer_tac true @{context}
    @{thm sinterleave_def}
    @{thm corec_stream_def}
    @{thms BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms stream.dtor_corec_transfer}
    @{thms rel_pre_stream_def}
    @{thms stream.disc_eq_case}
    @{thms stream.case}
    @{thms stream.case_distrib}
    @{thms stream.case_cong}
  \<close>)
done

*)

(* prim corec myapp :: "'a mylist \<Rightarrow> 'a mylist \<Rightarrow> 'a mylist" where
  "myapp xs ys =
     (if xs = MyNil then ys
      else if ys = MyNil then xs
      else MyCons (myhd xs) (myapp (mytl xs) ys))" *)

(* fail because myhd and mytl are not parametric *)
lemma "(rel_mylist R ===> rel_mylist R ===> rel_mylist R) myapp myapp"
  unfolding myapp_def corec_mylist_def split_beta if_conn
  apply (simp only: mylist.disc_eq_case mylist.case_distrib[of "\<lambda>x. If x t e" for t e]
    if_True if_False mylist.case cong: mylist.case_cong)
  (* by transfer_prover *) sorry

primcorec shuffle_sp :: "('a \<Colon> ord, 'b \<Colon> ord, 'c, 'd) some_passive \<Rightarrow> ('d, 'a, 'b, 'c) some_passive" where
  "shuffle_sp sp =
     (case sp of
       SP1 sp' \<Rightarrow> SP1 (shuffle_sp sp')
     | SP2 a \<Rightarrow> SP3 a
     | SP3 b \<Rightarrow> SP4 b
     | SP4 c \<Rightarrow> SP5 c
     | SP5 d \<Rightarrow> SP2 d)"
(*
lemma "(rel_some_passive R1 R2 R3 R4 ===> rel_some_passive R1 R2 R3 R4) shuffle_sp shuffle_sp"
  unfolding shuffle_sp_def by transfer_prover
*)
primcorec rename_lam :: "(string \<Rightarrow> string) \<Rightarrow> lambda \<Rightarrow> lambda" where
  "rename_lam f l =
     (case l of
       Var s \<Rightarrow> Var (f s)
     | App l l' \<Rightarrow> App (rename_lam f l) (rename_lam f l')
     | Abs s l \<Rightarrow> Abs (f s) (rename_lam f l)
     | Let SL l \<Rightarrow> Let (fimage (map_prod f (rename_lam f)) SL) (rename_lam f l))"

thm rename_lam.transfer
lemma "((op = ===> op =) ===> op =) rename_lam rename_lam"
  (* unfolding rename_lam_def by transfer_prover *)
  apply (tactic \<open>mk_primcorec_transfer_tac true @{context}
    @{thm rename_lam_def}
    @{thm corec_lambda_def}
    @{thms BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms }
    @{thms }
    @{thms }
    @{thms }
    @{thms }
    @{thms }
  \<close>)
done

primcorec
  j1_sum :: "('a \<Rightarrow> bool) \<Rightarrow> 'a \<Rightarrow> ('a \<Rightarrow> 'a \<Rightarrow> 'a) \<Rightarrow> 'a \<Rightarrow> 'a J1" and
  j2_sum :: "('a \<Rightarrow> bool) \<Rightarrow> 'a \<Rightarrow> ('a \<Rightarrow> 'a \<Rightarrow> 'a) \<Rightarrow> 'a \<Rightarrow> 'a J2"
where
  "P n \<Longrightarrow> is_J11 (j1_sum P z p n)" |
  "un_J111 (j1_sum P z p _) = z" |
  "un_J112 (j1_sum P z p _) = j1_sum P z p z" |
  "un_J121 (j1_sum P z p n) = p n z" |
  "un_J122 (j1_sum P z p n) = j2_sum P z p (p n z)" |
  "P n \<Longrightarrow> j2_sum P z p n = J21" |
  "un_J221 (j2_sum P z p n) = j1_sum P z p (p n z)" |
  "un_J222 (j2_sum P z p n) = j2_sum P z p (p n z)"

(* declare J1.dtor_corec_transfer[unfolded rel_pre_J1_def rel_pre_J2_def, transfer_rule] *)
(* declare J2.dtor_corec_transfer[unfolded rel_pre_J1_def rel_pre_J2_def, transfer_rule] *)
(* declare Abs_transfer[OF type_definition_J1_pre_J1 type_definition_J1_pre_J1, transfer_rule] *)

lemma "((R ===> op =) ===> R ===> (R ===> R ===> R) ===> R ===> rel_J1 R) j1_sum j1_sum"
(*  unfolding j1_sum_def corec_J1_def split_beta if_conn
  apply (simp only: J1.disc_eq_case J1.case_distrib[of "\<lambda>x. If x t e" for t e]
    if_True if_False J1.case cong: J1.case_cong)
  by transfer_prover *)
  apply (tactic \<open>mk_primcorec_transfer_tac true @{context}
    @{thm j1_sum_def}
    @{thm corec_J1_def}
    @{thms type_definition_J1_pre_J1 BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms J1.dtor_corec_transfer J2.dtor_corec_transfer}
    @{thms rel_pre_J1_def rel_pre_J2_def}
    @{thms }
    @{thms }
    @{thms }
    @{thms }
  \<close>)
done

lemma "((R ===> op =) ===> R ===> (R ===> R ===> R) ===> R ===> rel_J2 R) j2_sum j2_sum"
  (* unfolding j2_sum_def corec_J2_def split_beta if_conn
  apply (simp only: J1.disc_eq_case J1.case_distrib[of "\<lambda>x. If x t e" for t e]
    if_True if_False llist.case cong: llist.case_cong)
  by transfer_prover *)
  apply (tactic \<open>mk_primcorec_transfer_tac true @{context}
    @{thm j2_sum_def}
    @{thm corec_J2_def}
    @{thms type_definition_J1_pre_J1 BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms J1.dtor_corec_transfer J2.dtor_corec_transfer}
    @{thms rel_pre_J1_def rel_pre_J2_def}
    @{thms }
    @{thms }
    @{thms }
    @{thms }
  \<close>)
done

primcorec forest_of_mylist :: "'a tree mylist \<Rightarrow> 'a forest" where
  "forest_of_mylist ts =
     (case ts of
       MyNil \<Rightarrow> FNil
     | MyCons t ts \<Rightarrow> FCons t (forest_of_mylist ts))"

(* fail because of undefined *)
lemma "(rel_mylist (rel_tree R) ===> rel_forest R) forest_of_mylist forest_of_mylist"
  unfolding forest_of_mylist_def corec_forest_def split_beta
  apply (simp only: mylist.disc_eq_case mylist.case_distrib[of "\<lambda>x. If x t e" for t e]
    if_True if_False mylist.case cong: mylist.case_cong)
  (* by transfer_prover *)
  sorry

primcorec mylist_of_forest :: "'a forest \<Rightarrow> 'a tree mylist" where
  "mylist_of_forest f =
     (case f of
       FNil \<Rightarrow> MyNil
     | FCons t ts \<Rightarrow> MyCons t (mylist_of_forest ts))"

thm forest.case_distrib
  forest.case_distrib[of "\<lambda>x. If x t e"]
  forest.case_distrib[of "\<lambda>x. If x t e" for t e]

lemma "(rel_forest R ===> rel_mylist (rel_tree R)) mylist_of_forest mylist_of_forest"
(*   unfolding mylist_of_forest_def corec_mylist_def split_beta if_conn
  apply (simp only: forest.disc_eq_case forest.case_distrib[of "\<lambda>x. If x t e" for t e]
    if_True if_False forest.case cong: forest.case_cong)
  by transfer_prover *)
  apply (tactic \<open>mk_primcorec_transfer_tac true @{context}
    @{thm mylist_of_forest_def}
    @{thm corec_mylist_def}
    @{thms BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms forest.dtor_corec_transfer mylist.dtor_corec_transfer tree.dtor_corec_transfer}
    @{thms rel_pre_forest_def rel_pre_mylist_def rel_pre_tree_def}
    @{thms forest.disc_eq_case mylist.disc_eq_case tree.disc_eq_case}
    @{thms forest.case mylist.case tree.disc_eq_case}
    @{thms forest.case_distrib mylist.case_distrib tree.case_distrib}
    @{thms forest.case_cong mylist.case_cong tree.case}
  \<close>)
done

primcorec semi_stream :: "'a stream \<Rightarrow> 'a stream" where
  "semi_stream s = Stream (shd s) (semi_stream (stl (stl s)))"

lemma semi_stream_transfer[transfer_rule]:
  "(rel_stream R ===> rel_stream R) semi_stream semi_stream"
(*   unfolding semi_stream_def corec_stream_def split_beta if_conn
  apply (simp only: stream.case_distrib[of "\<lambda>x. If x t e" for t e] if_True if_False stream.case cong: stream.case_cong)
  by transfer_prover *)
  apply (tactic \<open>mk_primcorec_transfer_tac true @{context}
    @{thm semi_stream_def}
    @{thm corec_stream_def}
    @{thms BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms stream.dtor_corec_transfer}
    @{thms rel_pre_stream_def}
    @{thms stream.disc_eq_case}
    @{thms stream.case}
    @{thms stream.case_distrib}
    @{thms stream.case_cong}
  \<close>)
done

primcorec                   
  tree'_of_stream :: "'a stream \<Rightarrow> 'a tree'" and
  branch_of_stream :: "'a stream \<Rightarrow> 'a branch"
where
  "tree'_of_stream s =
     TNode' (branch_of_stream (semi_stream s)) (branch_of_stream (semi_stream (stl s)))" |
  "branch_of_stream s = (case s of Stream h t \<Rightarrow> Branch h (tree'_of_stream t))"

(* declare tree'.dtor_corec_transfer[unfolded rel_pre_tree'_def rel_pre_branch_def, transfer_rule] *)

lemma "(rel_stream R ===> rel_tree' R) tree'_of_stream tree'_of_stream"
unfolding tree'_of_stream_def corec_tree'_def split_beta if_conn
(*   apply (simp only: tree'.disc_eq_case stream.case_distrib[of "\<lambda>x. If x t e" for t e]
    if_True if_False tree'.case cong: tree'.case_cong)
 by transfer_prover *)
  apply (tactic \<open>mk_primcorec_transfer_tac true @{context}
    @{thm tree'_of_stream_def}
    @{thm corec_tree'_def}
    @{thms BNF_Composition.type_definition_id_bnf_UNIV BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms tree'.dtor_corec_transfer branch.dtor_corec_transfer}
    @{thms rel_pre_tree'_def rel_pre_branch_def}
    @{thms stream.disc_eq_case}
    @{thms stream.case}
    @{thms stream.case_distrib}
    @{thms stream.case_cong}
  \<close>)
done

(* declare branch.dtor_corec_transfer[unfolded rel_pre_tree'_def rel_pre_branch_def, transfer_rule] *)

lemma "(rel_stream R ===> rel_branch R) branch_of_stream branch_of_stream"
(*   unfolding branch_of_stream_def corec_branch_def split_beta if_conn
  apply (simp only: stream.case_distrib[of "\<lambda>x. If x t e" for t e] if_True if_False)
  by transfer_prover *)
  apply (tactic \<open>mk_primcorec_transfer_tac true @{context}
    @{thm branch_of_stream_def}
    @{thm corec_branch_def}
    @{thms BNF_Composition.type_definition_id_bnf_UNIV}
    @{thms tree'.dtor_corec_transfer branch.dtor_corec_transfer}
    @{thms rel_pre_tree'_def rel_pre_branch_def}
    @{thms stream.disc_eq_case}
    @{thms stream.case}
    @{thms stream.case_distrib}
    @{thms stream.case_cong}
  \<close>)
done

primcorec
  id_tree :: "'a bin_rose_tree \<Rightarrow> 'a bin_rose_tree" and
  id_trees1 :: "'a bin_rose_tree mylist \<Rightarrow> 'a bin_rose_tree mylist" and
  id_trees2 :: "'a bin_rose_tree mylist \<Rightarrow> 'a bin_rose_tree mylist"
where
  "id_tree t = (case t of BRTree a ts ts' \<Rightarrow> BRTree a (id_trees1 ts) (id_trees2 ts'))" |
  "id_trees1 ts = (case ts of
       MyNil \<Rightarrow> MyNil
     | MyCons t ts \<Rightarrow> MyCons (id_tree t) (id_trees1 ts))" |
  "id_trees2 ts = (case ts of
       MyNil \<Rightarrow> MyNil
     | MyCons t ts \<Rightarrow> MyCons (id_tree t) (id_trees2 ts))"

(* declare bin_rose_tree.dtor_corec_transfer[unfolded rel_pre_bin_rose_tree_def, transfer_rule] *)

(* Fail because of n2m *)
lemma "(rel_bin_rose_tree R ===> rel_bin_rose_tree R) id_tree id_tree"
  unfolding id_tree_def id_tree.corec_n2m_bin_rose_tree_def  split_beta if_conn
  apply (simp only: mylist.disc_eq_case bin_rose_tree.case_distrib[of "\<lambda>x. If x t e" for t e] mylist.case_distrib[of "\<lambda>x. If x t e" for t e]
    if_True if_False bin_rose_tree.case mylist.case cong: bin_rose_tree.case_cong mylist.case_cong)
  (* by transfer_prover *)
  sorry

lemma "(rel_mylist (rel_bin_rose_tree R) ===> rel_mylist (rel_bin_rose_tree R)) id_trees1 id_trees1"
  unfolding id_trees1_def id_trees1.corec_n2m_bin_rose_tree_mylist_def
  apply (simp add: BNF_Composition.id_bnf_def bin_rose_tree.case_eq_if)
  (* by transfer_prover *) sorry

lemma "(rel_mylist (rel_bin_rose_tree R) ===> rel_mylist (rel_bin_rose_tree R)) id_trees2 id_trees2"
  unfolding id_trees2_def id_trees2.corec_n2m_bin_rose_tree_mylista_def
  apply (simp add: BNF_Composition.id_bnf_def bin_rose_tree.case_eq_if)
  (* by transfer_prover *) sorry

primcorec
  trunc_tree :: "'a bin_rose_tree \<Rightarrow> 'a bin_rose_tree" and
  trunc_trees1 :: "'a bin_rose_tree mylist \<Rightarrow> 'a bin_rose_tree mylist" and
  trunc_trees2 :: "'a bin_rose_tree mylist \<Rightarrow> 'a bin_rose_tree mylist"
where
  "trunc_tree t = (case t of BRTree a ts ts' \<Rightarrow> BRTree a (trunc_trees1 ts) (trunc_trees2 ts'))" |
  "trunc_trees1 ts = (case ts of
       MyNil \<Rightarrow> MyNil
     | MyCons t _ \<Rightarrow> MyCons (trunc_tree t) MyNil)" |
  "trunc_trees2 ts = (case ts of
       MyNil \<Rightarrow> MyNil
     | MyCons t ts \<Rightarrow> MyCons (trunc_tree t) MyNil)"

lemma "(rel_bin_rose_tree R ===> rel_bin_rose_tree R) trunc_tree trunc_tree"
  unfolding trunc_tree_def id_tree.corec_n2m_bin_rose_tree_def
  apply (simp add: BNF_Composition.id_bnf_def bin_rose_tree.case_eq_if)
  (* by transfer_prover *) sorry

lemma "(rel_mylist (rel_bin_rose_tree R) ===> rel_mylist (rel_bin_rose_tree R)) trunc_trees1 trunc_trees1"
  unfolding trunc_trees1_def id_trees1.corec_n2m_bin_rose_tree_mylist_def
  apply (simp add: BNF_Composition.id_bnf_def bin_rose_tree.case_eq_if)
  (* by transfer_prover *) sorry

lemma "(rel_mylist (rel_bin_rose_tree R) ===> rel_mylist (rel_bin_rose_tree R)) trunc_trees2 trunc_trees2"
  unfolding trunc_trees2_def id_trees2.corec_n2m_bin_rose_tree_mylista_def
  apply (simp add: BNF_Composition.id_bnf_def bin_rose_tree.case_eq_if mylist.case_eq_if)
  (* by transfer_prover *) sorry

primcorec
  freeze_exp :: "('b \<Rightarrow> 'a) \<Rightarrow> ('a, 'b) exp \<Rightarrow> ('a, 'b) exp" and
  freeze_trm :: "('b \<Rightarrow> 'a) \<Rightarrow> ('a, 'b) trm \<Rightarrow> ('a, 'b) trm" and
  freeze_factor :: "('b \<Rightarrow> 'a) \<Rightarrow> ('a, 'b) factor \<Rightarrow> ('a, 'b) factor"
where
  "freeze_exp g e =
     (case e of
       Term t \<Rightarrow> Term (freeze_trm g t)
     | Sum t e \<Rightarrow> Sum (freeze_trm g t) (freeze_exp g e))" |
  "freeze_trm g t =
     (case t of
       Factor f \<Rightarrow> Factor (freeze_factor g f)
     | Prod f t \<Rightarrow> Prod (freeze_factor g f) (freeze_trm g t))" |
  "freeze_factor g f =
     (case f of
       C a \<Rightarrow> C a
     | V b \<Rightarrow> C (g b)
     | Paren e \<Rightarrow> Paren (freeze_exp g e))"

lemma "(rel_mylist (rel_bin_rose_tree R) ===> rel_mylist (rel_bin_rose_tree R)) trunc_trees2 trunc_trees2"
  unfolding trunc_trees2_def id_trees2.corec_n2m_bin_rose_tree_mylista_def
  apply (simp add: BNF_Composition.id_bnf_def bin_rose_tree.case_eq_if mylist.case_eq_if
    cong: if_cong)
  (* by transfer_prover *)
  sorry
end
