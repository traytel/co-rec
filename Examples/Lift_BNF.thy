theory Lift_BNF
imports Main
keywords
  "copy_bnf" :: thy_decl and
  "lift_bnf" :: thy_goal
begin

ML_file "bnf_lift.ML"

end