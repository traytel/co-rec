theory "Cabs" 
imports Main
begin 

declare [[quick_and_dirty]]

(* Â\<section>6.5 Expressions *)
datatype cabs_expression =
  (* Â\<section>6.5.1 Primary expressions, Syntax *)
    CabsEident " nat "
  | CabsEconst " nat "
  | CabsEstring " string "
  | CabsEgeneric " cabs_expression0 " " cabs_generic_association list "
  (* Â\<section>6.5.2 Postfix operators, Syntax *)
  | CabsEsubscript " cabs_expression0 " " cabs_expression0 "
  | CabsEcall " cabs_expression0 " " cabs_expression0 list "
  | CabsEmemberof " cabs_expression0 " " nat "
  | CabsEmemberofptr " cabs_expression0 " " nat "
  | CabsEpostincr " cabs_expression0 "
  | CabsEpostdecr " cabs_expression0 "
  | CabsEcompound " type_name " " ( ( designator list)option * initializer) list "
  (* Â\<section>6.5.3 Unary operators, Syntax *)
  | CabsEpreincr " cabs_expression0 "
  | CabsEpredecr " cabs_expression0 "
  | CabsEunary " nat " " cabs_expression0 "
  | CabsEsizeof_expr " cabs_expression0 "
  | CabsEsizeof_type " type_name "
  | CabsEalignof " type_name "
  (* Â\<section>6.5.4 Cast operators, Syntax *)
  | CabsEcast " type_name " " cabs_expression0 "
  (* Â\<section>6.5.5-14 Multiplicative/... operators, Syntax *)
  | CabsEbinary " nat " " cabs_expression0 " " cabs_expression0 "
  (* Â\<section>6.5.15 Conditional operator, Syntax *)
  | CabsEcond " cabs_expression0 " " cabs_expression0 " " cabs_expression0 "
  (* Â\<section>6.5.16 Assignment operators, Syntax *)
  | CabsEassign " nat " " cabs_expression0 " " cabs_expression0 "
  (* Â\<section>6.5.17 Comma operator, Syntax *)
  | CabsEcomma " cabs_expression0 " " cabs_expression0 "
  (* NOTE: the following are suppose to be in the library, but need special treatment *)
  | CabsEassert " cabs_expression0 "
  | CabsEoffsetof " type_name " " nat "
  | CabsEva_start " cabs_expression0 " " nat "
  | CabsEva_arg " cabs_expression0 " " type_name "
and cabs_expression0 =
    CabsExpression "nat " " cabs_expression "

(* Â\<section>6.5.1.1 Generic selection, Syntax *)
and cabs_generic_association =
    GA_type " type_name " " cabs_expression0 "
  | GA_default " cabs_expression0 "

(* Â\<section>6.7 Declarations, Syntax *)
and declaration0 =
    Declaration_base " specifiers " " init_declarator list "
  | Declaration_static_assert " static_assert_declaration "

and specifiers = Specifiers
   " nat      list " 
   " cabs_type_specifier      list " 
   " nat      list " 
   " nat  list " 
   " alignment_specifier list " 

and init_declarator =
    InitDecl "nat " " declarator " "  initializer option "

(* Â\<section>6.7.2 Type specifiers, Syntax *)
and cabs_type_specifier =
    TSpec_void
  | TSpec_char
  | TSpec_short
  | TSpec_int
  | TSpec_long
  | TSpec_float
  | TSpec_double
  | TSpec_signed
  | TSpec_unsigned
  | TSpec_Bool
  | TSpec_Complex
  | TSpec_Atomic " type_name " (* Â\<section>6.7.2.4 Atomic type specifiers, Syntax *)
  | TSpec_struct "  nat option " "  ( struct_declaration list)option "
  | TSpec_union  "  nat option " "  ( struct_declaration list)option "
  | TSpec_enum   "  nat option " "  ( enumerator list)option "
  | TSpec_name   " nat "

(* Â\<section>6.7.2.1 Structure and union specifiers, Syntax *)
and struct_declaration =
    Struct_declaration " cabs_type_specifier list " " nat list " " struct_declarator list "
  | Struct_assert      " static_assert_declaration "

and struct_declarator =
    SDecl_simple   " declarator "
  | SDecl_bitfield "  declarator option " " cabs_expression0 "

(* Â\<section>6.7.2.2 Enumeration specifiers, Syntax *)
and enumerator =" nat *  cabs_expression0 option "


(* Â\<section>6.7.5 Alignment specifier, Syntax *)
and alignment_specifier =
    AS_type " type_name "
  | AS_expr " cabs_expression0 "

(* Â\<section>6.7.6 Declarators, Syntax *)
and declarator =
    Declarator "  pointer_declarator option " " direct_declarator "

and direct_declarator =
    DDecl_identifier " nat "
  | DDecl_declarator " declarator "
  | DDecl_array      " direct_declarator " " array_declarator "
  | DDecl_function   " direct_declarator " " parameter_type_list "
and array_declarator =
    (* the bool indicate the occurence of the static keyword *)
    ADecl "nat " " nat list " " bool " "  array_declarator_size option "
and array_declarator_size =
    ADeclSize_expression " cabs_expression0 "
  | ADeclSize_asterisk

and pointer_declarator =
    PDecl " nat list " "  pointer_declarator option "

and parameter_type_list =
    Params " parameter_declaration list " " bool " (* the boolean indicate a variadic function *)

and parameter_declaration =
    PDeclaration_decl     " specifiers " " declarator "
  | PDeclaration_abs_decl " specifiers " "  abstract_declarator option "

(* Â\<section>6.7.7 Type names, Syntax *)
and type_name =
    Type_name " cabs_type_specifier list " " nat list " "  abstract_declarator option "

and abstract_declarator =
    AbsDecl_pointer " pointer_declarator "
  | AbsDecl_direct  "  pointer_declarator option " " direct_abstract_declarator "

and direct_abstract_declarator =
    DAbs_abs_declarator " abstract_declarator "
  | DAbs_array          "  direct_abstract_declarator option " " array_declarator "
  | DAbs_function       "  direct_abstract_declarator option " " parameter_type_list "

(* Â\<section>6.7.9 Initialization, Syntax *)
and initializer =
    Init_expr " cabs_expression0 "
  | Init_list " ( ( designator list)option * initializer) list "

and designator =
    Desig_array " cabs_expression0 "
  | Desig_member " nat "

(* Â\<section>6.7.10 Static assertions *)
and static_assert_declaration =
   Static_assert " cabs_expression0 " " string "

end
