theory Simplify_Set
imports "~~/src/HOL/BNF_LFP"
begin

datatype_new x = C bool nat | C2 bool

declare [[bnf_note_all]]
datatype_new ('a, 'b) sum (infixl "ö" 60) = L 'a | R 'b
datatype_new 'a F = N | C 'a "'a F"
datatype_new 'a G = N | C 'a "'a G"
datatype_new 'a H = X "(nat F * 'a) * ('a F * nat G)"
thm set1_pre_H_def
thm prod.set_map[no_vars]
lemma prod_set_map0:
  "fsts o (map_pair f1 f2) = image f1 o fsts"
  "snds o (map_pair f1 f2) = image f2 o snds"
  sorry

lemma *: "(\<lambda>x. P x) = (\<lambda>(x, y). P (x, y))"
  by auto
lemma "(Union \<circ> collect {fsts, snds} \<circ>
map_pair (Union \<circ> collect {fsts, snds} \<circ> map_pair (\<lambda>_. {}) (\<lambda>x. {x}))
 (Union \<circ> collect {fsts, snds} \<circ> map_pair set_F (\<lambda>_. {}))) = foo"
unfolding comp_assoc collect_comp image_insert image_empty prod_set_map0
unfolding collect_def[abs_def] UN_insert UN_empty Un_empty_right Un_empty_left
 o_def Union_Un_distrib Union_image_eq UN_empty2 UN_singleton
apply (rule trans[OF *])
unfolding prod_set_simps UN_insert UN_empty Un_empty_right Un_empty_left
oops

find_theorems collect
lemma "Union \<circ> collect {set1_sum, set2_sum} \<circ> map_sum set_F set_G = bar"
unfolding sum.collect_set_map comp_assoc
unfolding collect_def[abs_def] UN_insert UN_empty Un_empty_right
unfolding o_def Union_Un_distrib Union_image_eq
find_theorems "Union (_ ` _)"
oops
term "UNION A f"
lemma "Union \<circ> collect {set1_sum, set2_sum} \<circ>
map_sum (Union \<circ> collect {set1_sum, set2_sum} \<circ> map_sum set_F (\<lambda>x. {x}))
 (Union \<circ> collect {set1_sum, set2_sum} \<circ> map_sum set_F (\<lambda>_. {})) = foo"
unfolding sum.collect_set_map comp_assoc
unfolding collect_def[abs_def] UN_insert UN_empty Un_empty_right Un_empty_left
 o_def Union_Un_distrib Union_image_eq UN_empty2 UN_singleton
find_theorems "UNION _ (\<lambda>_. {_})"

end