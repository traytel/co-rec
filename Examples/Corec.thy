theory Corec
imports "~~/src/HOL/BNF/BNF"
begin

codatatype 'a xforest = LNil | LNil' | LCons 'a "'a xforest"
       and 'a xtree = LNode 'a "'a xforest" | LNode'

term xforest_corec

thm xforest.corec[no_vars]
    xtree.corec[no_vars]

ML {*
BNF_FP_Rec_Sugar_Util.corec_specs_of [@{binding f}, @{binding g}]
  [@{typ nat}, @{typ nat}] (rev [@{typ "'a xforest"}, @{typ "'a xtree"}])
  (fn t => if exists_subterm (fn Free ("f", _) => true | Free ("g", _) => true | _ => false) t then [0] else [])
  [[]]
  @{context}
*}

codatatype 'a llist = LNil | LCons 'a "'a llist"
codatatype 'a ltree = LNode 'a "'a ltree llist"

thm ltree.coinduct
    xtree.coinduct

end
