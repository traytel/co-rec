theory ctr_transfer
imports "~~/src/HOL/Transfer"
(* imports "~~/src/HOL/Sledgehammer" *)
begin

context lifting_syntax (* enables the ===> syntax for rel_fun *)
begin

declare [[bnf_note_all]]

datatype_new 'a list
  = Nil
  | Cons (head: 'a) (tail: "'a list")
  where
    "tail Nil = Nil"



datatype_new 'a big_list
  = nil: BNil
  | BCons 'a 'a "'a big_list"

datatype_new 'a option
  = None
  | Some (the: 'a)

datatype_new 'a tree
  = Node (val: 'a) (children: "'a tree list")

datatype_new ('a, 'b) example
  = N1 'a
  | N2 'b
  | N3 'a 'b
  | N4
  | ConsA 'a "('a, 'b) example"
  | ConsB 'b "('a, 'b) example"

datatype_new 'a tree_mutual = TNode 'a "'a forest_mutual"
       and 'a forest_mutual = FNil | FCons "'a tree_mutual" "'a forest_mutual"

datatype_new ('a, 'b) F1 = F1 "'a \<Rightarrow> 'b"

datatype_new ('a, 'b, 'c) F2 = F2 "'a \<Rightarrow> 'b \<Rightarrow> 'c"

datatype_new ('a, 'b) T = C "'a \<Rightarrow> bool" | D "'b list"

datatype_new ('t, 'id) foo = Foo "('t \<Rightarrow> 't) + 't" | Bar (bar: 'id)

ML {*
open Ctr_Sugar_Tactics
open BNF_Util

fun mk_ctr_transfer_tac ctxt rel_intros =
  HEADGOAL Goal.conjunction_tac THEN
    ALLGOALS (REPEAT o (resolve_tac (@{thm rel_funI} :: rel_intros) THEN'
      TRY o (REPEAT_DETERM1 o atac ORELSE'
      K (unfold_thms_tac ctxt @{thms fun.rel_eq sum.rel_eq}) THEN' hyp_subst_tac ctxt THEN' rtac refl)));
*}

thm list.ctr_transfer
lemma list_ctr_transfer:
  "rel_list R Nil Nil"
  "(R ===> rel_list R ===> rel_list R) Cons Cons"
  apply (tactic {* mk_ctr_transfer_tac @{context} @{thms list.rel_intros} *})
(*   apply (rule rel_funI list.rel_intros)+
  apply (assumption+) *)
done

lemma big_list_ctr_transfer:
  "rel_big_list R BNil BNil"
  "(R ===> R ===> rel_big_list R ===> rel_big_list R) BCons BCons"
  apply (rule rel_funI big_list.rel_intros)+
  apply (assumption+)
done

lemma option_ctr_transfer:
  "rel_option R None None"
  "(R ===> rel_option R) Some Some"
  apply (tactic {* mk_ctr_transfer_tac @{context} @{thms option.rel_intros} *})
  (* apply (tactic {* HEADGOAL Goal.conjunction_tac *})
  apply ((rule rel_funI option.rel_intros)+ , (assumption+)?) *)
done

lemma Node_transfer: "(R ===> rel_list (rel_tree R) ===> rel_tree R) Node Node"
  apply (tactic {* mk_ctr_transfer_tac @{context} @{thms tree.rel_intros} *})
(*   apply (tactic {* HEADGOAL Goal.conjunction_tac *})
  apply ((rule rel_funI tree.rel_intros)+ , (assumption+)?) *)
done

lemma example_transfer:
  "(R1 ===> rel_example R1 R2) N1 N1"
  "(R2 ===> rel_example R1 R2) N2 N2"
  "(R1 ===> R2 ===> rel_example R1 R2) N3 N3"
  "rel_example R1 R2 N4 N4"
  "(R1 ===> rel_example R1 R2 ===> rel_example R1 R2) ConsA ConsA"
  "(R2 ===> rel_example R1 R2 ===> rel_example R1 R2) ConsB ConsB"
  apply (tactic {* mk_ctr_transfer_tac @{context} @{thms example.rel_intros} *})
done

lemma F1_transfer:
  "((op = ===> R) ===> rel_F1 R) F1 F1"
  apply (tactic {* mk_ctr_transfer_tac @{context} @{thms F1.rel_intros} *})
(*   apply (rule rel_funI)
  apply (rule F1.rel_intros)
  apply (assumption) *)
done

lemma F2_transfer:
  "((op = ===> op = ===> R) ===> rel_F2 R) F2 F2 "
  apply (tactic {* mk_ctr_transfer_tac @{context} @{thms F2.rel_intros} *})
  (* apply (rule rel_funI F2.rel_intros, assumption?)+ *)
done

lemma T_ctr_transfer:
  "(op = ===> rel_T R) C C"
  "(rel_list R ===> rel_T R) D D"
  apply (tactic {* mk_ctr_transfer_tac @{context} @{thms T.rel_intros} *})
(*   apply (rule rel_funI T.rel_intros)+
  apply (hypsubst, rule refl) *)

thm foo.ctr_transfer

lemma
  "(op = ===> rel_foo R) Foo Foo"
  "(R ===> rel_foo R) Bar Bar"
  (* sledgehammer (del: foo.ctr_transfer) *)
  (* by (simp add: fun.rel_eq rel_funI sum.rel_eq) *)
  apply (tactic {* mk_ctr_transfer_tac @{context} @{thms foo.rel_intros} *})
(* apply (rule rel_funI)
apply (rule foo.rel_intros)
apply (unfold fun.rel_eq sum.rel_eq)
apply (hypsubst, rule refl) *)
done

end

end
