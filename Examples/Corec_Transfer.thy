theory Corec_Transfer
imports Main
begin

declare [[bnf_note_all]]

codatatype (sset: 'a) stream =
  SCons (shd: 'a) (stl: "'a stream") (infixr "##" 65)
for
  map: smap
  rel: stream_all2

codatatype (lset: 'a) llist =
    lnull: LNil
  | LCons (lhd: 'a) (ltl: "'a llist")
for
  map: lmap
  rel: llist_all2
where
  "lhd LNil = undefined"
| "ltl LNil = LNil"

declare stream.dtor_corec_transfer[unfolded rel_pre_stream_def vimage2p_def BNF_Composition.id_bnf_def,
  transfer_rule]

declare llist.dtor_corec_transfer[unfolded rel_pre_llist_def vimage2p_def BNF_Composition.id_bnf_def,
  transfer_rule]

context
begin
interpretation lifting_syntax .

primcorec sconst where
  "sconst a = a ## sconst a"

thm sconst_def

primcorec siterate where
  "shd (siterate f x) = x"
| "stl (siterate f x) = siterate f (f x)"

primcorec salterate where
  "shd (salterate f g x) = x"
| "stl (salterate f g x) = salterate g f (f x)"

primcorec semi_stream :: "'a stream \<Rightarrow> 'a stream" where
  "semi_stream s = shd s ## semi_stream (stl (stl s))"

primcorec lconst where
  "lconst a = LCons a (lconst a)"

primcorec lboring :: "'a \<Rightarrow> 'a llist" where
  "lboring a = LNil"

primcorec literate where
  "\<not> lnull (literate f x)"
| "lhd (literate f x) = x"
| "ltl (literate f x) = literate f (f x)"

lemma sconst_transfer[transfer_rule]: "(R ===> stream_all2 R) sconst sconst"
  unfolding sconst_def corec_stream_def if_False BNF_Composition.id_bnf_def by transfer_prover

lemma siterate_transfer[transfer_rule]: "((R ===> R) ===> R ===> stream_all2 R) siterate siterate"
  unfolding siterate_def corec_stream_def split_beta if_False BNF_Composition.id_bnf_def by transfer_prover

lemma salterate_transfer[transfer_rule]: "((R ===> R) ===> (R ===> R) ===> R ===> stream_all2 R) salterate salterate"
  unfolding salterate_def corec_stream_def split_beta if_False BNF_Composition.id_bnf_def by transfer_prover

lemma semi_stream_transfer[transfer_rule]: "(stream_all2 R ===> stream_all2 R) semi_stream semi_stream"
  unfolding semi_stream_def corec_stream_def if_False BNF_Composition.id_bnf_def by transfer_prover

lemma lconst_transfer[transfer_rule]: "(R ===> llist_all2 R) lconst lconst"
  unfolding lconst_def corec_llist_def if_False BNF_Composition.id_bnf_def by transfer_prover

lemma lboring_transfer[transfer_rule]: "(R ===> llist_all2 R) lboring lboring"
  unfolding lboring_def corec_llist_def if_True BNF_Composition.id_bnf_def by transfer_prover

lemma literate_transfer[transfer_rule]: "((R ===> R) ===> R ===> llist_all2 R) literate literate"
  unfolding literate_def corec_llist_def split_beta if_False BNF_Composition.id_bnf_def by transfer_prover



primcorec lappend :: "'a llist \<Rightarrow> 'a llist \<Rightarrow> 'a llist" where
  "lappend xs ys = (case xs of LNil \<Rightarrow> ys | LCons x xs' \<Rightarrow> LCons x (lappend xs' ys))"

(*FUN*)
lemma "lappend =
  (\<lambda>xs ys. dtor_corec_llist (\<lambda>(xs, ys). case (xs, ys) of
       (LNil, LNil) \<Rightarrow> Inl ()
     | (LCons x xs, LNil) \<Rightarrow> Inr (x, Inr (xs, LNil))
     | (LNil, LCons y ys) \<Rightarrow> Inr (y, Inl ys)
     | (LCons x xs, LCons y zs) \<Rightarrow> Inr (x, Inr (xs, ys))) (xs, ys))"
  unfolding lappend_def corec_llist_def BNF_Composition.id_bnf_def
  apply (auto simp: fun_eq_iff)
  apply (rule arg_cong2[where f = dtor_corec_llist])
  apply (auto simp: fun_eq_iff split: llist.splits)
  done

primcorec lappend2 :: "'a llist \<Rightarrow> 'a llist \<Rightarrow> 'a llist" where
  "lappend2 xs ys = (if lnull xs then if lnull ys then LNil else LCons (lhd ys) (ltl ys)
     else LCons (lhd xs) (lappend2 (ltl xs) ys))"

primcorec lappend3 :: "'a list \<Rightarrow> 'a llist \<Rightarrow> 'a llist" where
  "lappend3 xs ys = (if xs = Nil then if lnull ys then LNil else LCons (lhd ys) (ltl ys)
     else LCons (hd xs) (lappend3 (tl xs) ys))"

primcorec lappendx :: "'a \<Rightarrow> 'a llist \<Rightarrow> 'a llist \<Rightarrow> 'a llist"
where
  "lappendx x xs ys = (case xs of LNil \<Rightarrow> LCons x (lappendx x xs ys) | LCons x xs' \<Rightarrow> LCons x (lappendx x xs' ys))"

lemma case_llist_distrib:
  "f (case_llist n c x) = case_llist (f n) (\<lambda>x xs. f (c x xs)) x"
  by (metis llist.case_eq_if)

lemma lnull_case_llist: "lnull x = case_llist True (\<lambda>_ _. False) x"
  by (auto split: llist.splits)

lemma if_conn:
  "(if P \<and> Q then t else e) = (if P then if Q then t else e else e)"
  "(if P \<or> Q then t else e) = (if P then t else if Q then t else e)"
  "(if P \<longrightarrow> Q then t else e) = (if P then if Q then t else e else t)"
  "(if \<not> P then t else e) = (if P then e else t)"
  by auto

thm case_llist_distrib[of "\<lambda>x. If x t e" for t e]

lemma lappend_transfer [transfer_rule]:
  "(llist_all2 A ===> llist_all2 A ===> llist_all2 A) lappend lappend"
  unfolding lappend_def corec_llist_def BNF_Composition.id_bnf_def split_beta if_conn
  apply (simp only: lnull_case_llist lhd_def ltl_def case_llist_distrib[of "\<lambda>x. If x t e" for t e]
    if_True if_False llist.case
    cong: llist.case_cong)
  by transfer_prover

lemma lappend2_transfer [transfer_rule]:
  "(llist_all2 A ===> llist_all2 A ===> llist_all2 A) lappend2 lappend2"
  unfolding lappend2_def corec_llist_def BNF_Composition.id_bnf_def split_beta if_conn
  apply (simp only: lnull_case_llist lhd_def ltl_def case_llist_distrib[of "\<lambda>x. If x t e" for t e]
    if_True if_False llist.case
    cong: llist.case_cong)
  by transfer_prover

lemma case_list_distrib:
  "f (case_list n c x) = case_list (f n) (\<lambda>x xs. f (c x xs)) x"
  by (metis list.case_eq_if)

lemma disc_case_list: "(x = Nil) = case_list True (\<lambda>_ _. False) x"
  by (auto split: list.splits)

lemma lappend3_transfer [transfer_rule]:
  "(list_all2 A ===> llist_all2 A ===> llist_all2 A) lappend3 lappend3"
  unfolding lappend3_def corec_llist_def BNF_Composition.id_bnf_def split_beta if_conn
  apply (simp only: lnull_case_llist disc_case_list hd_def tl_def lhd_def ltl_def
    case_llist_distrib[of "\<lambda>x. If x t e" for t e]
    case_list_distrib[of "\<lambda>x. If x t e" for t e]
    if_True if_False llist.case list.case
    cong: llist.case_cong list.case_cong)
  by transfer_prover

lemma lappendx_transfer [transfer_rule]:
  "(A ===> llist_all2 A ===> llist_all2 A ===> llist_all2 A) lappendx lappendx"
  unfolding lappendx_def corec_llist_def BNF_Composition.id_bnf_def split_beta if_conn
  apply (simp only: lnull_case_llist lhd_def ltl_def case_llist_distrib[of "\<lambda>x. If x t e" for t e]
    if_True if_False llist.case
    cong: llist.case_cong)
  by transfer_prover



end

end
