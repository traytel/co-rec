theory Bug_Dmi3
imports "~~/src/HOL/BNF/BNF"
begin


declare [[bnf_note_all]]
declare [[bnf_timing]]

datatype_new 'a lst = Nl | Cns 'a "'a lst"
datatype_new 'a tre = Tre 'a "'a tre lst"

primrec_new tre0_rec_1 and tre0_rec_2 where
tr1: "tre0_rec_1 f1 f2 f3 (Tre a list) = f1 a list (tre0_rec_2 f1 f2 f3 list)" |
tr2a: "tre0_rec_2 f1 f2 f3 Nl = f2" |
tr2b: "tre0_rec_2 f1 f2 f3 (Cns tre list) = f3 tre list (tre0_rec_1 f1 f2 f3 tre) (tre0_rec_2 f1 f2 f3 list)"

ML {* @{term tre0_rec_1.n2m_tre_ctor_fold} *}
ML {* @{term tre0_rec_1.n2m_tre_fold} *}



datatype_new ('a, 'b) prod = Pair 'a 'b
datatype_new ('a, 'b) tree = N | Tree "('a, ('a, 'b) tree) prod" "('b, ('a, 'b) tree) prod"

;primrec_new
  ft :: "(nat, nat) tree \<Rightarrow> (nat, nat) tree" and
  fp :: "(nat, (nat, nat) tree) prod \<Rightarrow> (nat, (nat, nat) tree) prod"
where
  "ft N = N" |
  "ft (Tree l r) = Tree (fp l) (fp r)" |
  "fp (Pair n t) = Pair n (ft t)"


locale X
begin

datatype_new 'a list = Nil | Cons 'a "'a list"
datatype_new 'a tree = Tree 'a "'a tree list"

;primrec_new f and fs where
"f (Tree x ts) = Tree x (fs ts)" |
"fs Nil = Nil" |
"fs (Cons t ts) = Cons (f t) (fs ts)"

end



datatype_new ('u, 'v) l = C "'u \<Rightarrow> 'v" "('u, 'v) l" | N
datatype_new ('x, 'y, 'z) F = CF 'z "'y \<Rightarrow> ('x, ('x, 'y, 'z) F) l"

;primrec_new fl :: "(nat, (nat, int, nat) F) l \<Rightarrow> nat" and fF :: "(nat, int, nat) F \<Rightarrow> nat" where
  "fF (CF b f) = b + undefined (fl o f)"
| "fl N = (0 :: nat)"
| "fl (C x xs) = undefined (fF o x) + fl xs"


end
